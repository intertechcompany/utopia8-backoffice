<?php
/**
 * Sphinx class
 */
class Sphinx
{
	private $chunk_size = 500;
	private $product_fields = [
		'p.product_id',
		'p.active',
		'p.product_alias',
		'p.product_published',
		'p.category_id',
		'p.brand_id',
		'p.product_rating',
		'p.product_price',
		'p.product_instock',
		'pl_uk.product_title as product_title_uk',
		'pl_ru.product_title as product_title_ru',
		'pl_en.product_title as product_title_en',
		'pl_uk.product_description as product_description_uk',
		'pl_ru.product_description as product_description_ru',
		'pl_en.product_description as product_description_en',
		'cl_uk.category_name as category_name_uk',
		'cl_ru.category_name as category_name_ru',
		'cl_en.category_name as category_name_en',
		'bl_uk.brand_name as brand_name_uk',
		'bl_ru.brand_name as brand_name_ru',
		'bl_en.brand_name as brand_name_en',
	];
	private $badge_fields = [
		'b.badge_id',
		'bl_uk.badge_name as badge_name_uk',
		'bl_ru.badge_name as badge_name_ru',
		'bl_en.badge_name as badge_name_en',
	];
	private $category_fields = [
		'c.category_id',
		'cl_uk.category_name as category_name_uk',
		'cl_ru.category_name as category_name_ru',
		'cl_en.category_name as category_name_en',
	];
	private $tag_fields = [
		't.tag_id',
		'tl_uk.tag_name as tag_name_uk',
		'tl_ru.tag_name as tag_name_ru',
		'tl_en.tag_name as tag_name_en',
	];
	private $insert_columns = [
		'id',
		'articul',
		'title_uk',
		'title_ru',
		'title_en',
		'description_uk',
		'description_ru',
		'description_en',
		'category_uk',
		'category_ru',
		'category_en',
		'tag_uk',
		'tag_ru',
		'tag_en',
		'badge_uk',
		'badge_ru',
		'badge_en',
		'brand_uk',
		'brand_ru',
		'brand_en',
		'alias',
		'active',
		'published',
		'in_stock',
		'rating',
		'brand_id',
		'price',
		'categories',
		'tags',
		'badges',
		'values',
	];

	public function __construct()
	{
		ini_set('memory_limit', '512M');
		set_time_limit(10 * 60);
	}

	public static function escapeMatchValue($str)
    {
        return str_replace(
            ['\\', '/', '"', '(', ')', '|', '-', '!', '@', '~', '&', '^', '$', '=', '>', '<', "\x00", "\n", "\r", "\x1a"],
            ['\\\\', '\\/', '\\"', '\\(', '\\)', '\\|', '\\-', '\\!', '\\@', '\\~', '\\&', '\\^', '\\$', '\\=', '\\>', '\\<',  "\\x00", "\\n", "\\r", "\\x1a"],
            $str
        );
	}

	public static function getFieldWeights()
	{	
		$field_weights = [
			'title_uk=9',
			'title_ru=9',
			'title_en=9',
			'articul=8',
			'brand_uk=6',
			'brand_ru=6',
			'brand_en=6',
			'tag_uk=5',
			'tag_ru=5',
			'tag_en=5',
			'description_uk=2',
			'description_ru=2',
			'description_en=2',
			'category_uk=2',
			'category_ru=2',
			'category_en=2',
			'badge_uk=1',
			'badge_ru=1',
			'badge_en=1',
		];
		
		return '(' . implode(', ', $field_weights) . ')';
	}

	public function testFacet()
	{
		$conn = new PDO('mysql:host=127.0.0.1;port=9306');
		$stmt = $conn->prepare("SELECT WEIGHT(), * 
							  FROM product 
							  WHERE MATCH(:match) AND active = 1
							  ORDER BY WEIGHT() DESC
							  LIMIT 0,5
							  OPTION field_weights = " . Sphinx::getFieldWeights() . "
							  FACET categories
							  FACET tags
							  FACET badges
							  FACET values;
							  SHOW META");
		$stmt->bindValue(':match', Sphinx::escapeMatchValue('TEST'), PDO::PARAM_STR);
		$stmt->execute();
		
		do {
			try {
				echo '<pre>', print_r($stmt->fetchAll(PDO::FETCH_ASSOC), true), '</pre>';
			} catch (PDOException $e) {
				echo $e->getMessage();
			}
		} while ($stmt->nextRowset());

		/* $data = Yii::app()->sphinx
			// ->createCommand("SELECT * FROM product WHERE MATCH('сумка') AND active = 1 ORDER BY WEIGHT() DESC") // LIMIT 995,10
			->createCommand("SELECT id FROM product WHERE active = 1 LIMIT 995,10 OPTION max_matches=3000") // LIMIT 995,10
			->queryAll();
		$data = Yii::app()->sphinx
			->createCommand("SHOW META")
			->queryAll();
		echo '<pre>', print_r($data, true), '</pre>'; */

		/* 
		$data = Yii::app()->sphinx
			->createCommand("SELECT WEIGHT(), * 
							 FROM product 
							 WHERE MATCH(:match) AND active = 1
							 ORDER BY WEIGHT() DESC
							 LIMIT 0,5
							 OPTION field_weights = " . Sphinx::getFieldWeights() . "
							 FACET categories
							 FACET tags
							 FACET badges
							 FACET values")
			->bindValue(':match', Sphinx::escapeMatchValue('фут'), PDO::PARAM_STR)
			->queryAll();

		echo '<pre>ROWS: — ', print_r($data, true), '</pre>';

		$data = Yii::app()->sphinx
			->createCommand("SHOW META")
			->queryAll();
		echo '<pre>', print_r($data, true), '</pre>'; */
	}

	public function isActive()
	{
		try {
			Yii::app()->sphinx->active = true;
		} catch (CDbException $e) {
			// no connection
			return false;
		}

		return true;
	}

	public function optimizeIndex()
	{
		Yii::app()->sphinx
			->createCommand("OPTIMIZE INDEX product")
			->execute();
	}
	
	public function createIndex()
	{
		if (!$this->isActive()) {
			return false;
		}

		Yii::app()->sphinx
			->createCommand("TRUNCATE RTINDEX product")
			->execute();
		
		$total_products = Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM product WHERE active = 1 AND product_published != '0000-00-00 00:00:00'")
			->queryScalar();

		$products_offset = 0;

		while ($products_offset < $total_products) {
			$products = Yii::app()->db
				->createCommand("SELECT " . implode(',', $this->product_fields) . " 
								 FROM product as p 
								 JOIN product_lang as pl_uk 
								 ON p.product_id = pl_uk.product_id AND pl_uk.language_code = 'uk' 
								 JOIN product_lang as pl_ru 
								 ON p.product_id = pl_ru.product_id AND pl_ru.language_code = 'ru' 
								 JOIN product_lang as pl_en 
								 ON p.product_id = pl_en.product_id AND pl_en.language_code = 'en' 
								 LEFT JOIN category_lang as cl_uk 
								 ON p.category_id = cl_uk.category_id AND cl_uk.language_code = 'uk' 
								 LEFT JOIN category_lang as cl_ru 
								 ON p.category_id = cl_ru.category_id AND cl_ru.language_code = 'ru' 
								 LEFT JOIN category_lang as cl_en 
								 ON p.category_id = cl_en.category_id AND cl_en.language_code = 'en' 
								 LEFT JOIN brand_lang as bl_uk 
								 ON p.brand_id = bl_uk.brand_id AND bl_uk.language_code = 'uk' 
								 LEFT JOIN brand_lang as bl_ru 
								 ON p.brand_id = bl_ru.brand_id AND bl_ru.language_code = 'ru' 
								 LEFT JOIN brand_lang as bl_en 
								 ON p.brand_id = bl_en.brand_id AND bl_en.language_code = 'en' 
								 WHERE p.active = 1 AND p.product_published != '0000-00-00 00:00:00'
								 ORDER BY p.product_id
								 LIMIT {$products_offset},{$this->chunk_size}")
				->queryAll();

			$this->replaceProducts($products);
			
			$products_offset += $this->chunk_size;
		}
	}

	public function updateProduct($product_id)
	{
		if (!$this->isActive()) {
			return false;
		}
		
		$products = Yii::app()->db
			->createCommand("SELECT " . implode(',', $this->product_fields) . " 
								FROM product as p 
								JOIN product_lang as pl_uk 
								ON p.product_id = pl_uk.product_id AND pl_uk.language_code = 'uk' 
								JOIN product_lang as pl_ru 
								ON p.product_id = pl_ru.product_id AND pl_ru.language_code = 'ru' 
								JOIN product_lang as pl_en 
								ON p.product_id = pl_en.product_id AND pl_en.language_code = 'en' 
								LEFT JOIN category_lang as cl_uk 
								ON p.category_id = cl_uk.category_id AND cl_uk.language_code = 'uk' 
								LEFT JOIN category_lang as cl_ru 
								ON p.category_id = cl_ru.category_id AND cl_ru.language_code = 'ru' 
								LEFT JOIN category_lang as cl_en 
								ON p.category_id = cl_en.category_id AND cl_en.language_code = 'en' 
								LEFT JOIN brand_lang as bl_uk 
								ON p.brand_id = bl_uk.brand_id AND bl_uk.language_code = 'uk' 
								LEFT JOIN brand_lang as bl_ru 
								ON p.brand_id = bl_ru.brand_id AND bl_ru.language_code = 'ru' 
								LEFT JOIN brand_lang as bl_en 
								ON p.brand_id = bl_en.brand_id AND bl_en.language_code = 'en' 
								WHERE p.product_id = :id AND p.active = 1 AND p.product_published != '0000-00-00 00:00:00'")
			->bindValue(':id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($products)) {
			$this->replaceProducts($products);
		}
	}

	public function deleteProduct($product_id)
	{
		if (!$this->isActive()) {
			return false;
		}
		
		Yii::app()->sphinx
			->createCommand("DELETE FROM product WHERE id = :id")
			->bindValue(':id', (int) $product_id, PDO::PARAM_INT)
			->execute();
	}

	private function collectProductData($product_id)
	{
		$product_data = [];
		
		$product_data['badges'] = Yii::app()->db
			->createCommand("SELECT " . implode(',', $this->badge_fields) . " 
							FROM product_badge as pb 
							JOIN badge as b 
							ON pb.badge_id = b.badge_id 
							JOIN badge_lang as bl_uk 
							ON b.badge_id = bl_uk.badge_id AND bl_uk.language_code = 'uk' 
							JOIN badge_lang as bl_ru 
							ON b.badge_id = bl_ru.badge_id AND bl_ru.language_code = 'ru' 
							JOIN badge_lang as bl_en 
							ON b.badge_id = bl_en.badge_id AND bl_en.language_code = 'en' 
							WHERE pb.product_id = :id
							ORDER BY b.badge_id")
			->bindValue(':id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();
		
		$product_data['categories'] = Yii::app()->db
			->createCommand("SELECT " . implode(',', $this->category_fields) . " 
							FROM category_product as cp 
							JOIN category as c 
							ON cp.category_id = c.category_id 
							JOIN category_lang as cl_uk 
							ON c.category_id = cl_uk.category_id AND cl_uk.language_code = 'uk' 
							JOIN category_lang as cl_ru 
							ON c.category_id = cl_ru.category_id AND cl_ru.language_code = 'ru' 
							JOIN category_lang as cl_en 
							ON c.category_id = cl_en.category_id AND cl_en.language_code = 'en' 
							WHERE cp.product_id = :id
							ORDER BY c.category_id")
			->bindValue(':id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();
		
		$product_data['tags'] = Yii::app()->db
			->createCommand("SELECT " . implode(',', $this->tag_fields) . " 
							FROM product_tag as pt 
							JOIN tag as t 
							ON pt.tag_id = t.tag_id 
							JOIN tag_lang as tl_uk 
							ON t.tag_id = tl_uk.tag_id AND tl_uk.language_code = 'uk' 
							JOIN tag_lang as tl_ru 
							ON t.tag_id = tl_ru.tag_id AND tl_ru.language_code = 'ru' 
							JOIN tag_lang as tl_en 
							ON t.tag_id = tl_en.tag_id AND tl_en.language_code = 'en' 
							WHERE pt.product_id = :id
							ORDER BY t.tag_id")
			->bindValue(':id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();
		
		$product_data['values'] = Yii::app()->db
			->createCommand("SELECT value_id 
							FROM property_product 
							WHERE product_id = :id
							ORDER BY value_id")
			->bindValue(':id', (int) $product_id, PDO::PARAM_INT)
			->queryColumn();

		return $product_data;
	}

	private function replaceProducts($products)
	{
		$products_insert = [];
			
		foreach ($products as $index => $product) {
			$product_data = $this->collectProductData($product['product_id']);
			$products[$index]['data'] = $product_data;
			$category_ids = [];
			
			if (!empty($product['category_id'])) {
				$category_ids[] = $product['category_id'];
			}

			if (!empty($product_data['categories'])) {
				$category_ids = array_merge($category_ids, array_column($product_data['categories'], 'category_id'));
				$category_ids = array_unique($category_ids);
			}

			$tag_ids = [];
			
			if (!empty($product_data['tags'])) {
				$tag_ids = array_merge($tag_ids, array_column($product_data['tags'], 'tag_id'));
				$tag_ids = array_unique($tag_ids);
			}
			
			$badge_ids = [];
			
			if (!empty($product_data['badges'])) {
				$badge_ids = array_merge($badge_ids, array_column($product_data['badges'], 'badge_id'));
				$badge_ids = array_unique($badge_ids);
			}

			$insert_data = [
				$product['product_id'],
				':articul_' . $index,
				':title_uk_' . $index,
				':title_ru_' . $index,
				':title_en_' . $index,
				':description_uk_' . $index,
				':description_ru_' . $index,
				':description_en_' . $index,
				':category_uk_' . $index,
				':category_ru_' . $index,
				':category_en_' . $index,
				':tag_uk_' . $index,
				':tag_ru_' . $index,
				':tag_en_' . $index,
				':badge_uk_' . $index,
				':badge_ru_' . $index,
				':badge_en_' . $index,
				':brand_uk_' . $index,
				':brand_ru_' . $index,
				':brand_en_' . $index,
				':alias_' . $index,
				$product['active'],
				strtotime($product['product_published']),
				$product['product_instock'] == 'in_stock' ? 1 : 0,
				$product['product_rating'],
				$product['brand_id'],
				(int) $product['product_price'],
				'(' . implode(',', $category_ids) . ')',
				'(' . implode(',', $tag_ids) . ')',
				'(' . implode(',', $badge_ids) . ')',
				'(' . implode(',', $product_data['values']) . ')',
			];

			$products_insert[] = "(" . implode(', ', $insert_data) . ")";
		}
		
		$insert_command = Yii::app()->sphinx
			->createCommand("REPLACE INTO product (" . implode(', ', $this->insert_columns) . ") VALUES " . implode(',', $products_insert));
		
		foreach ($products as $index => $product) {
			$category_uk = [];
			$category_ru = [];
			$category_en = [];
			
			if (!empty($product['category_id'])) {
				$category_uk[] = $product['category_name_uk'];
				$category_ru[] = $product['category_name_ru'];
				$category_en[] = $product['category_name_en'];
			}
			
			if (!empty($product['data']['categories'])) {
				$category_uk = array_merge($category_uk, array_column($product['data']['categories'], 'category_name_uk'));
				$category_uk = array_unique($category_uk);
				
				$category_ru = array_merge($category_ru, array_column($product['data']['categories'], 'category_name_ru'));
				$category_ru = array_unique($category_ru);
				
				$category_en = array_merge($category_en, array_column($product['data']['categories'], 'category_name_en'));
				$category_en = array_unique($category_en);
			}

			$tag_uk = [];
			$tag_ru = [];
			$tag_en = [];
			
			if (!empty($product['data']['tags'])) {
				$tag_uk = array_column($product['data']['tags'], 'tag_name_uk');
				$tag_uk = array_unique($tag_uk);
				
				$tag_ru = array_column($product['data']['tags'], 'tag_name_ru');
				$tag_ru = array_unique($tag_ru);
				
				$tag_en = array_column($product['data']['tags'], 'tag_name_en');
				$tag_en = array_unique($tag_en);
			}

			$badge_uk = [];
			$badge_ru = [];
			$badge_en = [];
			
			if (!empty($product['data']['badges'])) {
				$badge_uk = array_column($product['data']['badges'], 'badge_name_uk');
				$badge_uk = array_unique($badge_uk);
				
				$badge_ru = array_column($product['data']['badges'], 'badge_name_ru');
				$badge_ru = array_unique($badge_ru);
				
				$badge_en = array_column($product['data']['badges'], 'badge_name_en');
				$badge_en = array_unique($badge_en);
			}
			
			$insert_command->bindValue(':articul_' . $index, $product['product_alias'], PDO::PARAM_STR);
			$insert_command->bindValue(':title_uk_' . $index, $product['product_title_uk'], PDO::PARAM_STR);
			$insert_command->bindValue(':title_ru_' . $index, $product['product_title_ru'], PDO::PARAM_STR);
			$insert_command->bindValue(':title_en_' . $index, $product['product_title_en'], PDO::PARAM_STR);
			$insert_command->bindValue(':description_uk_' . $index, strip_tags($product['product_description_uk']), PDO::PARAM_STR);
			$insert_command->bindValue(':description_ru_' . $index, strip_tags($product['product_description_ru']), PDO::PARAM_STR);
			$insert_command->bindValue(':description_en_' . $index, strip_tags($product['product_description_en']), PDO::PARAM_STR);
			$insert_command->bindValue(':category_uk_' . $index, implode(' ', $category_uk), PDO::PARAM_STR);
			$insert_command->bindValue(':category_ru_' . $index, implode(' ', $category_ru), PDO::PARAM_STR);
			$insert_command->bindValue(':category_en_' . $index, implode(' ', $category_en), PDO::PARAM_STR);
			$insert_command->bindValue(':tag_uk_' . $index, implode(' ', $tag_uk), PDO::PARAM_STR);
			$insert_command->bindValue(':tag_ru_' . $index, implode(' ', $tag_ru), PDO::PARAM_STR);
			$insert_command->bindValue(':tag_en_' . $index, implode(' ', $tag_en), PDO::PARAM_STR);
			$insert_command->bindValue(':badge_uk_' . $index, implode(' ', $badge_uk), PDO::PARAM_STR);
			$insert_command->bindValue(':badge_ru_' . $index, implode(' ', $badge_ru), PDO::PARAM_STR);
			$insert_command->bindValue(':badge_en_' . $index, implode(' ', $badge_en), PDO::PARAM_STR);
			$insert_command->bindValue(':brand_uk_' . $index, $product['brand_name_uk'], PDO::PARAM_STR);
			$insert_command->bindValue(':brand_ru_' . $index, $product['brand_name_ru'], PDO::PARAM_STR);
			$insert_command->bindValue(':brand_en_' . $index, $product['brand_name_en'], PDO::PARAM_STR);
			$insert_command->bindValue(':alias_' . $index, $product['product_alias'], PDO::PARAM_STR);
		}

		$insert_command->execute();
	}
}