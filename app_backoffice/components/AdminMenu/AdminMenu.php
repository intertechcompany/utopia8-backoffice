<?php
class AdminMenu extends CWidget
{ 
    public function run()
    {
		$new_orders = Order::model()->getNewOrdersTotal();
		$new_requests = Request::model()->getNewRequestsTotal();

		$this->render('adminMenu', array(
			'new_orders' => $new_orders,
			'new_requests' => $new_requests,
		));
    }
}