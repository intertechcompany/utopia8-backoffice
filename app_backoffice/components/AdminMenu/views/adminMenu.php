<?php
  $_c = Yii::app()->getController();
  $user_name = Yii::app()->user->name;
?>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="top-menu">
<ul class="nav navbar-nav">
  	<li class="dropdown">
  		<a id="dropdownMenu1" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Catalog menu')?> <span class="caret"></span></a>
  		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
		  	<li class="dropdown-header">Справочники</li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('categories')?>"><?=Yii::t('app', 'Categories menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('basecategories')?>"><?=Yii::t('app', 'Base categories menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('brands')?>"><?=Yii::t('app', 'Brands menu')?></a></li>
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('badges')?>"><?=Yii::t('app', 'Badges menu')?></a></li>
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('reclamations')?>"><?=Yii::t('app', 'Return menu')?></a></li>
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('cares')?>"><?=Yii::t('app', 'Cares menu')?></a></li>
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('sizes')?>"><?=Yii::t('app', 'Sizes menu')?></a></li>
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('tags')?>"><?=Yii::t('app', 'Tags menu')?></a></li>
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('stores')?>"><?=Yii::t('app', 'Stores menu')?></a></li>
        	<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('properties')?>"><?=Yii::t('app', 'Properties menu')?></a></li>
			<li class="divider"></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('products')?>"><?=Yii::t('app', 'Products menu')?></a></li>
  		</ul>
  	</li>
	<?php /* <li class="dropdown">
  		<a id="dropdownMenu11" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Articles menu')?> <span class="caret"></span></a>
  		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu11">
		    <li class="dropdown-header">Новости</li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('blogcategories')?>"><?=Yii::t('app', 'Categories menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('blogs')?>"><?=Yii::t('app', 'Blog menu')?></a></li>
			<li class="divider"></li>
			<li class="dropdown-header">База знаний</li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('basecategories')?>"><?=Yii::t('app', 'Categories menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('bases')?>"><?=Yii::t('app', 'Knowledge base menu')?></a></li>
		</ul>
	</li> */ ?>
	<li><a href="<?=$_c->createUrl('orders')?>"><?=Yii::t('app', 'Orders menu')?><?php if (!empty($new_orders)) { ?> <span class="badge"><?=$new_orders?></span><?php } ?></a></li>
	<li><a href="<?=$_c->createUrl('discounts')?>"><?=Yii::t('app', 'Discounts menu')?></a></li>
	<li><a href="<?=$_c->createUrl('pages')?>"><?=Yii::t('app', 'Pages menu')?></a></li>
  	<li><a href="<?=$_c->createUrl('banners')?>"><?=Yii::t('app', 'Banners menu')?></a></li>
	<!-- <li><a href="<?=$_c->createUrl('users')?>"><?=Yii::t('app', 'Users menu')?></a></li> -->
	<li class="dropdown">
		<a id="dropdownMenu2" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Users menu')?> <span class="caret"></span></a>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('users')?>"><?=Yii::t('app', 'Clients menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('managers')?>"><?=Yii::t('app', 'Managers menu')?> <span class="badge">beta</span></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('roles')?>"><?=Yii::t('app', 'Roles menu')?> <span class="badge">beta</span></a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a id="dropdownMenu21" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Settings menu')?> <span class="caret"></span></a>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu21">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('translations')?>"><?=Yii::t('app', 'Translations menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('settings')?>"><?=Yii::t('app', 'Settings menu')?></a></li>
		</ul>
	</li>
  	<li><a href="<?=$_c->createUrl('logout')?>"><?=Yii::t('app', 'Logout menu')?></a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
	<li><a target="_blank" href="<?=Yii::app()->params->url?>"><?=Yii::t('app', 'Go to the website')?></a></li>
</ul>
</div><!-- /.navbar-collapse -->