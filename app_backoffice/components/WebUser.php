<?php
class WebUser extends CWebUser
{
	public $rights_aliases = [
		'admin/category' => 'admin/categories',
		'admin/product' => 'admin/products',
		'admin/property' => 'admin/properties',
		'admin/propertyvalues' => 'admin/properties',
		'admin/propertyvalue' => 'admin/properties',
		'admin/order' => 'admin/orders',
		'admin/request' => 'admin/requests',
		'admin/page' => 'admin/pages',
		'admin/banner' => 'admin/banners',
		'admin/user' => 'admin/users',
		'admin/manager' => 'admin/managers',
		'admin/role' => 'admin/roles',
		'admin/translation' => 'admin/translations',
	];
	
	public function init()
	{
		// Yii::trace('webuser init');
		parent::init();
	}

	public function restoreFromCookie()
	{
		// Yii::trace('restore');
		parent::restoreFromCookie();
	}

	public function renewCookie()
	{
		// Yii::trace('renew');
		parent::renewCookie();
	}

	protected function beforeLogin($id,$states,$fromCookie)
	{
		// Yii::trace('from cookie ' . var_export($fromCookie, true));
		return parent::beforeLogin($id,$states,$fromCookie);
	}

	public function hasAccess($route, $write = false)
	{
		if ($this->isGuest) {
			return false;
		}
		
		if ($this->type == 'admin') {
			return true;
		}
		
		$user_data = $this->user_data;
		$rights = isset($user_data['rights']) ? $user_data['rights'] : [];

		if (isset($this->rights_aliases[$route])) {
			$route = $this->rights_aliases[$route];
		}

		if (!$write && isset($rights[$route])) {
			return true;
		} elseif ($write && isset($rights[$route]['write'])) {
			return true;
		} else {
			return false;
		}
	}

	public function validateToken()
	{
		$user_id = $this->id;

        $user_type = $this->hasState('type') ? $this->getState('type') : 'manager';

		if ($user_type == 'manager') {
			$token = $this->hasState('token') ? $this->getState('token') : '';
		
			if (!Manager::model()->isValidToken($user_id, $token)) {
				$this->logout(false);
			}
		} else {
			$token = $this->hasState('token') ? $this->getState('token') : '';
			$hash = hash_hmac('sha256', $this->name, Yii::app()->params->secret);

			if ($token !== $hash) {
				$this->logout(false);
			}
		}

		return $this;
	}

	public function initUserData($reinit = false)
	{
		if ($this->isGuest) {
			// skip if not logged
			return $this;
		}

        $user_type = $this->hasState('type') ? $this->getState('type') : 'manager';

		if ($user_type != 'manager') {
			// admin account
			if (!$this->hasState('user_data')) {
				$this->setState('user_data', array(
					'saved' => date('Y-m-d H:i:s'),
					'manager_email' => '',
					'manager_first_name' => '',
					'manager_middle_name' => '',
					'manager_last_name' => '',
					'rights' => [],
				));
			}

			return $this;
		}

		if (!$reinit && $this->hasState('type') && $this->hasState('user_data') && $this->hasState('saved')) {
			$current_user_data = $this->getState('user_data');

			// force reinit if manager data was changed
			if ($current_user_data['saved'] != $this->getState('saved')) {
				$reinit = true;
			}
		} else {
			// no manager data or force reinit
			$reinit = true;
		}

		if ($reinit) {
			$manager_model = Manager::model();
			$user_data = array();

			// get manager data
			$manager = $manager_model->getManager();

			$get_keys = array(
				'saved',
				'manager_email',
				'manager_first_name',
				'manager_middle_name',
				'manager_last_name',
			);

			foreach ($get_keys as $key) {
				if (isset($manager[$key])) {
					$user_data[$key] = $manager[$key];
				}
			}

			// setup roles and their rights
			$user_data['rights'] =  $manager_model->getManagerRights();

			$this->setState('user_data', $user_data);
		}

		return $this;
	}
}