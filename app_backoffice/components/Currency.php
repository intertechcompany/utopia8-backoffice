<?php
/**
 * Currency class
 */
class Currency
{
	public static function format($price, $currency_code = '')
	{
		$currency_code = !isset(Yii::app()->params->currencies[$currency_code]) ? 'eur' : $currency_code;
		$currency = Yii::app()->params->currencies[$currency_code];
		
		$formatted_currency = $currency['prefix'] . number_format($price, 2, '.', ' ') . $currency['suffix'];
		
		return $formatted_currency;
	}

	public static function sign($currency_code)
	{
		$currency_code = !isset(Yii::app()->params->currencies[$currency_code]) ? 'eur' : $currency_code;
		$currency = Yii::app()->params->currencies[$currency_code];
		
		return trim($currency['prefix'] . $currency['suffix']);
    }
}