<?php
use BenTools\Psr7\RequestMatcherInterface;
use Psr\Http\Message\RequestInterface;

class RetailCrmRequestMatcher implements RequestMatcherInterface
{
    /**
     * @inheritDoc
     */
    public function matchRequest(RequestInterface $request)
    {
        return strpos($request->getUri()->getHost(), 'retailcrm.ru') !== false;
    }
}