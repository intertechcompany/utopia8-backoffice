<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		if ($this->username == 'utopia8_admin' && $this->password == 'kate_Utopia8.new') {
			$this->id = -1;
			$this->errorCode = self::ERROR_NONE;

			$hash = hash_hmac('sha256', $this->username, Yii::app()->params->secret);
			$this->setState('token', $hash);
			$this->setState('type', 'admin');
		} else {
			$manager_model = Manager::model();
			$manager = $manager_model->getManagerByLogin($this->username);
			
			if (empty($manager)) {
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
				$this->errorMessage = Yii::t('login', 'Invalid email or password! err');
			} elseif (!$manager_model->validatePassword($this->password, $manager['manager_password'])) {
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
				$this->errorMessage = Yii::t('login', 'Invalid email or password! err');
			} elseif ($manager['active'] != 1) {
				$this->errorCode = self::ERROR_BLOCKED;
				$this->errorMessage = Yii::t('login', 'Account blocked! err');
			} else {
				$this->id = $manager['manager_id'];

				// generate token
				$token = md5($manager['created'] . $manager['manager_password']);
				$this->setState('token', $token);
                $this->setState('type', 'manager');
				$manager_model->saveToken($token, $this->id);
				
				$this->errorCode = self::ERROR_NONE;
            }
		}
		
		return $this->errorCode == self::ERROR_NONE;
	}
	
	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->id;
	}
}