<?php
class MailgunMailer extends Mailer
{
	private $api_key;
	private $from_email;
	private $from_name;

	public function __construct($from_email = '', $from_name = '')
	{
		$this->api_key = Yii::app()->params->mailer['mailgun']['api_key'];
		$this->from_email = !empty($from_email) ? $from_email : Yii::app()->params->mailer['from_email'];
		$this->from_name = !empty($from_name) ? $from_name : Yii::app()->params->mailer['from_name'];
	}

	/**
	 * Send message via Mailgun API.
	 * 
	 * @param Array $to_list an array of messages and their metadata. 
	 * Each array within personalizations can be thought of as an envelope - it defines who should receive 
	 * an individual message and how that message should be handled.
	 * 
	 * An array parameters below:
	 * array(
	 * 	   'email' => 'email@addess.com',
	 *     'name' => 'John Smith',
	 *     'substitutions' => array(
	 *         'substitution_variable' => 'value to substitute',
	 *     ),
	 * )
	 * email — required;
	 * name — optional;
	 * substitutions — optional.
	 * @param string $subject the subject of your email.
	 * @param string $body the body of your email.
	 * @return boolean success send or failed.
	 */
	public function send(array $to_list, $subject, $body)
	{
		if (Yii::app()->params->dev) {
			Yii::log($this->from_email . ' ' . $this->from_name . "\n\n" . var_export($to_list, true) . "\n\n" . $subject . "\n\n" . $body, 'error', 'mailgun');

			return false;
		}

		$emails = array();
		$substitutions = array();

		foreach ($to_list as $to) {
			if (isset($to['name'])) {
				$emails[] = $to['name'] . ' <' . $to['email'] . '>';
			} else {
				$emails[] = $to['email'];
			}

			if (isset($to['substitutions'])) {
				$substitutions[$to['email']] = $to['substitutions'];
			}
		}

		if (empty($emails)) {
			return false;
		}

		$form_params = array(
			'from' => $this->from_name . ' <' . $this->from_email . '>',
			'to' => $emails,
			'subject' => $subject,
			'html' => $this->wrapEmailBody($subject, $body),
		);

		if (isset($substitutions)) {
			$form_params['recipient-variables'] = json_encode($substitutions);
		}

		try {
			$options = array(
				'http' => array(
					'header' => "Content-type: application/x-www-form-urlencoded\r\n" . 
								"Authorization: Basic " . base64_encode('api:' . $this->api_key),
					'method' => 'POST',
					'content' => http_build_query($form_params),
				),
			);

			$context = stream_context_create($options);
			$server_response = file_get_contents('https://api.mailgun.net/v3/' . Yii::app()->params->mailer['mailgun']['domain'] . '/messages', false, $context);

			// parse server response JSON
			$result = json_decode($server_response, true);

			if (json_last_error()) {
				// error from server — couldn't parse JSON
				Yii::log($server_response, 'error', 'mailgun');
			} elseif (!isset($result['id'])) {
				// another error from MailGun
				Yii::log(json_encode($result), 'error', 'mailgun');
			} else {
				// ok, message sent
				return true;
			}
		} catch (Exception $e) {
			// some server error (for instance, internal server error 500) or something else
			Yii::log($e->getMessage(), 'error', 'mailgun');
		}

		return false;
	}
}