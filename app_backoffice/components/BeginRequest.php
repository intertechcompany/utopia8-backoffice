<?php
/**
 * BeginRequest class
 */
class BeginRequest
{
	public static function onStartSite()
	{
		// before start request
		$_app_request = Yii::app()->getRequest();
		
		// base url
		$_base_url = $_app_request->getBaseUrl();

		// remove trailing slashes
		$_full_uri = $_app_request->getRequestUri();
		list($_path, ) = explode('?', $_full_uri);
		$_clean_path = rtrim($_path, '/');

		if ($_clean_path == $_base_url) {
			$_clean_path = $_base_url . '/';
		}
		
		if ($_path != $_clean_path) {
			if ($_app_request->getQueryString() != '') {
				$_app_request->redirect($_app_request->getHostInfo() . $_clean_path . '?' . $_app_request->getQueryString(), true, 301);
			}
			else {
				$_app_request->redirect($_app_request->getHostInfo() . $_clean_path, true, 301);
			}
		}

		// get settings
		$db = Yii::app()->db;
		
		// get app config
		$app_settings = $db->createCommand("SELECT * FROM setting WHERE setting_id = 1")
			->queryRow();
		
		if (!empty($app_settings)) {
			$disabled_setting_fields = array(
				'setting_id',
				'created',
				'saved',
			);
			
			$settings_data = array();
			
			foreach ($app_settings as $setting_key => $setting_value) {
				if (in_array($setting_key, $disabled_setting_fields)) {
					continue;
				}
				else {
					$setting_key = str_replace('setting_', '', $setting_key);
				}
				
				$settings_data[$setting_key] = $setting_value;
			}
			
			Yii::app()->setParams(array('settings' => $settings_data));
		}
		else {
			throw new CException('Can\'t load site settings');
		}

		// validate user
		$app_user = Yii::app()->getUser();

		if (!$app_user->isGuest) {
			$app_user->validateToken()->initUserData();
		}
	}
  
	public static function onStopSite()
	{  
		// after request complete
		if (!Yii::app()->request->isAjaxRequest) {
			echo sprintf('%0.5f',Yii::getLogger()->getExecutionTime());
		}
	}
}