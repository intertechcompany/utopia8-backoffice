<?php
class SendGridMailer extends Mailer
{
	private $api_key;
	private $from_email;
	private $from_name;

	public function __construct($from_email = '', $from_name = '')
	{
		$this->api_key = Yii::app()->params->mailer['sendgrid']['api_key'];
		$this->from_email = !empty($from_email) ? $from_email : Yii::app()->params->mailer['from_email'];
		$this->from_name = !empty($from_name) ? $from_name : Yii::app()->params->mailer['from_name'];
	}

	/**
	 * Send message via SendGrid API.
	 * 
	 * @param Array $to_list an array of messages and their metadata. 
	 * Each array within personalizations can be thought of as an envelope - it defines who should receive 
	 * an individual message and how that message should be handled.
	 * 
	 * An array parameters below:
	 * array(
	 * 	   'email' => 'email@addess.com',
	 *     'name' => 'John Smith',
	 *     'substitutions' => array(
	 *         '%substitution_tag%' => 'value to substitute',
	 *     ),
	 * )
	 * email — required;
	 * name — optional;
	 * substitutions — optional.
	 * @param string $subject the subject of your email.
	 * @param string $body the body of your email.
	 * @param array $attachments array of files. Each file array contains name and path to file.
	 * @param string $template_id UUID of SendGrid template.
	 * @return boolean success send or failed.
	 */
	public function send(array $to_list, $subject, $body, $attachments = array(), $template_id = '')
	{
		if (Yii::app()->params->dev) {
			Yii::log($this->from_email . ' ' . $this->from_name . "\n\n" . var_export($to_list, true) . "\n\n" . $subject . "\n\n" . $body, 'error', 'sendgrid');

			return false;
		}

		$personalizations = $this->filterToList($to_list);

		if (empty($personalizations)) {
			return false;
		}

		if (!empty($template_id)) {
			$body_data = array(
				'personalizations' => $personalizations,
				'from' => array(
					'email' => $this->from_email,
					'name' => $this->from_name,
				),
				'reply_to' => array(
					'email' => $this->from_email,
					'name' => $this->from_name,
				),
				'template_id' => $template_id,
				'subject' => ' ',
				'content' => array(
					array(
						'type' => 'text/plain',
						'value' => ' ',
					),
					array(
						'type' => 'text/html',
						'value' => ' ',
					),
				),
			);
		} else {
			$body_data = array(
				'personalizations' => $personalizations,
				'from' => array(
					'email' => $this->from_email,
					'name' => $this->from_name,
				),
				'reply_to' => array(
					'email' => $this->from_email,
					'name' => $this->from_name,
				),
				'subject' => $subject,
				'content' => array(
					array(
						'type' => 'text/html',
						'value' => $body,
					),
				),
			);
		}

		if (!empty($attachments)) {
			$body_data['attachments'] = array();

			foreach ($attachments as $attachment) {
				if (is_file($attachment['file'])) {
					$body_data['attachments'][] = array(
						'filename' => $attachment['name'],
						'content' => base64_encode(file_get_contents($attachment['file'])),
					);
				}
			}
		}

		try {
			$options = array(
				'http' => array(
					'header' => "Content-type: application/json\n" . 
								"Authorization: Bearer " . $this->api_key,
					'method' => 'POST',
					'content' => json_encode($body_data),
				),
			);

			$context = stream_context_create($options);
			$server_response = file_get_contents('https://api.sendgrid.com/v3/mail/send', false, $context);

			// parse server response JSON
			$result = json_decode($server_response, true);

			if (json_last_error()) {
				// error from server — couldn't parse JSON
				Yii::log($server_response, 'error', 'sendgrid');
			} else {
				// ok, message sent
				return true;
			}
		} catch (Exception $e) {
			// some server error (for instance, internal server error 500) or something else
			Yii::log($e->getMessage(), 'error', 'sendgrid');
		}

		return false;
	}

	private function filterToList($to_list)
	{
		$personalizations = array();

		foreach ($to_list as $to) {
			if (is_string($to)) {
				$email = trim($to);
			} else {
				$email = trim($to['email']);
			}

			if (empty($email)) {
				continue;
			}

			$to_data = array(
				'email' => $email,
			);

			if (is_array($to) && isset($to['name'])) {
				$to_data['name'] = $to['name'];
			}

			if (is_array($to) && isset($to['substitutions'])) {
				$personalizations[] = array(
					'to' => array($to_data),
					'substitutions' => $to['substitutions'],
				);
			} else {
				$personalizations[] = array(
					'to' => array($to_data),
				);
			}
		}

		return $personalizations;
	}
}