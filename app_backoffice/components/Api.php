<?php
use BenTools\GuzzleHttp\Middleware\Storage\Adapter\ArrayAdapter;
use BenTools\GuzzleHttp\Middleware\ThrottleConfiguration;
use BenTools\GuzzleHttp\Middleware\ThrottleMiddleware;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

/**
 * Api class
 */
class Api
{
	const RETAIL_PRICE_TYPE = '000000001';

	private $client;
	
	private $category_model;
	private $base_category_model;
	private $brand_model;
	private $property_model;
	private $property_value_model;
	private $store_model;
	private $product_model;

	private $categories_map = [];
	private $brands_map = [];
	private $properties_map = [];
	private $stores_map = [];
	private $offers_map = [];
	
	public function __construct()
	{
		ini_set('memory_limit', '512M');
		set_time_limit(10 * 60);
		
		// GuzzleHttp
		require_once Yii::getPathOfAlias('application.vendor.GuzzleHttp') . DS . 'autoload.php';

		$stack = HandlerStack::create();
		$middleware = new ThrottleMiddleware(new ArrayAdapter());

		// max 9 request per second
		$max_requests = 9;
		$duration = 1;
		$middleware->registerConfiguration(
			new ThrottleConfiguration(new RetailCrmRequestMatcher(), $max_requests, $duration, 'retailcrm')
		);

		$stack->push($middleware, 'throttle');
		$this->client = new Client([
			'handler' => $stack,
			'timeout' => 15.0,
		]);
		
		$this->category_model = Category::model();
		$this->base_category_model = BaseCategory::model();
		$this->brand_model = Brand::model();
		$this->property_model = Property::model();
		$this->property_value_model = PropertyValue::model();
		$this->store_model = Store::model();
		$this->product_model = Product::model();

		$this->categories_map = $this->base_category_model->getCategoriesMap();
		$this->brands_map = $this->brand_model->getBrandsMap();
		$this->properties_map = $this->property_model->getPropertiesMap();
		$this->stores_map = $this->store_model->getStoresMap();
	}
	
	public function update()
	{
		$start = microtime(true);
			
		$this->updateCategories();
		$this->updateProperties();
		$this->updateProducts();

		// echo '<pre>';
		echo microtime(true) - $start;
		// print_r($this->categories_map);
		// print_r($this->properties_map);
		// print_r($this->stores_map);
		// print_r($this->brands_map);
		// echo '</pre>';
	}

	public function addCategories($categories, $parent_id = 0)
	{
		foreach ($categories as $index => $category) {
			$category_id = $this->addCategory($category['title'], $parent_id, $index + 1);

			if (!empty($category['sub'])) {
				$this->addCategories($category['sub'], $category_id);
			}
		}
	}

	private function addCategory($category_name, $parent_id, $position)
	{
		// import URLify library
        Yii::import('application.vendor.URLify.URLify');
		
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$insert_category = [
			'created' => $today,
			'saved' => $today,
			'active' => 1,
			'category_position' => (int) $position,
			'category_alias' => URLify::filter($category_name, 200),
			'parent_id' => $parent_id,
		];
		
		try {
			$rs = $builder->createInsertCommand('category', $insert_category)->execute();

			if ($rs) {
				$category_id = (int) Yii::app()->db->getLastInsertID();
				
				foreach (Yii::app()->params->langs as $language_code => $language_name) {
					// save details
					$insert_category_lang = array(
						'category_id' => $category_id,
						'language_code' => $language_code,
						'category_visible' => 1,
						'created' => $today,
						'saved' => $today,
						'category_name' => $category_name,
					);

					$rs = $builder->createInsertCommand('category_lang', $insert_category_lang)->execute();

					if (!$rs) {
						$delete_criteria = new CDbCriteria(
							array(
								"condition" => "category_id = :category_id" , 
								"params" => array(
									"category_id" => $category_id,
								)
							)
						);
						
						$builder->createDeleteCommand('category', $delete_criteria)->execute();

						return false;
					}
                }

				return $category_id;
            }
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function updateCategories()
	{
		$page = 1;
		$last_page = 1;

		do {
			// https://slashdotdash.retailcrm.ru/api/v5/store/product-groups?filter[sites][]=000000001&page=1&limit=100&apiKey=Bj2DWQ4YB4tEVy112kxUdNREXTyjROds
			$result = $this->request('GET', '/store/product-groups', [
				'filter' => [
					'sites' => [
						'000000001'
					]
				],
				'page' => $page,
				'limit' => 100,
			]);

			if (!isset($result['success']) || $result['success'] !== true) {
				break;
			}

			foreach ($result['productGroup'] as $product_group) {
				$this->updateCategory($product_group);
			}

			$page++;
			$last_page = $result['pagination']['totalPageCount'];
		} while ($page <= $last_page);
	}
	
	/* 
	Array
	(
		?[parentId] => 2245
		[site] => 000000001
		[id] => 2245
		[name] => Блокноты 
		[externalId] => А000000043
		[active] => 1
	) */
	private function updateCategory($product_group) 
	{
		$category_crm_id = $product_group['id'];
		
		if (!isset($this->categories_map[$category_crm_id])) {
			if (isset($product_group['parentId']) && isset($this->categories_map[$product_group['parentId']])) {
				$product_group['parentId'] = $this->categories_map[$product_group['parentId']]['category_id'];
			} else {
				$product_group['parentId'] = 0;
			}
			
			$category_data = $this->base_category_model->add($product_group);
			
			if ($category_data !== false) {
				$this->categories_map[$category_crm_id] = $category_data;
			} else {
				Yii::log('Category creation issue. ID: ' . $category_crm_id . "\n" . var_export($product_group, true), 'error', 'RetailCRM');
			}
		}
	}
	
	public function updateProperties()
	{
		$page = 1;
		$last_page = 1;

		do {
			// https://slashdotdash.retailcrm.ru/api/v5/store/products/properties?filter[sites][]=000000001&page=1&limit=100&apiKey=Bj2DWQ4YB4tEVy112kxUdNREXTyjROds
			$result = $this->request('GET', '/store/products/properties', [
				'filter' => [
					'sites' => [
						'000000001'
					]
				],
				'page' => $page,
				'limit' => 100,
			]);

			if (!isset($result['success']) || $result['success'] !== true) {
				break;
			}

			foreach ($result['properties'] as $property) {
				$this->updateProperty($property);
			}

			$page++;
			$last_page = $result['pagination']['totalPageCount'];
		} while ($page <= $last_page);
	}
	
	/* 
	Array
	(
		[sites] => Array
			(
				[0] => 000000001
			)

		[code] => 000000016
		[name] => Длина ручки
	) */
	private function updateProperty($property) 
	{
		$property_crm_code = $property['code'];
		
		if (!isset($this->properties_map[$property_crm_code])) {
			$property_data = $this->property_model->add($property);
			
			if ($property_data !== false) {
				$this->properties_map[$property_crm_code] = $property_data;
			} else {
				Yii::log('Property creation issue. ID: ' . $property_crm_code . "\n" . var_export($property, true), 'error', 'RetailCRM');
			}
		}
	}
	
	public function updateProducts()
	{
		$page = 1;
		$last_page = 1;

		do {
			// https://slashdotdash.retailcrm.ru/api/v5/store/products?filter[sites][]=000000001&limit=100&apiKey=Bj2DWQ4YB4tEVy112kxUdNREXTyjROds
			$result = $this->request('GET', '/store/products', [
				'filter' => [
					'sites' => [
						'000000001'
					],
					/* 'ids' => [
						'132018',
						'132020',
						'132016',
						'132015',
					], */
					/* 'ids' => [
						'131652',
					], */
					/* 'ids' => [
						'136435',
						'132758',
					], */
				],
				'page' => $page,
				'limit' => 100,
			]);

			if (!isset($result['success']) || $result['success'] !== true) {
				break;
			}

			$this->offers_map = [];

			foreach ($result['products'] as $index => $product) {
				$prices = [];
				
				foreach ($product['offers'] as $offer) {
					$offer['price'] = (float) $offer['price'];
					
					if (empty($offer['price'])) {
						continue;
					}

					$offer_id = $offer['id'];
					$this->offers_map[$offer_id] = [
						'id' => $offer['id'],
						'article' => isset($offer['article']) ? $offer['article'] : '',
						'quantity' => 0,
						'properties' => $offer['properties'],
						'stores' => [],
					];

					foreach ($offer['prices'] as $price) {
						if ($price['priceType'] == self::RETAIL_PRICE_TYPE) {
							$prices[] = ceil($price['price']); // (float) $price['price'];
							$this->offers_map[$offer_id]['price'] = ceil($price['price']); // (float) $price['price'];
							break;
						}
					}

					if (!isset($this->offers_map[$offer_id]['price'])) {
						foreach ($offer['prices'] as $price) {
							if ($price['priceType'] == 'base') {
								$prices[] = ceil($price['price']); // (float) $price['price'];
								$this->offers_map[$offer_id]['price'] = ceil($price['price']); // (float) $price['price'];
								break;
							}
						}
					}
				}

				// TODO: operate products without prices
				if (empty($prices)) {
					unset($result['products'][$index]);
					continue;
					// echo '<pre>';
					// print_r($result['products'][$index]);
					// print_r($offer);
					// echo '</pre>';
				}

				$result['products'][$index]['price_type'] = count($product['offers']) > 1 ? 'variants' : 'item';
				$result['products'][$index]['offer_id'] = count($product['offers']) > 1 ? 0 : $offer_id;
				$result['products'][$index]['minPrice'] = min($prices);
				$result['products'][$index]['maxPrice'] = max($prices);
			}

			$this->getOffersStores();

            foreach ($result['products'] as $index => $product) {
				$product_data = $this->product_model->getProductByAliasAdmin($product['id']);

				if (empty($product_data)) {
					$product_id = $this->addProduct($product);
				} else {
					$product_id = $this->updateProduct($product_data['product_id'], $product);
				}

				// update product variants properties
				$this->product_model->updateProperties($product_id);
            }

			$page++;
			$last_page = $result['pagination']['totalPageCount'];
		} while ($page <= $last_page);
	}

	/* 
	Array
	(
		[id] => 124282
		[externalId] => 00000033766
		[xmlId] => 00000033766
		[site] => 000000001
		[purchasePrice] => 64
		[quantity] => 3
		[stores] => Array
			(
				[0] => Array
					(
						[quantity] => 0
						[purchasePrice] => 64
						[store] => 000000003
					)

				[1] => Array
					(
						[quantity] => 0
						[purchasePrice] => 64
						[store] => 000000007
					)

				[2] => Array
					(
						[quantity] => 0
						[purchasePrice] => 64
						[store] => 000000009
					)

				[3] => Array
					(
						[quantity] => 0
						[purchasePrice] => 64
						[store] => 000000010
					)

				[4] => Array
					(
						[quantity] => 3
						[purchasePrice] => 64
						[store] => 000000001
					)

			)

	) */
	private function getOffersStores()
	{
		$page = 1;
		$last_page = 1;

		do {
			// https://slashdotdash.retailcrm.ru/api/v5/store/inventories?filter[sites][]=000000001&filter[ids][]=124282&filter[details]=1&apiKey=Bj2DWQ4YB4tEVy112kxUdNREXTyjROds
			$result = $this->request('GET', '/store/inventories', [
				'filter' => [
					'sites' => [
						'000000001'
					],
					'ids' => array_keys($this->offers_map),
					'details' => '1',
				],
				'page' => $page,
				'limit' => 100,
			]);

			if (!isset($result['success']) || $result['success'] !== true) {
				break;
			}

            foreach ($result['offers'] as $offer) {
				$offer_id = $offer['id'];

				if (!isset($this->offers_map[$offer_id])) {
					continue;
				}

				$is_store = false;
				$is_dropship = false;
				$is_preorder = false;

				foreach ($offer['stores'] as $store) {
					$store_code = $store['store'];

					if (isset($this->stores_map[$store_code]) && $store['quantity'] > 0) {
						$this->offers_map[$offer_id]['stores'][$store_code] = (int) $store['quantity'];

						if ($this->stores_map[$store_code]['store_type'] == 'warehouse') {
							$is_store = true;
						} elseif ($this->stores_map[$store_code]['store_type'] == 'dropship') {
							$is_dropship = true;
						} elseif ($this->stores_map[$store_code]['store_type'] == 'preorder') {
							$is_preorder = true;
						}
					}
				}

				$this->offers_map[$offer_id]['quantity'] = array_sum($this->offers_map[$offer_id]['stores']);

				if ($is_store) {
					$this->offers_map[$offer_id]['type'] = 'store';
				} elseif ($is_dropship) {
					$this->offers_map[$offer_id]['type'] = 'online';
				} elseif ($is_preorder) {
					$this->offers_map[$offer_id]['type'] = 'preorder';
				} else {
					$this->offers_map[$offer_id]['type'] = 'out_of_stock';
				}
			}

			$page++;
			$last_page = $result['pagination']['totalPageCount'];
		} while ($page <= $last_page);
	}
	
	/* 
	Array
	(
		[minPrice] => 0
		[maxPrice] => 2300
		[id] => 131972
		[article] => 70733
		[name] => Хаки костюм
		[url] => 
		[imageUrl] => 
		[description] => 
		[groups] => Array
			(
				[0] => Array
					(
						[id] => 2327
						[externalId] => А000000258
					)

			)

		[externalId] => 00000033754
		[manufacturer] => big bird
		[offers] => Array
			(
				[0] => Array
					(
						[name] => Хаки костюм
						[price] => 0
						[images] => Array
							(
							)

						[id] => 124250
						[externalId] => 00000033754
						[xmlId] => 00000033754
						[article] => 70733
						[prices] => Array
							(
								[0] => Array
									(
										[priceType] => 000000001
										[price] => 0
										[ordering] => 990
									)

								[1] => Array
									(
										[priceType] => base
										[price] => 0
										[ordering] => 991
									)

							)

						[purchasePrice] => 0
						[properties] => Array
							(
							)

						[unit] => Array
							(
								[code] => pc
								[name] => Штука
								[sym] => шт.
							)

					)

				[1] => Array
					(
						[name] => Хаки костюм (One Size)
						[price] => 2300
						[images] => Array
							(
							)

						[id] => 124251
						[externalId] => 00000033754#00001
						[xmlId] => 00000033754#00001
						[article] => 70733
						[prices] => Array
							(
								[0] => Array
									(
										[priceType] => 000000001
										[price] => 2300
										[ordering] => 990
									)

								[1] => Array
									(
										[priceType] => base
										[price] => 2300
										[ordering] => 991
									)

							)

						[purchasePrice] => 1495
						[properties] => Array
							(
								[000000012] => One Size
							)

						[unit] => Array
							(
								[code] => pc
								[name] => Штука
								[sym] => шт.
							)

					)

			)

		[active] => 1
		[quantity] => 1
		[markable] => 
	) */
	private function addProduct($product) 
	{
		$brand_id = $this->getProductBrand($product);
		$categories = $this->getProductCategories($product);
		$product = $this->getProductStores($product);

		$group_id = isset($product['groups'][0]['id']) ? $product['groups'][0]['id'] : 0;
		$base_category_id = isset($this->categories_map[$group_id]) ? $this->categories_map[$group_id]['category_id'] : 0;
		
		$product_id = $this->product_model->add($product, $brand_id, $categories, $base_category_id);

		if ($product_id === false) {
			Yii::log('Product add/update issue. ID: ' . $product['id'], 'error', 'RetailCRM');
			return false;
		}

		if ($product['price_type'] == 'variants') {
			$this->addVariants($product_id, $product['offers'], $categories);
		}

		return $product_id;
	}

	private function updateProduct($product_id, $product) 
	{
		$brand_id = $this->getProductBrand($product);
		$categories = $this->getProductCategories($product);
		$product = $this->getProductStores($product);

		$group_id = isset($product['groups'][0]['id']) ? $product['groups'][0]['id'] : 0;
		$base_category_id = isset($this->categories_map[$group_id]) ? $this->categories_map[$group_id]['category_id'] : 0;
		
		$product_id = $this->product_model->update($product_id, $product, $brand_id, $categories, $base_category_id);
		
		if ($product['price_type'] == 'variants') {
			$this->updateVariants($product_id, $product['offers'], $categories);
		}

		return $product_id;
	}

	private function getProductBrand($product)
	{
		$brand_name = trim($product['manufacturer']);
		
		if (!isset($this->brands_map[$brand_name])) {
			$brand_data = $this->brand_model->add($brand_name);
			
			if ($brand_data !== false) {
				$this->brands_map[$brand_name] = $brand_data;
			} else {
				Yii::log('Brand creation issue. ID: ' . $brand_name, 'error', 'RetailCRM');
			}
		}

		$brand_id = isset($this->brands_map[$brand_name]) ? $this->brands_map[$brand_name]['brand_id'] : 0;

		return $brand_id;
    }
	
	private function getProductCategories($product)
	{
		$group_id = isset($product['groups'][0]['id']) ? $product['groups'][0]['id'] : 0;
		
		$categories = isset($this->categories_map[$group_id]) ? $this->categories_map[$group_id]['categories'] : [];

		return $categories;
	}

	private function getProductStores($product)
	{
		$is_store = false;
		$is_dropship = false;
		$is_preorder = false;

		$product['stores'] = [];

		foreach ($product['offers'] as $index => $offer) {
			$offer_id = $offer['id'];

			if (!isset($this->offers_map[$offer_id])) {
				unset($product['offers'][$index]);
				continue;
			}

			if ($this->offers_map[$offer_id]['type'] == 'store') {
				$is_store = true;
			} elseif ($this->offers_map[$offer_id]['type'] == 'online') {
				$is_dropship = true;
			} elseif ($this->offers_map[$offer_id]['type'] == 'preorder') {
				$is_preorder = true;
			}

			foreach ($this->offers_map[$offer_id]['stores'] as $store_code => $quantity) {
				$store_id = $this->stores_map[$store_code]['store_id'];

				if (isset($product['stores'][$store_id])) {
					$product['stores'][$store_id] += $quantity;
				} else {
					$product['stores'][$store_id] = $quantity;
				}
			}
		}

		if ($is_store) {
			$product['stock_type'] = 'store';
		} elseif ($is_dropship) {
			$product['stock_type'] = 'online';
		} elseif ($is_preorder) {
			$product['stock_type'] = 'preorder';
		} else {
			$product['stock_type'] = 'out_of_stock';
		}

		return $product;
	}

	private function addVariants($product_id, $offers, $categories = [])
	{
		foreach ($offers as $offer_index => $offer) {
			$this->addVariant($product_id, $offer_index, $offer, $categories);
		}
	}
	
	private function updateVariants($product_id, $offers, $categories = [])
	{
		// get all product variants
		$variants = $this->product_model->getProductVariants($product_id);
		
		foreach ($offers as $offer_index => $offer) {
			// get variant
			$variant = $this->product_model->getVariantsBySku($offer['id']);
			
			if (empty($variant)) {
				$this->addVariant($product_id, $offer_index, $offer, $categories);
			} else {
				$this->updateVariant($product_id, $variant['variant_id'], $offer_index, $offer);
				unset($variants[$variant['variant_id']]);
			}
		}

		// remove unnecessary variants
		if (!empty($variants)) {
			foreach ($variants as $variant_id => $variant_item) {
				$this->product_model->deleteVariant($product_id, $variant_id);
			}
		}
	}

	private function addVariant($product_id, $offer_index, $offer, $categories = [])
	{
		$offer_id = $offer['id'];
		$offer_data = $this->offers_map[$offer_id];

		$variant_properties = [];
		$variant_values = [];

		if (!empty($offer_data['properties'])) {
			foreach ($offer_data['properties'] as $property_code => $value) {
				$value = trim($value);
				
				if (isset($this->properties_map[$property_code])) {
					$property_id = $this->properties_map[$property_code]['property_id'];
					
					if (!isset($this->properties_map[$property_code]['values'][$value])) {
						$value_id = $this->property_value_model->add($property_id, $value);

						if ($value_id !== false) {
							$this->properties_map[$property_code]['values'][$value] = $value_id;
						} else {
							Yii::log('Value creation issue. ID: ' . $value, 'error', 'RetailCRM');
						}
					} else {
						$value_id = $this->properties_map[$property_code]['values'][$value];
					}

					$variant_properties[] = $property_id;
					$variant_values[] = $value_id;
				}
			}
		}

		if (!empty($variant_properties)) {
			foreach ($variant_properties as $property_id) {
				foreach ($categories as $category_id) {
					$this->property_model->saveCategory($property_id, $category_id);
				}
			}
		}

		if (!isset($offer_data['price'])) {
			Yii::log(print_r($offer_data, true), 'error', 'app');
		}

		$variant_id = $this->product_model->addVariant($product_id, [
			'variant_sku' => $offer_id,
			'variant_price' => $offer_data['price'],
			'variant_price_old' => 0,
			'variant_price_type' => 'item',
			'variant_instock' => $offer_data['quantity'] > 0 ? 'in_stock' : 'out_of_stock',
			'variant_stock_qty' => $offer_data['quantity'],
			'variant_weight' => 0,
			'values' => $variant_values,
		], $offer_index);

		if ($variant_id !== false) {
			$this->saveVariantStores($product_id, $variant_id, $offer_data);
		} else {
			Yii::log('Variant creation issue. Offer ID: ' . $offer_id, 'error', 'RetailCRM');
		}
	}
	
	private function updateVariant($product_id, $variant_id, $offer_index, $offer)
	{
		$offer_id = $offer['id'];
		$offer_data = $this->offers_map[$offer_id];

		$variant_properties = [];
		$variant_values = [];

		if (!empty($offer_data['properties'])) {
			foreach ($offer_data['properties'] as $property_code => $value) {
				$value = trim($value);
				
				if (isset($this->properties_map[$property_code])) {
					$property_id = $this->properties_map[$property_code]['property_id'];
					
					if (!isset($this->properties_map[$property_code]['values'][$value])) {
						$value_id = $this->property_value_model->add($property_id, $value);

						if ($value_id !== false) {
							$this->properties_map[$property_code]['values'][$value] = $value_id;
						} else {
							Yii::log('Value creation issue. ID: ' . $value, 'error', 'RetailCRM');
						}
					} else {
						$value_id = $this->properties_map[$property_code]['values'][$value];
					}

					$variant_properties[] = $property_id;
					$variant_values[] = $value_id;
				}
			}
		}

		$this->product_model->updateVariant($product_id, $variant_id, [
			'variant_sku' => $offer_id,
			'variant_price' => $offer_data['price'],
			'variant_price_old' => 0,
			'variant_price_type' => 'item',
			'variant_instock' => $offer_data['quantity'] > 0 ? 'in_stock' : 'out_of_stock',
			'variant_stock_qty' => $offer_data['quantity'],
			'variant_weight' => 0,
			'values' => $variant_values,
		], $offer_index, true);

		$this->saveVariantStores($product_id, $variant_id, $offer_data);
	}

	private function saveVariantStores($product_id, $variant_id, $offer)
	{
		$stores = [];

		foreach ($offer['stores'] as $store_code => $quantity) {
			$store_id = $this->stores_map[$store_code]['store_id'];

			if (isset($stores[$store_id])) {
				$stores[$store_id] += $quantity;
			} else {
				$stores[$store_id] = $quantity;
			}
		}
		
		// stores
		$this->product_model->saveVariantStores($product_id, $variant_id, $stores);
	}
	
	public function order($order_data)
	{
	}

	// TEST URL: https://slashdotdash.retailcrm.ru/api/v5/store/products?filter[sites][]=000000001&filter[manufacturer]=test&apiKey=Bj2DWQ4YB4tEVy112kxUdNREXTyjROds
	private function request($type = 'GET', $action, $data = [])
	{
		$base_url = 'https://slashdotdash.retailcrm.ru/api/v5';
		$api_key = Yii::app()->params->retail_crm['api_key'];
		
		try {
			if ($type == 'POST') {
				$url = $base_url . $action;
				
				$response = $this->client->post($url, [
					'headers' => [
						'Content-Type'  => 'application/json',
					],
					'form_params' => array_merge($data, [
						'apiKey' => $api_key,
					]),
					// 'body' => json_encode($data),
				]);
			} else {
				$url = $base_url . $action . '?' . http_build_query(array_merge($data, [
					'apiKey' => $api_key,
				]));
				
				$response = $this->client->get($url, [
					'headers' => [
						'Content-Type'  => 'application/json',
					],
				]);
			}

			$data = json_decode($response->getBody(), true);

			if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
				Yii::log($response->getBody(), 'error', 'RetailCRM');
			} else {
				return $data;
			}
		} catch (GuzzleHttp\Exception\RequestException $e) {
			$exception = "Status code: " . $e->getCode() . ". Error message: " . $e->getMessage();

			if ($e->hasResponse()) {
				$exception .= $e->getResponse()->getBody();
			}

			Yii::log($exception, 'error', 'RetailCRM');
		}

		return false;	
	}
}