<?php
abstract class Mailer
{
	abstract public function send(array $to_list, $subject, $body);

	public function prepareEmailsFromString($emails_str)
	{
		$emails = array();
		$emails_str = trim($emails_str);

		if (empty($emails_str)) {
			return $emails;
		}

		$emails_list = explode(',', $emails_str);

		foreach ($emails_list as $email) {
			$email = trim($email);

			if (preg_match('/^(([\-\_\.a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i', $email)) {
				$emails[] = array(
					'email'=> $email,
				);
			}
		}

		return $emails;
	}

	public function wrapEmailBody($subject, $body, $template = 'email')
	{
		$email_html = Yii::app()->getController()->renderPartial('//email/' . $template, array(
			'subject' => $subject,
			'body' => $body,
		), true);

		return $email_html;
	}
}

class MailerException extends CException
{
}