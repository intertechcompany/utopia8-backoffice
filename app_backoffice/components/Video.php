<?php


class Video
{
    public function convert($video, $dir)
    {
        $video_params = [
            'width' => 0,
            'height' => 0,
            'fps' => 0,
            'rotate' => 0,
            'duration' => 0,
        ];

        $videoPath = $dir . $video;

        exec("ffprobe -v error -show_entries stream_tags=rotate -show_entries format=duration -show_entries stream=r_frame_rate,width,height -of default=noprint_wrappers=1 $videoPath 2>&1", $video_data, $code);

        if ($code !== 0) {
            throw new Exception('Invalid video file!');
        }

        foreach ($video_data as $video_param_value) {
            $video_param_parts = explode('=', $video_param_value);

            if (count($video_param_parts) !== 2) {
                continue;
            }

            list($video_param, $video_value) = $video_param_parts;

            if ($video_param === 'width') {
                $video_params['width'] = $video_value;
            } elseif ($video_param === 'height') {
                $video_params['height'] = $video_value;
            } elseif ($video_param === 'r_frame_rate' && $video_params['fps'] === 0) {
                if (preg_match('/^(\d+)\/(\d+)$/', $video_value, $fps)) {
                    $video_value = $fps[1] / $fps[2];
                }
    
                $video_params['fps'] = round($video_value, 2);
            } elseif ($video_param === 'TAG:rotate') {
                $video_params['rotate'] = $video_value;
            } elseif ($video_param === 'duration') {
                $video_params['duration'] = round($video_value);
            }
        }

        if ($video_params['rotate'] == 90 || $video_params['rotate'] == 270) {
            $height = $video_params['width'];
            $width = $video_params['height'];
    
            $video_params['width'] = $width;
            $video_params['height'] = $height;
        }

        $newName =  'video_web.mp4';
        $newVideoPath = $dir . $newName;

        set_time_limit(60 * 5);

        if ($video_params['fps'] > 30) {
            exec("ffmpeg -i $videoPath -c:v libx264 -preset slow -crf 18 -profile:v high -level 4.0 -bf 2 -coder 1 -pix_fmt yuv420p -b:v 3M -maxrate 5M -bufsize 5M -r 30 -an -movflags +faststart -vf scale=960:-2 $newVideoPath 2>&1");
        } else {
            exec("ffmpeg -i $videoPath -c:v libx264 -preset slow -crf 18 -profile:v high -level 4.0 -bf 2 -coder 1 -pix_fmt yuv420p -b:v 3M -maxrate 5M -bufsize 5M -an -movflags +faststart -vf scale=960:-2 $newVideoPath 2>&1");
        }

        return ['file' => $newName, 'params' => $video_params];
    }
}