<?php
class ApiController extends Controller
{
	/**
	 * Overrided method from CController.
	 * Set global indexing site setting.
	 * 
	 * @param CAction $action the action to be executed.
	 * @return boolean whether the action should be executed.
	 */
	protected function beforeAction($action)
	{
		// отключаем web-логи для ajax запросов
		foreach (Yii::app()->log->routes as $route) {
			if ($route instanceof CWebLogRoute || $route instanceof CProfileLogRoute) {
				$route->enabled = false;
			}
		}
		
		return parent::beforeAction($action);
	}

	/**
	 * Update categories, properties, products from RetailCRM.
	 */
	public function actionUpdate()
	{
		(new Api)->update();
		echo 'Ok.';
	}
	
	/**
	 * Create search index.
	 */
	public function actionCreateIndex()
	{
		// (new Sphinx)->testFacet();
		(new Sphinx)->createIndex();

		echo 'Ok.';
	}
	
	/**
	 * Optimze search index.
	 */
	public function actionOptimizeIndex()
	{
		// Product::model()->updateImages();
		(new Sphinx)->optimizeIndex();

		echo 'Ok.';
	}
	
	/**
	 * Remove products.
	 */
	public function actionRemove()
	{
		/* for ($i = 2062; $i > 0; $i--) {
			Product::model()->delete($i);
		} */
		
		echo 'Ok.';
    }
    
    /**
	 * Send order to RetailCRM.
	 */
	public function actionOrder($order_data)
	{
		(new Api)->order($order_data);
		echo 'Ok.';
	}
}