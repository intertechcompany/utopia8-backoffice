<?php

class AdminController extends Controller
{
	public $json = array();
	private $perPage = 10;

	/**
	 * @return array action filters.
	 */
	public function filters()
	{
		return array(
			'accessControl - login, logout',
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules.
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'users' => array('@'),
			),
			array(
				'deny',
				'users' => array('*'),
			),
		);
	}

	/**
	 * Overrided method from CController.
	 * Prepare user data for some site sections (e.g. customer or agent account).
	 * 
	 * @param CAction $action the action to be executed.
	 * @return boolean whether the action should be executed.
	 */
	protected function beforeAction($action)
	{
		// validate user hash
		if (!Yii::app()->user->isGuest) {
			$token = Yii::app()->user->hasState('token') ? Yii::app()->user->getState('token') : '';
			$hash = hash_hmac('sha256', Yii::app()->user->name, Yii::app()->params->secret);

			if ($token !== $hash) {
				Yii::app()->user->logout(false);
				$this->redirect(array('admin/login'));
			}
		}

		$this->layout = '//layouts/admin';
		
		return parent::beforeAction($action);
	}

	public function actionIndex()
	{
		$user_name = Yii::app()->user->name;

		$this->redirect(array('orders'));
	}

	/**
	 * Categories list.
	 */
	public function actionCategories()
	{
		$category_model = Category::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$category_id = Yii::app()->request->getPost('category_id', null);
		
		// bulk operation
		$category = Yii::app()->request->getPost('category', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($category_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $category_model->toggle($category_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $category_model->toggle($category_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $category_model->delete($category_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($category) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($category as $category_id => $position) {
				$rs = $category_model->setPosition($category_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $category_id) {
					$rs = $category_model->toggle($category_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $category_id) {
					$rs = $category_model->toggle($category_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $category_id) {
					$rs = $category_model->delete($category_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Categories have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('categories', 'Categories h1');

		// get parent category id
		$parent_id = Yii::app()->request->getQuery('parent_id', 0);

		$categories = array();
		$category = array();

		if (empty($parent_id)) {
			// root categories
			$all_categories = $category_model->getCategoriesTree();

			if (!empty($all_categories)) {
				foreach ($all_categories as $category_item) {
					$categories[] = $category_item;
				}
			}
		} else {
			// subcategory
			$all_categories = $category_model->getCategories();

			if (!isset($all_categories[$parent_id])) {
				throw new CHttpException(404, 'Category not found');
			}

			$category = $all_categories[$parent_id];

			if (!empty($category['sub'])) {
				foreach ($category['sub'] as $category_item) {
					$categories[] = $category_item;
				}
			}
		}
		
		$this->render('categories', array(
			'parent_id' => $parent_id,
			'category' => $category,
			'categories' => $categories,
			'all_categories' => $all_categories,
		));
	}

	/**
	 * Add or edit category.
	 */
	public function actionCategory($id)
	{
		$category_model = Category::model();

		$category = Yii::app()->request->getPost('category', null);
		$category_lang = Yii::app()->request->getPost('category_lang', null);
		
		if (!empty($category)) {
			$model = new CategoryForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $category;
			
			$model_lang = new CategoryLangForm;
			$model_lang->attributes = $category_lang;
			
			// set image files
			$model->category_photo = CUploadedFile::getInstanceByName('category[category_photo]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $category_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been successfully added! success'));
						
						if ($model->parent_id) {
							$this->redirect(array('categories', 'parent_id' => $model->parent_id));
						} else {
							$this->redirect(array('categories'));
						}
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$parent_id = Yii::app()->request->getQuery('parent_id', 0);
		$all_categories = $category_model->getCategories();
		$categories_tree = $category_model->getCategoriesTree();

		// get base categories
		$base_category_model = BaseCategory::model();
		$base_categories = $base_category_model->getCategories();
		$base_categories_tree = $base_category_model->getCategoriesTree();

        $tags = Tag::model()->getTagsListAdmin();

		if ($id == 'new') {
			$this->pageTitle = Yii::t('categories', 'Add category h1');
			$this->render('categoryAdd', array(
				'parent_id' => $parent_id,
				'categories_tree' => $categories_tree,
				'base_categories' => $base_categories,
				'base_categories_tree' => $base_categories_tree,
                'tags' => $tags,
			));
		}
		else {
			if (empty($all_categories[$id])) {
				throw new CHttpException(404, 'Page not found');
			}

			$category = $category_model->getCategoryByIdAdmin($id);

			$this->pageTitle = Yii::t('categories', 'Edit category h1') . ' «' . $category[Yii::app()->params->lang]['category_name'] . '»';

			$this->render('categoryEdit', array(
				'category' => $category,
				'parent_id' => $parent_id,
				'categories_tree' => $categories_tree,
				'base_categories' => $base_categories,
				'base_categories_tree' => $base_categories_tree,
                'tags' => $tags,
			));
		}
	}

	/**
	 * Badges list.
	 */
	public function actionBadges()
	{
		$badge_model = Badge::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$badge_id = Yii::app()->request->getPost('badge_id', null);
		
		// bulk operation
		$badge = Yii::app()->request->getPost('badge', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($badge_id) && !empty($action) && in_array($action, array('delete'))) {
			if($action == 'delete') {
				$rs = $badge_model->delete($badge_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('badges', 'A badge has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($badge) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($badge as $badge_id => $position) {
				$rs = $badge_model->setPosition($badge_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
			$counter = 0;

			if ($bulk == 'delete') {
				foreach ($selected as $badge_id) {
					$rs = $badge_model->delete($badge_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('badges', 'A badge has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('badges', 'Badges have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('badges', 'Badges h1');

		$badges = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $badge_model->getBadgesAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$badges = $badge_model->getBadgesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('badges', array(
			'badges' => $badges,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit badge.
	 */
	public function actionBadge($id)
	{
		$badge_model = Badge::model();

		$badge = Yii::app()->request->getPost('badge', null);
		$badge_lang = Yii::app()->request->getPost('badge_lang', null);
		
		if (!empty($badge)) {
			$model = new BadgeForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $badge;
			
			$model_lang = new BadgeLangForm;
			$model_lang->attributes = $badge_lang;
			
			// set image files
			$model->badge_logo = CUploadedFile::getInstanceByName('badge[badge_logo]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $badge_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('badges', 'A badge has been successfully added! success'));
						$this->redirect(array('badges'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('badges', 'A badge has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('badges', 'Add badge h1');
			$this->render('badgeAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get badge by ID
			$badge = $badge_model->getBadgeByIdAdmin($id);

			if (empty($badge)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('badges', 'Edit badge h1') . ' «' . $badge[Yii::app()->params->lang]['badge_name'] . '»';
			
			$this->render('badgeEdit', array(
				'badge' => $badge,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Reclamations list.
	 */
	public function actionReclamations()
	{
		$reclamation_model = Reclamation::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$reclamation_id = Yii::app()->request->getPost('reclamation_id', null);
		
		// bulk operation
		$reclamation = Yii::app()->request->getPost('reclamation', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($reclamation_id) && !empty($action) && in_array($action, array('delete'))) {
			if($action == 'delete') {
				$rs = $reclamation_model->delete($reclamation_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('reclamations', 'A reclamation has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($reclamation) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($reclamation as $reclamation_id => $position) {
				$rs = $reclamation_model->setPosition($reclamation_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
			$counter = 0;

			if ($bulk == 'delete') {
				foreach ($selected as $reclamation_id) {
					$rs = $reclamation_model->delete($reclamation_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('reclamations', 'A reclamation has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('reclamations', 'Reclamations have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('reclamations', 'Reclamations h1');

		$reclamations = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $reclamation_model->getReclamationsAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$reclamations = $reclamation_model->getReclamationsAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('reclamations', array(
			'reclamations' => $reclamations,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit reclamation.
	 */
	public function actionReclamation($id)
	{
		$reclamation_model = Reclamation::model();

		$reclamation = Yii::app()->request->getPost('reclamation', null);
		$reclamation_lang = Yii::app()->request->getPost('reclamation_lang', null);
		
		if (!empty($reclamation)) {
			$model = new ReclamationForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $reclamation;
			
			$model_lang = new ReclamationLangForm;
			$model_lang->attributes = $reclamation_lang;

			if ($model->validate() && $model_lang->validate()) {
				$rs = $reclamation_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('reclamations', 'A reclamation has been successfully added! success'));
						$this->redirect(array('reclamations'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('reclamations', 'A reclamation has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('reclamations', 'Add reclamation h1');
			$this->render('reclamationAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get reclamation by ID
			$reclamation = $reclamation_model->getReclamationByIdAdmin($id);

			if (empty($reclamation)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('reclamations', 'Edit reclamation h1') . ' «' . $reclamation[Yii::app()->params->lang]['reclamation_name'] . '»';
			
			$this->render('reclamationEdit', array(
				'reclamation' => $reclamation,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Cares list.
	 */
	public function actionCares()
	{
		$care_model = Care::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$care_id = Yii::app()->request->getPost('care_id', null);
		
		// bulk operation
		$care = Yii::app()->request->getPost('care', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($care_id) && !empty($action) && in_array($action, array('delete'))) {
			if($action == 'delete') {
				$rs = $care_model->delete($care_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('cares', 'A care has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($care) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($care as $care_id => $position) {
				$rs = $care_model->setPosition($care_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
			$counter = 0;

			if ($bulk == 'delete') {
				foreach ($selected as $care_id) {
					$rs = $care_model->delete($care_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('cares', 'A care has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('cares', 'Cares have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('cares', 'Cares h1');

		$cares = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $care_model->getCaresAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$cares = $care_model->getCaresAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('cares', array(
			'cares' => $cares,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit care.
	 */
	public function actionCare($id)
	{
		$care_model = Care::model();

		$care = Yii::app()->request->getPost('care', null);
		$care_lang = Yii::app()->request->getPost('care_lang', null);
		
		if (!empty($care)) {
			$model = new CareForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $care;
			
			$model_lang = new CareLangForm;
			$model_lang->attributes = $care_lang;

			if ($model->validate() && $model_lang->validate()) {
				$rs = $care_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('cares', 'A care has been successfully added! success'));
						$this->redirect(array('cares'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('cares', 'A care has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('cares', 'Add care h1');
			$this->render('careAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get care by ID
			$care = $care_model->getCareByIdAdmin($id);

			if (empty($care)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('cares', 'Edit care h1') . ' «' . $care[Yii::app()->params->lang]['care_name'] . '»';
			
			$this->render('careEdit', array(
				'care' => $care,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Sizes list.
	 */
	public function actionSizes()
	{
		$size_model = Size::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$size_id = Yii::app()->request->getPost('size_id', null);
		
		// bulk operation
		$size = Yii::app()->request->getPost('size', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($size_id) && !empty($action) && in_array($action, array('delete'))) {
			if($action == 'delete') {
				$rs = $size_model->delete($size_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('sizes', 'A size has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($size) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($size as $size_id => $position) {
				$rs = $size_model->setPosition($size_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
			$counter = 0;

			if ($bulk == 'delete') {
				foreach ($selected as $size_id) {
					$rs = $size_model->delete($size_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('sizes', 'A size has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('sizes', 'Sizes have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('sizes', 'Sizes h1');

		$sizes = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $size_model->getSizesAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$sizes = $size_model->getSizesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('sizes', array(
			'sizes' => $sizes,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit size.
	 */
	public function actionSize($id)
	{
		$size_model = Size::model();

		$size = Yii::app()->request->getPost('size', null);
		$size_lang = Yii::app()->request->getPost('size_lang', null);
		
		if (!empty($size)) {
			$model = new SizeForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $size;
			
			$model_lang = new SizeLangForm;
			$model_lang->attributes = $size_lang;

			if ($model->validate() && $model_lang->validate()) {
				$rs = $size_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('sizes', 'A size has been successfully added! success'));
						$this->redirect(array('sizes'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('sizes', 'A size has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('sizes', 'Add size h1');
			$this->render('sizeAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get size by ID
			$size = $size_model->getSizeByIdAdmin($id);

			if (empty($size)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('sizes', 'Edit size h1') . ' «' . $size[Yii::app()->params->lang]['size_name'] . '»';
			
			$this->render('sizeEdit', array(
				'size' => $size,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
     * Tags list.
     */
    public function actionTags()
    {
        $tag_model = Tag::model();

        // single operation
        $action = Yii::app()->request->getPost('action', null);
        $tag_id = Yii::app()->request->getPost('tag_id', null);

        // bulk operation
        $tag = Yii::app()->request->getPost('tag', null);
        $selected = Yii::app()->request->getPost('selected', null);
        $bulk = Yii::app()->request->getPost('bulkAction', null);

        if (Yii::app()->getUser()->hasAccess($this->route, true)) {
            if (!empty($tag_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
                if ($action == 'block') {
                    $rs = $tag_model->toggle($tag_id, 0);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('tags', 'Tag has been blocked! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    }
                }
                elseif ($action == 'active') {
                    $rs = $tag_model->toggle($tag_id, 1);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('tags', 'Tag has been activated! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    }
                }
                elseif($action == 'delete') {
                    $rs = $tag_model->delete($tag_id);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('tags', 'Tag has been deleted! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
                    }
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
            elseif (!empty($tag) && $bulk == 'save') {
                // save positions
                $counter = 0;

                foreach ($tag as $tag_id => $position) {
                    $rs = $tag_model->setPosition($tag_id, $position);

                    if ($rs) {
                        $counter++;
                    }
                }

                if ($counter) {
                    Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
            elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
                $counter = 0;

                if ($bulk == 'block') {
                    foreach ($selected as $tag_id) {
                        $rs = $tag_model->toggle($tag_id, 0);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                    }
                }
                elseif ($bulk == 'active') {
                    foreach ($selected as $tag_id) {
                        $rs = $tag_model->toggle($tag_id, 1);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                    }
                }
                elseif ($bulk == 'delete') {
                    foreach ($selected as $tag_id) {
                        $rs = $tag_model->delete($tag_id);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter == 1) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('tags', 'Tag has been deleted! success'));
                    }
                    elseif ($counter > 1) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('tags', 'Tags have been deleted! success'));
                    }
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
        }

        $this->pageTitle = Yii::t('tags', 'Tags h1');

        $tags = array();

        $keyword = Yii::app()->request->getQuery('keyword', null);

        $sort = Yii::app()->request->getQuery('sort', 'default');
        $direction = Yii::app()->request->getQuery('direction', 'asc');
        $direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

        $total = $tag_model->getTagsAdminTotal($this->perPage, $keyword);

        $pages = new CPagination($total['total']);
        $pages->setPageSize($this->perPage);

        // set pages params
        $pages->params = array();

        if ($sort != 'default') {
            $pages->params['sort'] = $sort;
            $pages->params['direction'] = $direction;
        }

        if (!empty($keyword)) {
            $pages->params['keyword'] = $keyword;
        }

        if ($total['total']) {
            $offset = $pages->getCurrentPage() * $this->perPage;
            $tags = $tag_model->getTagsAdmin($sort, $direction, $offset, $this->perPage, $keyword);
        }

        $this->render('tags', array(
            'tags' => $tags,
            'total' => $total,
            'pages' => $pages,
            'page' => $pages->getCurrentPage(),
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
        ));
    }

    /**
     * Add or edit tag.
     */
    public function actionTag($id)
    {
        $tag_model = Tag::model();

        $tag = Yii::app()->request->getPost('tag', null);
        $tag_lang = Yii::app()->request->getPost('tag_lang', null);

        if (!empty($tag) && Yii::app()->getUser()->hasAccess($this->route, true)) {
            $model = new TagForm;
            $model->scenario = ($id == 'new') ? 'add' : 'edit';
            $model->attributes = $tag;

            $model_lang = new TagLangForm;
			$model_lang->attributes = $tag_lang;
			
			// set image files
			$model->tag_image = CUploadedFile::getInstanceByName('tag[tag_image]');

            if ($model->validate() && $model_lang->validate()) {
                $rs = $tag_model->save($model, $model_lang);

                if ($rs) {
                    if ($model->scenario == 'add') {
                        // set success flash message and redirect
                        Yii::app()->user->setFlash('success_msg', Yii::t('tags', 'Tag has been successfully added! success'));
                        $this->redirect(array('tags'));
                    }
                    else {
                        // set success flash message and redirect
                        Yii::app()->user->setFlash('success_msg', Yii::t('tags', 'Tag has been successfully saved! success'));
                        $this->redirect(Yii::app()->getRequest()->getRequestUri());
                    }
                }
                else {
                    // set error flash message and redirect
                    Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    $this->redirect(Yii::app()->getRequest()->getRequestUri());
                }
            }
            else {
                $errors = $model->jsonErrors();

                // set error flash message and redirect
                Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
        }

        $sort = Yii::app()->request->getQuery('sort', null);
        $direction = Yii::app()->request->getQuery('direction', 'asc');
        $keyword = Yii::app()->request->getQuery('keyword', null);
        $page = Yii::app()->request->getQuery('page', null);

        if ($id == 'new') {
            $this->pageTitle = Yii::t('tags', 'Add tag h1');
            $this->render('tagAdd', array(
                'sort' => $sort,
                'direction' => $direction,
                'keyword' => $keyword,
                'page' => $page,
            ));
        }
        else {
            // get tag by ID
            $tag = $tag_model->getTagByIdAdmin($id);

            if (empty($tag)) {
                throw new CHttpException(404, 'Page not found');
            }

            $this->pageTitle = Yii::t('tags', 'Edit tag h1') . ' «' . $tag[Yii::app()->params->lang]['tag_name'] . '»';

            $this->render('tagEdit', array(
                'tag' => $tag,
                'sort' => $sort,
                'direction' => $direction,
                'keyword' => $keyword,
                'page' => $page,
            ));
        }
    }

	/**
	 * Brands list.
	 */
	public function actionBrands()
	{
		$brand_model = Brand::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$brand_id = Yii::app()->request->getPost('brand_id', null);
		
		// bulk operation
		$brand = Yii::app()->request->getPost('brand', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($brand_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $brand_model->toggle($brand_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('brands', 'Brand has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $brand_model->toggle($brand_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('brands', 'Brand has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $brand_model->delete($brand_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('brands', 'Brand has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $brand_id) {
					$rs = $brand_model->toggle($brand_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $brand_id) {
					$rs = $brand_model->toggle($brand_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $brand_id) {
					$rs = $brand_model->delete($brand_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('brands', 'Brand has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('brands', 'Brands have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('brands', 'Brands h1');

		$brands = array();
		
		$show_all = Yii::app()->request->getQuery('show_all', 0);
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $brand_model->getBrandsAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($show_all)) {
			$pages->params['show_all'] = $show_all;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			if (!empty($show_all)) {
				$this->perPage = $total['total'];
			}
			
			$offset = $pages->getCurrentPage() * $this->perPage;
			$brands = $brand_model->getBrandsAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('brands', array(
			'brands' => $brands,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'show_all' => $show_all,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit brand.
	 */
	public function actionBrand($id)
	{
		$brand_model = Brand::model();

		$brand = Yii::app()->request->getPost('brand', null);
		$brand_lang = Yii::app()->request->getPost('brand_lang', null);
		
		if (!empty($brand)) {
			$model = new BrandForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $brand;
			
			$model_lang = new BrandLangForm;
			$model_lang->attributes = $brand_lang;

			// set image files
			$model->brand_logo = CUploadedFile::getInstanceByName('brand[brand_logo]');
			$model->brand_photo = CUploadedFile::getInstanceByName('brand[brand_photo]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $brand_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('brands', 'Brand has been successfully added! success'));
						$this->redirect(array('brands'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('brands', 'Brand has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$show_all = Yii::app()->request->getQuery('show_all', null);
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('brands', 'Add brand h1');
			$this->render('brandAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'show_all' => $show_all,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get brand by ID
			$brand = $brand_model->getBrandByIdAdmin($id);

			if (empty($brand)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('brands', 'Edit brand h1') . ' «' . $brand[Yii::app()->params->lang]['brand_name'] . '»';

			$this->render('brandEdit', array(
				'brand' => $brand,
				'sort' => $sort,
				'direction' => $direction,
				'show_all' => $show_all,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}
	
	/**
	 * Collections list.
	 */
	public function actionCollections()
	{
		$collection_model = Collection::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$collection_id = Yii::app()->request->getPost('collection_id', null);
		
		// bulk operation
		$collection = Yii::app()->request->getPost('collection', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($collection_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $collection_model->toggle($collection_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('collections', 'Collection has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $collection_model->toggle($collection_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('collections', 'Collection has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $collection_model->delete($collection_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('collections', 'Collection has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($collection) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($collection as $collection_id => $rating) {
				$rs = $collection_model->setRating($collection_id, $rating);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $collection_id) {
					$rs = $collection_model->toggle($collection_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $collection_id) {
					$rs = $collection_model->toggle($collection_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $collection_id) {
					$rs = $collection_model->delete($collection_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('collections', 'Collection has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('collections', 'Collections have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('collections', 'Collections h1');

		$collections = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $collection_model->getCollectionsAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$collections = $collection_model->getCollectionsAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('collections', array(
			'collections' => $collections,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit collection.
	 */
	public function actionCollection($id)
	{
		$collection_model = Collection::model();

		$collection = Yii::app()->request->getPost('collection', null);
		$collection_lang = Yii::app()->request->getPost('collection_lang', null);
		$del_gallery = Yii::app()->request->getPost('del_gallery', null);
		$gallery_position = Yii::app()->request->getPost('gallery_position', null);
		
		if (!empty($collection)) {
			$model = new CollectionForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $collection;
			
			$model_lang = new CollectionLangForm;
			$model_lang->attributes = $collection_lang;

			// gallery image files
			$gallery_photos = CUploadedFile::getInstancesByName('collection[gallery]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $collection_model->save($model, $model_lang, $del_gallery, $gallery_position, $gallery_photos);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('collections', 'Collection has been successfully added! success'));
						$this->redirect(array('collections'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('collections', 'Collection has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('collections', 'Add collection h1');
			$this->render('collectionAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get collection by ID
			$collection = $collection_model->getCollectionByIdAdmin($id);

			if (empty($collection)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('collections', 'Edit collection h1') . ' «' . $collection[Yii::app()->params->lang]['collection_title'] . '»';
			
			// get gallery photos
			$gallery = $collection_model->getGalleryPhotos($collection['collection_id']);

			$this->render('collectionEdit', array(
				'collection' => $collection,
				'gallery' => $gallery,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Stores list.
	 */
	public function actionStores()
	{
		$store_model = Store::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$store_id = Yii::app()->request->getPost('store_id', null);
		
		// bulk operation
		$store = Yii::app()->request->getPost('store', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($store_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $store_model->toggle($store_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('stores', 'Store has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $store_model->toggle($store_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('stores', 'Store has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $store_model->delete($store_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('stores', 'Store has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($store) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($store as $store_id => $position) {
				$rs = $store_model->setPosition($store_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $store_id) {
					$rs = $store_model->toggle($store_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $store_id) {
					$rs = $store_model->toggle($store_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $store_id) {
					$rs = $store_model->delete($store_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('stores', 'Store has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('stores', 'Stores have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('stores', 'Stores h1');

		$stores = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $store_model->getStoresAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$stores = $store_model->getStoresAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('stores', array(
			'stores' => $stores,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit store.
	 */
	public function actionStore($id)
	{
		$store_model = Store::model();

		$store = Yii::app()->request->getPost('store', null);
		$store_lang = Yii::app()->request->getPost('store_lang', null);
		
		if (!empty($store)) {
			$model = new StoreForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $store;

			$model_lang = new StoreLangForm;
			$model_lang->attributes = $store_lang;

			// set image files
			$model->store_image = CUploadedFile::getInstanceByName('store[store_image]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $store_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('stores', 'Store has been successfully added! success'));
						$this->redirect(array('stores'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('stores', 'Store has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('stores', 'Add store h1');
			$this->render('storeAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get store by ID
			$store = $store_model->getStoreByIdAdmin($id);

			if (empty($store)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('stores', 'Edit store h1') . ' «' . $store[Yii::app()->params->lang]['store_name'] . '»';

			$this->render('storeEdit', array(
				'store' => $store,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Properties list.
	 */
	public function actionProperties()
	{
		$property_model = Property::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$property_id = Yii::app()->request->getPost('property_id', null);
		
		// bulk operation
		$property = Yii::app()->request->getPost('property', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($property_id) && !empty($action) && in_array($action, array('delete'))) {
			if ($action == 'delete') {
				$rs = $property_model->delete($property_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('properties', 'Property has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
			$counter = 0;

			if ($bulk == 'delete') {
				foreach ($selected as $property_id) {
					$rs = $property_model->delete($property_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('properties', 'Property has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('properties', 'Properties have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$category_model = Category::model();
		$categories = $category_model->getCategories();

		$this->pageTitle = Yii::t('properties', 'Properties h1');

		$properties = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $property_model->getPropertiesAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$properties = $property_model->getPropertiesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('properties', array(
			'properties' => $properties,
			'categories' => $categories,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit property.
	 */
	public function actionProperty($id)
	{
		$property_model = Property::model();

		$property = Yii::app()->request->getPost('property', null);
		$property_lang = Yii::app()->request->getPost('property_lang', null);
		
		if (!empty($property)) {
			$model = new PropertyForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $property;

			$model_lang = new PropertyLangForm;
			$model_lang->attributes = $property_lang;

			$categories = Yii::app()->request->getPost('categories', array());
			$filter_categories = Yii::app()->request->getPost('filter_categories', array());

			if ($model->validate() && $model_lang->validate()) {
				$rs = $property_model->save($model, $model_lang, $categories, $filter_categories);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('properties', 'Property has been successfully added! success'));
						$this->redirect(array('properties'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('properties', 'Property has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$category_model = Category::model();
		$categories = $category_model->getCategories();
		$categories_tree = $category_model->getCategoriesTree();

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('properties', 'Add property h1');
			$this->render('propertyAdd', array(
				'categories' => $categories,
				'categories_tree' => $categories_tree,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get property by ID
			$property = $property_model->getPropertyByIdAdmin($id);

			if (empty($property)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('properties', 'Edit property h1') . ' «' . $property[Yii::app()->params->lang]['property_title'] . '»';

			$property_categories = $property_model->getPropertyCategories($id);
			$property_filters = $property_model->getFilterCategories($id);

			$this->render('propertyEdit', array(
				'property' => $property,
				'property_categories' => $property_categories,
				'property_filters' => $property_filters,
				'categories' => $categories,
				'categories_tree' => $categories_tree,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Property values list.
	 */
	public function actionPropertyValues($id)
	{
		$property_model = Property::model();

		// get property by ID
		$property = $property_model->getPropertyByIdAdmin($id);

		if (empty($property)) {
			throw new CHttpException(404, 'Page not found');
		}

		$value_model = PropertyValue::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$value_id = Yii::app()->request->getPost('value_id', null);
		
		// bulk operation
		$property_value = Yii::app()->request->getPost('property_value', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($value_id) && !empty($action) && in_array($action, array('delete'))) {
			if ($action == 'delete') {
				$rs = $value_model->delete($value_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('values', 'Value has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($property_value) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($property_value as $value_id => $position) {
				$rs = $value_model->setPosition($value_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
			$counter = 0;

			if ($bulk == 'delete') {
				foreach ($selected as $value_id) {
					$rs = $value_model->delete($value_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('values', 'Value has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('values', 'Values have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('values', 'Values h1') . ' «' . $property[Yii::app()->params->lang]['property_title'] . '»';

		$values = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $value_model->getValuesAdminTotal($property['property_id'], $this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array(
			'id' => $id,
		);

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$values = $value_model->getValuesAdmin($property['property_id'], $sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('values', array(
			'property' => $property,
			'values' => $values,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit property value.
	 */
	public function actionPropertyValue($property_id, $id)
	{
		$property_model = Property::model();

		// get property by ID
		$property = $property_model->getPropertyByIdAdmin($property_id);

		if (empty($property)) {
			throw new CHttpException(404, 'Page not found');
		}

		$value_model = PropertyValue::model();

		$value = Yii::app()->request->getPost('value', null);
		$value_lang = Yii::app()->request->getPost('value_lang', null);
		
		if (!empty($value)) {
			$model = new PropertyValueForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $value;
			
			$model_lang = new PropertyValueLangForm;
			$model_lang->attributes = $value_lang;

			if ($model->validate() && $model_lang->validate()) {
				$rs = $value_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('values', 'Value has been successfully added! success'));
						$this->redirect(array('propertyvalues', 'id' => $property['property_id']));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('values', 'Value has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('values', 'Add value h1') . ' «' . $property[Yii::app()->params->lang]['property_title'] . '»';
			$this->render('valueAdd', array(
				'property' => $property,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get property value by ID
			$value = $value_model->getValueByIdAdmin($id);

			if (empty($value)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('values', 'Edit value h1') . ' «' . $property[Yii::app()->params->lang]['property_title'] . '»';

			$this->render('valueEdit', array(
				'property' => $property,
				'value' => $value,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Products list.
	 */
	public function actionProducts()
	{
		$product_model = Product::model();

		if (Yii::app()->request->getQuery('make_variants') == 1) {
			$rs = $product_model->makeVariantsFromProperties();

			if ($rs) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			} else {
				Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
			}

			$this->redirect([$this->route]);
		} elseif (Yii::app()->request->getQuery('make_keywords') == 1) {
			$rs = $product_model->makeKeywords();

			if ($rs) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			} else {
				Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
			}

			$this->redirect([$this->route]);
		}
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$product_id = Yii::app()->request->getPost('product_id', null);
		
		// bulk operation
		$product = Yii::app()->request->getPost('product', null);
		$price = Yii::app()->request->getPost('price', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($product_id) && !empty($action) && in_array($action, array('block', 'active', 'copy', 'delete'))) {
			if ($action == 'block') {
				$rs = $product_model->toggle($product_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('products', 'Product has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $product_model->toggle($product_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('products', 'Product has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'copy') {
				$rs = $product_model->copy($product_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('products', 'Product has been copied! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $product_model->delete($product_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('products', 'Product has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($product) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($product as $product_id => $rating) {
				$rs = $product_model->setRating($product_id, $rating);
				$product_model->setPriceOld($product_id, $price[$product_id]);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $product_id) {
					$rs = $product_model->toggle($product_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $product_id) {
					$rs = $product_model->toggle($product_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $product_id) {
					$rs = $product_model->delete($product_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('products', 'Product has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('products', 'Products have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('products', 'Products h1');

		$products = array();
		
		$show_all = Yii::app()->request->getQuery('show_all', 0);
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $product_model->getProductsAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($show_all)) {
			$pages->params['show_all'] = $show_all;
		}
		
		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			if ($show_all) {
				$this->perPage = $total['total'];
			}
			
			$offset = $pages->getCurrentPage() * $this->perPage;
			$products = $product_model->getProductsAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('products', array(
			'products' => $products,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'show_all' => $show_all,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit product.
	 */
	public function actionProduct($id)
	{
		$product_model = Product::model();

		$product = Yii::app()->request->getPost('product', null);
		$product_lang = Yii::app()->request->getPost('product_lang', null);
		$variant = Yii::app()->request->getPost('variant', null);
		$bulk_variants = Yii::app()->request->getPost('bulk_variants', 0);
		$bulk_variant = Yii::app()->request->getPost('bulk_variant', array());
		$option = Yii::app()->request->getPost('option', null);
		$property = Yii::app()->request->getPost('property', null);
		$del_gallery = Yii::app()->request->getPost('del_gallery', null);
		$gallery_position = Yii::app()->request->getPost('gallery_position', null);
		
		if (!empty($product)) {
			$model = new ProductForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $product;
			
			$model_lang = new ProductLangForm;
			$model_lang->attributes = $product_lang;

			// video
			$model->product_video = CUploadedFile::getInstanceByName('product[product_video]');
			
			// gallery image files
			$gallery_photos = CUploadedFile::getInstancesByName('product[gallery]');

			$bulk_variant = empty($bulk_variants) ? array() : $bulk_variant;

			if ($model->validate() && $model_lang->validate()) {
				$rs = $product_model->save($model, $model_lang, $variant, $bulk_variant, $option, $property, $del_gallery, $gallery_position, $gallery_photos);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('products', 'Product has been successfully added! success'));
						$this->redirect(array('products'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('products', 'Product has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		// get tags
		$tags = Tag::model()->getTagsListAdmin();
		
		// get badges
		$badges = Badge::model()->getBadgesListAdmin();
		
		// get sizes
		$sizes = Size::model()->getSizesListAdmin();

		// get cares
		$cares = Care::model()->getCaresListAdmin();
		
		// get reclamations
		$reclamations = Reclamation::model()->getReclamationsListAdmin();

		// get brands
		$brands = Brand::model()->getBrandsListAdmin();
		
		// get collections
		$collections = Collection::model()->getCollectionsListAdmin();
		
		// get categories
		$category_model = Category::model();
		$categories = $category_model->getCategories();
		$categories_tree = $category_model->getCategoriesTree();
		
		// get base categories
		$base_category_model = BaseCategory::model();
		$base_categories = $base_category_model->getCategories();
		$base_categories_tree = $base_category_model->getCategoriesTree();

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$show_all = Yii::app()->request->getQuery('show_all', null);
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('products', 'Add product h1');
			$this->render('productAdd', array(
				'tags' => $tags,
				'badges' => $badges,
				'sizes' => $sizes,
				'cares' => $cares,
				'reclamations' => $reclamations,
				'brands' => $brands,
				'collections' => $collections,
				'categories' => $categories,
				'categories_tree' => $categories_tree,
				'base_categories' => $base_categories,
				'base_categories_tree' => $base_categories_tree,
				'sort' => $sort,
				'direction' => $direction,
				'show_all' => $show_all,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get product by ID
			$product = $product_model->getProductByIdAdmin($id);

			if (empty($product)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('products', 'Edit product h1') . ' «' . $product[Yii::app()->params->lang]['product_title'] . '»';
			
			$gallery = $product_model->getGalleryPhotos($product['product_id']);

			// get properties
			$property_model = Property::model();
			$properties = $property_model->getPropertiesListAdmin($product['category_id']);
			$product_properties = $product_model->getProductProperties($product['product_id']);
			$variants = $product_model->getProductVariants($product['product_id']);
			$variant_properties = $property_model->getPropertiesVariants($product['category_id']);
			$options = $product_model->getProductOptions($product['product_id']);
			$option_properties = $property_model->getPropertiesOptions($product['category_id']);

			$this->render('productEdit', array(
				'product' => $product,
				'gallery' => $gallery,
				'tags' => $tags,
				'badges' => $badges,
				'sizes' => $sizes,
				'cares' => $cares,
				'reclamations' => $reclamations,
				'brands' => $brands,
				'collections' => $collections,
				'categories' => $categories,
				'categories_tree' => $categories_tree,
				'base_categories' => $base_categories,
				'base_categories_tree' => $base_categories_tree,
				'properties' => $properties,
				'variants' => $variants,
				'variant_properties' => $variant_properties,
				'options' => $options,
				'option_properties' => $option_properties,
				'product_properties' => $product_properties,
				'sort' => $sort,
				'direction' => $direction,
				'show_all' => $show_all,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Discounts list.
	 */
	public function actionDiscounts()
	{
		$discount_model = Discount::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$discount_id = Yii::app()->request->getPost('discount_id', null);
		
		// bulk operation
		$discount = Yii::app()->request->getPost('discount', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($discount_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $discount_model->toggle($discount_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('discounts', 'Discount has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $discount_model->toggle($discount_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('discounts', 'Discount has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $discount_model->delete($discount_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('discounts', 'Discount has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $discount_id) {
					$rs = $discount_model->toggle($discount_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $discount_id) {
					$rs = $discount_model->toggle($discount_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $discount_id) {
					$rs = $discount_model->delete($discount_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('discounts', 'Discount has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('discounts', 'Discounts have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('discounts', 'Discounts h1');

		$discounts = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $discount_model->getDiscountsAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$discounts = $discount_model->getDiscountsAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('discounts', array(
			'discounts' => $discounts,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit discount.
	 */
	public function actionDiscount($id)
	{
		$discount_model = Discount::model();

		$discount = Yii::app()->request->getPost('discount', null);
		$discount_lang = Yii::app()->request->getPost('discount_lang', null);
		
		if (!empty($discount)) {
			$model = new DiscountForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $discount;

			if ($model->validate()) {
				$rs = $discount_model->save($model);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('discounts', 'Discount has been successfully added! success'));
						$this->redirect(array('discounts'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('discounts', 'Discount has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('discounts', 'Add discount h1');
			$this->render('discountAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get discount by ID
			$discount = $discount_model->getDiscountByIdAdmin($id);

			if (empty($discount)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('discounts', 'Edit discount h1') . ' «' . $discount['discount_code'] . '»';

			$this->render('discountEdit', array(
				'discount' => $discount,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Orders list.
	 */
	public function actionOrders()
	{
		$order_model = Order::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$order_id = Yii::app()->request->getPost('order_id', null);
		
		// bulk operation
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($order_id) && !empty($action) && in_array($action, array('delete'))) {
			if ($action == 'delete') {
				$rs = $order_model->delete($order_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('orders', 'Order has been deleted! success'));
				} else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
			$counter = 0;

			if ($bulk == 'delete') {
				foreach ($selected as $order_id) {
					$rs = $order_model->delete($order_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('orders', 'Order has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('orders', 'Orders have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('orders', 'Orders h1');

		$orders = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$date_from = Yii::app()->request->getQuery('date_from', null);
		$date_to = Yii::app()->request->getQuery('date_to', null);
		$program = Yii::app()->request->getQuery('program', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $order_model->getOrdersAdminTotal($this->perPage, $keyword, $date_from, $date_to, $program);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}

		if (!empty($date_from)) {
			$pages->params['date_from'] = $date_from;
		}

		if (!empty($date_to)) {
			$pages->params['date_to'] = $date_to;
		}

		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$orders = $order_model->getOrdersAdmin($sort, $direction, $offset, $this->perPage, $keyword, $date_from, $date_to, $program);
		}

		$this->render('orders', array(
			'orders' => $orders,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
			'date_from' => $date_from,
			'date_to' => $date_to,
		));
	}

	/**
	 * Add or edit order.
	 */
	public function actionOrder($id)
	{
		$order_model = Order::model();
		$order = Yii::app()->request->getPost('order', null);
		
		if (!empty($order)) {
			$model = new OrderForm('edit');
			$model->attributes = $order;
			
			if ($model->validate()) {
				$rs = $order_model->save($model);

				if ($rs) {
					// set success flash message and redirect
					Yii::app()->user->setFlash('success_msg', Yii::t('orders', 'Order has been successfully saved! success'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$date_from = Yii::app()->request->getQuery('date_from', null);
		$date_to = Yii::app()->request->getQuery('date_to', null);
		$page = Yii::app()->request->getQuery('page', null);

		// get order by ID
		$order = $order_model->getOrderByIdAdmin($id);

		if (empty($order)) {
			throw new CHttpException(404, 'Page not found');
		}

		$this->pageTitle = Yii::t('orders', 'Edit order h1') . ' #' . $order['order_id'];
		
		if ($order['is_new']) {
			$order_model->toggleNew($order['order_id']);
		}

		$products = $order_model->getOrderProducts($id);

		$this->render('orderEdit', array(
			'order' => $order,
			'products' => $products,
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'page' => $page,
		));
	}

	/**
	 * Courses list.
	 */
	public function actionCourses()
	{
		$course_model = Course::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$course_id = Yii::app()->request->getPost('course_id', null);
		
		// bulk operation
		$course = Yii::app()->request->getPost('course', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($course_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $course_model->toggle($course_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('courses', 'Course has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $course_model->toggle($course_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('courses', 'Course has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $course_model->delete($course_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('courses', 'Course has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($course) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($course as $course_id => $position) {
				$rs = $course_model->setPosition($course_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $course_id) {
					$rs = $course_model->toggle($course_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $course_id) {
					$rs = $course_model->toggle($course_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $course_id) {
					$rs = $course_model->delete($course_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('courses', 'Course has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('courses', 'Courses have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('courses', 'Courses h1');

		$courses = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $course_model->getCoursesAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$courses = $course_model->getCoursesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('courses', array(
			'courses' => $courses,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit course.
	 */
	public function actionCourse($id)
	{
		$course_model = Course::model();

		$course = Yii::app()->request->getPost('course', null);
		$course_lang = Yii::app()->request->getPost('course_lang', null);
		
		if (!empty($course)) {
			$model = new CourseForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $course;

			$model_lang = new CourseLangForm;
			$model_lang->attributes = $course_lang;

			// set image files
			$model->course_image = CUploadedFile::getInstanceByName('course[course_image]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $course_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('courses', 'Course has been successfully added! success'));
						$this->redirect(array('courses'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('courses', 'Course has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		// get authors
		$authors = Author::model()->getAuthorsListAdmin();
		
		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('courses', 'Add course h1');
			$this->render('courseAdd', array(
				'authors' => $authors,
				// 'categories' => $categories,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get course by ID
			$course = $course_model->getCourseByIdAdmin($id);

			if (empty($course)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('courses', 'Edit course h1') . ' «' . $course[Yii::app()->params->lang]['course_name'] . '»';

			$this->render('courseEdit', array(
				'course' => $course,
				'authors' => $authors,
				// 'categories' => $categories,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Authors list.
	 */
	public function actionAuthors()
	{
		$author_model = Author::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$author_id = Yii::app()->request->getPost('author_id', null);
		
		// bulk operation
		$author = Yii::app()->request->getPost('author', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($author_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $author_model->toggle($author_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('authors', 'Author has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $author_model->toggle($author_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('authors', 'Author has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $author_model->delete($author_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('authors', 'Author has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($author) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($author as $author_id => $position) {
				$rs = $author_model->setPosition($author_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $author_id) {
					$rs = $author_model->toggle($author_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $author_id) {
					$rs = $author_model->toggle($author_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $author_id) {
					$rs = $author_model->delete($author_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('authors', 'Author has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('authors', 'Authors have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('authors', 'Authors h1');

		$authors = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $author_model->getAuthorsAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$authors = $author_model->getAuthorsAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('authors', array(
			'authors' => $authors,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit author.
	 */
	public function actionAuthor($id)
	{
		$author_model = Author::model();

		$author = Yii::app()->request->getPost('author', null);
		$author_lang = Yii::app()->request->getPost('author_lang', null);
		
		if (!empty($author)) {
			$model = new AuthorForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $author;

			$model_lang = new AuthorLangForm;
			$model_lang->attributes = $author_lang;

			// set image files
			$model->author_image = CUploadedFile::getInstanceByName('author[author_image]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $author_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('authors', 'Author has been successfully added! success'));
						$this->redirect(array('authors'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('authors', 'Author has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('authors', 'Add author h1');
			$this->render('authorAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get author by ID
			$author = $author_model->getAuthorByIdAdmin($id);

			if (empty($author)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('authors', 'Edit author h1') . ' «' . $author[Yii::app()->params->lang]['author_name'] . '»';

			$this->render('authorEdit', array(
				'author' => $author,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
     * Requests list.
     */
    public function actionRequests()
    {
        $request_model = Request::model();

        // single operation
        $action = Yii::app()->request->getPost('action', null);
        $request_id = Yii::app()->request->getPost('request_id', null);

        // bulk operation
        $selected = Yii::app()->request->getPost('selected', null);
        $bulk = Yii::app()->request->getPost('bulkAction', null);

        if (Yii::app()->getUser()->hasAccess($this->route, true)) {
            if (!empty($request_id) && !empty($action) && in_array($action, array('delete'))) {
                if ($action == 'delete') {
                    $rs = $request_model->delete($request_id);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('requests', 'Request has been deleted! success'));
                    } else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
                    }
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
            elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
                $counter = 0;

                if ($bulk == 'delete') {
                    foreach ($selected as $request_id) {
                        $rs = $request_model->delete($request_id);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter == 1) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('requests', 'Request has been deleted! success'));
                    }
                    elseif ($counter > 1) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('requests', 'Requests have been deleted! success'));
                    }
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
        }

        $this->pageTitle = Yii::t('requests', 'Requests h1');

        $requests = array();

        $keyword = Yii::app()->request->getQuery('keyword', null);
        $date_from = Yii::app()->request->getQuery('date_from', null);
        $date_to = Yii::app()->request->getQuery('date_to', null);

        $sort = Yii::app()->request->getQuery('sort', 'default');
        $direction = Yii::app()->request->getQuery('direction', 'asc');
        $direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

        $total = $request_model->getRequestsAdminTotal($this->perPage, $keyword, $date_from, $date_to);

        $pages = new CPagination($total['total']);
        $pages->setPageSize($this->perPage);

        // set pages params
        $pages->params = array();

        if ($sort != 'default') {
            $pages->params['sort'] = $sort;
            $pages->params['direction'] = $direction;
        }

        if (!empty($keyword)) {
            $pages->params['keyword'] = $keyword;
        }

        if (!empty($date_from)) {
            $pages->params['date_from'] = $date_from;
        }

        if (!empty($date_to)) {
            $pages->params['date_to'] = $date_to;
        }

        if ($total['total']) {
            $offset = $pages->getCurrentPage() * $this->perPage;
            $requests = $request_model->getRequestsAdmin($sort, $direction, $offset, $this->perPage, $keyword, $date_from, $date_to);
        }

        $this->render('requests', array(
            'requests' => $requests,
            'total' => $total,
            'pages' => $pages,
            'page' => $pages->getCurrentPage(),
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ));
    }

    /**
     * Add or edit request.
     */
    public function actionRequest($id)
    {
        $request_model = Request::model();
        $request = Yii::app()->request->getPost('request', null);

        if (!empty($request) && Yii::app()->getUser()->hasAccess($this->route, true)) {
            $model = new RequestForm('edit');
            $model->attributes = $request;

            if ($model->validate()) {
                $rs = $request_model->save($model);

                if ($rs) {
                    // set success flash message and redirect
                    Yii::app()->user->setFlash('success_msg', Yii::t('requests', 'Request has been successfully saved! success'));
                    $this->redirect(Yii::app()->getRequest()->getRequestUri());
                }
                else {
                    // set error flash message and redirect
                    Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    $this->redirect(Yii::app()->getRequest()->getRequestUri());
                }
            }
            else {
                $errors = $model->jsonErrors();

                // set error flash message and redirect
                Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
        }

        $sort = Yii::app()->request->getQuery('sort', null);
        $direction = Yii::app()->request->getQuery('direction', 'asc');
        $keyword = Yii::app()->request->getQuery('keyword', null);
        $date_from = Yii::app()->request->getQuery('date_from', null);
        $date_to = Yii::app()->request->getQuery('date_to', null);
        $page = Yii::app()->request->getQuery('page', null);

        // get request by ID
        $request = $request_model->getRequestByIdAdmin($id);

        if (empty($request)) {
            throw new CHttpException(404, 'Page not found');
        }

        $this->pageTitle = Yii::t('requests', 'Edit request h1') . ' #' . $request['request_id'];

        if ($request['is_new']) {
            $request_model->toggleNew($request['request_id']);
        }

        $this->render('requestEdit', array(
            'request' => $request,
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'page' => $page,
        ));
    }

	/**
     * Categories list.
     */
    public function actionBlogCategories()
    {
        $category_model = BlogCategory::model();

        // single operation
        $action = Yii::app()->request->getPost('action', null);
        $category_id = Yii::app()->request->getPost('category_id', null);

        // bulk operation
        $category = Yii::app()->request->getPost('category', null);
        $selected = Yii::app()->request->getPost('selected', null);
        $bulk = Yii::app()->request->getPost('bulkAction', null);

        // actions for admin only
        if (Yii::app()->getUser()->hasAccess($this->route, true)) {
            if (!empty($category_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
                if ($action == 'block') {
                    $rs = $category_model->toggle($category_id, 0);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been blocked! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    }
                }
                elseif ($action == 'active') {
                    $rs = $category_model->toggle($category_id, 1);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been activated! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    }
                }
                elseif($action == 'delete') {
                    $rs = $category_model->delete($category_id);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been deleted! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
                    }
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
            elseif (!empty($category) && $bulk == 'save') {
                // save positions
                $counter = 0;

                foreach ($category as $category_id => $position) {
                    $rs = $category_model->setPosition($category_id, $position);

                    if ($rs) {
                        $counter++;
                    }
                }

                if ($counter) {
                    Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
            elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
                $counter = 0;

                if ($bulk == 'block') {
                    foreach ($selected as $category_id) {
                        $rs = $category_model->toggle($category_id, 0);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                    }
                }
                elseif ($bulk == 'active') {
                    foreach ($selected as $category_id) {
                        $rs = $category_model->toggle($category_id, 1);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                    }
                }
                elseif ($bulk == 'delete') {
                    foreach ($selected as $category_id) {
                        $rs = $category_model->delete($category_id);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter == 1) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been deleted! success'));
                    }
                    elseif ($counter > 1) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Categories have been deleted! success'));
                    }
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
        }

        $this->pageTitle = Yii::t('categories', 'Blog categories h1');

        $categories = array();

        $keyword = Yii::app()->request->getQuery('keyword', null);

        $sort = Yii::app()->request->getQuery('sort', 'default');
        $direction = Yii::app()->request->getQuery('direction', 'asc');
        $direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

        $total = $category_model->getCategoriesAdminTotal($this->perPage, $keyword);

        $pages = new CPagination($total['total']);
        $pages->setPageSize($this->perPage);

        // set pages params
        $pages->params = array();

        if ($sort != 'default') {
            $pages->params['sort'] = $sort;
            $pages->params['direction'] = $direction;
        }

        if (!empty($keyword)) {
            $pages->params['keyword'] = $keyword;
        }

        if ($total['total']) {
            $offset = $pages->getCurrentPage() * $this->perPage;
            $categories = $category_model->getCategoriesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
        }

        $this->render('blogCategories', array(
            'categories' => $categories,
            'total' => $total,
            'pages' => $pages,
            'page' => $pages->getCurrentPage(),
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
        ));
    }

    /**
     * Add or edit category.
     */
    public function actionBlogCategory($id)
    {
        $category_model = BlogCategory::model();

        $category = Yii::app()->request->getPost('category', null);
        $category_lang = Yii::app()->request->getPost('category_lang', null);

        if (!empty($category) && Yii::app()->getUser()->hasAccess($this->route, true)) {
            $model = new BlogCategoryForm;
            $model->scenario = ($id == 'new') ? 'add' : 'edit';
            $model->attributes = $category;

            $model_lang = new BlogCategoryLangForm;
            $model_lang->attributes = $category_lang;

            // set image files
            $model->category_photo = CUploadedFile::getInstanceByName('category[category_photo]');

            if ($model->validate() && $model_lang->validate()) {
                $rs = $category_model->save($model, $model_lang);

                if ($rs) {
                    if ($model->scenario == 'add') {
                        // set success flash message and redirect
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been successfully added! success'));

                        $this->redirect(array('blogcategories'));
                    }
                    else {
                        // set success flash message and redirect
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been successfully saved! success'));
                        $this->redirect(Yii::app()->getRequest()->getRequestUri());
                    }
                }
                else {
                    // set error flash message and redirect
                    Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    $this->redirect(Yii::app()->getRequest()->getRequestUri());
                }
            }
            else {
                $errors = $model->jsonErrors();

                // set error flash message and redirect
                Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
        }

        $sort = Yii::app()->request->getQuery('sort', null);
        $direction = Yii::app()->request->getQuery('direction', 'asc');
        $keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

        if ($id == 'new') {
            $this->pageTitle = Yii::t('categories', 'Add blog category h1');
            $this->render('blogCategoryAdd', array(
                'sort' => $sort,
                'direction' => $direction,
                'keyword' => $keyword,
                'page' => $page,
            ));
        }
        else {
            $category = $category_model->getCategoryByIdAdmin($id);

            if (empty($category)) {
                throw new CHttpException(404, 'Page not found');
            }

            $this->pageTitle = Yii::t('categories', 'Edit blog category h1') . ' «' . $category[Yii::app()->params->lang]['category_name'] . '»';

            $this->render('blogCategoryEdit', array(
                'category' => $category,
                'sort' => $sort,
                'direction' => $direction,
                'keyword' => $keyword,
                'page' => $page,
            ));
        }
    }

	/**
	 * Blogs list.
	 */
	public function actionBlogs()
	{
		$blog_model = Blog::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$blog_id = Yii::app()->request->getPost('blog_id', null);
		
		// bulk operation
		$blog = Yii::app()->request->getPost('blog', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($blog_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $blog_model->toggle($blog_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('blogs', 'Blog has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $blog_model->toggle($blog_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('blogs', 'Blog has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $blog_model->delete($blog_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('blogs', 'Blog has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $blog_id) {
					$rs = $blog_model->toggle($blog_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $blog_id) {
					$rs = $blog_model->toggle($blog_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $blog_id) {
					$rs = $blog_model->delete($blog_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('blogs', 'Blog has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('blogs', 'Blogs have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('blogs', 'Blogs h1');

		$blogs = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $blog_model->getBlogsAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$blogs = $blog_model->getBlogsAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('blogs', array(
			'blogs' => $blogs,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit blog.
	 */
	public function actionBlog($id)
	{
		$blog_model = Blog::model();

		$blog = Yii::app()->request->getPost('blog', null);
		$blog_lang = Yii::app()->request->getPost('blog_lang', null);
		
		if (!empty($blog)) {
			$model = new BlogForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $blog;
			
			$model_lang = new BlogLangForm;
			$model_lang->attributes = $blog_lang;

			$artists = Yii::app()->request->getPost('artists', array());
			
			// set image files
			$model->blog_photo = CUploadedFile::getInstanceByName('blog[blog_photo]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $blog_model->save($model, $model_lang, $artists);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('blogs', 'Blog has been successfully added! success'));
						$this->redirect(array('blogs'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('blogs', 'Blog has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		$categories = BlogCategory::model()->getCategoriesListAdmin();

		if ($id == 'new') {
			$this->pageTitle = Yii::t('blogs', 'Add blog h1');
			$this->render('blogAdd', array(
				'categories' => $categories,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get blog by ID
			$blog = $blog_model->getBlogByIdAdmin($id);

			if (empty($blog)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('blogs', 'Edit blog h1') . ' «' . $blog[Yii::app()->params->lang]['blog_title'] . '»';
			
			$this->render('blogEdit', array(
				'categories' => $categories,
				'blog' => $blog,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
     * Categories list.
     */
    public function actionBaseCategories()
    {
        $category_model = BaseCategory::model();

        // single operation
        $action = Yii::app()->request->getPost('action', null);
        $category_id = Yii::app()->request->getPost('category_id', null);

        // bulk operation
        $category = Yii::app()->request->getPost('category', null);
        $selected = Yii::app()->request->getPost('selected', null);
        $bulk = Yii::app()->request->getPost('bulkAction', null);

        // actions for admin only
        if (Yii::app()->getUser()->hasAccess($this->route, true)) {
            if (!empty($category_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
                if ($action == 'block') {
                    $rs = $category_model->toggle($category_id, 0);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been blocked! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    }
                }
                elseif ($action == 'active') {
                    $rs = $category_model->toggle($category_id, 1);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been activated! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    }
                }
                elseif($action == 'delete') {
                    $rs = $category_model->delete($category_id);

                    if ($rs) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been deleted! success'));
                    }
                    else {
                        Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
                    }
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
            elseif (!empty($category) && $bulk == 'save') {
                // save positions
                $counter = 0;

                foreach ($category as $category_id => $position) {
                    $rs = $category_model->setPosition($category_id, $position);

                    if ($rs) {
                        $counter++;
                    }
                }

                if ($counter) {
                    Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
            elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
                $counter = 0;

                if ($bulk == 'block') {
                    foreach ($selected as $category_id) {
                        $rs = $category_model->toggle($category_id, 0);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                    }
                }
                elseif ($bulk == 'active') {
                    foreach ($selected as $category_id) {
                        $rs = $category_model->toggle($category_id, 1);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
                    }
                }
                elseif ($bulk == 'delete') {
                    foreach ($selected as $category_id) {
                        $rs = $category_model->delete($category_id);

                        if ($rs) {
                            $counter++;
                        }
                    }

                    if ($counter == 1) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been deleted! success'));
                    }
                    elseif ($counter > 1) {
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Categories have been deleted! success'));
                    }
                }

                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
        }

		$this->pageTitle = Yii::t('categories', 'Base categories h1');
		
		// get parent category id
		$parent_id = Yii::app()->request->getQuery('parent_id', 0);

		$categories = array();
		$category = array();

		if (empty($parent_id)) {
			// root categories
			$all_categories = $category_model->getCategoriesTree();

			if (!empty($all_categories)) {
				foreach ($all_categories as $category_item) {
					$categories[] = $category_item;
				}
			}
		} else {
			// subcategory
			$all_categories = $category_model->getCategories();

			if (!isset($all_categories[$parent_id])) {
				throw new CHttpException(404, 'Category not found');
			}

			$category = $all_categories[$parent_id];

			if (!empty($category['sub'])) {
				foreach ($category['sub'] as $category_item) {
					$categories[] = $category_item;
				}
			}
		}
		
		$this->render('baseCategories', array(
			'parent_id' => $parent_id,
			'category' => $category,
			'categories' => $categories,
			'all_categories' => $all_categories,
		));

		/*
        $categories = array();

        $keyword = Yii::app()->request->getQuery('keyword', null);

        $sort = Yii::app()->request->getQuery('sort', 'default');
        $direction = Yii::app()->request->getQuery('direction', 'asc');
        $direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

        $total = $category_model->getCategoriesAdminTotal($this->perPage, $keyword);

        $pages = new CPagination($total['total']);
        $pages->setPageSize($this->perPage);

        // set pages params
        $pages->params = array();

        if ($sort != 'default') {
            $pages->params['sort'] = $sort;
            $pages->params['direction'] = $direction;
        }

        if (!empty($keyword)) {
            $pages->params['keyword'] = $keyword;
        }

        if ($total['total']) {
            $offset = $pages->getCurrentPage() * $this->perPage;
            $categories = $category_model->getCategoriesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
        }

        $this->render('baseCategories', array(
            'categories' => $categories,
            'total' => $total,
            'pages' => $pages,
            'page' => $pages->getCurrentPage(),
            'sort' => $sort,
            'direction' => $direction,
            'keyword' => $keyword,
		));
		*/
    }

    /**
     * Add or edit category.
     */
    public function actionBaseCategory($id)
    {
        $category_model = BaseCategory::model();

        $category = Yii::app()->request->getPost('category', null);
        $category_lang = Yii::app()->request->getPost('category_lang', null);

        if (!empty($category) && Yii::app()->getUser()->hasAccess($this->route, true)) {
            $model = new BaseCategoryForm;
            $model->scenario = ($id == 'new') ? 'add' : 'edit';
            $model->attributes = $category;

            $model_lang = new BaseCategoryLangForm;
            $model_lang->attributes = $category_lang;

            // set image files
            $model->category_photo = CUploadedFile::getInstanceByName('category[category_photo]');

            if ($model->validate() && $model_lang->validate()) {
                $rs = $category_model->save($model, $model_lang);

                if ($rs) {
                    if ($model->scenario == 'add') {
                        // set success flash message and redirect
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been successfully added! success'));

						if ($model->parent_id) {
							$this->redirect(array('basecategories', 'parent_id' => $model->parent_id));
						} else {
							$this->redirect(array('basecategories'));
						}
                    }
                    else {
                        // set success flash message and redirect
                        Yii::app()->user->setFlash('success_msg', Yii::t('categories', 'Category has been successfully saved! success'));
                        $this->redirect(Yii::app()->getRequest()->getRequestUri());
                    }
                }
                else {
                    // set error flash message and redirect
                    Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
                    $this->redirect(Yii::app()->getRequest()->getRequestUri());
                }
            }
            else {
                $errors = $model->jsonErrors();

                // set error flash message and redirect
                Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
                $this->redirect(Yii::app()->getRequest()->getRequestUri());
            }
		}
		
		$parent_id = Yii::app()->request->getQuery('parent_id', 0);
		$all_categories = $category_model->getCategories();
		$categories_tree = $category_model->getCategoriesTree();

		// get cares
		$cares = Care::model()->getCaresListAdmin();
		
		// get reclamations
		$reclamations = Reclamation::model()->getReclamationsListAdmin();
		
		// get sizes
		$sizes = Size::model()->getSizesListAdmin();

		if ($id == 'new') {
			$this->pageTitle = Yii::t('categories', 'Add base category h1');
			$this->render('baseCategoryAdd', array(
				'parent_id' => $parent_id,
				'categories_tree' => $categories_tree,
				'cares' => $cares,
				'reclamations' => $reclamations,
				'sizes' => $sizes,
			));
		}
		else {
			if (empty($all_categories[$id])) {
				throw new CHttpException(404, 'Page not found');
			}

			$category = $category_model->getCategoryByIdAdmin($id);

			$this->pageTitle = Yii::t('categories', 'Edit base category h1') . ' «' . $category['category_alias'] . '»';
			
			$this->render('baseCategoryEdit', array(
				'category' => $category,
				'parent_id' => $parent_id,
				'categories_tree' => $categories_tree,
				'cares' => $cares,
				'reclamations' => $reclamations,
				'sizes' => $sizes,
			));
		}

		/*
        $sort = Yii::app()->request->getQuery('sort', null);
        $direction = Yii::app()->request->getQuery('direction', 'asc');
        $keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

        if ($id == 'new') {
            $this->pageTitle = Yii::t('categories', 'Add base category h1');
            $this->render('baseCategoryAdd', array(
                'sort' => $sort,
                'direction' => $direction,
                'keyword' => $keyword,
                'page' => $page,
            ));
        }
        else {
            $category = $category_model->getCategoryByIdAdmin($id);

            if (empty($category)) {
                throw new CHttpException(404, 'Page not found');
            }

            $this->pageTitle = Yii::t('categories', 'Edit base category h1') . ' «' . $category[Yii::app()->params->lang]['category_name'] . '»';

            $this->render('baseCategoryEdit', array(
                'category' => $category,
                'sort' => $sort,
                'direction' => $direction,
                'keyword' => $keyword,
                'page' => $page,
            ));
		}
		*/
    }

	/**
	 * Bases list.
	 */
	public function actionBases()
	{
		$base_model = Base::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$base_id = Yii::app()->request->getPost('base_id', null);
		
		// bulk operation
		$base = Yii::app()->request->getPost('base', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($base_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $base_model->toggle($base_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('bases', 'Base has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $base_model->toggle($base_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('bases', 'Base has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $base_model->delete($base_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('bases', 'Base has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $base_id) {
					$rs = $base_model->toggle($base_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $base_id) {
					$rs = $base_model->toggle($base_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $base_id) {
					$rs = $base_model->delete($base_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('bases', 'Base has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('bases', 'Bases have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('bases', 'Bases h1');

		$bases = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $base_model->getBasesAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$bases = $base_model->getBasesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('bases', array(
			'bases' => $bases,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit base.
	 */
	public function actionBase($id)
	{
		$base_model = Base::model();

		$base = Yii::app()->request->getPost('base', null);
		$base_lang = Yii::app()->request->getPost('base_lang', null);
		
		if (!empty($base)) {
			$model = new BaseForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $base;
			
			$model_lang = new BaseLangForm;
			$model_lang->attributes = $base_lang;

			$artists = Yii::app()->request->getPost('artists', array());
			
			// set image files
			$model->base_photo = CUploadedFile::getInstanceByName('base[base_photo]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $base_model->save($model, $model_lang, $artists);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('bases', 'Base has been successfully added! success'));
						$this->redirect(array('bases'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('bases', 'Base has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		$categories = BaseCategory::model()->getCategoriesListAdmin();

		if ($id == 'new') {
			$this->pageTitle = Yii::t('bases', 'Add base h1');
			$this->render('baseAdd', array(
				'categories' => $categories,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get base by ID
			$base = $base_model->getBaseByIdAdmin($id);

			if (empty($base)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('bases', 'Edit base h1') . ' «' . $base[Yii::app()->params->lang]['base_title'] . '»';
			
			$this->render('baseEdit', array(
				'categories' => $categories,
				'base' => $base,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Members list.
	 */
	public function actionMembers()
	{
		$member_model = Member::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$member_id = Yii::app()->request->getPost('member_id', null);
		
		// bulk operation
		$member = Yii::app()->request->getPost('member', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($member_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $member_model->toggle($member_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('members', 'Member has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $member_model->toggle($member_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('members', 'Member has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $member_model->delete($member_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('members', 'Member has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($member) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($member as $member_id => $position) {
				$rs = $member_model->setPosition($member_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $member_id) {
					$rs = $member_model->toggle($member_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $member_id) {
					$rs = $member_model->toggle($member_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $member_id) {
					$rs = $member_model->delete($member_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('members', 'Member has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('members', 'Members have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('members', 'Members h1');

		$members = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $member_model->getMembersAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$members = $member_model->getMembersAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('members', array(
			'members' => $members,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit member.
	 */
	public function actionMember($id)
	{
		$member_model = Member::model();

		$member = Yii::app()->request->getPost('member', null);
		$member_lang = Yii::app()->request->getPost('member_lang', null);
		
		if (!empty($member)) {
			$model = new MemberForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $member;

			$model_lang = new MemberLangForm;
			$model_lang->attributes = $member_lang;

			// set image files
			$model->member_image = CUploadedFile::getInstanceByName('member[member_image]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $member_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('members', 'Member has been successfully added! success'));
						$this->redirect(array('members'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('members', 'Member has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('members', 'Add member h1');
			$this->render('memberAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get member by ID
			$member = $member_model->getMemberByIdAdmin($id);

			if (empty($member)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('members', 'Edit member h1') . ' «' . $member[Yii::app()->params->lang]['member_name'] . '»';

			$this->render('memberEdit', array(
				'member' => $member,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Pages list.
	 */
	public function actionPages()
	{
		$page_model = Page::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$page_id = Yii::app()->request->getPost('page_id', null);
		
		// bulk operation
		$page = Yii::app()->request->getPost('page', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($page_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $page_model->toggle($page_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('pages', 'Page has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $page_model->toggle($page_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('pages', 'Page has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $page_model->delete($page_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('pages', 'Page has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($page) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($page as $page_id => $position) {
				$rs = $page_model->setPosition($page_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $page_id) {
					$rs = $page_model->toggle($page_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $page_id) {
					$rs = $page_model->toggle($page_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $page_id) {
					$rs = $page_model->delete($page_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('pages', 'Page has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('pages', 'Pages have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('pages', 'Pages h1');

		$pages_list = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $page_model->getPagesAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$pages_list = $page_model->getPagesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('pages', array(
			'pages_list' => $pages_list,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit page.
	 */
	public function actionPage($id)
	{
		$page_model = Page::model();

		$page = Yii::app()->request->getPost('page', null);
		$page_lang = Yii::app()->request->getPost('page_lang', null);
		
		if (!empty($page)) {
			$model = new PageForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $page;
			
			$model_lang = new PageLangForm;
			$model_lang->attributes = $page_lang;

			// set image files
			$model->page_photo = CUploadedFile::getInstanceByName('page[page_photo]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $page_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('pages', 'Page has been successfully added! success'));
						$this->redirect(array('pages'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('pages', 'Page has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('pages', 'Add page h1');
			$this->render('pageAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get page by ID
			$page_item = $page_model->getPageByIdAdmin($id);

			if (empty($page_item)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('pages', 'Edit page h1') . ' «' . $page_item[Yii::app()->params->lang]['page_title'] . '»';
			
			$this->render('pageEdit', array(
				'page_item' => $page_item,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Banners list.
	 */
	public function actionBanners()
	{
		$banner_model = Banner::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$banner_id = Yii::app()->request->getPost('banner_id', null);
		
		// bulk operation
		$banner = Yii::app()->request->getPost('banner', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($banner_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $banner_model->toggle($banner_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('banners', 'A banner has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $banner_model->toggle($banner_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('banners', 'A banner has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $banner_model->delete($banner_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('banners', 'A banner has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($banner) && $bulk == 'save') {
			// save positions
			$counter = 0;

			foreach ($banner as $banner_id => $position) {
				$rs = $banner_model->setPosition($banner_id, $position);

				if ($rs) {
					$counter++;
				}
			}

			if ($counter) {
				Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $banner_id) {
					$rs = $banner_model->toggle($banner_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $banner_id) {
					$rs = $banner_model->toggle($banner_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $banner_id) {
					$rs = $banner_model->delete($banner_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('banners', 'A banner has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('banners', 'Banners have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('banners', 'Banners h1');

		$banners = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $banner_model->getBannersAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$banners = $banner_model->getBannersAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}
		
		$this->render('banners', array(
			'banners' => $banners,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit banner.
	 */
	public function actionBanner($id)
	{
		$banner_model = Banner::model();

		$banner = Yii::app()->request->getPost('banner', null);
		$banner_lang = Yii::app()->request->getPost('banner_lang', null);
		
		if (!empty($banner)) {
			$model = new BannerForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $banner;
			
			$model_lang = new BannerLangForm;
			$model_lang->attributes = $banner_lang;
			
			// set image files
			$model->banner_logo = CUploadedFile::getInstanceByName('banner[banner_logo]');

			if ($model->validate() && $model_lang->validate()) {
				$rs = $banner_model->save($model, $model_lang);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('banners', 'A banner has been successfully added! success'));
						$this->redirect(array('banners'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('banners', 'A banner has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$categories = Category::model()->getCategoriesTree();

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('banners', 'Add banner h1');
			$this->render('bannerAdd', array(
				'categories' => $categories,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get banner by ID
			$banner = $banner_model->getBannerByIdAdmin($id);

			if (empty($banner)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('banners', 'Edit banner h1') . ' «' . $banner[Yii::app()->params->lang]['banner_name'] . '»';
			
			$this->render('bannerEdit', array(
				'categories' => $categories,
				'banner' => $banner,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Users list.
	 */
	public function actionUsers()
	{
		$user_model = User::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$user_id = Yii::app()->request->getPost('user_id', null);
		
		// bulk operation
		$user = Yii::app()->request->getPost('user', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($user_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
			if ($action == 'block') {
				$rs = $user_model->toggle($user_id, 0);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('users', 'User has been blocked! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif ($action == 'active') {
				$rs = $user_model->toggle($user_id, 1);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('users', 'User has been activated! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
				}
			}
			elseif($action == 'delete') {
				$rs = $user_model->delete($user_id);

				if ($rs) {
					Yii::app()->user->setFlash('success_msg', Yii::t('users', 'User has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
			$counter = 0;

			if ($bulk == 'block') {
				foreach ($selected as $user_id) {
					$rs = $user_model->toggle($user_id, 0);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'active') {
				foreach ($selected as $user_id) {
					$rs = $user_model->toggle($user_id, 1);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
				}
			}
			elseif ($bulk == 'delete') {
				foreach ($selected as $user_id) {
					$rs = $user_model->delete($user_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('users', 'User has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('users', 'Users have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('users', 'Users h1');

		$users = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $user_model->getUsersAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$users = $user_model->getUsersAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}

		$this->render('users', array(
			'users' => $users,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit user.
	 */
	public function actionUser($id)
	{
		Yii::app()->params->returnUrl = $this->createUrl('users');

		$user_model = User::model();
		$user = Yii::app()->request->getPost('user', null);
		
		if (!empty($user)) {
			$model = new UserForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $user;
			
			if ($model->validate()) {
				$rs = $user_model->save($model);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('users', 'User has been successfully added! success'));
						$this->redirect(array('users'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('users', 'User has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('users', 'Add user h1');
			$this->render('userAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get user by ID
			$user = $user_model->getUserByIdAdmin($id);

			if (empty($user)) {
				throw new CHttpException(404, 'User not found');
			}

			// get user orders
			$orders = Order::model()->getUserOrders($user['user_id']);

			$this->pageTitle = Yii::t('users', 'Edit user h1') . ' «' . $user['user_first_name'] . '»';
			
			$this->render('userEdit', array(
				'user' => $user,
				'orders' => $orders,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Managers list.
	 */
	public function actionManagers()
	{
		$manager_model = Manager::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$manager_id = Yii::app()->request->getPost('manager_id', null);
		
		// bulk operation
		$manager = Yii::app()->request->getPost('manager', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (Yii::app()->getUser()->hasAccess($this->route, true)) {
			if (!empty($manager_id) && !empty($action) && in_array($action, array('block', 'active', 'delete'))) {
				if ($action == 'block') {
					$rs = $manager_model->toggle($manager_id, 0);

					if ($rs) {
						Yii::app()->user->setFlash('success_msg', Yii::t('managers', 'Manager has been blocked! success'));
					}
					else {
						Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					}
				}
				elseif ($action == 'active') {
					$rs = $manager_model->toggle($manager_id, 1);

					if ($rs) {
						Yii::app()->user->setFlash('success_msg', Yii::t('managers', 'Manager has been activated! success'));
					}
					else {
						Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					}
				}
				elseif($action == 'delete') {
					$rs = $manager_model->delete($manager_id);

					if ($rs) {
						Yii::app()->user->setFlash('success_msg', Yii::t('managers', 'Manager has been deleted! success'));
					}
					else {
						Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
					}
				}

				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
			elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('active', 'block', 'delete'))) {
				$counter = 0;

				if ($bulk == 'block') {
					foreach ($selected as $manager_id) {
						$rs = $manager_model->toggle($manager_id, 0);

						if ($rs) {
							$counter++;
						}
					}

					if ($counter) {
						Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
					}
				}
				elseif ($bulk == 'active') {
					foreach ($selected as $manager_id) {
						$rs = $manager_model->toggle($manager_id, 1);

						if ($rs) {
							$counter++;
						}
					}

					if ($counter) {
						Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
					}
				}
				elseif ($bulk == 'delete') {
					foreach ($selected as $manager_id) {
						$rs = $manager_model->delete($manager_id);

						if ($rs) {
							$counter++;
						}
					}

					if ($counter == 1) {
						Yii::app()->user->setFlash('success_msg', Yii::t('managers', 'Manager has been deleted! success'));
					}
					elseif ($counter > 1) {
						Yii::app()->user->setFlash('success_msg', Yii::t('managers', 'Managers have been deleted! success'));
					}
				}

				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$this->pageTitle = Yii::t('managers', 'Managers h1');

		$managers = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $manager_model->getManagersAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$managers = $manager_model->getManagersAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}

		$this->render('managers', array(
			'managers' => $managers,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit manager.
	 */
	public function actionManager($id)
	{
		Yii::app()->params->returnUrl = $this->createUrl('managers');

		$manager_model = Manager::model();
		$manager = Yii::app()->request->getPost('manager', null);
		$roles = Yii::app()->request->getPost('roles', []);
		
		if (!empty($manager) && Yii::app()->getUser()->hasAccess($this->route, true)) {
			$model = new ManagerForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $manager;
			
			if ($model->validate()) {
				$rs = $manager_model->save($model, $roles);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('managers', 'Manager has been successfully added! success'));
						$this->redirect(array('managers'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('managers', 'Manager has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$roles = Role::model()->getRolesListAdmin();

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('managers', 'Add manager h1');
			$this->render('managerAdd', array(
				'roles' => $roles,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get manager by ID
			$manager = $manager_model->getManagerByIdAdmin($id);

			if (empty($manager)) {
				throw new CHttpException(404, 'Manager not found');
			}

			$this->pageTitle = Yii::t('managers', 'Edit manager h1') . ' «' . $manager['manager_first_name'] . '»';
			
			$this->render('managerEdit', array(
				'manager' => $manager,
				'roles' => $roles,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Roles list.
	 */
	public function actionRoles()
	{
		$role_model = Role::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$role_id = Yii::app()->request->getPost('role_id', null);
		
		// bulk operation
		$role = Yii::app()->request->getPost('role', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (Yii::app()->getUser()->hasAccess($this->route, true)) {
			if (!empty($role_id) && !empty($action) && in_array($action, array('delete'))) {
				if($action == 'delete') {
					$rs = $role_model->delete($role_id);

					if ($rs) {
						Yii::app()->user->setFlash('success_msg', Yii::t('roles', 'Role has been deleted! success'));
					}
					else {
						Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
					}
				}

				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
			elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
				$counter = 0;

				if ($bulk == 'delete') {
					foreach ($selected as $role_id) {
						$rs = $role_model->delete($role_id);

						if ($rs) {
							$counter++;
						}
					}

					if ($counter == 1) {
						Yii::app()->user->setFlash('success_msg', Yii::t('roles', 'Role has been deleted! success'));
					}
					elseif ($counter > 1) {
						Yii::app()->user->setFlash('success_msg', Yii::t('roles', 'Roles have been deleted! success'));
					}
				}

				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$this->pageTitle = Yii::t('roles', 'Roles h1');

		$roles = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $role_model->getRolesAdminTotal($this->perPage, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$roles = $role_model->getRolesAdmin($sort, $direction, $offset, $this->perPage, $keyword);
		}

		$this->render('roles', array(
			'roles' => $roles,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
		));
	}

	/**
	 * Add or edit role.
	 */
	public function actionRole($id)
	{
		Yii::app()->params->returnUrl = $this->createUrl('roles');

		$role_model = Role::model();
		$role = Yii::app()->request->getPost('role', null);
		
		if (!empty($role) && Yii::app()->getUser()->hasAccess($this->route, true)) {
			$model = new RoleForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $role;
			
			if ($model->validate()) {
				$rs = $role_model->save($model);

				if ($rs) {
					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('roles', 'Role has been successfully added! success'));
						$this->redirect(array('roles'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('roles', 'Role has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('roles', 'Add role h1');
			$this->render('roleAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
		else {
			// get role by ID
			$role = $role_model->getRoleByIdAdmin($id);

			if (empty($role)) {
				throw new CHttpException(404, 'Role not found');
			}

			$this->pageTitle = Yii::t('roles', 'Edit role h1') . ' «' . $role['role_name'] . '»';
			
			$this->render('roleEdit', array(
				'role' => $role,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'page' => $page,
			));
		}
	}

	/**
	 * Translations list.
	 */
	public function actionTranslations()
	{
		$translation_model = Translation::model();
		
		// single operation
		$action = Yii::app()->request->getPost('action', null);
		$translation_id = Yii::app()->request->getPost('translation_id', null);
		
		// bulk operation
		$translation = Yii::app()->request->getPost('translation', null);
		$selected = Yii::app()->request->getPost('selected', null);
		$bulk = Yii::app()->request->getPost('bulkAction', null);

		if (!empty($translation_id) && !empty($action) && in_array($action, array('delete'))) {
			if ($action == 'delete') {
				$rs = $translation_model->delete($translation_id);

				if ($rs) {
					$translation_model->warmUpCache();

					Yii::app()->user->setFlash('success_msg', Yii::t('translations', 'Translation has been deleted! success'));
				}
				else {
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been deleted'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}
		elseif (!empty($selected) && !empty($bulk) && in_array($bulk, array('delete'))) {
			$counter = 0;

			if ($bulk == 'delete') {
				foreach ($selected as $translation_id) {
					$rs = $translation_model->delete($translation_id);

					if ($rs) {
						$counter++;
					}
				}

				if ($counter) {
					$translation_model->warmUpCache();
				}

				if ($counter == 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('translations', 'Translation has been deleted! success'));
				}
				elseif ($counter > 1) {
					Yii::app()->user->setFlash('success_msg', Yii::t('translations', 'Translations have been deleted! success'));
				}
			}

			$this->redirect(Yii::app()->getRequest()->getRequestUri());
		}

		$this->pageTitle = Yii::t('translations', 'Translations h1');

		$translations = array();
		
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$group = Yii::app()->request->getQuery('group', null);
		
		$sort = Yii::app()->request->getQuery('sort', 'default');
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$direction = ($direction == 'asc' || $direction == 'desc') ? $direction : 'asc';

		$total = $translation_model->getTranslationsAdminTotal($this->perPage, $keyword, $group);

		$pages = new CPagination($total['total']);
		$pages->setPageSize($this->perPage);
		
		// set pages params
		$pages->params = array();

		if ($sort != 'default') {
			$pages->params['sort'] = $sort;
			$pages->params['direction'] = $direction;
		}

		if (!empty($keyword)) {
			$pages->params['keyword'] = $keyword;
		}
		
		if (!empty($group)) {
			$pages->params['group'] = $group;
		}			
		
		if ($total['total']) {
			$offset = $pages->getCurrentPage() * $this->perPage;
			$translations = $translation_model->getTranslationsAdmin($sort, $direction, $offset, $this->perPage, $keyword, $group);
		}

		$groups = $translation_model->getTranslationGroups();
		
		$this->render('translations', array(
			'translations' => $translations,
			'groups' => $groups,
			'total' => $total,
			'pages' => $pages,
			'page' => $pages->getCurrentPage(),
			'sort' => $sort,
			'direction' => $direction,
			'keyword' => $keyword,
			'group' => $group,
		));
	}

	/**
	 * Add or edit translation.
	 */
	public function actionTranslation($id)
	{
		$translation_model = Translation::model();

		$translation = Yii::app()->request->getPost('translation', null);
		$translation_lang = Yii::app()->request->getPost('translation_lang', null);
		
		if (!empty($translation)) {
			$model = new TranslationForm;
			$model->scenario = ($id == 'new') ? 'add' : 'edit';
			$model->attributes = $translation;
			
			$model_lang = new TranslationLangForm;
			$model_lang->attributes = $translation_lang;
			
			if ($model->validate() && $model_lang->validate()) {
				$rs = $translation_model->save($model, $model_lang);

				if ($rs) {
					$translation_model->warmUpCache();

					if ($model->scenario == 'add') {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('translations', 'Translation has been successfully added! success'));
						$this->redirect(array('translations'));
					}
					else {
						// set success flash message and redirect
						Yii::app()->user->setFlash('success_msg', Yii::t('translations', 'Translation has been successfully saved! success'));
						$this->redirect(Yii::app()->getRequest()->getRequestUri());
					}
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$sort = Yii::app()->request->getQuery('sort', null);
		$direction = Yii::app()->request->getQuery('direction', 'asc');
		$keyword = Yii::app()->request->getQuery('keyword', null);
		$group = Yii::app()->request->getQuery('group', null);
		$page = Yii::app()->request->getQuery('page', null);

		if ($id == 'new') {
			$this->pageTitle = Yii::t('translations', 'Add translation h1');
			$this->render('translationAdd', array(
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'group' => $group,
				'page' => $page,
			));
		}
		else {
			// get translation by ID
			$translation = $translation_model->getTranslationByIdAdmin($id);

			if (empty($translation)) {
				throw new CHttpException(404, 'Page not found');
			}

			$this->pageTitle = Yii::t('translations', 'Edit translation h1') . ' «' . $translation['translation_code'] . '»';
			
			$this->render('translationEdit', array(
				'translation' => $translation,
				'sort' => $sort,
				'direction' => $direction,
				'keyword' => $keyword,
				'group' => $group,
				'page' => $page,
			));
		}
	}

	/**
	 * Settings section.
	 */
	public function actionSettings()
	{
		$setting_model = Setting::model();
		$setting = Yii::app()->request->getPost('setting', null);
		
		if (!empty($setting)) {
			$model = new SettingForm;
			$model->attributes = $setting;
			
			if ($model->validate()) {
				$rs = $setting_model->save($model);

				if ($rs) {
					// set success flash message and redirect
					Yii::app()->user->setFlash('success_msg', Yii::t('app', 'Data saved!'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
				else {
					// set error flash message and redirect
					Yii::app()->user->setFlash('error_msg', Yii::t('app', 'The data has not been saved'));
					$this->redirect(Yii::app()->getRequest()->getRequestUri());
				}
			}
			else {
				$errors = $model->jsonErrors();

				// set error flash message and redirect
				Yii::app()->user->setFlash('error_msg', implode("<br>\n", $errors['msg']));
				$this->redirect(Yii::app()->getRequest()->getRequestUri());
			}
		}

		$this->pageTitle = Yii::t('settings', 'Settings h1');
		$settings = $setting_model->getSettings();
		
		$this->render('settings', array('settings' => $settings));
	}

	/**
	 * Displays login page and do user log in to account.
	 */
	public function actionLogin()
	{
		// if logged in - redirect to account
		if (!Yii::app()->user->isGuest) {
			$this->redirect(array('index'));
		}
		
		$this->pageTitle = Yii::t('login', 'Log In h1');
		$this->pageDescription = '';
		$this->pageKeywords = '';
		
		$this->breadcrumbs = array(
			Yii::t('login', 'Log In h1'),
		);
		
		$model = new LoginForm;
		$login = Yii::app()->request->getPost('login', null);

		// collect user input data
		if (!empty($login)) {
			$model->attributes = $login;
			
			if ($model->validate()) {
				$identity = new UserIdentity($model->mail, $model->password);
				
				if ($identity->authenticate()) {
					// 5 days
					$duration = 3600 * 24 * 5;
					
					Yii::app()->user->login($identity, $duration);
					
					$user_name = Yii::app()->user->name;

					/* if ($user_name == 'minime_admin') {
						$this->redirect(array('orders'));
					} elseif ($user_name == 'minime_content') {
						$this->redirect(array('products'));
					} elseif ($user_name == 'minime_translator') {
						$this->redirect(array('translations'));
					} */
					$this->redirect(array('orders'));
				} else {
					$model->addError('password', $identity->errorMessage);
				}
			}
		}
		
		// display the login form
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout(false);
		$this->redirect(array('login'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				$this->json['html'] = $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	protected function afterAction($action)
	{
		if (Yii::app()->request->isAjaxRequest) {
			header('Vary: Accept');
			 
			if (strpos(Yii::app()->request->getAcceptTypes(), 'application/json') !== false) {
				header('Content-type: application/json; charset=utf-8');
			}

			echo json_encode($this->json);
			Yii::app()->end();
		}
	}
}