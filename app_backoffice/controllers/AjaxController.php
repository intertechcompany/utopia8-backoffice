<?php

class AjaxController extends Controller
{
	public $json = array();
	
	protected function beforeAction($action)
	{
		// отключаем web-логи для ajax запросов
		foreach (Yii::app()->log->routes as $route) {
			if ($route instanceof CWebLogRoute || $route instanceof CProfileLogRoute) {
				$route->enabled = false;
			}
		}
		
		return parent::beforeAction($action);
	}
	
	public function init() {        
		parent::init();
		Yii::app()->errorHandler->errorAction = 'ajax/error';
	}
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// block direct access to ajax.php
		throw new CHttpException(403, 'Access denied.');
	}

	public function actionProducts()
	{
		$this->json = array(
			'items' => array(),
			'total' => 0,
		);

		$keyword = Yii::app()->request->getQuery('keyword', null);
		$keyword = trim($keyword);

		if (mb_strlen($keyword, 'utf-8') < 3) {
			return;
		}

		$product_model = Product::model();
		$total = $product_model->getProductsAdminTotal(10, $keyword);

		$pages = new CPagination($total['total']);
		$pages->setPageSize(10);

		if ($total['total']) {
			$offset = $pages->getCurrentPage() * 10;
			$products = $product_model->getProductsAdmin('default', 'desc', $offset, 10, $keyword);

			$this->json = array(
				'items' => $products,
				'total' => $total['total'],
			);
		}
	}

	public function actionProperties()
	{
		$category_id = Yii::app()->request->getPost('category_id');
		$index = Yii::app()->request->getPost('index');

		$properties = Property::model()->getPropertiesListAdmin($category_id);
		$variant_properties = Property::model()->getPropertiesVariants($category_id);
		$option_properties = Property::model()->getPropertiesOptions($category_id);

		$this->json = array(
			'html' => $this->renderPartial('properties', array('properties' => $properties), true),
			'bulk_variants_html' => $this->renderPartial('bulkVariantProperties', array('properties' => $properties), true),
			'variants_html' => $this->renderPartial('variantProperties', array('index' => $index, 'variant_properties' => $variant_properties), true),
			'options_html' => $this->renderPartial('optionProperties', array('properties' => $option_properties), true),
			'total' => count($properties),
		);
	}

	public function actionUploadProductPhoto()
	{
		$product = Yii::app()->request->getPost('product', null);

		if (!empty($product)) {
			$model = new AddProductPhotoForm('photo');
			//$model->attributes = $product;
			$model->product_id = isset($product['product_id']) ? $product['product_id'] : 0;
			$model->photo = CUploadedFile::getInstanceByName('product[gallery]');

			if ($model->validate()) {
				$image_rs = Product::model()->addGalleryPhoto($model);

				if ($image_rs !== false) {
					$this->json = $image_rs;
				} else {
					$this->json = array(
						'error' => true,
						'message' => 'Ошибка! Не удалось загрузить изображение.',
					);
				}
			} else {
				// parse error
				$errors = $model->jsonErrors();

				$this->json = array(
					'error' => true,
					'message' => implode("<br>\n", $errors['msg']),
				);
			}
		} else {
			$this->json = array(
				'error' => true,
				'message' => 'Ошибка! Некорректные данные.',
			);
		}
	}

	public function actionCurrenciesUpdate()
	{
		if (Yii::app()->user->isGuest) {
			$this->json = ['error' => true];
			return;
		}

		$currencies_data = file_get_contents('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=' . date('Ymd') . '&json');
		$currencies_rate = json_decode($currencies_data, true);
		
		if (!empty($currencies_rate)) {
			$currencies = [
				'uah' => 1,
				'usd' => 1,
				'eur' => 1,
			];

			foreach ($currencies_rate as $currency_rate) {
				$code = strtolower($currency_rate['cc']);

				if (isset($currencies[$code])) {
					$currencies[$code] = $currency_rate['rate'];
				}
			}

			// update settings
			Setting::model()->updateCurrenciesRate($currencies);
		} else {
			$this->json = ['error' => true];
		}
	}

	public function actionInstagram()
	{
		$token = Yii::app()->request->getPost('token');
		
		if (empty($token)) {
			$token = Yii::app()->params->settings['instagram_token'];
		}

		if (!empty($token)) {
			list($user_id,) = explode('.', $token);

			$posts = file_get_contents('https://api.instagram.com/v1/users/' . $user_id . '/media/recent/?access_token=' . $token . '&count=10');
			$posts = json_decode($posts, true);

			if (!empty($posts['data'])) {
				$save_data = [];
				
				foreach ($posts['data'] as $post) {
					$save_data[] = [
						'url' => $post['link'],
						'photo' => $post['images']['standard_resolution']['url'],
					];
				}

				Setting::model()->updateInstaFeed($save_data);
			} else {
				$this->json = ['error' => true];
			}
		}
	}

	public function actionTypograph()
	{
		$text = Yii::app()->request->getPost('text', null);
		$this->json['text'] = '';

		if (!empty($text)) {
			// import remote typograf library
			Yii::import('application.vendor.RemoteTypograf.RemoteTypograf');

			$typograf = new RemoteTypograf('UTF-8');
		
			// no entities for utf-8
			$typograf->noEntities();
			// $typograf->htmlEntities();
			
			$typograf->br(false);
			$typograf->p(false);
			$typograf->nobr(3);
			$typograf->quotA('laquo raquo');
			$typograf->quotB('bdquo ldquo');
			
			$text = preg_replace('#<span style="white\-space: nowrap;"[^>]*>([^<]*)</span>#u', '$1', $text);

			$text = $typograf->processText($text);
			$this->json['text'] = preg_replace('#<nobr[^>]*>([^<]*)</nobr>#u', '<span style="white-space: nowrap;">$1</span>', $text);
		}
		else {
			throw new CHttpException(400, 'Bad request!');
		}
	}

	public function actionUploadImage()
	{
		$save_result = false;
		$message = '';

		$image = CUploadedFile::getInstanceByName('file');

		if (!empty($image)) {
			$model = new AddFileForm('photo');
			$model->photo = $image;

			if ($model->validate()) {
				// import URLify library
				Yii::import('application.vendor.URLify.URLify');

				$photo_id = uniqid();
				$photo_name = strtolower(str_replace('-', '_', URLify::filter($model->photo->getName(), 60, 'ru', true)));
				
				$photo_dir = Yii::app()->getAssetManager()->getBasePath() . DS . 'images' . DS . $photo_id . DS;
				$photo_path = $photo_dir . $photo_name;

				$dir_rs = CFileHelper::createDirectory($photo_dir, 0777, true);

				if ($dir_rs) {
					$save_photo_rs = $model->photo->saveAs($photo_path);

					if ($save_photo_rs) {
						$save_result = true;
					}
				}
			}
			else {
				$errors = $model->jsonErrors();
				$message = implode("<br>\n", $errors['msg']);
			}
		}

		if ($save_result) {
			$this->json = array(
				'filelink' => Yii::app()->getAssetManager()->getBaseUrl() . '/images/' . $photo_id . '/' . $photo_name,
			);
		}
		else {
			$this->json = array(
				'error' => true,
				'message' => !empty($message) ? $message : 'Произошла ошибка во время загрузки файла',
			);
		}
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			//echo $error['message'];
			$this->json = array('error' => 'Y', 'errorCode' => $error['message'], 'errorFields' => array());
		}
	}
	
	protected function afterAction($action)
	{
		header('Vary: Accept');
		 
		if (strpos(Yii::app()->request->getAcceptTypes(), 'application/json') !== false) {
			header('Content-type: application/json; charset=utf-8');
		}

		echo json_encode($this->json);
		Yii::app()->end();
	}
}