<?php /* @var $this AjaxController */ ?>
<?php if (!empty($properties)) { ?>
	<?php foreach ($properties as $property) { ?>
	<?php if (!$property['property_variant']) { continue; } ?>
	<div class="form-group">
		<label for="form-bulk_variant_<?=$property['property_id']?>" class="col-md-3 control-label"><?=CHtml::encode($property['property_title'])?>:</label>
		<div class="col-md-9">
			<select id="form-bulk_variant_<?=$property['property_id']?>" class="select2 form-control" name="bulk_variant[<?=$property['property_id']?>][]" multiple data-placeholder="<?=Yii::t('products', 'Select a values...')?>">
				<?php if (!empty($property['values'])) { ?>
				<?php foreach ($property['values'] as $value) { ?>
				<option value="<?=$value['value_id']?>"><?=CHtml::encode($value['value_title'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>
	<?php } ?>
<?php } ?>