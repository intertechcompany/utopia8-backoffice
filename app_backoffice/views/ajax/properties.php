<?php
/* @var $this AjaxController */
?>
<?php if (!empty($properties)) { ?>
<select class="select2 form-control" name="property[]" data-placeholder="---">
	<option value="">---</option>
	<?php foreach ($properties as $property) { ?>
	<optgroup label="<?=CHtml::encode($property['property_title'])?>">
		<?php if (!empty($property['values'])) { ?>
		<?php foreach ($property['values'] as $value) { ?>
		<option value="<?=$value['value_id']?>"><?=CHtml::encode($value['value_title'])?></option>
		<?php } ?>
		<?php } ?>
	</optgroup>
	<?php } ?>
</select>
<?php } ?>