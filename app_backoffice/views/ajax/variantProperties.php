<?php
/* @var $this AjaxController */
?>
<?php if (!empty($variant_properties)) { ?>
<?php foreach ($variant_properties as $property) { ?><div>
	<select class="select2 form-control" name="variant[<?=$index?>][values][]" data-placeholder="<?=CHtml::encode($property['property_title'])?>" data-group="<?=CHtml::encode($property['property_title'])?>">
		<option value=""><?=CHtml::encode($property['property_title'])?></option>

		<?php if (!empty($property['values'])) { ?>
		<?php foreach ($property['values'] as $value) { ?>
		<option value="<?=$value['value_id']?>"><?=CHtml::encode($value['value_title'])?></option>
		<?php } ?>
		<?php } ?>
	</select>
</div><?php } ?>
<?php } ?>