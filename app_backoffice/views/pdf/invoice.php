<?php
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<html>
	<head>
		<title><?=Lang::t('pdf.title.invoice', array('{order_id}' => $order['order_id']))?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			body {
				color: #43484d;
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
			}

			table {
				border-collapse: collapse;
				border-spacing: 0;
			}

			td,
			th {
				padding: 0;
			}

			.invoice-title {
				border-top: 2px solid #43484d;
				padding-top: 50px;
				margin: 20px 0 30px;
				font-size: 24px;
			}

			.product th,
			.product td,
			.product-bottom td {
				padding: 10px;
				font-size: 12px;
				vertical-align: middle;
			}

			.product th,
			.product td,
			.product-bottom td {
				padding-left: 7px;
				padding-right: 7px;
			}

			.product {
				margin-top: 10px;
				margin-bottom: 10px;
			}

			.product th {
				border-bottom: 2px solid #43484d;
			}

			.product td {
				border-bottom: 1px solid #43484d;
			}

			.product a {
				color: #43484d;
				text-decoration: underline;
			}

			.product-bottom td {
				font-size: 14px;
			}
		</style>
	</head>
	<body>
		<div style="padding: 0 20px;">
			<div class="invoice-head">
				<table style="width: 100%;" autosize="1">
					<tr>
						<td><img src="<?=$assetsUrl . '/inorex.svg'?>" width="200" height="46" alt="Inorex"></td>
						<td style="text-align: right; vertical-align: bottom;">
							<?=Yii::app()->params->settings['phone']?><br>
							<?=Yii::app()->params->settings['mail']?>
						</td>
					</tr>
				</table>
			</div>

			<div class="invoice-title"><?=Lang::t('pdf.title.invoice', array('{order_id}' => $order['order_id']))?></div>

			<div class="product">
				<table style="width: 100%;" autosize="1">
					<thead>
						<tr>
							<th style="width: 16%">&nbsp;</th>
							<th style="width: 35%; text-align: left"><?=Lang::t('pdf.tip.thProduct')?></th>
							<th style="width: 14%"><?=Lang::t('pdf.tip.thPrice')?></th>
							<th style="width: 14%"><?=Lang::t('pdf.tip.thQuantity')?></th>
							<th style="width: 21%"><?=Lang::t('pdf.tip.thAmount')?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($products as $product) { ?>
						<?php
							$product_image = '';

							if (!empty($product['product_alias'])) {
								$product_url = $this->createAbsoluteUrl('site/product', array('alias' => $product['product_alias']));

								if (!empty($product['product_photo'])) {
									$product_image = json_decode($product['product_photo'], true);
									$product_image_size = $product_image['size']['catalog'];
									$product_image = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog'];
									$product_image = '<a href="' . $product_url . '"><img src="' . $product_image . '" width="' . $product_image_size['w'] . '" height="' . $product_image_size['h'] . '" style="max-width: 70px; max-height: 70px; width: auto; height: auto;"></a>';
								}

								$variant_options = '';

								if (!empty($product['variant'])) {
									$product['product_sku'] = $product['variant']['variant_sku'];

									if (!empty($product['variant']['values'])) {
										$variant_values = array();

										foreach ($product['variant']['values'] as $value) {
											$variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
										}
									
										$variant_options = '<br><span style="font-size: 10px;">' . implode("<br>\n", $variant_values) . '</span>';
									}
								}
			
								$product_title = '<a href="' . $product_url . '">' . CHtml::encode($product['product_title']) . '</a><br><span style="font-size: 10px; color: #aaa">' . Lang::t('accountOrder.tip.productCode') . ' ' . $product['product_sku'] . '</span>' . $variant_options;
							} else {
								$product_title = CHtml::encode($product['title']);

								if (!empty($product['variant_title'])) {
									$product_title .= '<br><span style="color: #aaa">' . str_replace("\n", "<br>\n", CHtml::encode($product['variant_title'])) . '</span>';
								}
							}
						?>
						<tr>
							<td style="text-align: center;"><?=$product_image?></td>
							<td style="text-align: left;"><?=$product_title?></td>
							<td style="text-align: center;">€<?=$product['price']?></td>
							<td style="text-align: center;"><?=$product['quantity']?></td>
							<td style="text-align: center;">€<?=number_format($product['price'] * $product['quantity'], 2, '.', '')?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="product-bottom">
				<table style="width: 100%;" autosize="1">
					<tr>
						<td style="width: 79%; text-align: right;"><?=Lang::t('accountOrder.title.totalAmount')?></td>
						<td style="width: 21%; text-align: center;"><b>€<?=$order['price']?></b></td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>