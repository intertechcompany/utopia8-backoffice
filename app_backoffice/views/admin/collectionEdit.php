<?php
/* @var $this AdminController */
?>
<?php
	$collections_url_data = array();

	if (!empty($sort)) {
		$collections_url_data['sort'] = $sort;
		$collections_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$collections_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$collections_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('collections', $collections_url_data);

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/collection';
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-collection" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="collection[collection_id]" value="<?=$collection['collection_id']?>">
		
	<div class="page-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>
	
	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label">ID:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$collection['collection_id']?></p>
		</div>
	</div>
		
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-3">
			<select id="form-active" name="collection[active]" class="form-control">
				<option value="0"<?php if ($collection['active'] == 0) { ?> selected<?php } ?>><?=Yii::t('collections', 'Blocked')?></option>
				<option value="1"<?php if ($collection['active'] == 1) { ?> selected<?php } ?>><?=Yii::t('collections', 'Active')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-collection_alias" class="col-md-3 control-label"><?=Yii::t('app', 'Alias (URL code)')?>:</label>
		<div class="col-md-6">
			<div class="input-group">
				<span class="input-group-addon">/collection/</span>
				<input id="form-collection_alias" class="form-control" type="text" name="collection[collection_alias]" value="<?=CHtml::encode($collection['collection_alias'])?>">
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-collection_rating" class="col-md-3 control-label"><?=Yii::t('collections', 'Rating')?>:</label>
		<div class="col-md-1">
			<input id="form-collection_rating" class="form-control" type="text" name="collection[collection_rating]" value="<?=CHtml::encode($collection['collection_rating'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-collection_title" class="col-md-3 control-label"><?=Yii::t('collections', 'Collection title')?>:</label>
		<div class="col-md-6 control-required">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-collection_title_<?=$code?>" aria-controls="form-collection_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-collection_title_<?=$code?>">
						<input class="form-control" type="text" name="collection_lang[collection_title][<?=$code?>]" value="<?=CHtml::encode($collection[$code]['collection_title'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<?php /* <div class="form-group">
		<label for="form-collection_country" class="col-md-3 control-label"><?=Yii::t('collections', 'Country/Manufacturer')?>:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-collection_country_<?=$code?>" aria-controls="form-collection_country_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-collection_country_<?=$code?>">
						<input class="form-control" type="text" name="collection_lang[collection_country][<?=$code?>]" value="<?=CHtml::encode($collection[$code]['collection_country'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-collection_description_full" class="col-md-3 control-label"><?=Yii::t('collections', 'Show full description')?>:</label>
		<div class="col-md-3">
			<div class="checkbox">
				<label for="form-collection_description_full">
					<input id="form-collection_description_full" type="checkbox" name="collection[collection_description_full]" value="1"<?php if ($collection['collection_description_full']) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>  */ ?>

	<div class="form-group">
		<label for="form-collection_description" class="col-md-3 control-label"><?=Yii::t('collections', 'Description')?>:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-collection_description_<?=$code?>" aria-controls="form-collection_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-collection_description_<?=$code?>">
						<textarea class="form-control form-redactor" name="collection_lang[collection_description][<?=$code?>]" rows="6"><?=CHtml::encode($collection[$code]['collection_description'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<style>
		.photos-list {
			margin-bottom: 20px;
		}

		.photos-list > li {
			width: 100px;
			text-align: center;
		}

		.photos-list img {
			width: auto;
			max-height: 100%;
			vertical-align: middle;
		}

		.pl-thumb {
			height: 78px;
			line-height: 78px;
		}

		.photos-list .pl-placeholder {
			width: 88px;
			height: 78px;
		}

		.pl-placeholder:before {
			height: 78px;
		}

		.pl-thumb-del {
			position: relative;
		}

		.pl-thumb-el {
			display: inline-block;
			vertical-align: middle;
		}
		
		.pl-thumb-el.checkbox {
			padding-top: 3px;
		}

		.pl-thumb-title {
			position: absolute;
			border: 1px solid #ccc;
			background-color: #fff;
			width: 300px;
			padding: 10px;
			margin-top: 8px;
			z-index: 2;
			border-radius: 6px;
		}

		.pl-thumb-title.hidden {
			display: none;
		}

		.pl-thumb-title:before,
		.pl-thumb-title:after {
			content: '';
			display: block;
			position: absolute;
			border-style: solid;
			border-width: 0 8px 10px 8px;
			border-color: transparent transparent #ccc transparent;
			width: 0;
			height: 0;
			left: 16px;
			top: -10px;
		}

		.pl-thumb-title:after {
			border-bottom-color: #fff;
			top: -9px;
		}

		.pl-thumb-title textarea {
			resize: none;
		}
	</style>

	<div class="page-header">
		<h3><?=Yii::t('app', 'Gallery')?></h3>
	</div>

	<ul id="photos-list" class="photos-list list-inline">
		<?php if (!empty($gallery)) { ?>
		<?php 
			foreach ($gallery as $photo) {
				$photo_path = json_decode($photo['photo_path'], true);
				$photo_size = json_decode($photo['photo_size'], true);
		 ?><li>
			<div class="pl-thumb">
				<input type="hidden" name="gallery_position[<?=$photo['photo_id']?>]" value="<?=$photo['position']?>">
				<img src="<?=Yii::app()->assetManager->getBaseUrl() . '/collection/' . $collection['collection_id'] . '/' . $photo_path['thumb']?>" alt="" width="<?=$photo_size['thumb']['w']?>" height="<?=$photo_size['thumb']['h']?>">
			</div>
			<div class="pl-thumb-del text-center">
				<div class="pl-thumb-el checkbox">
					<label for="form-del-photo-<?=$photo['photo_id']?>">
						<input id="form-del-photo-<?=$photo['photo_id']?>" type="checkbox" name="del_gallery[]" value="<?=$photo['photo_id']?>"> <small>del</small>
					</label>
				</div>
			</div>
		</li><?php } ?>
		<?php } ?>
	</ul>

	<div class="form-group">
		<label for="form-gallery" class="col-md-3 control-label"><?=Yii::t('app', 'Upload photos')?>:</label>
		<div class="col-md-8">
			<input id="form-gallery" type="file" name="collection[gallery][]" multiple>
			<small><?=Yii::t('app', 'Images file requirements', array('{types}' => 'jpg, gif, png', '{size}' => '10 MB', '{total}' => 10))?></small>
		</div>
	</div>

	<div id="section-seo" class="page-header">
		<h3><?=Yii::t('app', 'SEO data')?></h3>
	</div>

	<div class="form-group">
		<label for="form-collection_meta_title" class="col-md-3 control-label"><?=Yii::t('app', 'No index')?>:</label>
		<div class="col-md-6">
			<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
			<div class="checkbox">
				<label for="form-collection_no_index<?=$code?>">
					<input id="form-collection_no_index<?=$code?>" type="checkbox" name="collection_lang[collection_no_index][<?=$code?>]" value="1"<?php if ($collection[$code]['collection_no_index'] == 1) { ?> checked<?php } ?>> <?=$lang?>
				</label>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="form-group">
		<label for="form-collection_meta_title" class="col-md-3 control-label">Meta title:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-collection_meta_title_<?=$code?>" aria-controls="form-collection_meta_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-collection_meta_title_<?=$code?>">
						<input class="form-control" type="text" name="collection_lang[collection_meta_title][<?=$code?>]" value="<?=CHtml::encode($collection[$code]['collection_meta_title'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-collection_meta_keywords" class="col-md-3 control-label">Meta keywords:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-collection_meta_keywords_<?=$code?>" aria-controls="form-collection_meta_keywords_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-collection_meta_keywords_<?=$code?>">
						<textarea class="form-control" name="collection_lang[collection_meta_keywords][<?=$code?>]" rows="2"><?=CHtml::encode($collection[$code]['collection_meta_keywords'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-collection_meta_description" class="col-md-3 control-label">Meta description:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-collection_meta_description_<?=$code?>" aria-controls="form-collection_meta_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-collection_meta_description_<?=$code?>">
						<textarea class="form-control" name="collection_lang[collection_meta_description][<?=$code?>]" rows="4"><?=CHtml::encode($collection[$code]['collection_meta_description'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<div class="btn-group-vertical btn-insert-link" role="group" aria-label="<?=Yii::t('app', 'Insert/remove a link label')?>">
  <button id="typograph-link-btn" title="<?=Yii::t('app', 'Typography text btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-text-size"></i></button>
  <button id="insert-link-btn" title="<?=Yii::t('app', 'Insert a link btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
  <button id="remove-link-btn" title="<?=Yii::t('app', 'Remove all links btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
</div>

<!-- Insert Link Modal -->
<div class="modal fade" id="insertLinkModal" tabindex="-1" role="dialog" aria-labelledby="insertLinkModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="<?=Yii::t('app', 'Close')?>"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="insertLinkModalLabel"><?=Yii::t('app', 'Insert a link label')?></h4>
			</div>
			<div class="modal-body">
			<form id="insert-link">
				<div class="form-group">
					<label for="link-text" class="control-label"><?=Yii::t('app', 'Link text')?>:</label>
					<input type="text" class="form-control" id="link-text">
				</div>
				<div class="form-group">
					<label for="link-url" class="control-label"><?=Yii::t('app', 'Link tip')?>:</label>
					<input type="text" class="form-control" id="link-url">
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label for="link-blank">
							<input id="link-blank" type="checkbox" value="1"> <?=Yii::t('app', 'Open in new tab')?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="link-title" class="control-label"><?=Yii::t('app', 'Link tip (title)')?>:</label>
					<input type="text" class="form-control" id="link-title">
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=Yii::t('app', 'Cancel btn')?></button>
				<button id="do-insert" type="button" class="btn btn-primary"><?=Yii::t('app', 'Insert btn')?></button>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var form = $("#manage-collection"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	$('#typograph-link-btn').on('click', function(e) {
		var input = focused,
			text = input.val(),
			btn = $(this);

		if (focused != null) {
			btn.prop('disabled', true);
			
			$.ajax({type: "POST", url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/typograph', data: $.param({text: focused.val()}), cache: false, dataType: "json",
				success: function(data) {
					btn.prop('disabled', false);
					
					if (data.error == 'Y') {
						bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					}
					else {
						if (focused.hasClass('form-redactor')) {
							$('#' + focused[0].id).redactor('code.set', data.text);
						}
						else {
							focused.val(data.text);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					btn.prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});
	
	$('#insert-link-btn').on('click', function(e) {
		var input = focused,
			len = input.val().length,
			start = input[0].selectionStart,
			end = input[0].selectionEnd,
			selectedText = input.val().substring(start, end);

		$('#link-text').val(selectedText);
		$('#link-url').val('');
		$('#link-title').val('');
		$('#link-blank').prop('checked', false);

		if (focused != null) {
			$('#insertLinkModal').modal('show');
		}
	});

	$('#remove-link-btn').on('click', function(e) {
		if (focused != null) {
			var input = focused,
				new_value = input.val().replace(/<a[^>]*>([\s\S]*?)<\/a>/ig, '$1');

			input.val(new_value);
		}
	});

	$('#insert-link').submit(function() {
		var link_url = $.trim($('#link-url').val()),
			link_text = $.trim($('#link-text').val()),
			link_title = $.trim($('#link-title').val()),
			link_blank = $('#link-blank').prop('checked');

		if (link_url == '' || link_text == '') {
			$('#insertLinkModal').modal('hide');
			return false;
		}

		var new_link = $('<a href="' + link_url + '"/>');
		new_link.text(link_text);

		if (link_title != '') {
			new_link.attr('title', link_title);
		}

		if (link_blank) {
			new_link.attr('target', '_blank');
		}

		var len = focused.val().length,
			start = focused[0].selectionStart,
			end = focused[0].selectionEnd,
			selectedText = focused.val().substring(start, end);

		focused.val(focused.val().substring(0, start) + new_link[0].outerHTML + focused.val().substring(end, len));
		focused = null;

		$('#insertLinkModal').modal('hide');

		return false;
	});

	$('#do-insert').click(function() {
		$('#insert-link').submit();

		return false;
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				// error for lang fields
				if ($(this).parent().hasClass('tab-pane')) {
					var that = this,
						tab = $(this).parent().parent().prev().children(':eq(' + $(this).parent().index() + ')').children(),
						alert_callback = function() {
							if (that.id == 'form-collection_description') {
								setTimeout(function() {
									tab.click();
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									tab.click();
									$(that).focus();
								}, 21);
							}
						};

					if ($(this).parent()[0].id.indexOf('form-collection_title') === 0) {
						bootbox.alert("<?=Yii::t('collections', 'Enter a collection title!')?>", alert_callback);
					}
				}
				else {
					var that = this,
						alert_callback = function() {
							if (that.id == 'form-redactor') {
								setTimeout(function() {
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									$(that).focus();
								}, 21);
							}
						};

					switch (this.id) {
						case 'form-collection_title':
							bootbox.alert("<?=Yii::t('collections', 'Enter a collection title!')?>", alert_callback);
							break;
					}
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});

	var sortable_config = {
		handle: '.pl-thumb',
		items: '> li',
		placeholder: 'pl-placeholder',
		cursor: 'move',
		zIndex: 1019,
		opacity: .7,
		update: function(event, ui) {
			$(this).find('li').each(function(index) {
				$(this).find('input[type="hidden"]').val(index + 1);
			});
		}
	};

	$('#photos-list').sortable(sortable_config).find('.pl-thumb-el.btn').click(function(e) {
		e.preventDefault();

		$(this).next().next().toggleClass('hidden').parent().parent().siblings().find('.pl-thumb-title').addClass('hidden');
	});
});
</script>