<?php
/* @var $this AdminController */
?>
<h1>Settings <!-- Настройки --></h1>

<form id="manage-settings" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="setting[setting_id]" value="<?=$settings['setting_id']?>">
		
	<div class="page-header">
		<h3><?=Yii::t('settings', 'Common settings')?></h3>
	</div>

	<div class="form-group">
		<label for="form-setting_rev" class="col-md-3 control-label"><?=Yii::t('settings', 'Assets version (for developers)')?>:</label>
		<div class="col-md-2 control-required">
			<input id="form-setting_rev" class="form-control" type="text" name="setting[setting_rev]" value="<?=CHtml::encode($settings['setting_rev'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_notify_mail" class="col-md-3 control-label"><?=Yii::t('settings', 'Notify emails')?>:</label>
		<div class="col-md-6">
			<textarea id="form-setting_notify_mail" class="form-control" name="setting[setting_notify_mail]" rows="2"><?=CHtml::encode($settings['setting_notify_mail'])?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_phone" class="col-md-3 control-label"><?=Yii::t('settings', 'Phone')?>:</label>
		<div class="col-md-6">
			<input id="form-setting_phone" class="form-control" type="text" name="setting[setting_phone]" value="<?=CHtml::encode($settings['setting_phone'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_mail" class="col-md-3 control-label">Email:</label>
		<div class="col-md-6">
			<input id="form-setting_mail" class="form-control" type="text" name="setting[setting_mail]" value="<?=CHtml::encode($settings['setting_mail'])?>">
		</div>
	</div>

	<div class="page-header">
		<h3>Каталог</h3>
	</div>

	<div class="form-group">
		<label for="form-setting_newest" class="col-md-3 control-label">Новинки за:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-setting_newest" class="form-control" type="text" name="setting[setting_newest]" value="<?=CHtml::encode($settings['setting_newest'])?>">
				<span class="input-group-addon">дней</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_quick_buy" class="col-md-3 control-label">Быстрая покупка:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-setting_quick_buy">
					<input id="form-setting_quick_buy" type="checkbox" name="setting[setting_quick_buy]" value="1"<?php if ($settings['setting_quick_buy'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_stock" class="col-md-3 control-label">Учет остатков:</label>
		<div class="col-md-6">
			<div class="radio">
				<label for="form-setting_stock-1">
					<input id="form-setting_stock-1" type="radio" name="setting[setting_stock]" value="none"<?php if ($settings['setting_stock'] == 'none') { ?> checked<?php } ?>>
					не учитывать
				</label>
			</div>
			<div class="radio">
				<label for="form-setting_stock-2">
					<input id="form-setting_stock-2" type="radio" name="setting[setting_stock]" value="order"<?php if ($settings['setting_stock'] == 'order') { ?> checked<?php } ?>>
					вычитать после заказа
				</label>
			</div>
			<div class="radio">
				<label for="form-setting_stock-3">
					<input id="form-setting_stock-3" type="radio" name="setting[setting_stock]" value="payment"<?php if ($settings['setting_stock'] == 'payment') { ?> checked<?php } ?>>
					вычитать после оплаты
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_min_qty" class="col-md-3 control-label">Предупреждать о мин. количестве в корзине:</label>
		<div class="col-md-2">
			<input id="form-setting_min_qty" class="form-control" type="text" name="setting[setting_min_qty]" value="<?=CHtml::encode($settings['setting_min_qty'])?>">
		</div>
	</div>

	<!-- <div class="form-group">
		<label for="form-setting_last_size" class="col-md-3 control-label">Добавлять в Last size:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-setting_last_size">
					<input id="form-setting_last_size" type="checkbox" name="setting[setting_last_size]" value="1"<?php if ($settings['setting_last_size'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_last_size_cid" class="col-md-3 control-label">ID категории Last size:</label>
		<div class="col-md-2">
			<input id="form-setting_last_size_cid" class="form-control" type="text" name="setting[setting_last_size_cid]" value="<?=CHtml::encode($settings['setting_last_size_cid'])?>">
		</div>
	</div> -->

	<div class="page-header">
		<h3>Оплата и доставка</h3>
	</div>

	<div class="form-group">
		<label for="form-setting_max_order_amount" class="col-md-3 control-label">Максимальная сумма для оплаты онлайн:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-setting_max_order_amount" class="form-control" type="text" name="setting[setting_max_order_amount]" value="<?=CHtml::encode($settings['setting_max_order_amount'])?>">
				<span class="input-group-addon">грн.</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_courier" class="col-md-3 control-label">Курьер:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-setting_courier" class="form-control" type="text" name="setting[setting_courier]" value="<?=CHtml::encode($settings['setting_courier'])?>">
				<span class="input-group-addon">грн.</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_np_address" class="col-md-3 control-label">НП адресная:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-setting_np_address" class="form-control" type="text" name="setting[setting_np_address]" value="<?=CHtml::encode($settings['setting_np_address'])?>">
				<span class="input-group-addon">грн.</span>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-setting_np_department" class="col-md-3 control-label">НП отделение:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-setting_np_department" class="form-control" type="text" name="setting[setting_np_department]" value="<?=CHtml::encode($settings['setting_np_department'])?>">
				<span class="input-group-addon">грн.</span>
			</div>
		</div>
	</div>
	
	<!-- <div class="form-group">
		<label for="form-setting_delivery_world" class="col-md-3 control-label">Доставка мир:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-setting_delivery_world" class="form-control" type="text" name="setting[setting_delivery_world]" value="<?=CHtml::encode($settings['setting_delivery_world'])?>">
				<span class="input-group-addon">&euro;</span>
			</div>
		</div>
	</div> -->

	<!-- <div class="page-header">
		<h3><?=Yii::t('settings', 'Currency rates')?></h3>
	</div>

	<?php
		$currency_codes = [
			'uah' => [
				'name' => 'Гривна',
				'prefix' => '',
				'postfix' => 'грн.',
			],
			'usd' => [
				'name' => 'US Dollar',
				'prefix' => '$',
				'postfix' => '',
			],
			'eur' => [
				'name' => 'Euro',
				'prefix' => '',
				'postfix' => '€',
			],
		];
		
		if (!empty($settings['setting_currency'])) {
			$currencies = json_decode($settings['setting_currency'], true);

			if (!isset($currencies['margin'])) {
				$currencies['margin'] = 0;
			}
		} else {
			$currencies = [
				'uah' => 1,
				'usd' => 1,
				'eur' => 1,
				'margin' => 0,
			];
		}
	?>

	<?php foreach ($currencies as $currency_code => $currency_rate) { ?>
	<?php if ($currency_code == 'margin') { continue; } ?>
	<div class="form-group">
		<label for="form-setting_currency_<?=$currency_code?>" class="col-md-3 control-label"><?=CHtml::encode($currency_codes[$currency_code]['name'])?>:</label>
		<div class="col-md-2">
			<input id="form-setting_currency_<?=$currency_code?>" class="form-control" type="text" name="setting[setting_currency][<?=$currency_code?>]" value="<?=CHtml::encode($currency_rate)?>">
		</div>
	</div>
	<?php } ?>

	<div class="form-group">
		<div class="col-md-6 col-md-offset-3">
			<button id="currency-update" type="button" class="btn btn-default">Обновить курсы валют</button>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_currency_margin" class="col-md-3 control-label">Наценка курса гривны:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-setting_currency_margin" class="form-control" type="text" name="setting[setting_currency][margin]" value="<?=CHtml::encode($currencies['margin'])?>">
				<span class="input-group-addon">%</span>
			</div>
		</div>
	</div> -->

	<!-- <div class="form-group">
		<label for="form-setting_currency_usd" class="col-md-3 control-label">USD:</label>
		<div class="col-md-6">
			<input id="form-setting_currency_usd" class="form-control" type="text" name="setting[setting_currency_usd]" value="<?=CHtml::encode($settings['setting_currency_usd'])?>">
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-setting_currency_eur" class="col-md-3 control-label">EUR:</label>
		<div class="col-md-6">
			<input id="form-setting_currency_eur" class="form-control" type="text" name="setting[setting_currency_eur]" value="<?=CHtml::encode($settings['setting_currency_eur'])?>">
		</div>
	</div> -->
	
	<div class="page-header">
		<h3><?=Yii::t('settings', 'Social networks')?></h3>
	</div>

	<div class="form-group">
		<label for="form-setting_facebook" class="col-md-3 control-label">Facebook:</label>
		<div class="col-md-6">
			<input id="form-setting_facebook" class="form-control" type="text" name="setting[setting_facebook]" value="<?=CHtml::encode($settings['setting_facebook'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_twitter" class="col-md-3 control-label">Twitter:</label>
		<div class="col-md-6">
			<input id="form-setting_twitter" class="form-control" type="text" name="setting[setting_twitter]" value="<?=CHtml::encode($settings['setting_twitter'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_instagram" class="col-md-3 control-label">Instagram:</label>
		<div class="col-md-6">
			<input id="form-setting_instagram" class="form-control" type="text" name="setting[setting_instagram]" value="<?=CHtml::encode($settings['setting_instagram'])?>">
		</div>
	</div>

	<div class="page-header">
		<h3><?=Yii::t('settings', 'Instagram posts')?></h3>
	</div>

	<div class="form-group">
		<label for="form-setting_instagram_token" class="col-md-3 control-label">Instagram token:</label>
		<div class="col-md-6">
			<input id="form-setting_instagram_token" class="form-control" type="text" name="setting[setting_instagram_token]" value="<?=CHtml::encode($settings['setting_instagram_token'])?>">
		</div>
		<div class="col-md-1">
			<button id="instagram-update" type="button" class="btn btn-default">Обновить</button>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_instagram_token" class="col-md-3 control-label">Instagram feed:</label>
		<div class="col-md-6">
			<?php if (!empty($settings['setting_instagram_data'])) { ?>
			<div style="margin: 0 -2px">
				<?php
					$instagram_posts = json_decode($settings['setting_instagram_data'], true);

					foreach ($instagram_posts as $instagram_post) {
				?>
				<a target="_blank" href="<?=$instagram_post['url']?>"><img src="<?=$instagram_post['photo']?>" alt="" style="max-width: 120px; max-height: 90px; margin: 0 2px 4px;"></a>
				<?php } ?>
			</div>
			<?php } else { ?>
			<div class="form-control-static">—</div>
			<?php } ?>
		</div>
	</div>

	<div class="page-header">
		<h3><?=Yii::t('settings', 'Google map')?></h3>
	</div>

	<div class="form-group">
		<label for="form-setting_lat" class="col-md-3 control-label"><?=Yii::t('settings', 'Latitude')?>:</label>
		<div class="col-md-6">
			<input id="form-setting_lat" class="form-control" type="text" name="setting[setting_lat]" value="<?=CHtml::encode($settings['setting_lat'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_long" class="col-md-3 control-label"><?=Yii::t('settings', 'Longtitude')?>:</label>
		<div class="col-md-6">
			<input id="form-setting_long" class="form-control" type="text" name="setting[setting_long]" value="<?=CHtml::encode($settings['setting_long'])?>">
		</div>
	</div>

	<div class="page-header">
		<h3><?=Yii::t('settings', 'SEO optimization')?></h3>
	</div>

	<div class="form-group">
		<label for="form-setting_no_index" class="col-md-3 control-label"><?=Yii::t('settings', 'No index site')?>:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-setting_no_index">
					<input id="form-setting_no_index" type="checkbox" name="setting[setting_no_index]" value="1"<?php if ($settings['setting_no_index'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-setting_ga" class="col-md-3 control-label"><?=Yii::t('settings', 'Google Analytics code')?>:</label>
		<div class="col-md-9">
			<textarea id="form-setting_ga" class="form-control" name="setting[setting_ga]" rows="3"><?=CHtml::encode($settings['setting_ga'])?></textarea>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
		</div>
	</div>
</form>

<script>
$(function(){
	$('#currency-update').on('click', function() {
		$(this).prop('disabled', true);
		
		$.ajax({
			type: "POST", 
			url: '<?=$this->createUrl('ajax/currenciesupdate')?>',
			cache: false, 
			dataType: "json",
			success: function(data) {
				if (data.error) {
					$('#currency-update').toggleClass('btn-danger btn-default').prop('disabled', false);
				} else {
					window.location.reload();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) { 
				bootbox.alert("Ошибка запроса!");
				$('#currency-update').prop('disabled', false);
				
				// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
			}
		});
	});

	$('#instagram-update').on('click', function() {
		$(this).prop('disabled', true);
		
		$.ajax({
			type: "POST", 
			url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/instagram', 
			data: $.param({
				token: $('#form-setting_instagram_token').val()
			}), 
			cache: false, 
			dataType: "json",
			success: function(data) {
				if (data.error) {
					$('#instagram-update').toggleClass('btn-danger btn-default').prop('disabled', false);
				} else {
					window.location.reload();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) { 
				bootbox.alert("<?=Yii::t('app', 'Request error')?>");
				$('#instagram-update').prop('disabled', false);
				
				// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
			}
		});
	});
	
	var form = $('#manage-settings'),
		input = form.find('.control-required input, .control-required select, .control-required textarea');
	
	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				var that = this,
					alert_callback = function() {
						if (that.id == 'form-update_info') {
							setTimeout(function() {
								$(that).redactor('focus.setStart');
							}, 21);
						}
						else {
							setTimeout(function() {
								$(that).focus();
							}, 21);
						}
					};

				// non language fields
				switch (this.id) {
					case 'form-setting_rev':
						bootbox.alert("<?=Yii::t('settings', 'Field \'Assets version\' is required!')?>", alert_callback);
						break;
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});
});
</script>