<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('translations', 'Translations h1')?></h1>

<?php
	$translation_url_data = array();
	$sort_data = array();

	if ($sort != 'default') {
		$translation_url_data['sort'] = $sort;
		$translation_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$translation_url_data['keyword'] = $keyword;
		$sort_data['keyword'] = $keyword;
	}

	if (!empty($group)) {
		$translation_url_data['group'] = $group;
		$sort_data['group'] = $group;
	}

	if (!empty($page)) {
		$translation_url_data['page'] = $page + 1;
	}

	$translation_new_url = $this->createUrl('translation', array_merge(array('id' => 'new'), $translation_url_data));
?>
<p class="text-center"><a class="btn btn-success" href="<?=$translation_new_url?>"><?=Yii::t('translations', 'Add translation btn')?></a></p>

<form class="search-form text-center" method="get">
	<div class="form-group form-inline">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('translations', 'ID | translation code placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
	</div>
	<?php if (!empty($groups)) { ?>
	<div class="form-group form-inline">
		<?=Yii::t('translations', 'Group:')?>
		<select class="form-control input-sm" name="group">
			<option value=""><?=Yii::t('translations', 'All')?></option>
			<?php foreach ($groups as $group_item) { ?>
			<option value="<?=CHtml::encode($group_item)?>"<?php if ($group == $group_item) { ?> selected<?php } ?>><?=CHtml::encode($group_item)?></option>
			<?php } ?>
		</select>
	</div>
	<?php } ?>
	<?php if ($sort != 'default' || !empty($keyword) || !empty($group)) { ?>
	<a href="<?=$this->createUrl('translations')?>" style="display: inline-block;">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
	<?php } ?>
</form>

<?php if (!empty($translations)) { ?>
<style>
	.t-field {
		display: block;
		width: 260px;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
	}

	.t-field-double {
		width: 380px;
	}
</style>

<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-translations" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="translation_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<th style="width: 4%"></th>
				<th style="width: 10%">
					<?php if ($sort == 'translation_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('translations', array_merge(array('sort' => 'translation_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'translation_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('translations', array_merge(array('sort' => 'translation_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('translations', array_merge(array('sort' => 'translation_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th style="width: 30%">
					<?php if ($sort == 'translation_code' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('translations', array_merge(array('sort' => 'translation_code', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('translations', 'Translation code col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'translation_code' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('translations', array_merge(array('sort' => 'translation_code', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('translations', 'Translation code col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('translations', array_merge(array('sort' => 'translation_code', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('translations', 'Translation code col')?></a>
					<?php } ?>
				</th>
				<th style="width: 42%"><?=Yii::t('translations', 'Translation value col')?></th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($translations as $id => $translation) { ?>
			<?php
				$translation_id = $translation['translation_id'];

				$translation_url = $this->createUrl('translation', array_merge(array('id' => $translation_id), $translation_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$translation_id?>">
				</td>
				
				<td><a href="<?=$translation_url?>"><?=$translation_id?></a></td>
				<td>
					<span class="t-field"><a href="<?=$translation_url?>"><?=CHtml::encode($translation['translation_code'])?></a></span>
				</td>
				<td>
					<span class="t-field t-field-double"><a href="<?=$translation_url?>"><?=CHtml::encode($translation['translation_value'])?></a></span>
				</td>
				
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$translation_id?>">
						<div class="btn-group">
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$translation_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}
			
			submit_form = true;
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-translations').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-translations').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-translations").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>