<?php
/* @var $this AdminController */
?>
<?php
	$bases_url_data = array();

	if (!empty($sort)) {
		$bases_url_data['sort'] = $sort;
		$bases_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$bases_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$bases_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('bases', $bases_url_data);

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/base';
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-base" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="base[base_id]" value="<?=$base['base_id']?>">
		
	<div class="page-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>
	
	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label">ID:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$base['base_id']?></p>
		</div>
	</div>
		
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-3">
			<select id="form-active" name="base[active]" class="form-control">
				<option value="0"<?php if ($base['active'] == 0) { ?> selected<?php } ?>><?=Yii::t('bases', 'Blocked')?></option>
				<option value="1"<?php if ($base['active'] == 1) { ?> selected<?php } ?>><?=Yii::t('bases', 'Active')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-base_alias" class="col-md-3 control-label"><?=Yii::t('app', 'Alias (URL code)')?>:</label>
		<div class="col-md-6">
			<div class="input-group">
				<span class="input-group-addon">/knowledge-base/</span>
				<input id="form-base_alias" class="form-control" type="text" name="base[base_alias]" value="<?=CHtml::encode($base['base_alias'])?>">
			</div>
		</div>
	</div>

	<div class="form-group">
        <label for="form-category_id" class="col-md-3 control-label"><?=Yii::t('bases', 'Category')?>:</label>
        <div class="col-md-4 control-required">
            <select id="form-category_id" class="form-control" name="base[category_id]">
                <option value="">---</option>
                <?php if (!empty($categories)) { ?>
                <?php foreach ($categories as $category) { ?>
                <option value="<?=$category['category_id']?>"<?php if ($base['category_id'] == $category['category_id']) { ?> selected<?php } ?>><?=CHtml::encode($category['category_name'])?></option>
                <?php } ?>
                <?php } ?>
            </select>
        </div>
    </div>

	<div class="form-group">
		<label for="form-base_title" class="col-md-3 control-label"><?=Yii::t('bases', 'Base title')?>:</label>
		<div class="col-md-6 control-required">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-base_title_<?=$code?>" aria-controls="form-base_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-base_title_<?=$code?>">
						<input class="form-control" type="text" name="base_lang[base_title][<?=$code?>]" value="<?=CHtml::encode($base[$code]['base_title'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-base_photo" class="col-md-3 control-label"><?=Yii::t('bases', 'Photo')?>:</label>
		<div class="col-md-4">
			<?php
				if (!empty($base['base_photo'])) {
					$photo = json_decode($base['base_photo'], true);

					$photo_url = $assetsUrl . '/' . $base['base_id'] . '/' . $photo['list']['1x']['path'];
			?>
			<img class="img-responsive" src="<?=$photo_url?>" alt="" width="<?=$photo['list']['1x']['size']['w']?>" height="<?=$photo['list']['1x']['size']['h']?>">
			<div class="checkbox">
				<label for="del_base_photo"><input id="del_base_photo" type="checkbox" name="base[del_base_photo]" value="<?=$photo['list']['1x']['path']?>"> <?=Yii::t('app', 'delete photo')?></label>
			</div>
			<br>
			<?php }	?>

			<input id="form-base_photo" type="file" name="base[base_photo]">
			<small class="text-nowrap"><?=Yii::t('app', 'Image file requirements', array('{types}' => 'jpg, gif, png', '{size}' => '10 MB', '{dimension}' => '1230x700 px'))?></small>
		</div>
	</div>

	<div class="form-group">
		<label for="form-base_description" class="col-md-3 control-label"><?=Yii::t('bases', 'Description')?>:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-base_description_<?=$code?>" aria-controls="form-base_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-base_description_<?=$code?>">
						<textarea class="form-control form-redactor" name="base_lang[base_description][<?=$code?>]" rows="6"><?=CHtml::encode($base[$code]['base_description'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div id="section-seo" class="page-header">
		<h3><?=Yii::t('app', 'SEO data')?></h3>
	</div>

	<div class="form-group">
		<label for="form-base_meta_title" class="col-md-3 control-label"><?=Yii::t('app', 'No index')?>:</label>
		<div class="col-md-6">
			<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
			<div class="checkbox">
				<label for="form-base_no_index<?=$code?>">
					<input id="form-base_no_index<?=$code?>" type="checkbox" name="base_lang[base_no_index][<?=$code?>]" value="1"<?php if ($base[$code]['base_no_index'] == 1) { ?> checked<?php } ?>> <?=$lang?>
				</label>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="form-group">
		<label for="form-base_meta_title" class="col-md-3 control-label">Meta title:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-base_meta_title_<?=$code?>" aria-controls="form-base_meta_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-base_meta_title_<?=$code?>">
						<input class="form-control" type="text" name="base_lang[base_meta_title][<?=$code?>]" value="<?=CHtml::encode($base[$code]['base_meta_title'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-base_meta_keywords" class="col-md-3 control-label">Meta keywords:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-base_meta_keywords_<?=$code?>" aria-controls="form-base_meta_keywords_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-base_meta_keywords_<?=$code?>">
						<textarea class="form-control" name="base_lang[base_meta_keywords][<?=$code?>]" rows="2"><?=CHtml::encode($base[$code]['base_meta_keywords'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-base_meta_description" class="col-md-3 control-label">Meta description:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-base_meta_description_<?=$code?>" aria-controls="form-base_meta_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-base_meta_description_<?=$code?>">
						<textarea class="form-control" name="base_lang[base_meta_description][<?=$code?>]" rows="4"><?=CHtml::encode($base[$code]['base_meta_description'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<div class="btn-group-vertical btn-insert-link" role="group" aria-label="<?=Yii::t('app', 'Insert/remove a link label')?>">
  <button id="typograph-link-btn" title="<?=Yii::t('app', 'Typography text btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-text-size"></i></button>
  <button id="insert-link-btn" title="<?=Yii::t('app', 'Insert a link btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
  <button id="remove-link-btn" title="<?=Yii::t('app', 'Remove all links btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
</div>

<!-- Insert Link Modal -->
<div class="modal fade" id="insertLinkModal" tabindex="-1" role="dialog" aria-labelledby="insertLinkModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="<?=Yii::t('app', 'Close')?>"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="insertLinkModalLabel"><?=Yii::t('app', 'Insert a link label')?></h4>
			</div>
			<div class="modal-body">
			<form id="insert-link">
				<div class="form-group">
					<label for="link-text" class="control-label"><?=Yii::t('app', 'Link text')?>:</label>
					<input type="text" class="form-control" id="link-text">
				</div>
				<div class="form-group">
					<label for="link-url" class="control-label"><?=Yii::t('app', 'Link tip')?>:</label>
					<input type="text" class="form-control" id="link-url">
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label for="link-blank">
							<input id="link-blank" type="checkbox" value="1"> <?=Yii::t('app', 'Open in new tab')?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="link-title" class="control-label"><?=Yii::t('app', 'Link tip (title)')?>:</label>
					<input type="text" class="form-control" id="link-title">
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=Yii::t('app', 'Cancel btn')?></button>
				<button id="do-insert" type="button" class="btn btn-primary"><?=Yii::t('app', 'Insert btn')?></button>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var form = $("#manage-base"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	$('#typograph-link-btn').on('click', function(e) {
		var input = focused,
			text = input.val(),
			btn = $(this);

		if (focused != null) {
			btn.prop('disabled', true);
			
			$.ajax({type: "POST", url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/typograph', data: $.param({text: focused.val()}), cache: false, dataType: "json",
				success: function(data) {
					btn.prop('disabled', false);
					
					if (data.error == 'Y') {
						bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					}
					else {
						if (focused.hasClass('form-redactor')) {
							$('#' + focused[0].id).redactor('code.set', data.text);
						}
						else {
							focused.val(data.text);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					btn.prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});
	
	$('#insert-link-btn').on('click', function(e) {
		var input = focused,
			len = input.val().length,
			start = input[0].selectionStart,
			end = input[0].selectionEnd,
			selectedText = input.val().substring(start, end);

		$('#link-text').val(selectedText);
		$('#link-url').val('');
		$('#link-title').val('');
		$('#link-blank').prop('checked', false);

		if (focused != null) {
			$('#insertLinkModal').modal('show');
		}
	});

	$('#remove-link-btn').on('click', function(e) {
		if (focused != null) {
			var input = focused,
				new_value = input.val().replace(/<a[^>]*>([\s\S]*?)<\/a>/ig, '$1');

			input.val(new_value);
		}
	});

	$('#insert-link').submit(function() {
		var link_url = $.trim($('#link-url').val()),
			link_text = $.trim($('#link-text').val()),
			link_title = $.trim($('#link-title').val()),
			link_blank = $('#link-blank').prop('checked');

		if (link_url == '' || link_text == '') {
			$('#insertLinkModal').modal('hide');
			return false;
		}

		var new_link = $('<a href="' + link_url + '"/>');
		new_link.text(link_text);

		if (link_title != '') {
			new_link.attr('title', link_title);
		}

		if (link_blank) {
			new_link.attr('target', '_blank');
		}

		var len = focused.val().length,
			start = focused[0].selectionStart,
			end = focused[0].selectionEnd,
			selectedText = focused.val().substring(start, end);

		focused.val(focused.val().substring(0, start) + new_link[0].outerHTML + focused.val().substring(end, len));
		focused = null;

		$('#insertLinkModal').modal('hide');

		return false;
	});

	$('#do-insert').click(function() {
		$('#insert-link').submit();

		return false;
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				// error for lang fields
				if ($(this).parent().hasClass('tab-pane')) {
					var that = this,
						tab = $(this).parent().parent().prev().children(':eq(' + $(this).parent().index() + ')').children(),
						alert_callback = function() {
							if (that.id == 'form-base_description') {
								setTimeout(function() {
									tab.click();
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									tab.click();
									$(that).focus();
								}, 21);
							}
						};

					if ($(this).parent()[0].id.indexOf('form-base_title') === 0) {
						bootbox.alert("<?=Yii::t('bases', 'Enter a base title!')?>", alert_callback);
					}
				}
				else {
					var that = this,
						alert_callback = function() {
							if (that.id == 'form-redactor') {
								setTimeout(function() {
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									$(that).focus();
								}, 21);
							}
						};

					switch (this.id) {
						case 'form-base_title':
							bootbox.alert("<?=Yii::t('bases', 'Enter a base title!')?>", alert_callback);
							break;
						case 'form-category_id':
                            bootbox.alert("<?=Yii::t('bases', '\'Category\' is required!')?>", alert_callback);
                            break;
					}
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});
});
</script>