<?php
    /* @var $this AdminController */

    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $requests_url_data = array();

    if (!empty($sort)) {
        $requests_url_data['sort'] = $sort;
        $requests_url_data['direction'] = $direction;
    }

    if (!empty($keyword)) {
        $requests_url_data['keyword'] = $keyword;
    }

    if (!empty($date_from)) {
        $requests_url_data['date_from'] = $date_from;
    }

    if (!empty($date_to)) {
        $requests_url_data['date_to'] = $date_to;
    }

    if (!empty($page)) {
        $requests_url_data['page'] = $page + 1;
    }

    $back_link = $this->createUrl('requests', $requests_url_data);

    $has_rights = Yii::app()->getUser()->hasAccess($this->route, true);
?>
<h1><?=Yii::t('requests', 'Edit request h1')?> #<?=$request['request_id']?></h1>

<p class="text-center">
    <a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-course-request" class="form-horizontal" method="post" enctype="multipart/form-data">
    <input type="hidden" name="request[request_id]" value="<?=$request['request_id']?>">

    <div class="page-header">
        <h3><?=Yii::t('requests', 'Request details')?></h3>
    </div>

    <div class="form-group">
        <label for="form-id" class="col-md-3 control-label"><?=Yii::t('requests', 'Request #')?>:</label>
        <div class="col-md-5 form-control-static">
            <?=$request['request_id']?>
        </div>
    </div>

    <div class="form-group">
        <label for="form-id" class="col-md-3 control-label"><?=Yii::t('requests', 'Course')?>:</label>
        <div class="col-md-5 form-control-static">
            <?php if (!empty($request['course_name'])) { ?>
            <a href="<?=$this->createUrl('admin/course', ['id' => $request['course_id']])?>"><?=CHtml::encode($request['course_name'])?></a>
            <?php } else { ?>
            —
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <label for="form-first_name" class="col-md-3 control-label"><?=Yii::t('requests', 'Name')?>:</label>
        <div class="col-md-4">
            <input id="form-first_name" class="form-control" type="text" name="request[first_name]" value="<?=CHtml::encode($request['first_name'])?>">
        </div>
    </div>
    
    <div class="form-group">
        <label for="form-last_name" class="col-md-3 control-label"><?=Yii::t('requests', 'Last name')?>:</label>
        <div class="col-md-4">
            <input id="form-last_name" class="form-control" type="text" name="request[last_name]" value="<?=CHtml::encode($request['last_name'])?>">
        </div>
    </div>

    <div class="form-group">
        <label for="form-email" class="col-md-3 control-label"><?=Yii::t('requests', 'Email')?>:</label>
        <div class="col-md-4">
            <input id="form-email" class="form-control" type="text" name="request[email]" value="<?=CHtml::encode($request['email'])?>">
        </div>
    </div>

    <div class="form-group">
        <label for="form-phone" class="col-md-3 control-label"><?=Yii::t('requests', 'Phone')?>:</label>
        <div class="col-md-4">
            <input id="form-phone" class="form-control" type="text" name="request[phone]" value="<?=CHtml::encode($request['phone'])?>">
        </div>
    </div>

    <div class="form-group">
        <label for="form-message" class="col-md-3 control-label"><?=Yii::t('requests', 'Comment')?>:</label>
        <div class="col-md-6">
            <textarea id="form-message" class="form-control" name="request[message]" rows="4"><?=CHtml::encode($request['message'])?></textarea>
        </div>
    </div>

    <hr>

    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-primary btn-lg"<?php if (!$has_rights) { ?> disabled<?php } ?>><?=Yii::t('app', 'Save btn')?></button>
            <a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
        </div>
    </div>
</form>

<script>
$(document).ready(function(){
    <?php if (!$has_rights) { ?>
    $('form').find(':input').prop('disabled', true);
    <?php } else { ?>

    var focused,
        form = $("#manage-course-request"),
        required_input = form.find('.control-required input, .control-required select, .control-required textarea');

    form.find('input, textarea').focus(function() {
        focused = $(this);
    });

    $('#cancel').click(function() {
        form_submit = true;
    });

    $(window).on('beforeunload', function() {
        if (form_changed && !form_submit) {
            return '<?=Yii::t('app', 'The form data will not be saved!')?>';
        }
    });

    form.one('change', 'input, select, textarea', function(){
        form_changed = true;
    });

    form.submit(function(e) {
        var error = false;

        required_input.each(function(){
            if ($.trim($(this).val()) == '') {
                error = true;

                var that = this,
                    alert_callback = function() {
                        if (that.id == 'form-redactor') {
                            setTimeout(function() {
                                $(that).redactor('focus.setStart');
                            }, 21);
                        }
                        else {
                            setTimeout(function() {
                                $(that).focus();
                            }, 21);
                        }
                    };

                switch (this.id) {
                    case 'form-first_name':
                        bootbox.alert("Введите имя!", alert_callback);
                        break;
                    case 'form-last_name':
                        bootbox.alert("Введите фамилию!", alert_callback);
                        break;
                    case 'form-mail':
                        bootbox.alert("Введите email!", alert_callback);
                        break;
                    case 'form-phone':
                        bootbox.alert("Введите телефон!", alert_callback);
                        break;
                }

                return false;
            }
        });

        if (error) {
            return false;
        }
        else {
            form_submit = true;
        }
    });
    <?php } ?>
});
</script>
