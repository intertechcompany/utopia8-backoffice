<?php
	/* @var $this AdminController */
?>
<h1><?=Yii::t('managers', 'Managers h1')?></h1>

<?php
	$manager_url_data = array();

	if ($sort != 'default') {
		$manager_url_data['sort'] = $sort;
		$manager_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$manager_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$manager_url_data['page'] = $page + 1;
	}

	$manager_new_url = $this->createUrl('manager', array_merge(array('id' => 'new'), $manager_url_data));

	$has_rights = Yii::app()->getUser()->hasAccess($this->route, true);
?>
<p class="text-center"><a class="btn btn-success" href="<?=$manager_new_url?>"><?=Yii::t('managers', 'Add manager btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('managers', 'ID | manager name placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<?php if ($sort != 'default' || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('managers')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($managers)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-managers" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="manager_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array('keyword' => $keyword);
					} else {
						$sort_data = array();
					}
				?>
				<th style="width: 2%"></th>
				<th style="width: 6%">
					<?php if ($sort == 'manager_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('managers', array_merge(array('sort' => 'manager_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'manager_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('managers', array_merge(array('sort' => 'manager_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('managers', array_merge(array('sort' => 'manager_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th width="24%"><?=Yii::t('managers', 'Manager name col')?></th>
				<th width="16%"><?=Yii::t('managers', 'Manager login col')?></th>
				<th width="18%"><?=Yii::t('managers', 'Manager roles col')?></th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($managers as $id => $manager) { ?>
			<?php
				$manager_id = $manager['manager_id'];

				$manager_url = $this->createUrl('manager', array_merge(array('id' => $manager_id), $manager_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$manager_id?>">
				</td>
				<td>
					<a href="<?=$manager_url?>"><?=$manager_id?></a>
				</td>
				<td>
					<a href="<?=$manager_url?>"><?=CHtml::encode($manager['manager_first_name'] . ' ' . $manager['manager_last_name'])?></a>
				</td>
				<td>
					<a href="<?=$manager_url?>"><?=CHtml::encode($manager['manager_login'])?></a>
				</td>
				<td>
					<a href="<?=$manager_url?>"><?=CHtml::encode(implode(', ', $manager['roles']))?></a>
				</td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$manager_id?>">
						<div class="btn-group">
							<?php if ($has_rights) { ?>
							<?php if ($manager['active']) { ?>
							<a title="<?=Yii::t('managers', 'Active')?>" class="active-btn btn btn-default btn-sm btn-success" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ok"></span></a>
							<?php } else { ?>
							<a title="<?=Yii::t('managers', 'Blocked')?>" class="block-btn btn btn-default btn-sm btn-danger" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ban-circle"></span></a>
							<?php } ?>
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$manager_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
							<?php } else { ?>
							<a title="<?=Yii::t('app', 'View btn')?>" class="btn btn-default btn-sm" href="<?=$manager_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-eye-open"></span></a>
							<?php } ?>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<?php if ($has_rights) { ?>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="active"><?=Yii::t('app', 'Activate selected')?></option>
								<option value="block"><?=Yii::t('app', 'Block selected')?></option>
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
		<?php } ?>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}

			submit_form = true;
			
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-managers').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-managers').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-managers").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>