<?php
/* @var $this AdminController */
?>
<?php
	function showCategoriesTree($categories, $category_id = 0, $level = 0, $active = 1)
	{
		$html = '';

		foreach ($categories as $category) {
			if ($category['active'] && $active) {
				$category_active = ' data-active="1"';
				$subcategory_active = 1;
			} else {
				$category_active = ' data-active="0"';
				$subcategory_active = 0;
			}

			if ((is_array($category_id) && in_array($category['category_id'], $category_id)) || $category['category_id'] == $category_id) {
				$html .= '<option value="' . $category['category_id'] . '"' . $category_active . ' selected>' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_name']) . '</option>';
			} else {
				$html .= '<option value="' . $category['category_id'] . '"' . $category_active . '>' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_name']) . '</option>';
			}

			if (!empty($category['sub'])) {
				$html .= showCategoriesTree($category['sub'], $category_id, $level + 1, $subcategory_active);
			}
		}

		return $html;
	}
	
	function showBaseCategoriesTree($categories, $category_id = 0, $level = 0)
	{
		$html = '';

		foreach ($categories as $category) {
			if ((is_array($category_id) && in_array($category['category_id'], $category_id)) || $category['category_id'] == $category_id) {
				$html .= '<option value="' . $category['category_id'] . '" selected>' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_alias']) . '</option>';
			} else {
				$html .= '<option value="' . $category['category_id'] . '">' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_alias']) . '</option>';
			}

			if (!empty($category['sub'])) {
				$html .= showBaseCategoriesTree($category['sub'], $category_id, $level + 1);
			}
		}

		return $html;
	}

	$products_url_data = array();

	if (!empty($sort)) {
		$products_url_data['sort'] = $sort;
		$products_url_data['direction'] = $direction;
	}

	if (!empty($show_all)) {
		$products_url_data['show_all'] = $show_all;
	}
	
	if (!empty($keyword)) {
		$products_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$products_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('products', $products_url_data);

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/product';
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-product" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="product[product_id]" value="<?=$product['product_id']?>">
		
	<div class="page-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>
	
	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label">ID:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$product['product_id']?></p>
		</div>
	</div>
		
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-3">
			<select id="form-active" name="product[active]" class="form-control">
				<option value="0"<?php if ($product['active'] == 0) { ?> selected<?php } ?>><?=Yii::t('products', 'Blocked')?></option>
				<option value="1"<?php if ($product['active'] == 1) { ?> selected<?php } ?>><?=Yii::t('products', 'Active')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-3 control-label">Опубликован:</label>
		<div class="col-md-3 form-control-static">
			<?php if ($product['product_published'] == '0000-00-00 00:00:00') { ?>
			—
			<?php } else { ?>
			<?=date('d.m.Y H:i:s', strtotime($product['product_published']))?>
			<?php } ?>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_alias" class="col-md-3 control-label"><?=Yii::t('app', 'Alias (URL code)')?>:</label>
		<div class="col-md-6">
			<div class="input-group">
				<span class="input-group-addon">/product/</span>
				<input id="form-product_alias" class="form-control" type="text" name="product[product_alias]" value="<?=CHtml::encode($product['product_alias'])?>" readonly>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_title" class="col-md-3 control-label"><?=Yii::t('products', 'Product title')?>:</label>
		<div class="col-md-6 control-required">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_title_<?=$code?>" aria-controls="form-product_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_title_<?=$code?>">
						<input class="form-control" type="text" name="product_lang[product_title][<?=$code?>]" value="<?=CHtml::encode($product[$code]['product_title'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_sku" class="col-md-3 control-label"><?=Yii::t('products', 'SKU')?>:</label>
		<div class="col-md-4">
			<input id="form-product_sku" class="form-control" type="text" name="product[product_sku]" value="<?=CHtml::encode($product['product_sku'])?>" readonly>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-product_crm_id" class="col-md-3 control-label"><?=Yii::t('products', 'CRM ID')?>:</label>
		<div class="col-md-4">
			<input id="form-product_crm_id" class="form-control" type="text" name="product[product_crm_id]" value="<?=CHtml::encode($product['product_crm_id'])?>" readonly>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-product_1c_id" class="col-md-3 control-label"><?=Yii::t('products', '1C ID')?>:</label>
		<div class="col-md-4">
			<input id="form-product_1c_id" class="form-control" type="text" name="product[product_1c_id]" value="<?=CHtml::encode($product['product_1c_id'])?>" readonly>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_newest" class="col-md-3 control-label"><?=Yii::t('products', 'Newest')?>:</label>
		<div class="col-md-3">
			<div class="checkbox">
				<label for="form-product_newest">
					<input id="form-product_newest" type="checkbox" name="product[product_newest]" value="1"<?php if ($product['product_newest']) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_bestseller" class="col-md-3 control-label"><?=Yii::t('products', 'Best seller')?>:</label>
		<div class="col-md-3">
			<div class="checkbox">
				<label for="form-product_bestseller">
					<input id="form-product_bestseller" type="checkbox" name="product[product_bestseller]" value="1"<?php if ($product['product_bestseller']) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_sale" class="col-md-3 control-label"><?=Yii::t('products', 'Sale')?>:</label>
		<div class="col-md-3">
			<div class="checkbox">
				<label for="form-product_sale">
					<input id="form-product_sale" type="checkbox" name="product[product_sale]" value="1"<?php if ($product['product_sale']) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_rating" class="col-md-3 control-label"><?=Yii::t('products', 'Rating')?>:</label>
		<div class="col-md-1">
			<input id="form-product_rating" class="form-control" type="text" name="product[product_rating]" value="<?=CHtml::encode($product['product_rating'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-badges" class="col-md-3 control-label">Бэйджи:</label>
		<div class="col-md-6">
			<select id="form-badges" class="form-control select2" name="product[badges][]" multiple data-placeholder="Выберите бэйджи из списка...">
				<?php if (!empty($badges)) { ?>
				<?php foreach ($badges as $badge) { ?>
				<option value="<?=$badge['badge_id']?>"<?php if (in_array($badge['badge_id'], $product['badges'])) { ?> selected<?php } ?>><?=CHtml::encode($badge['badge_name'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-brand_id" class="col-md-3 control-label"><?=Yii::t('products', 'Brand')?>:</label>
		<div class="col-md-4">
			<select id="form-brand_id" class="form-control" name="product[brand_id]">
				<option value="0">---</option>
				<?php if (!empty($brands)) { ?>
				<?php foreach ($brands as $brand) { ?>
				<option value="<?=$brand['brand_id']?>"<?php if ($product['brand_id'] == $brand['brand_id']) { ?> selected<?php } ?>><?=CHtml::encode($brand['brand_name'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<!-- <div class="form-group">
		<label for="form-collection_id" class="col-md-3 control-label"><?=Yii::t('products', 'Collection')?>:</label>
		<div class="col-md-4">
			<select id="form-collection_id" class="form-control" name="product[collection_id]">
				<option value="">---</option>
				<?php if (!empty($collections)) { ?>
				<?php foreach ($collections as $collection) { ?>
				<option value="<?=$collection['collection_id']?>"<?php if ($product['collection_id'] == $collection['collection_id']) { ?> selected<?php } ?>><?=CHtml::encode($collection['collection_title'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div> -->

	<div class="form-group">
		<label for="form-base_category_id" class="col-md-3 control-label">Категория 1С:</label>
		<div class="col-md-4">
			<select id="form-base_category_id" class="form-control" name="product[base_category_id]">
				<option value="">---</option>
				<?php if (!empty($base_categories_tree)) { ?>
				<?=showBaseCategoriesTree($base_categories_tree, $product['base_category_id'])?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-category_id" class="col-md-3 control-label"><?=Yii::t('products', 'Category')?>:</label>
		<div class="col-md-4 control-required">
			<select id="form-category_id" class="form-control" name="product[category_id]">
				<option value="">---</option>
				<?php if (!empty($categories_tree)) { ?>
				<?=showCategoriesTree($categories_tree, $product['category_id'])?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-categories" class="col-md-3 control-label">Доп. категории:</label>
		<div class="col-md-6">
			<select id="form-categories" class="form-control select2" name="product[categories][]" multiple data-placeholder="Выберите категории из списка...">
				<?php if (!empty($categories_tree)) { ?>
				<?=showCategoriesTree($categories_tree, $product['categories'])?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-tags" class="col-md-3 control-label">Тэги:</label>
		<div class="col-md-6">
			<select id="form-tags" class="form-control select2" name="product[tags][]" multiple data-placeholder="Выберите тэги из списка...">
				<?php if (!empty($tags)) { ?>
				<?php foreach ($tags as $tag) { ?>
				<option value="<?=$tag['tag_id']?>"<?php if (in_array($tag['tag_id'], $product['tags'])) { ?> selected<?php } ?>><?=CHtml::encode($tag['tag_name'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_price_type" class="col-md-3 control-label"><?=Yii::t('products', 'Product price type')?>:</label>
		<div class="col-md-3">
			<select id="form-product_price_type" name="product[product_price_type]" class="form-control" disabled>
				<?php /* <option value="piece"<?php if ($product['product_price_type'] == 'piece') { ?> selected<?php } ?>><?=Yii::t('products', 'Price per piece')?></option>
				<option value="package"<?php if ($product['product_price_type'] == 'package') { ?> selected<?php } ?>><?=Yii::t('products', 'Price per package')?></option>
				<option value="per_meter"<?php if ($product['product_price_type'] == 'per_meter') { ?> selected<?php } ?>><?=Yii::t('products', 'Price per meter')?></option> */ ?>
				<option value="item"<?php if ($product['product_price_type'] == 'item') { ?> selected<?php } ?>><?=Yii::t('products', 'Price per item')?></option>
				<option value="variants"<?php if ($product['product_price_type'] == 'variants') { ?> selected<?php } ?>><?=Yii::t('products', 'Price per variant')?></option>
				<!-- <option value="set"<?php if ($product['product_price_type'] == 'set') { ?> selected<?php } ?>><?=Yii::t('products', 'Products set')?></option> -->
			</select>
			<input type="hidden" name="product[product_price_type]" value="<?=$product['product_price_type']?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_price" class="col-md-3 control-label"><?=Yii::t('products', 'Price')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-product_price" class="form-control" type="text" name="product[product_price]" value="<?=CHtml::encode($product['product_price'])?>" readonly>
				<span class="input-group-addon">грн.</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_price_old" class="col-md-3 control-label"><?=Yii::t('products', 'Price old')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-product_price_old" class="form-control" type="text" name="product[product_price_old]" value="<?=CHtml::encode($product['product_price_old'])?>"<?php if ($product['product_price_type'] != 'item') { ?> readonly<?php } ?>>
				<span class="input-group-addon">грн.</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_instock_" class="col-md-3 control-label"><?=Yii::t('products', 'In stock')?>:</label>
		<div class="col-md-3">
			<select id="form-product_instock_" name="product[product_instock]" class="form-control" disabled>
				<option value="in_stock"<?php if ($product['product_instock'] == 'in_stock') { ?> selected<?php } ?>><?=Yii::t('products', 'Product in stock')?></option>
				<option value="out_of_stock"<?php if ($product['product_instock'] == 'out_of_stock') { ?> selected<?php } ?>><?=Yii::t('products', 'Product out of stock')?></option>
				<!-- <option value="preorder"<?php if ($product['product_instock'] == 'preorder') { ?> selected<?php } ?>><?=Yii::t('products', 'Product preorder')?></option> -->
			</select>
		</div>
	</div>

	<?php if ($product['product_instock'] == 'in_stock') { ?>
	<div class="form-group">
		<label for="form-product_stock_type_" class="col-md-3 control-label">Тип наличия:</label>
		<div class="col-md-3">
			<select id="form-product_stock_type_" name="product[product_stock_type]" class="form-control" disabled>
				<option value="store"<?php if ($product['product_stock_type'] == 'store') { ?> selected<?php } ?>>На складе</option>
				<option value="online"<?php if ($product['product_stock_type'] == 'online') { ?> selected<?php } ?>>Онлайн</option>
				<option value="preorder"<?php if ($product['product_stock_type'] == 'preorder') { ?> selected<?php } ?>>Предзаказ</option>
			</select>
		</div>
	</div>
	<?php } else { ?>
	<input type="hidden" name="product[product_stock_type]" value="<?=CHtml::encode($product['product_stock_type'])?>" disabled>
	<?php } ?>

	<div class="form-group">
		<label for="form-product_stock_qty" class="col-md-3 control-label">Количество на складе:</label>
		<div class="col-md-1">
			<input id="form-product_stock_qty" class="form-control" type="text" name="product[product_stock_qty]" value="<?=CHtml::encode($product['product_stock_qty'])?>" readonly>
		</div>
		<div class="col-md-6 form-control-static">
			<?php if (!empty($product['stores'])) { ?>
			<?php foreach ($product['stores'] as $index => $store) { ?>
			<?php if ($index > 0) { ?> | <?php } ?>
			<small><?=CHtml::encode($store['store_name'])?>: <?=$store['quantity']?></small>
			<?php } ?>
			<?php } ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-product_online_days" class="col-md-3 control-label">Ожидание дней онлайн:</label>
		<div class="col-md-1">
			<input id="form-product_online_days" class="form-control" type="text" name="product[product_online_days]" value="<?=CHtml::encode($product['product_online_days'])?>">
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-product_preorder_days" class="col-md-3 control-label">Ожидание дней под заказ:</label>
		<div class="col-md-1">
			<input id="form-product_preorder_days" class="form-control" type="text" name="product[product_preorder_days]" value="<?=CHtml::encode($product['product_preorder_days'])?>">
		</div>
	</div>

	<div class="form-group product-item">
		<label for="form-product_weight" class="col-md-3 control-label">Вес:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-product_weight" class="form-control" type="text" name="product[product_weight]" value="<?=CHtml::encode($product['product_weight'])?>">
				<span class="input-group-addon">кг</span>
			</div>
		</div>
	</div>

	<div class="form-group product-set">
		<label for="form-product_set" class="col-md-3 control-label"><?=Yii::t('products', 'Products set (product ID\'s)')?>:</label>
		<div class="col-md-6">
			<textarea id="form-product_set" class="form-control" name="product[product_set]" rows="2"><?=CHtml::encode($product['product_set'])?></textarea>
		</div>
	</div>

	<?php /* <div class="form-group product-package">
		<label for="form-product_length" class="col-md-3 control-label"><?=Yii::t('products', 'Length')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-product_length" class="form-control" type="text" name="product[product_length]" value="<?=CHtml::encode($product['product_length'])?>">
				<span class="input-group-addon">мм</span>
			</div>
		</div>
	</div>

	<div class="form-group product-package">
		<label for="form-product_width" class="col-md-3 control-label"><?=Yii::t('products', 'Width')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-product_width" class="form-control" type="text" name="product[product_width]" value="<?=CHtml::encode($product['product_width'])?>">
				<span class="input-group-addon">мм</span>
			</div>
		</div>
	</div>

	<div class="form-group product-package product-per_meter">
		<label for="form-product_pack_size" class="col-md-3 control-label"><?=Yii::t('products', 'Package size')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-product_pack_size" class="form-control" type="text" name="product[product_pack_size]" value="<?=CHtml::encode($product['product_pack_size'])?>">
				<span class="input-group-addon">&nbsp;м<sup>2</sup></span>
			</div>
		</div>
	</div>

	<div class="form-group product-package">
		<label for="form-product_pack_qty" class="col-md-3 control-label"><?=Yii::t('products', 'Package quantity')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-product_pack_qty" class="form-control" type="text" name="product[product_pack_qty]" value="<?=CHtml::encode($product['product_pack_qty'])?>">
				<span class="input-group-addon">шт.</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_calc_type" class="col-md-3 control-label"><?=Yii::t('products', 'Product calc type')?>:</label>
		<div class="col-md-3">
			<select id="form-product_calc_type" name="product[product_calc_type]" class="form-control">
				<option value=""><?=Yii::t('products', 'Calc without')?></option>
				<option value="floor"<?php if ($product['product_calc_type'] == 'floor') { ?> selected<?php } ?>><?=Yii::t('products', 'Calc floor tiles / floor')?></option>
				<option value="wall"<?php if ($product['product_calc_type'] == 'wall') { ?> selected<?php } ?>><?=Yii::t('products', 'Calc wall tiles')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_country" class="col-md-3 control-label"><?=Yii::t('products', 'Product country')?>:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_country_<?=$code?>" aria-controls="form-product_country_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_country_<?=$code?>">
						<input class="form-control" type="text" name="product_lang[product_country][<?=$code?>]" value="<?=CHtml::encode($product[$code]['product_country'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_brand" class="col-md-3 control-label"><?=Yii::t('products', 'Product brand')?>:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_brand_<?=$code?>" aria-controls="form-product_brand_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_brand_<?=$code?>">
						<input class="form-control" type="text" name="product_lang[product_brand][<?=$code?>]" value="<?=CHtml::encode($product[$code]['product_brand'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div> */ ?>

	<div class="page-header">
		<h3>Цвета</h3>
	</div>

	<div class="form-group">
		<label for="form-colors" class="col-md-3 control-label">Перелинковка:</label>
		<div class="col-md-9">
			<select id="form-colors" class="form-control" name="product[colors][]" multiple data-placeholder="Введите название или ID товара...">
				<option value="">Введите название или ID товара...</option>
				<?php if (!empty($product['colors'])) { ?>
				<?php foreach ($product['colors'] as $color) { ?>
				<option value="<?=$color['product_id']?>" selected><?=CHtml::encode($color['product_title'] . ' (код: ' . $color['product_alias'] . ')')?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<style>
		.variants {
			margin-top: -15px;
		}

		.variant,
		.option {
			position: relative;
			border-top: 1px solid #eee;
			padding: 15px 0;
		}

		.variant:first-child,
		.option:first-child {
			border-top: 0;
		}

		.variant .form-group:last-child,
		.option .form-group:last-child {
			margin-bottom: 0;
		}

		.v-placeholder {
			height: 241px;
			background-color: #f5f5f5;
		}

		.add-variant,
		.add-option {
			margin-top: 10px;
		}

		.del-variant,
		.del-option {
			position: relative;
		}

		.del-variant-tip,
		.del-option-tip {
			display: inline-block;
			margin-top: -3px;
			margin-left: 5px;
			vertical-align: middle;
		}

		.variant .btn-sm {
			position: absolute;
			top: 15px;
			left: 15px;
			z-index: 1;
			cursor: move;
		}

		.variant .variant-properties {
			margin-left: -10px;
			margin-top: -10px;
		}

		.variant .variant-properties div {
			display: inline-block;
			width: calc(33.33% - 10px);
			margin-left: 10px;
			margin-top: 10px;
		}

		.variant .variant-properties select {
			width: 100%;
		}

		.photos-list {
			margin-bottom: 20px;
		}

		.photos-list > li {
			width: 100px;
			text-align: center;
		}

		.photos-list img {
			width: auto;
			max-height: 100%;
			vertical-align: middle;
		}

		.pl-thumb {
			height: 78px;
			line-height: 78px;
		}

		.photos-list .pl-placeholder {
			width: 88px;
			height: 78px;
		}

		.pl-placeholder:before {
			height: 78px;
		}

		.pl-thumb-del {
			position: relative;
		}

		.pl-thumb-el {
			display: inline-block;
			vertical-align: middle;
		}
		
		.pl-thumb-el.checkbox {
			padding-top: 3px;
		}

		.pl-thumb-title {
			position: absolute;
			border: 1px solid #ccc;
			background-color: #fff;
			width: 300px;
			padding: 10px;
			margin-top: 8px;
			z-index: 2;
			border-radius: 6px;
		}

		.pl-thumb-title.hidden {
			display: none;
		}

		.pl-thumb-title:before,
		.pl-thumb-title:after {
			content: '';
			display: block;
			position: absolute;
			border-style: solid;
			border-width: 0 8px 10px 8px;
			border-color: transparent transparent #ccc transparent;
			width: 0;
			height: 0;
			left: 16px;
			top: -10px;
		}

		.pl-thumb-title:after {
			border-bottom-color: #fff;
			top: -9px;
		}

		.pl-thumb-title textarea {
			resize: none;
		}
	</style>

	<div class="page-header product-variants">
		<h3><?=Yii::t('products', 'Variants')?></h3>
	</div>

	<?php /* <div class="form-group product-variants">
		<div class="col-md-3"></div>
		<div class="col-md-9">
			<div class="checkbox">
				<label for="form-bulk_variations">
					<input id="form-bulk_variations" type="checkbox" name="bulk_variants" value="1">
					<?=Yii::t('products', 'Variants bulk')?>
				</label>
			</div>
		</div>
	</div>

	<div id="bulk-variants" class="product-variants">
		<div class="bulk-variants-generator hidden">
			<?php if (empty($properties)) { ?>
			<div class="form-group">
				<div class="col-md-3"></div>
				<div class="col-md-9"><?=Yii::t('products', 'Select a category...')?></div>
			</div>
			<?php } else { ?>
			<?php $this->renderPartial('//ajax/bulkVariantProperties', array('properties' => $properties)); ?>
			<?php } ?>
		</div>
	</div>

	<hr> */ ?>

	<div id="product-variants" class="variants product-variants">
		<?php $index = 0; ?>
		<?php if (!empty($variants)) { ?>
		<?php foreach ($variants as $variant) { ?>
		<div class="variant" data-index="<?=$index?>">
			<!-- <a href="#" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-menu-hamburger"></span></a> -->
			<input type="hidden" name="variant[<?=$index?>][variant_id]" value="<?=CHtml::encode($variant['variant_id'])?>">

			<div class="form-group">
				<label for="form-variant-sku__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'SKU')?> / ID:</label>
				<div class="col-md-4">
					<input id="form-variant-sku__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_sku]" value="<?=CHtml::encode($variant['variant_sku'])?>" readonly>
				</div>
				<div class="col-md-4 form-control-static del-variant">
					<!-- <input class="del-input" type="hidden" name="variant[<?=$index?>][del_variant]" value="<?=CHtml::encode($variant['variant_id'])?>" disabled>
					<a class="text-muted" href="#"><small><span class="glyphicon glyphicon-remove"></span><span class="del-variant-tip"> удалить</span></small></a> -->
				</div>
			</div>

			<div class="form-group">
				<label for="form-variant-price__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Price')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-price__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_price]" value="<?=CHtml::encode($variant['variant_price'])?>" readonly>
						<span class="input-group-addon">грн.</span>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="form-variant-price-old__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Price old')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-price-old__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_price_old]" value="<?=CHtml::encode($variant['variant_price_old'])?>"><!-- readonly -->
						<span class="input-group-addon">грн.</span>
					</div>
				</div>
			</div>

			<input id="form-variant-price-type__<?=$index?>" name="variant[<?=$index?>][variant_price_type]" class="variant-price-type" type="hidden" value="item">
			<input id="form-variant-length__<?=$index?>" name="variant[<?=$index?>][variant_length]" type="hidden" value="">
			<input id="form-variant-width__<?=$index?>" name="variant[<?=$index?>][variant_width]" type="hidden" value="">
			<input id="form-variant-pack-size__<?=$index?>" name="variant[<?=$index?>][variant_pack_size]" type="hidden" value="">
			<input id="form-variant-pack-qty__<?=$index?>" name="variant[<?=$index?>][variant_pack_qty]" type="hidden" value="">

			<div class="form-group">
				<label for="form-variant-instock__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'In stock')?>:</label>
				<div class="col-md-3">
					<select id="form-variant-instock__<?=$index?>" name="variant[<?=$index?>][variant_instock]" class="form-control" disabled>
						<option value="in_stock"<?php if ($variant['variant_instock'] == 'in_stock') { ?> selected<?php } ?>><?=Yii::t('products', 'Product in stock')?></option>
						<option value="out_of_stock"<?php if ($variant['variant_instock'] == 'out_of_stock') { ?> selected<?php } ?>><?=Yii::t('products', 'Product out of stock')?></option>
						<!-- <option value="preorder"<?php if ($variant['variant_instock'] == 'preorder') { ?> selected<?php } ?>><?=Yii::t('products', 'Product preorder')?></option> -->
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="form-variant-stock-qty__<?=$index?>" class="col-md-3 control-label">Количество на складе:</label>
				<div class="col-md-1">
					<input id="form-variant-stock-qty__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_stock_qty]" value="<?=CHtml::encode($variant['variant_stock_qty'])?>" readonly>
				</div>
				<div class="col-md-6 form-control-static">
					<?php if (!empty($variant['stores'])) { ?>
					<?php foreach ($variant['stores'] as $store_index => $store) { ?>
					<?php if ($store_index > 0) { ?> | <?php } ?>
					<small><?=CHtml::encode($store['store_name'])?>: <?=$store['quantity']?></small>
					<?php } ?>
					<?php } ?>
				</div>
			</div>

			<div class="form-group">
				<label for="form-variant-weight__<?=$index?>" class="col-md-3 control-label">Вес:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-weight__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_weight]" value="<?=CHtml::encode($variant['variant_weight'])?>" readonly>
						<span class="input-group-addon">кг</span>
					</div>
				</div>
			</div>
			
			<?php /* <div class="form-group">
				<label for="form-variant-price-type__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Product price type')?>:</label>
				<div class="col-md-3">
					<select id="form-variant-price-type__<?=$index?>" name="variant[<?=$index?>][variant_price_type]" class="form-control variant-price-type">
						<option value="item"<?php if ($variant['variant_price_type'] == 'item') { ?> selected<?php } ?>><?=Yii::t('products', 'Price per item')?></option>
						<option value="package"<?php if ($variant['variant_price_type'] == 'package') { ?> selected<?php } ?>><?=Yii::t('products', 'Price per package')?></option>
						<option value="per_meter"<?php if ($variant['variant_price_type'] == 'per_meter') { ?> selected<?php } ?>><?=Yii::t('products', 'Price per meter')?></option>
					</select>
				</div>
			</div>

			<div class="form-group variant-package hidden">
				<label for="form-variant-length__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Length')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-length__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_length]" value="<?=CHtml::encode($variant['variant_length'])?>">
						<span class="input-group-addon">мм</span>
					</div>
				</div>
			</div>

			<div class="form-group variant-package hidden">
				<label for="form-variant-width__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Width')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-width__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_width]" value="<?=CHtml::encode($variant['variant_width'])?>">
						<span class="input-group-addon">мм</span>
					</div>
				</div>
			</div>

			<div class="form-group variant-package variant-per_meter hidden">
				<label for="form-variant-pack-size__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Package size')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-pack-size__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_pack_size]" value="<?=CHtml::encode($variant['variant_pack_size'])?>">
						<span class="input-group-addon">&nbsp;м<sup>2</sup></span>
					</div>
				</div>
			</div>

			<div class="form-group variant-package hidden">
				<label for="form-variant-pack-qty__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Package quantity')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-pack-qty__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_pack_qty]" value="<?=CHtml::encode($variant['variant_pack_qty'])?>">
						<span class="input-group-addon">шт.</span>
					</div>
				</div>
			</div> */ ?>

			<div class="form-group">
				<label for="form-variant-properties__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Properties')?>:</label>
				<div class="col-md-9 variant-properties form-inline" data-index="<?=$index?>">
					<?php if (!empty($variant_properties)) { ?>
					<?php foreach ($variant_properties as $property) { ?><div>
						<select class="form-control" name="variant[<?=$index?>][values][]" data-placeholder="<?=CHtml::encode($property['property_title'])?>" data-group="<?=CHtml::encode($property['property_title'])?>" disabled>
							<option value=""><?=CHtml::encode($property['property_title'])?></option>

							<?php if (!empty($property['values'])) { ?>
							<?php foreach ($property['values'] as $value) { ?>
							<option value="<?=$value['value_id']?>"<?php if (in_array($value['value_id'], $variant['values'])) { ?> selected<?php } ?>><?=CHtml::encode($value['value_title'])?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</div><?php } ?>
					<?php } else { ?>
					<div class="form-control-static"><?=Yii::t('products', 'No variants properties')?></div>
					<?php } ?>
				</div>
			</div>

			<?php /* <div class="form-group">
				<label for="form-variant-photo__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('app', 'Upload photos')?>:</label>
				<div class="col-md-8">
					<input id="form-variant-photo__<?=$index?>" type="file" name="variant[<?=$index?>][photos][]" multiple>
					<small><?=Yii::t('app', 'Images file requirements', array('{types}' => 'jpg, gif, png', '{size}' => '10 MB', '{total}' => 10))?></small>
				</div>
			</div> */ ?>

			<?php /* <div class="form-group">
				<div class="col-md-8 col-md-offset-3">
					<ul class="photos-list list-inline">
						<?php if (!empty($variant['photos'])) { ?>
						<?php 
							foreach ($variant['photos'] as $photo) {
								$photo_path = json_decode($photo['photo_path'], true);
								$photo_size = json_decode($photo['photo_size'], true);
						 ?><li>
							<div class="pl-thumb">
								<input type="hidden" name="variant[<?=$index?>][position][<?=$photo['photo_id']?>]" value="<?=$photo['position']?>">
								<img src="<?=Yii::app()->assetManager->getBaseUrl() . '/product/' . $product['product_id'] . '/variant/' . $variant['variant_id'] . '/' . $photo_path['thumb']['1x']?>" alt="" width="<?=$photo_size['thumb']['1x']['w']?>" height="<?=$photo_size['thumb']['1x']['h']?>">
							</div>
							<div class="pl-thumb-del text-center">
								<div class="pl-thumb-el checkbox">
									<label for="form-del-variant-photo-<?=$photo['photo_id']?>">
										<input id="form-del-variant-photo-<?=$photo['photo_id']?>" type="checkbox" name="variant[<?=$index?>][delete][]" value="<?=$photo['photo_id']?>"> <small>del</small>
									</label>
								</div>
							</div>
						</li><?php } ?>
						<?php } ?>
					</ul>
				</div>
			</div> */ ?>
		</div>
		<?php $index++; } ?>
		<?php } ?>
		<div class="variant variant-start hidden" data-index="<?=$index?>">
			<a href="#" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-menu-hamburger"></span></a>

			<?php /* <div class="form-group">
				<label for="form-variant-sku__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'SKU')?>:</label>
				<div class="col-md-4">
					<input id="form-variant-sku__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_sku]" value="">
				</div>
			</div> */ ?>

			<div class="form-group">
				<label for="form-variant-price__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Price')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-price__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_price]" value="">
						<span class="input-group-addon">грн.</span>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label for="form-variant-price-old__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Price old')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-price-old__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_price_old]" value="">
						<span class="input-group-addon">грн.</span>
					</div>
				</div>
			</div>

			<input id="form-variant-sku__<?=$index?>" name="variant[<?=$index?>][variant_sku]" type="hidden" value="">
			<input id="form-variant-price-type__<?=$index?>" name="variant[<?=$index?>][variant_price_type]" class="variant-price-type" type="hidden" value="item">
			<input id="form-variant-length__<?=$index?>" name="variant[<?=$index?>][variant_length]" type="hidden" value="">
			<input id="form-variant-width__<?=$index?>" name="variant[<?=$index?>][variant_width]" type="hidden" value="">
			<input id="form-variant-pack-size__<?=$index?>" name="variant[<?=$index?>][variant_pack_size]" type="hidden" value="">
			<input id="form-variant-pack-qty__<?=$index?>" name="variant[<?=$index?>][variant_pack_qty]" type="hidden" value="">
			
			<div class="form-group">
				<label for="form-variant-instock__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'In stock')?>:</label>
				<div class="col-md-3">
					<select id="form-variant-instock__<?=$index?>" name="variant[<?=$index?>][variant_instock]" class="form-control">
						<option value="in_stock"><?=Yii::t('products', 'Product in stock')?></option>
						<option value="out_of_stock"><?=Yii::t('products', 'Product out of stock')?></option>
						<!-- <option value="preorder"><?=Yii::t('products', 'Product preorder')?></option> -->
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="form-variant-stock-qty__<?=$index?>" class="col-md-3 control-label">Количество на складе:</label>
				<div class="col-md-1">
					<input id="form-variant-stock-qty__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_stock_qty]" value="">
				</div>
			</div>

			<div class="form-group">
				<label for="form-variant-weight__<?=$index?>" class="col-md-3 control-label">Вес:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-weight__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_weight]" value="">
						<span class="input-group-addon">кг</span>
					</div>
				</div>
			</div>

			<?php /* <div class="form-group">
				<label for="form-variant-price-type__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Product price type')?>:</label>
				<div class="col-md-3">
					<select id="form-variant-price-type__<?=$index?>" name="variant[<?=$index?>][variant_price_type]" class="form-control variant-price-type">
						<option value="item"><?=Yii::t('products', 'Price per item')?></option>
						<option value="package"><?=Yii::t('products', 'Price per package')?></option>
						<option value="per_meter"><?=Yii::t('products', 'Price per meter')?></option>
					</select>
				</div>
			</div>

			<div class="form-group variant-package hidden">
				<label for="form-variant-length__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Length')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-length__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_length]" value="">
						<span class="input-group-addon">мм</span>
					</div>
				</div>
			</div>

			<div class="form-group variant-package hidden">
				<label for="form-variant-width__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Width')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-width__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_width]" value="">
						<span class="input-group-addon">мм</span>
					</div>
				</div>
			</div>

			<div class="form-group variant-package variant-per_meter hidden">
				<label for="form-variant-pack-size__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Package size')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-pack-size__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_pack_size]" value="">
						<span class="input-group-addon">&nbsp;м<sup>2</sup></span>
					</div>
				</div>
			</div>

			<div class="form-group variant-package hidden">
				<label for="form-variant-pack-qty__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Package quantity')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<input id="form-variant-pack-qty__<?=$index?>" class="form-control" type="text" name="variant[<?=$index?>][variant_pack_qty]" value="">
						<span class="input-group-addon">шт.</span>
					</div>
				</div>
			</div> */ ?>

			<div class="form-group">
				<label for="form-variant-properties__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Properties')?>:</label>
				<div class="col-md-9 variant-properties form-inline" data-index="<?=$index?>">
					<?php if (!empty($variant_properties)) { ?>
					<?php foreach ($variant_properties as $property) { ?><div>
						<select class="form-control" name="variant[<?=$index?>][values][]" data-placeholder="<?=CHtml::encode($property['property_title'])?>" data-group="<?=CHtml::encode($property['property_title'])?>">
							<option value=""><?=CHtml::encode($property['property_title'])?></option>

							<?php if (!empty($property['values'])) { ?>
							<?php foreach ($property['values'] as $value) { ?>
							<option value="<?=$value['value_id']?>"><?=CHtml::encode($value['value_title'])?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</div><?php } ?>
					<?php } else { ?>
					<div class="form-control-static"><?=Yii::t('products', 'No variants properties')?></div>
					<?php } ?>
				</div>
			</div>

			<?php /* <div class="form-group">
				<label for="form-variant-photo__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('app', 'Upload photos')?>:</label>
				<div class="col-md-8">
					<input id="form-variant-photo__<?=$index?>" type="file" name="variant[<?=$index?>][photos][]" multiple>
					<small><?=Yii::t('app', 'Images file requirements', array('{types}' => 'jpg, gif, png', '{size}' => '10 MB', '{total}' => 10))?></small>
				</div>
			</div> */ ?>
		</div>
	</div>

	<!-- <div id="add-variant" class="add-variant form-group product-variants">
		<div class="col-md-9 col-md-push-3">
			<button type="button" class="btn btn-default"><?=Yii::t('products', 'Add variant')?></button>
			&nbsp;
			<button type="button" class="btn btn-danger"><?=Yii::t('products', 'Remove variants')?></button>
		</div>
	</div> -->

	<?php /*
	<div class="page-header">
		<h3><?=Yii::t('products', 'Options')?></h3>
	</div>

	<div id="product-options" class="product-options">
		<?php $index = 0; ?>
		<?php if (!empty($options)) { ?>
		<?php foreach ($options as $option) { ?>
		<div class="option" data-index="<?=$index?>">
			<input type="hidden" name="option[<?=$index?>][option_id]" value="<?=CHtml::encode($option['option_id'])?>">

			<div class="form-group">
				<label for="form-option-property__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Property')?>:</label>
				<div class="col-md-6 option-property">
					<?php if (!empty($option_properties)) { ?>
					<select class="form-control" name="option[<?=$index?>][value_id]" data-placeholder="---">
						<option value="">---</option>
						<?php foreach ($option_properties as $property) { ?>
						<optgroup label="<?=CHtml::encode($property['property_title'])?>">
							<?php if (!empty($property['values'])) { ?>
							<?php foreach ($property['values'] as $value) { ?>
							<option value="<?=$value['value_id']?>"<?php if ($option['value_id'] == $value['value_id']) { ?> selected<?php } ?>><?=CHtml::encode($value['value_title'])?></option>
							<?php } ?>
							<?php } ?>
						</optgroup>
						<?php } ?>
					</select>
					<?php } else { ?>
					<div class="form-control-static"><?=Yii::t('products', 'Select a category...')?></div>
					<?php } ?>
				</div>
				<div class="col-md-3 form-control-static del-option">
					<input class="del-input" type="hidden" name="option[<?=$index?>][del_option]" value="<?=CHtml::encode($option['option_id'])?>" disabled>
					<a class="text-muted" href="#"><small><span class="glyphicon glyphicon-remove"></span><span class="del-option-tip"> удалить</span></small></a>
				</div>
			</div>

			<div class="form-group">
				<label for="form-option-price__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Price')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<span class="input-group-addon">грн.</span>
						<input id="form-option-price__<?=$index?>" class="form-control" type="text" name="option[<?=$index?>][option_price]" value="<?=CHtml::encode($option['option_price'])?>">
					</div>
				</div>
			</div>
		</div>
		<?php $index++; } ?>
		<?php } ?>
		<div class="option option-start" data-index="<?=$index?>">
			<div class="form-group">
				<label for="form-option-property__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Property')?>:</label>
				<div class="col-md-6 option-property">
					<?php if (!empty($option_properties)) { ?>
					<select class="form-control" name="option[<?=$index?>][value_id]" data-placeholder="---">
						<option value="">---</option>
						<?php foreach ($option_properties as $property) { ?>
						<optgroup label="<?=CHtml::encode($property['property_title'])?>">
							<?php if (!empty($property['values'])) { ?>
							<?php foreach ($property['values'] as $value) { ?>
							<option value="<?=$value['value_id']?>"><?=CHtml::encode($value['value_title'])?></option>
							<?php } ?>
							<?php } ?>
						</optgroup>
						<?php } ?>
					</select>
					<?php } else { ?>
					<div class="form-control-static"><?=Yii::t('products', 'Select a category...')?></div>
					<?php } ?>
				</div>
			</div>

			<div class="form-group">
				<label for="form-option-price__<?=$index?>" class="col-md-3 control-label"><?=Yii::t('products', 'Price')?>:</label>
				<div class="col-md-2">
					<div class="input-group">
						<span class="input-group-addon">грн.</span>
						<input id="form-option-price__<?=$index?>" class="form-control" type="text" name="option[<?=$index?>][option_price]" value="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="add-option" class="add-option form-group">
		<div class="col-md-9 col-md-push-3">
			<button type="button" class="btn btn-default"><?=Yii::t('products', 'Add option')?></button>
			&nbsp;
			<button type="button" class="btn btn-danger"><?=Yii::t('products', 'Remove options')?></button>
		</div>
	</div>
	*/ ?>

	<div class="page-header">
		<h3><?=Yii::t('products', 'Properties')?></h3>
	</div>

	<div id="product-properties" class="product-properties">
		<?php if (!empty($properties)) { ?>
		<?php
			$selected_values = array();

			if (!empty($product_properties)) {
				foreach ($product_properties as $product_property) {
					$property_id = $product_property['property_id'];
					$value_id = $product_property['value_id'];

					if (isset($properties[$property_id]['values'][$value_id])) {
						$selected_values[$value_id] = $value_id;
					}
				}
			}

			$total_selected_values = count($selected_values);
		?>
		<?php if ($total_selected_values) { ?>
		<?php for ($i = 0; $i < $total_selected_values; $i++) { $has_selected = false; ?>
		<div class="form-group">
			<div class="col-md-3"></div>
			<div class="col-md-4">
				<select class="form-control" name="property[]" data-placeholder="---">
					<option value="">---</option>
					<?php foreach ($properties as $property) { ?>
					<optgroup label="<?=CHtml::encode($property['property_title'])?>">
						<?php if (!empty($property['values'])) { ?>
						<?php foreach ($property['values'] as $value) { ?>
						<option value="<?=$value['value_id']?>"<?php if (!$has_selected && isset($selected_values[$value['value_id']])) { $has_selected = true; unset($selected_values[$value['value_id']]); ?> selected<?php } ?>><?=CHtml::encode($value['value_title'])?></option>
						<?php } ?>
						<?php } ?>
					</optgroup>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2" style="padding-top: 7px">
				<a class="text-muted del-property" href="#"><small style="margin-right: 4px"><span class="glyphicon glyphicon-remove"></span></small>удалить</a>
			</div>
		</div>
		<?php } ?>
		<?php } ?>

		<div class="form-group">
			<div class="col-md-3"></div>
			<div class="col-md-4">
				<select class="form-control" name="property[]" data-placeholder="---">
					<option value="">---</option>
					<?php foreach ($properties as $property) { ?>
					<optgroup label="<?=CHtml::encode($property['property_title'])?>">
						<?php if (!empty($property['values'])) { ?>
						<?php foreach ($property['values'] as $value) { ?>
						<option value="<?=$value['value_id']?>"><?=CHtml::encode($value['value_title'])?></option>
						<?php } ?>
						<?php } ?>
					</optgroup>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-3">&nbsp;</div>
			<div class="col-md-9">
				<button id="add-property" type="button" class="btn btn-default"><?=Yii::t('products', 'Add property')?></button>
			</div>
		</div>
		<?php } else { ?>
		<div><?=Yii::t('products', 'Select a category...')?></div>
		<?php } ?>
	</div>

	<?php /*
	<div class="page-header">
		<h3><?=Yii::t('products', 'Video, VR, 360')?></h3>
	</div>

	<div class="form-group">
		<label for="form-product_video" class="col-md-3 control-label"><?=Yii::t('products', 'Video (YouTube link)')?>:</label>
		<div class="col-md-6">
			<input id="form-product_video" class="form-control" type="text" name="product[product_video]" value="<?=CHtml::encode($product['product_video'])?>">
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-product_360_view" class="col-md-3 control-label"><?=Yii::t('products', 'VR (Roundme link)')?>:</label>
		<div class="col-md-6">
			<input id="form-product_360_view" class="form-control" type="text" name="product[product_360_view]" value="<?=CHtml::encode($product['product_360_view'])?>">
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-product_vr_view" class="col-md-3 control-label"><?=Yii::t('products', '360 (Roundme link)')?>:</label>
		<div class="col-md-6">
			<input id="form-product_vr_view" class="form-control" type="text" name="product[product_vr_view]" value="<?=CHtml::encode($product['product_vr_view'])?>">
		</div>
	</div>
	*/ ?>

	<div class="page-header">
		<h3>Видео</h3>
	</div>

	<div class="form-group">
		<label for="form-product_video" class="col-md-3 control-label"><?=Yii::t('products', 'Video')?>:</label>
		<div class="col-md-6">
			<?php
				if (!empty($product['product_video'])) {
					$video = json_decode($product['product_video'], true);
					$video_url = $assetsUrl . '/' . $product['product_id'] . '/' . $video['web_file'];
			?>
			<div style="width: 360px">
				<div class="embed-responsive embed-responsive-16by9" style="padding-bottom: <?=$video['params']['height'] / $video['params']['width'] * 100?>%">
					<video src="<?=$video_url?>" controls></video>
				</div>
			</div>
			<div class="checkbox">
				<label for="del_product_video"><input id="del_product_video" type="checkbox" name="product[del_product_video]" value="<?=$video['file']?>"> удалить видео</label>
			</div>
			<br>
			<?php }	?>
			
			<input id="form-product_video" type="file" name="product[product_video]">
			<small><?=Yii::t('app', 'File requirements', array('{types}' => 'mp4, mov, avi, webm', '{size}' => '100&nbsp;MB'))?></small>
		</div>
	</div>

	<div class="page-header">
		<h3><?=Yii::t('app', 'Gallery')?></h3>
	</div>

	<ul id="photos-list" class="photos-list list-inline">
		<?php if (!empty($gallery)) { ?>
		<?php 
			foreach ($gallery as $photo) {
				$photo_path = json_decode($photo['photo_path'], true);
				$photo_size = json_decode($photo['photo_size'], true);
		 ?><li>
			<div class="pl-thumb">
				<input type="hidden" name="gallery_position[<?=$photo['photo_id']?>]" value="<?=$photo['position']?>">
				<img src="<?=Yii::app()->assetManager->getBaseUrl() . '/product/' . $product['product_id'] . '/' . $photo_path['thumb']['1x']?>" alt="" width="<?=$photo_size['thumb']['1x']['w']?>" height="<?=$photo_size['thumb']['1x']['h']?>">
			</div>
			<div class="pl-thumb-del text-center">
				<div class="pl-thumb-el checkbox">
					<label for="form-del-photo-<?=$photo['photo_id']?>">
						<input id="form-del-photo-<?=$photo['photo_id']?>" type="checkbox" name="del_gallery[]" value="<?=$photo['photo_id']?>"> <small>del</small>
					</label>
				</div>
			</div>
		</li><?php } ?>
		<?php } ?>
	</ul>

	<div class="form-group">
		<label for="form-gallery" class="col-md-3 control-label"><?=Yii::t('app', 'Upload photos')?>:</label>
		<div class="col-md-8">
			<input id="form-gallery" type="file" name="product[gallery]" multiple>
			<small><?=Yii::t('app', 'Images file requirements', array('{types}' => 'jpg, gif, png', '{size}' => '10 MB', '{total}' => 10))?></small>
			<div id="upload-container" style="margin-top: 10px" data-url="<?=$this->createUrl('ajax/uploadproductphoto')?>"></div>
		</div>
	</div>

	<div id="section-description" class="page-header">
		<h3><?=Yii::t('products', 'Description')?></h3>
	</div>

	<div class="form-group">
		<label for="form-product_tip" class="col-md-3 control-label">Fun fact:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_tip_<?=$code?>" aria-controls="form-product_tip_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_tip_<?=$code?>">
						<textarea class="form-control" name="product_lang[product_tip][<?=$code?>]" rows="4"><?=CHtml::encode($product[$code]['product_tip'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_description" class="col-md-3 control-label"><?=Yii::t('products', 'Description')?>:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_description_<?=$code?>" aria-controls="form-product_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_description_<?=$code?>">
						<textarea class="form-control form-redactor form-redactor-counter" name="product_lang[product_description][<?=$code?>]" rows="4"><?=CHtml::encode($product[$code]['product_description'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="page-header">
		<h3>Размерная таблица</h3>
	</div>

	<div class="form-group">
		<label for="form-size_id" class="col-md-3 control-label">Категория:</label>
		<div class="col-md-4">
			<select id="form-size_id" class="form-control" name="product[size_id]">
				<option value="0">---</option>
				<?php if (!empty($sizes)) { ?>
				<?php foreach ($sizes as $size) { ?>
				<option value="<?=$size['size_id']?>"<?php if ($product['size_id'] == $size['size_id']) { ?> selected<?php } ?>><?=CHtml::encode($size['size_name'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_faq" class="col-md-3 control-label">Своя размерная таблица:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_faq_<?=$code?>" aria-controls="form-product_faq_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_faq_<?=$code?>">
						<textarea class="form-control form-redactor" name="product_lang[product_faq][<?=$code?>]" rows="4"><?=CHtml::encode($product[$code]['product_faq'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="page-header">
		<h3>Гарантия/возврат</h3>
	</div>

	<div class="form-group">
		<label for="form-reclamation_id" class="col-md-3 control-label">Категория:</label>
		<div class="col-md-4">
			<select id="form-reclamation_id" class="form-control" name="product[reclamation_id]">
				<option value="0">---</option>
				<?php if (!empty($reclamations)) { ?>
				<?php foreach ($reclamations as $reclamation) { ?>
				<option value="<?=$reclamation['reclamation_id']?>"<?php if ($product['reclamation_id'] == $reclamation['reclamation_id']) { ?> selected<?php } ?>><?=CHtml::encode($reclamation['reclamation_name'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_reclamation" class="col-md-3 control-label">Возврат:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_reclamation_<?=$code?>" aria-controls="form-product_reclamation_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_reclamation_<?=$code?>">
						<textarea class="form-control form-redactor" name="product_lang[product_reclamation][<?=$code?>]" rows="4"><?=CHtml::encode($product[$code]['product_reclamation'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="page-header">
		<h3>Уход</h3>
	</div>

	<div class="form-group">
		<label for="form-care_id" class="col-md-3 control-label">Категория:</label>
		<div class="col-md-4">
			<select id="form-care_id" class="form-control" name="product[care_id]">
				<option value="0">---</option>
				<?php if (!empty($cares)) { ?>
				<?php foreach ($cares as $care) { ?>
				<option value="<?=$care['care_id']?>"<?php if ($product['care_id'] == $care['care_id']) { ?> selected<?php } ?>><?=CHtml::encode($care['care_name'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_care" class="col-md-3 control-label">Уход:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_care_<?=$code?>" aria-controls="form-product_care_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_care_<?=$code?>">
						<textarea class="form-control form-redactor" name="product_lang[product_care][<?=$code?>]" rows="4"><?=CHtml::encode($product[$code]['product_care'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div id="section-seo" class="page-header">
		<h3><?=Yii::t('app', 'SEO data')?></h3>
	</div>

	<div class="form-group">
		<label for="form-product_meta_title" class="col-md-3 control-label"><?=Yii::t('app', 'No index')?>:</label>
		<div class="col-md-6">
			<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
			<div class="checkbox">
				<label for="form-product_no_index<?=$code?>">
					<input id="form-product_no_index<?=$code?>" type="checkbox" name="product_lang[product_no_index][<?=$code?>]" value="1"<?php if ($product[$code]['product_no_index'] == 1) { ?> checked<?php } ?>> <?=$lang?>
				</label>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_meta_title" class="col-md-3 control-label">Meta title:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_meta_title_<?=$code?>" aria-controls="form-product_meta_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_meta_title_<?=$code?>">
						<input class="form-control" type="text" name="product_lang[product_meta_title][<?=$code?>]" value="<?=CHtml::encode($product[$code]['product_meta_title'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_meta_keywords" class="col-md-3 control-label">Meta keywords:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_meta_keywords_<?=$code?>" aria-controls="form-product_meta_keywords_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_meta_keywords_<?=$code?>">
						<textarea class="form-control" name="product_lang[product_meta_keywords][<?=$code?>]" rows="2"><?=CHtml::encode($product[$code]['product_meta_keywords'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_meta_description" class="col-md-3 control-label">Meta description:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-product_meta_description_<?=$code?>" aria-controls="form-product_meta_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-product_meta_description_<?=$code?>">
						<textarea class="form-control" name="product_lang[product_meta_description][<?=$code?>]" rows="4"><?=CHtml::encode($product[$code]['product_meta_description'])?></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<div class="btn-group-vertical btn-insert-link" role="group" aria-label="<?=Yii::t('app', 'Insert/remove a link label')?>">
  <button id="typograph-link-btn" title="<?=Yii::t('app', 'Typography text btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-text-size"></i></button>
  <button id="insert-link-btn" title="<?=Yii::t('app', 'Insert a link btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
  <button id="remove-link-btn" title="<?=Yii::t('app', 'Remove all links btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
</div>

<!-- Insert Link Modal -->
<div class="modal fade" id="insertLinkModal" tabindex="-1" role="dialog" aria-labelledby="insertLinkModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="<?=Yii::t('app', 'Close')?>"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="insertLinkModalLabel"><?=Yii::t('app', 'Insert a link label')?></h4>
			</div>
			<div class="modal-body">
			<form id="insert-link">
				<div class="form-group">
					<label for="link-text" class="control-label"><?=Yii::t('app', 'Link text')?>:</label>
					<input type="text" class="form-control" id="link-text">
				</div>
				<div class="form-group">
					<label for="link-url" class="control-label"><?=Yii::t('app', 'Link tip')?>:</label>
					<input type="text" class="form-control" id="link-url">
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label for="link-blank">
							<input id="link-blank" type="checkbox" value="1"> <?=Yii::t('app', 'Open in new tab')?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="link-title" class="control-label"><?=Yii::t('app', 'Link tip (title)')?>:</label>
					<input type="text" class="form-control" id="link-title">
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=Yii::t('app', 'Cancel btn')?></button>
				<button id="do-insert" type="button" class="btn btn-primary"><?=Yii::t('app', 'Insert btn')?></button>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var form = $("#manage-product"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	var variants_init = false;

	$('#form-product_price_type').on('change', function() {
		var value = $(this).val();

		if (value == 'piece' || value == 'package') {
			$('.product-package').removeClass('hidden');
			$('.product-set').addClass('hidden');
			$('.product-item').find(':input').prop('disabled', true);
			$('.product-variants').addClass('hidden');
			$('.product-per_meter').find('.input-group-addon').html('&nbsp;м<sup>2</sup>');
		} else if (value == 'per_meter') {
			$('.product-package').addClass('hidden');
			$('.product-set').addClass('hidden');
			$('.product-item').find(':input').prop('disabled', true);
			$('.product-variants').addClass('hidden');
			$('.product-per_meter').removeClass('hidden').find('.input-group-addon').html('м');
		} else if (value == 'variants') {
			$('.product-variants').removeClass('hidden');
			$('.product-set').addClass('hidden');
			$('.product-item').find(':input').prop('disabled', true);
			$('.product-package').addClass('hidden');

			if (variants_init && variants.find('select').length) {
				variants_init = true;
				variants.find('select').addClass('select2').select2(select2_variants_settings);
			}
		} else if (value == 'set') {
			$('.product-set').removeClass('hidden');
			$('.product-variants').addClass('hidden');
			$('.product-item').find(':input').prop('disabled', true);
			$('.product-package').addClass('hidden');
		} else {
			$('.product-item').find(':input').prop('disabled', false);
			$('.product-set').addClass('hidden');
			$('.product-package').addClass('hidden');
			$('.product-variants').addClass('hidden');
		}
	}).change();

	$(document).on('change', '.variant-price-type', function() {
		var value = $(this).val(),
			variant = $(this).parents('.variant');

		if (value == 'package') {
			variant.find('.variant-package').removeClass('hidden');
			variant.find('.variant-per_meter').find('.input-group-addon').html('&nbsp;м<sup>2</sup>');
		} else if (value == 'per_meter') {
			variant.find('.variant-package').addClass('hidden');
			variant.find('.variant-per_meter').removeClass('hidden').find('.input-group-addon').html('м');
		} else {
			variant.find('.variant-package').addClass('hidden');
		}
	}).find('.variant-price-type').change();

	var variants = $('#product-variants'),
		bulk_variants = $('#bulk-variants'),
		variant_html = variants.find('.variant-start').html(),
		variant_first = variants.find('.variant-start').data('index'),
		variant_index = variant_first + 1,
		regexp_quote = function(str) {
			return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
		},
		select2_variants_settings = {
			allowClear: true,
			minimumResultsForSearch: 10,
			templateSelection: function(data, container) {
				if ($(data.element).parent().data('group')) {
					return $('<span><strong>' + $(data.element).parent().data('group') + ':</strong> ' + data.text + '</span>');
				} else {
					return $.trim(data.text);
				}
			}
		},
		options = $('#product-options'),
		option_html = options.find('.option-start').html(),
		option_index = options.find('.option').length,
		option_first = option_index - 1,
		properties = $('#product-properties'),
		properties_html,
		select2_settings = {
			allowClear: true,
			minimumResultsForSearch: 20,
			templateSelection: function(data, container) {
				if ($(data.element).parent().attr('label')) {
					return $('<span><strong>' + $(data.element).parent().attr('label') + ':</strong> ' + data.text + '</span>');
				} else {
					return $.trim(data.text);
				}
			}
		},
		addProperty = function() {
			var html = '<div class="form-group">';
			html += '<div class="col-md-3">';
			html += '</div>';
			html += '<div class="col-md-4">';
			html += properties_html;
			html += '</div>';
			html += '<div class="col-md-2" style="padding-top: 7px">';
			html += '<a class="text-muted del-property" href="#"><small style="margin-right: 4px"><span class="glyphicon glyphicon-remove"></span></small><?=Yii::t('app', 'Delete btn')?></a>';
			html += '</div>';
			html += '</div>';

			return html;
		};

	$('#form-bulk_variations').on('change', function() {
		if ($(this).is(':checked')) {
			bulk_variants.children().removeClass('hidden').find(':input').prop('disabled', false);

			bulk_variants.find('.select2').select2({
				minimumResultsForSearch: 10,
				width: '100%'
			});
		} else {
			bulk_variants.children().addClass('hidden').find(':input').prop('disabled', true);
			bulk_variants.find('.select2').select2('destroy');
		}
	});

	$('#add-variant').find('button:first').click(function(e) {
		e.preventDefault();

		var replace_id = new RegExp(regexp_quote('__' + variant_first), "g"),
			replace_input = new RegExp(regexp_quote('[' + variant_first + ']'), "g"),
			new_id = '__' + variant_index,
			new_input = '[' + variant_index + ']',
			new_variant_html = $('<div class="variant"/>').html(variant_html.replace(replace_id, new_id).replace(replace_input, new_input));

		new_variant_html.find('.form-group:first')
			.append('<div class="col-md-3 form-control-static del-variant"><a class="text-muted" href="#"><small><span class="glyphicon glyphicon-remove"></span><span class="del-variant-tip"> удалить</span></small></a></div>');
		
		variants.append(new_variant_html);
		variants.find('.variant:last').find('select:not(.variant-price-type)')
			.addClass('select2').select2(select2_variants_settings);

		variant_index++;
	}).next().click(function(e) {
		e.preventDefault();

		bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
			if (result) {
				variants.find('.del-variant a').click();
			}
		});
	});

	variants.on('click', '.del-variant a', function(e) {
		e.preventDefault();

		if ($(this).prev().length) {
			if ($(this).parents('.variant').hasClass('remove')) {
				$(this).parents('.variant').removeClass('remove').find('input:not(.del-input), select').prop('disabled', false);
				$(this).parents('.variant').find('.del-input').prop('disabled', true);
			} else {
				$(this).parents('.variant').addClass('remove').find('input:not(.del-input), select').prop('disabled', true);
				$(this).parents('.variant').find('.del-input').prop('disabled', false);
			}
		} else {
			$(this).parents('.variant').remove();
		}

		return false;
	});

	variants.sortable({
		handle: '.btn-sm',
		items: '.variant',
		placeholder: 'v-placeholder',
		cursor: 'move',
		zIndex: 1019,
		opacity: .7,
		update: function(event, ui) {
			variants.find('.variant').each(function(index) {
				$(this).find(':input').each(function() {
					var name = $(this).attr('name');
					name.replace(/\[\d+\]/g, '[' + index + ']');
					$(this).attr('name', name.replace(/\[\d+\]/g, '[' + index + ']'));
				});
			});
		}
	});

	$('#add-option').find('button:first').click(function(e) {
		e.preventDefault();

		var replace_id = new RegExp(regexp_quote('__' + option_first), "g"),
			replace_input = new RegExp(regexp_quote('[' + option_first + ']'), "g"),
			new_id = '__' + option_index,
			new_input = '[' + option_index + ']',
			new_option_html = $('<div class="option"/>').html(option_html.replace(replace_id, new_id).replace(replace_input, new_input));

		new_option_html.find('.form-group:first')
			.append('<div class="col-md-3 form-control-static del-option"><a class="text-muted" href="#"><small><span class="glyphicon glyphicon-remove"></span><span class="del-option-tip"> удалить</span></small></a></div>');
		
		options.append(new_option_html);
		options.find('.option:last').find('select')
			.addClass('select2').select2(select2_settings);

		option_index++;
	}).next().click(function(e) {
		e.preventDefault();

		bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
			if (result) {
				options.find('.del-option a').click();
			}
		});
	});

	options.on('click', '.del-option a', function(e) {
		e.preventDefault();

		if ($(this).prev().length) {
			if ($(this).parents('.option').hasClass('remove')) {
				$(this).parents('.option').removeClass('remove').find('input:not(.del-input), select').prop('disabled', false);
				$(this).parents('.option').find('.del-input').prop('disabled', true);
			} else {
				$(this).parents('.option').addClass('remove').find('input:not(.del-input), select').prop('disabled', true);
				$(this).parents('.option').find('.del-input').prop('disabled', false);
			}
		} else {
			$(this).parents('.option').remove();
		}

		return false;
	});

	setTimeout(function() {
		if (!variants.hasClass('hidden') && variants.find('select').length) {
			variants_init = true;
			variants.find('select:not(.variant-price-type)').addClass('select2').select2(select2_variants_settings);
		}

		options.find('select').addClass('select2').select2(select2_settings);

		properties.find('select').addClass('select2');
		properties_html = properties.find('select:last')[0].outerHTML;
		properties.find('select').select2(select2_settings);
	}, 21);

	properties.on('click', '#add-property', function(e) {
		e.preventDefault();
		this.blur();

		var html = addProperty();
		$(this).parent().parent().before(html);
		$(this).parent().parent().prev().find('.select2').select2(select2_settings);
	});
	
	properties.on('click', '.del-property', function(e) {
		e.preventDefault();

		$(this).parent().parent().remove();
	});

	$('#form-category_id').on('change', function() {
		variants.find('.variant:not(.variant-start)').remove();
		options.find('.option:not(.option-start)').remove();

		if ($(this).val() == '0') {
			bulk_variants.children().html('<div class="form-group"><div class="col-md-3"></div><div class="col-md-9"><?=Yii::t('products', 'Select a category...')?></div></div>');

			variants.find('.variant-properties').html('<div class="form-control-static"><?=Yii::t('products', 'Select a category...')?></div>');
			variant_html = variants.find('.variant-start').html();

			options.find('.option-property').html('<div class="form-control-static"><?=Yii::t('products', 'Select a category...')?></div>');
			option_html = options.find('.option-start').html();

			properties.html('<div><?=Yii::t('products', 'Select a category...')?></div>');
		} else {
			var category = $(this);
			
			category.prop('disabled', true);
			form.find('button').prop('disabled', true);
			variants.find(':input').prop('disabled', true);
			options.find(':input').prop('disabled', true);
			properties.find('select').prop('disabled', true);

			// send request
			$.ajax({
				type: "POST", 
				url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/properties', 
				data: $.param({
					category_id: category.val(),
					index: variants.find('.variant-start .variant-properties').data('index')
				}), 
				cache: false, 
				dataType: "json",
				success: function(data) {
					category.prop('disabled', false);
					variants.find(':input').prop('disabled', false);
					options.find(':input').prop('disabled', false);
					form.find('button').prop('disabled', false);

					// reset properties
					properties_html = '';
					
					if (!data.total) {
						bulk_variants.children().html('<div class="form-group"><div class="col-md-3"></div><div class="col-md-9"><?=Yii::t('products', 'Select a category...')?></div></div>');

						variants.find('.variant-properties').html('<div class="form-control-static"><?=Yii::t('products', 'Select a category...')?></div>');
						variant_html = variants.find('.variant-start').html();

						options.find('.option-property').html('<div class="form-control-static"><?=Yii::t('products', 'Select a category...')?></div>');
						option_html = options.find('.option-start').html();

						properties.html('<div><?=Yii::t('products', 'Select a category...')?></div>');
					} else {
						bulk_variants.children().html(data.bulk_variants_html);
						
						if ($('#form-bulk_variations').is(':checked')) {
							bulk_variants.find('.select2').select2({
								minimumResultsForSearch: 10,
								width: '100%'
							});
						}

						variants.find('.variant-properties').html(data.variants_html);
						variant_html = variants.find('.variant-start').html();

						if (variants.find('select').length) {
							variants_init = true;
							variants.find('select:not(.variant-price-type)').select2(select2_variants_settings);
						}

						var replace_option_input = new RegExp(regexp_quote('[0]'), "g"),
							new_option_input = '[' + options.find('.option-start').data('index') + ']';
						
						data.options_html = data.options_html.replace(replace_option_input, new_option_input);

						options.find('.option-property').html(data.options_html);
						option_html = options.find('.option-start').html();

						if (options.find('select').length) {
							options.find('select').select2(select2_settings);
						}

						properties_html = data.html;

						var html = '<div class="form-group">';
						html += '<div class="col-md-3">';
						html += '</div>';
						html += '<div class="col-md-4">';
						html += properties_html;
						html += '</div>';
						html += '</div>';

						html += '<div class="form-group">';
						html += '<div class="col-md-3">&nbsp;';
						html += '</div>';
						html += '<div class="col-md-9">';
						html += '<button id="add-property" type="button" class="btn btn-default"><?=Yii::t('products', 'Add property')?></button>';
						html += '</div>';
						html += '</div>';

						properties.html(html).find('.select2').select2(select2_settings);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					category.prop('disabled', false);
					form.find('button').prop('disabled', false);
					variants.find(':input').prop('disabled', false);
					options.find(':input').prop('disabled', false);
					properties.find('select').prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Request error')?>");
					
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});

	$('#typograph-link-btn').on('click', function(e) {
		var input = focused,
			text = input.val(),
			btn = $(this);

		if (focused != null) {
			btn.prop('disabled', true);
			
			$.ajax({type: "POST", url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/typograph', data: $.param({text: focused.val()}), cache: false, dataType: "json",
				success: function(data) {
					btn.prop('disabled', false);
					
					if (data.error == 'Y') {
						bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					}
					else {
						if (focused.hasClass('form-redactor')) {
							$('#' + focused[0].id).redactor('code.set', data.text);
						}
						else {
							focused.val(data.text);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					btn.prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});
	
	$('#insert-link-btn').on('click', function(e) {
		var input = focused,
			len = input.val().length,
			start = input[0].selectionStart,
			end = input[0].selectionEnd,
			selectedText = input.val().substring(start, end);

		$('#link-text').val(selectedText);
		$('#link-url').val('');
		$('#link-title').val('');
		$('#link-blank').prop('checked', false);

		if (focused != null) {
			$('#insertLinkModal').modal('show');
		}
	});

	$('#remove-link-btn').on('click', function(e) {
		if (focused != null) {
			var input = focused,
				new_value = input.val().replace(/<a[^>]*>([\s\S]*?)<\/a>/ig, '$1');

			input.val(new_value);
		}
	});

	$('#insert-link').submit(function() {
		var link_url = $.trim($('#link-url').val()),
			link_text = $.trim($('#link-text').val()),
			link_title = $.trim($('#link-title').val()),
			link_blank = $('#link-blank').prop('checked');

		if (link_url == '' || link_text == '') {
			$('#insertLinkModal').modal('hide');
			return false;
		}

		var new_link = $('<a href="' + link_url + '"/>');
		new_link.text(link_text);

		if (link_title != '') {
			new_link.attr('title', link_title);
		}

		if (link_blank) {
			new_link.attr('target', '_blank');
		}

		var len = focused.val().length,
			start = focused[0].selectionStart,
			end = focused[0].selectionEnd,
			selectedText = focused.val().substring(start, end);

		focused.val(focused.val().substring(0, start) + new_link[0].outerHTML + focused.val().substring(end, len));
		focused = null;

		$('#insertLinkModal').modal('hide');

		return false;
	});

	$('#do-insert').click(function() {
		$('#insert-link').submit();

		return false;
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	setTimeout(function() {
		form.one('change', 'input, select, textarea', function(){
			form_changed = true;
		});
	}, 21);

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				// error for lang fields
				if ($(this).parent().hasClass('tab-pane')) {
					var that = this,
						tab = $(this).parent().parent().prev().children(':eq(' + $(this).parent().index() + ')').children(),
						alert_callback = function() {
							if (that.id == 'form-product_description') {
								setTimeout(function() {
									tab.click();
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									tab.click();
									$(that).focus();
								}, 21);
							}
						};

					if ($(this).parent()[0].id.indexOf('form-product_title') === 0) {
						bootbox.alert("<?=Yii::t('products', 'Enter a product title!')?>", alert_callback);
					}
				}
				else {
					var that = this,
						alert_callback = function() {
							if (that.id == 'form-redactor') {
								setTimeout(function() {
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									$(that).focus();
								}, 21);
							}
						};

					switch (this.id) {
						case 'form-category_id':
							bootbox.alert("<?=Yii::t('products', 'Select a category!')?>", alert_callback);
							break;
						case 'form-product_title':
							bootbox.alert("<?=Yii::t('products', 'Enter a product title!')?>", alert_callback);
							break;
					}
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});

	var sortable_config = {
		handle: '.pl-thumb',
		items: '> li',
		placeholder: 'pl-placeholder',
		cursor: 'move',
		zIndex: 1019,
		opacity: .7,
		update: function(event, ui) {
			$(this).find('li').each(function(index) {
				$(this).find('input[type="hidden"]').val(index + 1);
			});
		}
	};

	$('.photos-list').sortable(sortable_config).find('.pl-thumb-el.btn').click(function(e) {
		e.preventDefault();

		$(this).next().next().toggleClass('hidden').parent().parent().siblings().find('.pl-thumb-title').addClass('hidden');
	});
});
</script>