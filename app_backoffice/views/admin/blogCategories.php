<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('categories', 'Blog categories h1')?></h1>

<?php
    $category_url_data = array();

    if ($sort != 'default') {
        $category_url_data['sort'] = $sort;
        $category_url_data['direction'] = $direction;
    }

    if (!empty($keyword)) {
        $category_url_data['keyword'] = $keyword;
    }

    if (!empty($page)) {
        $category_url_data['page'] = $page + 1;
    }

    $category_new_url = $this->createUrl('blogcategory', array_merge(array('id' => 'new'), $category_url_data));

    $assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/blog_category';

    $has_rights = Yii::app()->getUser()->hasAccess($this->route, true);
?>
<?php if ($has_rights) { ?>
<p class="text-center" style="margin-bottom: 20px"><a class="btn btn-success" href="<?=$category_new_url?>"><?=Yii::t('categories', 'Add category btn')?></a></p>
<?php } ?>

<form class="search-form form-inline text-center" method="get">
    <div class="form-group">
        <input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('categories', 'ID | category name placeholder')?>" value="<?=CHtml::encode($keyword)?>">
        <button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
        <?php if ($sort != 'default' || !empty($keyword)) { ?>
        <br><a href="<?=$this->createUrl('blogcategories')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
        <?php } ?>
    </div>
</form>

<?php if (!empty($categories)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=count($categories)?></strong></p>
<form id="manage-categories" class="form-inline" method="post">
    <input id="entity-id" type="hidden" name="category_id" value="">
    <input id="entity-action" type="hidden" name="action" value="">

    <table class="table-data table table-striped">
        <thead>
            <tr>
                <th style="width: 4%"></th>
                <th style="width: 10%">ID</th>
                <th style="width: 42%"><?=Yii::t('categories', 'Category title col')?></th>
                <th style="width: 10%"><?=Yii::t('categories', 'Position col')?></th>
                <th width="14%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $id => $category) { ?>
            <?php
                $category_id = $category['category_id'];
                $category_url = $this->createUrl('blogcategory', array_merge(array('id' => $category_id), $category_url_data));
            ?>
            <tr>
                <td>
                    <input type="checkbox" name="selected[]" value="<?=$category_id?>">
                </td>
                <td>
                    <a href="<?=$category_url?>"><?=$category_id?></a>
                </td>
                <td>
                    <a href="<?=$category_url?>"><?=CHtml::encode($category['category_name'])?></a>
                </td>
                <td>
                    <?php if ($has_rights) { ?>
                    <input class="form-control input-sm text-center" type="text" name="category[<?=$category_id?>]" value="<?=CHtml::encode($category['category_position'])?>" style="width: 50px">
                    <?php } else { ?>
                    <a href="<?=$category_url?>"><?=CHtml::encode($category['category_position'])?></a>
                    <?php } ?>
                </td>
                <td class="text-right" style="border-right: none;">
                    <span class="edit-btns" data-id="<?=$category_id?>">
                        <div class="btn-group">
                            <?php if ($has_rights) { ?>
                            <?php if ($category['active']) { ?>
                            <a title="<?=Yii::t('categories', 'Active')?>" class="active-btn btn btn-default btn-sm btn-success" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ok"></span></a>
                            <?php } else { ?>
                            <a title="<?=Yii::t('categories', 'Blocked')?>" class="block-btn btn btn-default btn-sm btn-danger" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ban-circle"></span></a>
                            <?php } ?>
                            <a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$category_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
                            <?php } else { ?>
                            <a title="<?=Yii::t('app', 'View btn')?>" class="btn btn-default btn-sm" href="<?=$category_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <?php } ?>
                        </div>
                    </span>
                </td>
            </tr>
            <?php } ?>
        </tbody>
        <?php if ($has_rights) { ?>
        <tfoot>
            <tr class="tBot">
                <td colspan="8">
                    <div class="bulk-actions clearfix">
                        <div class="form-group">
                            <span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
                        </div>
                        <div class="form-group">
                            <select id="bulkAction" class="form-control input-sm" name="bulkAction">
                                <option value="save"><?=Yii::t('app', 'Save positions option')?></option>
                                <option value="active"><?=Yii::t('app', 'Activate selected')?></option>
                                <option value="block"><?=Yii::t('app', 'Block selected')?></option>
                                <option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
                            </select>
                            <strong id="topMsg"></strong>
                        </div>
                        <button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
                    </div>
                </td>
            </tr>
        </tfoot>
        <?php } ?>
    </table>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
    $(document).ready(function(){
        var checkboxes = $(".table-data input[type=checkbox]"),
            submit_form = false;

        $(".block-btn, .active-btn").click( function(){
            if($(this).hasClass("block-btn")){
                $('#entity-action').val('active');
            } else {
                $('#entity-action').val('block');
            }

            submit_form = true;
            $('#entity-id').val($(this).parent().parent().attr("data-id"));
            $('#manage-categories').submit();

            return false;
        });

        $(".delete-btn").click( function(){
            var that = $(this);

            bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
                if (result) {
                    submit_form = true;

                    $('#entity-action').val('delete');
                    $('#entity-id').val(that.parent().parent().attr("data-id"));
                    $('#manage-categories').submit();
                }
            });

            return false;
        });

        $(".check-toggle").click( function(){
            if($(this).hasClass("checked"))
            {
                checkboxes.prop('checked', false);
            }
            else
            {
                checkboxes.prop('checked', true);
            }

            $(this).toggleClass("checked");
        });

        $("#manage-categories").submit(function() {
            if (submit_form) {
                return true;
            }

            if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
                return false;

            if ($("#bulkAction").val() == 'delete') {
                var that = $(this);

                bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
                    if (result) {
                        submit_form = true;
                        that.submit();
                    }
                });

                return false;
            }
        });
    });
</script>
