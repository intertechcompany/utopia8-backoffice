<?php
/* @var $this AdminController */
?>
<?php
	$roles_url_data = array();

	if (!empty($sort)) {
		$roles_url_data['sort'] = $sort;
		$roles_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$roles_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$roles_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('roles', $roles_url_data);

	$has_rights = Yii::app()->getUser()->hasAccess($this->route, true);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-role" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="role[role_id]" value="<?=$role['role_id']?>">

	<div class="role-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>

	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label">ID:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$role['role_id']?></p>
		</div>
	</div>

	<div class="form-group">
		<label for="form-role_name" class="col-md-3 control-label"><?=Yii::t('roles', 'Role name')?>:</label>
		<div class="col-md-4 control-required">
			<input id="form-role_name" class="form-control" type="text" name="role[role_name]" value="<?=CHtml::encode($role['role_name'])?>">
		</div>
	</div>

	<?php $role_rights = !empty($role['role_rights']) ? json_decode($role['role_rights'], true) : []; ?>
	<div class="form-group">
		<label for="form-role_rights" class="col-md-3 control-label"><?=Yii::t('roles', 'Role rights')?>:</label>
		<div id="roles" class="col-md-6">
			<div style="display: inline-block; margin-right: 30px; vertical-align: top">
				<strong>Категории</strong>
				<div class="checkbox"><label for="categories-read"><input id="categories-read" type="checkbox" name="role[role_rights][admin/categories][read]" value="1"<?php if (isset($role_rights['admin/categories']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
				<div class="checkbox"><label for="categories-write"><input id="categories-write" type="checkbox" name="role[role_rights][admin/categories][write]" value="1"<?php if (isset($role_rights['admin/categories']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			</div>
			<div style="display: inline-block; margin-right: 30px; vertical-align: top">
				<strong>Товары</strong>
				<div class="checkbox"><label for="products-read"><input id="products-read" type="checkbox" name="role[role_rights][admin/products][read]" value="1"<?php if (isset($role_rights['admin/products']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
				<div class="checkbox"><label for="products-write"><input id="products-write" type="checkbox" name="role[role_rights][admin/products][write]" value="1"<?php if (isset($role_rights['admin/products']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			</div>
			<div style="display: inline-block; margin-right: 30px; vertical-align: top">
				<strong>Свойства</strong>
				<div class="checkbox"><label for="properties-read"><input id="properties-read" type="checkbox" name="role[role_rights][admin/properties][read]" value="1"<?php if (isset($role_rights['admin/properties']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
				<div class="checkbox"><label for="properties-write"><input id="properties-write" type="checkbox" name="role[role_rights][admin/properties][write]" value="1"<?php if (isset($role_rights['admin/properties']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			</div>

			<hr>
			<strong>Заказы</strong>
			<div class="checkbox"><label for="orders-read"><input id="orders-read" type="checkbox" name="role[role_rights][admin/orders][read]" value="1"<?php if (isset($role_rights['admin/orders']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
			<div class="checkbox"><label for="orders-write"><input id="orders-write" type="checkbox" name="role[role_rights][admin/orders][write]" value="1"<?php if (isset($role_rights['admin/orders']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			<hr>
			<strong>Отзывы</strong>
			<div class="checkbox"><label for="requests-read"><input id="requests-read" type="checkbox" name="role[role_rights][admin/requests][read]" value="1"<?php if (isset($role_rights['admin/requests']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
			<div class="checkbox"><label for="requests-write"><input id="requests-write" type="checkbox" name="role[role_rights][admin/requests][write]" value="1"<?php if (isset($role_rights['admin/requests']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			<hr>
			<strong>Страницы</strong>
			<div class="checkbox"><label for="pages-read"><input id="pages-read" type="checkbox" name="role[role_rights][admin/pages][read]" value="1"<?php if (isset($role_rights['admin/pages']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
			<div class="checkbox"><label for="pages-write"><input id="pages-write" type="checkbox" name="role[role_rights][admin/pages][write]" value="1"<?php if (isset($role_rights['admin/pages']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			<hr>
			<strong>Баннеры</strong>
			<div class="checkbox"><label for="banners-read"><input id="banners-read" type="checkbox" name="role[role_rights][admin/banners][read]" value="1"<?php if (isset($role_rights['admin/banners']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
			<div class="checkbox"><label for="banners-write"><input id="banners-write" type="checkbox" name="role[role_rights][admin/banners][write]" value="1"<?php if (isset($role_rights['admin/banners']['write'])) { ?> checked<?php } ?>> изменение</label></div>

			<hr>
			<div style="display: inline-block; margin-right: 30px; vertical-align: top">
				<strong>Клиенты</strong>
				<div class="checkbox"><label for="users-read"><input id="users-read" type="checkbox" name="role[role_rights][admin/users][read]" value="1"<?php if (isset($role_rights['admin/users']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
				<div class="checkbox"><label for="users-write"><input id="users-write" type="checkbox" name="role[role_rights][admin/users][write]" value="1"<?php if (isset($role_rights['admin/users']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			</div>
			<div style="display: inline-block; margin-right: 30px; vertical-align: top">
				<strong>Менеджера</strong>
				<div class="checkbox"><label for="managers-read"><input id="managers-read" type="checkbox" name="role[role_rights][admin/managers][read]" value="1"<?php if (isset($role_rights['admin/managers']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
				<div class="checkbox"><label for="managers-write"><input id="managers-write" type="checkbox" name="role[role_rights][admin/managers][write]" value="1"<?php if (isset($role_rights['admin/managers']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			</div>
			<div style="display: inline-block; margin-right: 30px; vertical-align: top">
				<strong>Роли</strong>
				<div class="checkbox"><label for="roles-read"><input id="roles-read" type="checkbox" name="role[role_rights][admin/roles][read]" value="1"<?php if (isset($role_rights['admin/roles']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
				<div class="checkbox"><label for="roles-write"><input id="roles-write" type="checkbox" name="role[role_rights][admin/roles][write]" value="1"<?php if (isset($role_rights['admin/roles']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			</div>

			<hr>
			<div style="display: inline-block; margin-right: 30px; vertical-align: top">
				<strong>Переводы</strong>
				<div class="checkbox"><label for="translations-read"><input id="translations-read" type="checkbox" name="role[role_rights][admin/translations][read]" value="1"<?php if (isset($role_rights['admin/translations']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
				<div class="checkbox"><label for="translations-write"><input id="translations-write" type="checkbox" name="role[role_rights][admin/translations][write]" value="1"<?php if (isset($role_rights['admin/translations']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			</div>
			<div style="display: inline-block; margin-right: 30px; vertical-align: top">
				<strong>Настройки</strong>
				<div class="checkbox"><label for="settings-read"><input id="settings-read" type="checkbox" name="role[role_rights][admin/settings][read]" value="1"<?php if (isset($role_rights['admin/settings']['read'])) { ?> checked<?php } ?>> просмотр</label></div>
				<div class="checkbox"><label for="settings-write"><input id="settings-write" type="checkbox" name="role[role_rights][admin/settings][write]" value="1"<?php if (isset($role_rights['admin/settings']['write'])) { ?> checked<?php } ?>> изменение</label></div>
			</div>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"<?php if (!$has_rights) { ?> disabled<?php } ?>><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<script>
var is_role = true;

$(document).ready(function(){
	<?php if (!$has_rights) { ?>
	$('form').find(':input').prop('disabled', true);
	<?php } else {
        ?>
	
	$('#roles').on('change', 'input[type="checkbox"]', function() {
		var prev = $(this).parents('.checkbox').prev();

		if (prev.hasClass('checkbox')) {
			prev.find('input').prop('checked', true);
		}
	});
	
	var form = $("#manage-role"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				var that = this,
					alert_callback = function() {
						if (that.id == 'form-redactor') {
							setTimeout(function() {
								$(that).redactor('focus.setStart');
							}, 21);
						}
						else {
							setTimeout(function() {
								$(that).focus();
							}, 21);
						}
					};

				switch (this.id) {
					case 'form-role_name':
						bootbox.alert("<?=Yii::t('roles', 'Enter a role name!')?>", alert_callback);
						break;
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});

	var invoice_xhr,
		is_invoice_xhr = false;

	$('.send').on('click', function(e) {
		e.preventDefault();

		$('#invoice-sent').find('form').attr('action', $(this).data('url'));
		$('#invoice-sent').find('h4 span').text('#' + $(this).data('id'));
		$('#invoice-sent').modal('show');
	});

	$('#invoice-sent').on('submit', 'form', function(e) {
		e.preventDefault();

		if (is_invoice_xhr) {
			return false;
		}

		var form = $(this),
			form_data = form.serialize();
		
		is_invoice_xhr = true;
		form.find(':input').prop('disabled', true);

		invoice_xhr = $.ajax({
			type: 'POST', 
			url: form.attr('action'), 
			data: form_data, 
			cache: false, 
			dataType: 'json',
			success: function(data) {
				is_invoice_xhr = false;
				form.find(':input').prop('disabled', false);
				form.find('.has-error').removeClass('has-error').find('.help-block').remove();

				if (data.error) {
					$('#invoice-email').after('<span class="help-block">' + data.errorMessage + '</span>').parent().addClass('has-error');
					return;
				}

				form.addClass('hidden').after('<h4>' + data.message + '</h4>');
			},
			error: function(jqXHR, textStatus, errorThrown) { 
				is_invoice_xhr = false;
				form.find(':input').prop('disabled', false);
				
				alert('Request error!');
				// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
			}
		});
	}).on('hidden.bs.modal', function (e) {
		$(this).find('.has-error').removeClass('has-error').find('.help-block').remove();
		$(this).find('input[type="email"], textarea').val('');
		$(this).find('form').removeClass('hidden').next('h4').remove();

		if (is_invoice_xhr) {
			is_invoice_xhr = false;
			invoice_xhr.abort();
		}
	});

	<?php } ?>
});
</script>