<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('blogs', 'Blogs h1')?></h1>

<?php
	$blog_url_data = array();

	if ($sort != 'default') {
		$blog_url_data['sort'] = $sort;
		$blog_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$blog_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$blog_url_data['page'] = $page + 1;
	}

	$blog_new_url = $this->createUrl('blog', array_merge(array('id' => 'new'), $blog_url_data));

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/blog';
?>
<p class="text-center"><a class="btn btn-success" href="<?=$blog_new_url?>"><?=Yii::t('blogs', 'Add blog btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('blogs', 'ID | blog title placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<?php if ($sort != 'default' || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('blogs')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($blogs)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-blogs" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="blog_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array('keyword' => $keyword);
					} else {
						$sort_data = array();
					}
				?>
				<th style="width: 4%"></th>
				<th style="width: 8%">
					<?php if ($sort == 'blog_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'blog_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th style="width: 15%">
					<?=Yii::t('blogs', 'Photo')?>
				</th>
				<th style="width: 18%">
					<?php if ($sort == 'blog_published' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_published', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('blogs', 'Published col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'blog_published' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_published', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('blogs', 'Published col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_published', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('blogs', 'Published col')?></a>
					<?php } ?>
				</th>
				<th style="width: 41%">
					<?php if ($sort == 'blog_title' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_title', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('blogs', 'Blog title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'blog_title' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_title', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('blogs', 'Blog title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('blogs', array_merge(array('sort' => 'blog_title', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('blogs', 'Blog title col')?></a>
					<?php } ?>
				</th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($blogs as $id => $blog) { ?>
			<?php
				$blog_id = $blog['blog_id'];

				$blog_url = $this->createUrl('blog', array_merge(array('id' => $blog_id), $blog_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$blog_id?>">
				</td>
				<td>
					<a href="<?=$blog_url?>"><?=$blog_id?></a>
				</td>
				<td>
					<?php if (!empty($blog['blog_photo'])) { ?>
					<?php
						$cover = json_decode($blog['blog_photo'], true);
					?>
					<a href="<?=$blog_url?>"><img src="<?=$assetsUrl . '/' . $blog_id . '/' . $cover['list']['1x']['path']?>" width="<?=$cover['list']['1x']['size']['w']?>" height="<?=$cover['list']['1x']['size']['h']?>" alt="" style="max-width: 60px; height: auto"></a>
					<?php } ?>
				</td>
				<td>
					<?php if ($blog['blog_published'] != '0000-00-00') { ?>
					<?php
						$published_date = new DateTime($blog['blog_published'], new DateTimeZone(Yii::app()->timezone));
					?>
					<a href="<?=$blog_url?>"><?=$published_date->format('d.m.Y')?></a>
					<?php } ?>
				</td>
				<td>
					<a href="<?=$blog_url?>"><?=CHtml::encode($blog['blog_title'])?></a>
				</td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$blog_id?>">
						<div class="btn-group">
							<?php if ($blog['active']) { ?>
							<a title="<?=Yii::t('blogs', 'Active')?>" class="active-btn btn btn-default btn-sm btn-success" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ok"></span></a>
							<?php } else { ?>
							<a title="<?=Yii::t('blogs', 'Blocked')?>" class="block-btn btn btn-default btn-sm btn-danger" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ban-circle"></span></a>
							<?php } ?>
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$blog_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="active"><?=Yii::t('app', 'Activate selected')?></option>
								<option value="block"><?=Yii::t('app', 'Block selected')?></option>
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}
			
			submit_form = true;
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-blogs').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-blogs').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-blogs").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>