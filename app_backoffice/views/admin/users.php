<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('users', 'Users h1')?></h1>

<?php
	$user_url_data = array();

	if ($sort != 'default') {
		$user_url_data['sort'] = $sort;
		$user_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$user_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$user_url_data['page'] = $page + 1;
	}

	$user_new_url = $this->createUrl('user', array_merge(array('id' => 'new'), $user_url_data));
?>
<p class="text-center"><a class="btn btn-success" href="<?=$user_new_url?>"><?=Yii::t('users', 'Add user btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('users', 'ID | user name placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<?php if ($sort != 'default' || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('users')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($users)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-users" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="user_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array('keyword' => $keyword);
					} else {
						$sort_data = array();
					}
				?>
				<th style="width: 2%"></th>
				<th style="width: 6%">
					<?php if ($sort == 'user_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('users', array_merge(array('sort' => 'user_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'user_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('users', array_merge(array('sort' => 'user_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('users', array_merge(array('sort' => 'user_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th width="16%"><?=Yii::t('users', 'User name col')?></th>
				<th width="16%"><?=Yii::t('users', 'User login col')?></th>
				<th width="18%"><?=Yii::t('users', 'User email col')?></th>
				<th width="18%"><?=Yii::t('users', 'User phone col')?></th>
				<th width="10%"><?=Yii::t('users', 'Discount')?></th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($users as $id => $user) { ?>
			<?php
				$user_id = $user['user_id'];

				$user_url = $this->createUrl('user', array_merge(array('id' => $user_id), $user_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$user_id?>">
				</td>
				<td>
					<a href="<?=$user_url?>"><?=$user_id?></a>
				</td>
				<td>
					<a href="<?=$user_url?>"><?=CHtml::encode($user['user_first_name'])?></a>
				</td>
				<td>
					<a href="<?=$user_url?>"><?=CHtml::encode($user['user_login'])?></a>
				</td>
				<td>
					<a href="<?=$user_url?>"><?=CHtml::encode($user['user_email'])?></a>
				</td>
				<td>
					<a href="<?=$user_url?>"><?=CHtml::encode($user['user_phone'])?></a>
				</td>
				<td>
					<a href="<?=$user_url?>"><?=CHtml::encode($user['discount'])?>%</a>
				</td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$user_id?>">
						<div class="btn-group">
							<?php if ($user['active']) { ?>
							<a title="<?=Yii::t('users', 'Active')?>" class="active-btn btn btn-default btn-sm btn-success" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ok"></span></a>
							<?php } else { ?>
							<a title="<?=Yii::t('users', 'Blocked')?>" class="block-btn btn btn-default btn-sm btn-danger" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ban-circle"></span></a>
							<?php } ?>
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$user_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="active"><?=Yii::t('app', 'Activate selected')?></option>
								<option value="block"><?=Yii::t('app', 'Block selected')?></option>
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}

			submit_form = true;
			
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-users').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-users').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-users").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>