<?php
/* @var $this AdminController */
?>
<?php
	$pages_url_data = array();

	if (!empty($sort)) {
		$pages_url_data['sort'] = $sort;
		$pages_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$pages_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$pages_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('pages', $pages_url_data);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-page" class="form-horizontal" method="post" enctype="multipart/form-data">
	<div class="page-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>
		
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-3">
			<select id="form-active" name="page[active]" class="form-control">
				<option value="0"><?=Yii::t('pages', 'Blocked')?></option>
				<option value="1"><?=Yii::t('pages', 'Active')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-page_alias" class="col-md-3 control-label"><?=Yii::t('app', 'Alias (URL code)')?>:</label>
		<div class="col-md-6">
			<div class="input-group">
				<span class="input-group-addon">/page/</span>
				<input id="form-page_alias" class="form-control" type="text" name="page[page_alias]" value="">
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-page_position" class="col-md-3 control-label"><?=Yii::t('pages', 'Position')?>:</label>
		<div class="col-md-1">
			<input id="form-page_position" class="form-control" type="text" name="page[page_position]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-page_title" class="col-md-3 control-label"><?=Yii::t('pages', 'Page title')?>:</label>
		<div class="col-md-6 control-required">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-page_title_<?=$code?>" aria-controls="form-page_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-page_title_<?=$code?>">
						<input class="form-control" type="text" name="page_lang[page_title][<?=$code?>]" value="">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-page_type" class="col-md-3 control-label"><?=Yii::t('pages', 'Page type')?>:</label>
		<div class="col-md-3">
			<select id="form-page_type" name="page[page_type]" class="form-control">
				<option value="text"><?=Yii::t('pages', 'Type text')?></option>
				<option value="about"><?=Yii::t('pages', 'Type about')?></option>
				<option value="contact"><?=Yii::t('pages', 'Type contact')?></option>
				<option value="faq">FAQ</option>
				<!-- <option value="delivery"><?=Yii::t('pages', 'Type delivery')?></option> -->
			</select>
		</div>
	</div>

	<?php /*
	<div class="form-group">
		<label for="form-page_menu" class="col-md-3 control-label"><?=Yii::t('pages', 'Page menu')?>:</label>
		<div class="col-md-3">
			<select id="form-page_menu" name="page[page_menu]" class="form-control">
				<option value="top"><?=Yii::t('pages', 'Menu top')?></option>
				<option value="bottom1"><?=Yii::t('pages', 'Menu bottom 1')?></option>
				<!-- <option value="bottom2"><?=Yii::t('pages', 'Menu bottom 2')?></option>
				<option value="bottom3"><?=Yii::t('pages', 'Menu bottom 3')?></option> -->
			</select>
		</div>
	</div>
	*/ ?>

	<?php
		$menu_options = [
			'top' => Yii::t('pages', 'Menu top'), 
			'bottom' => Yii::t('pages', 'Menu bottom 1'),
		];
	?>
	<div class="form-group">
		<label for="form-page_menu" class="col-md-3 control-label"><?=Yii::t('pages', 'Page menu')?>:</label>
		<div class="col-md-6">
			<select id="form-page_menu" name="page[menu][]" class="form-control select2" multiple>
				<?php foreach ($menu_options as $menu_code => $menu_option) { ?>
				<option value="<?=$menu_code?>"><?=CHtml::encode($menu_option)?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<?php /* <div class="form-group">
		<label for="form-page_photo" class="col-md-3 control-label"><?=Yii::t('pages', 'Photo')?>:</label>
		<div class="col-md-4">
			<input id="form-page_photo" type="file" name="page[page_photo]">
			<small class="text-nowrap"><?=Yii::t('app', 'Image file requirements', array('{types}' => 'jpg, gif, png', '{size}' => '10 MB', '{dimension}' => 'не менее 600px'))?></small>
		</div>
	</div> */ ?>

	<div class="form-group page-text">
		<label for="form-page_description" class="col-md-3 control-label"><?=Yii::t('pages', 'Description')?>:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-page_description_<?=$code?>" aria-controls="form-page_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-page_description_<?=$code?>">
						<textarea class="form-control form-redactor" name="page_lang[page_description][<?=$code?>]" rows="6"></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group page-about">
		<label for="form-page_vacancy" class="col-md-3 control-label"><?=Yii::t('pages', 'Vacancies')?>:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-page_vacancy_<?=$code?>" aria-controls="form-page_vacancy_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-page_vacancy_<?=$code?>">
						<textarea class="form-control" name="page_lang[page_vacancy][<?=$code?>]" rows="6"></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="page-header page-about">
		<h3><?=Yii::t('pages', 'Content')?></h3>
	</div>

	<div id="content" class="info-blocks page-about" data-group="content">
		<?php $index = 0; ?>
		<div class="info-block info-block-start">
			<div href="#" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-menu-hamburger"></span></div>
			<input type="hidden" name="page[page_content][<?=$index?>][position]" value="<?=$index?>">

			<div class="form-group">
				<label for="form-content-photo-<?=$index?>" class="col-md-3 control-label">Фото:</label>
				<div class="col-md-5">
					<input id="form-content-photo-<?=$index?>" type="file" name="page[page_content][<?=$index?>][photo]">
					<small>Файл jpg, gif, png до 10 MB.</small>
				</div>
			</div>

			<?php /* <div class="form-group">
				<label for="form-content-video-<?=$index?>" class="col-md-3 control-label">Фоновое видео:</label>
				<div class="col-md-5">
					<input id="form-content-video-<?=$index?>" type="file" name="page[page_content][<?=$index?>][video]">
					<small>Файл mp4 до 10 MB.</small>
				</div>
			</div> */ ?>
			
			<div class="form-group">
				<label for="form-content-title-<?=$index?>" class="col-md-3 control-label"><?=Yii::t('pages', 'Content title')?>:</label>
				<div class="col-md-6">
					<div class="lang-tabs" role="tabpanel">
						<ul class="nav nav-tabs" role="tablist">
							<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
							<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
								<a href="#form-content-title_<?=$code?>-<?=$index?>" aria-controls="form-content-title_<?=$code?>-<?=$index?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
							</li>
							<?php } ?>
						</ul>
						<div class="tab-content">
							<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
							<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-content-title_<?=$code?>-<?=$index?>">
								<input type="text" class="form-control" name="page_lang[page_content][<?=$code?>][<?=$index?>][title]" value="">
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="form-content-text-<?=$index?>" class="col-md-3 control-label"><?=Yii::t('pages', 'Content description')?>:</label>
				<div class="col-md-9">
					<div class="lang-tabs" role="tabpanel">
						<ul class="nav nav-tabs" role="tablist">
							<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
							<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
								<a href="#form-content-text_<?=$code?>-<?=$index?>" aria-controls="form-content-text_<?=$code?>-<?=$index?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
							</li>
							<?php } ?>
						</ul>
						<div class="tab-content">
							<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
							<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-content-text_<?=$code?>-<?=$index?>">
								<textarea class="form-control" name="page_lang[page_content][<?=$code?>][<?=$index?>][text]" rows="4"></textarea>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<!--	<div id="add-content" class="add-info-block form-group page-about">-->
<!--		<div class="col-md-9 col-md-push-3">-->
<!--			<button type="button" class="btn btn-default">--><?//=Yii::t('pages', 'Add content')?><!--</button>-->
<!--		</div>-->
<!--	</div>-->

    <div class="page-header page-faq">
        <h3><?=Yii::t('pages', 'FAQ')?></h3>
    </div>

    <div id="content" class="info-blocks page-faq" data-group="content">
        <?php $index = 0; ?>
        <div class="info-block info-block-start">
            <div href="#" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-menu-hamburger"></span></div>
            <input type="hidden" name="page[page_content_faq][<?=$index?>][position]" value="<?=$index?>">

<!--            <div class="form-group">-->
<!--                <label for="form-content-photo---><?//=$index?><!--" class="col-md-3 control-label">Фото:</label>-->
<!--                <div class="col-md-5">-->
<!--                    <input id="form-content-photo-faq---><?//=$index?><!--" type="file" name="page[page_content_faq][--><?//=$index?><!--][photo]">-->
<!--                    <small>Файл jpg, gif, png до 10 MB.</small>-->
<!--                </div>-->
<!--            </div>-->

            <?php /* <div class="form-group">
				<label for="form-content-video-<?=$index?>" class="col-md-3 control-label">Фоновое видео:</label>
				<div class="col-md-5">
					<input id="form-content-video-<?=$index?>" type="file" name="page[page_content][<?=$index?>][video]">
					<small>Файл mp4 до 10 MB.</small>
				</div>
			</div> */ ?>

            <div class="form-group">
                <label for="form-content-title-<?=$index?>" class="col-md-3 control-label"><?=Yii::t('pages', 'Content title')?>:</label>
                <div class="col-md-6">
                    <div class="lang-tabs" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                                <li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
                                    <a href="#form-content-title-faq_<?=$code?>-<?=$index?>" aria-controls="form-content-title_<?=$code?>-<?=$index?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                                <div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-content-title-faq_<?=$code?>-<?=$index?>">
                                    <input type="text" class="form-control" name="page_lang[page_content_faq][<?=$code?>][<?=$index?>][title]" value="">
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="form-content-text-<?=$index?>" class="col-md-3 control-label"><?=Yii::t('pages', 'Content description')?>:</label>
                <div class="col-md-9">
                    <div class="lang-tabs" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                                <li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
                                    <a href="#form-content-text-faq_<?=$code?>-<?=$index?>" aria-controls="form-content-text_<?=$code?>-<?=$index?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                                <div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-content-text-faq_<?=$code?>-<?=$index?>">
                                    <textarea class="form-control form-redactor" name="page_lang[page_content_faq][<?=$code?>][<?=$index?>][text]" rows="4"></textarea>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	
	<div id="add-content" class="add-info-block form-group page-faq page-about">
        <div class="col-md-9 col-md-push-3">
            <button type="button" class="btn btn-default"><?=Yii::t('pages', 'Add content')?></button>
        </div>
    </div>

    <div class="page-header page-faq">
        <h3><?=Yii::t('pages', 'FAQ homepage')?></h3>
    </div>

    <div id="content-faq" class="info-blocks page-faq" data-group="content-faq">
        <?php $index = 0; ?>
        <div class="info-block info-block-start">
            <div href="#" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-menu-hamburger"></span></div>
            <input type="hidden" name="page[page_content_faq_main][<?=$index?>][position]" value="<?=$index?>">

            <!--            <div class="form-group">-->
            <!--                <label for="form-content-photo---><?//=$index?><!--" class="col-md-3 control-label">Фото:</label>-->
            <!--                <div class="col-md-5">-->
            <!--                    <input id="form-content-photo-faq---><?//=$index?><!--" type="file" name="page[page_content_faq][--><?//=$index?><!--][photo]">-->
            <!--                    <small>Файл jpg, gif, png до 10 MB.</small>-->
            <!--                </div>-->
            <!--            </div>-->

            <?php /* <div class="form-group">
				<label for="form-content-video-<?=$index?>" class="col-md-3 control-label">Фоновое видео:</label>
				<div class="col-md-5">
					<input id="form-content-video-<?=$index?>" type="file" name="page[page_content][<?=$index?>][video]">
					<small>Файл mp4 до 10 MB.</small>
				</div>
			</div> */ ?>

            <div class="form-group">
                <label for="form-content-title-<?=$index?>" class="col-md-3 control-label"><?=Yii::t('pages', 'Content title')?>:</label>
                <div class="col-md-6">
                    <div class="lang-tabs" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                                <li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
                                    <a href="#form-content-title-faq-main_<?=$code?>-<?=$index?>" aria-controls="form-content-title_<?=$code?>-<?=$index?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                                <div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-content-title-faq-main_<?=$code?>-<?=$index?>">
                                    <input type="text" class="form-control" name="page_lang[page_content_faq_main][<?=$code?>][<?=$index?>][title]" value="">
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="form-content-text-<?=$index?>" class="col-md-3 control-label"><?=Yii::t('pages', 'Content description')?>:</label>
                <div class="col-md-9">
                    <div class="lang-tabs" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                                <li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
                                    <a href="#form-content-text-faq-main_<?=$code?>-<?=$index?>" aria-controls="form-content-text_<?=$code?>-<?=$index?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                                <div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-content-text-faq-main_<?=$code?>-<?=$index?>">
                                    <textarea class="form-control form-redactor" name="page_lang[page_content_faq_main][<?=$code?>][<?=$index?>][text]" rows="4"></textarea>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="add-content-faq" class="add-info-block form-group page-faq page-about">
        <div class="col-md-9 col-md-push-3">
            <button type="button" class="btn btn-default"><?=Yii::t('pages', 'Add content')?></button>
        </div>
    </div>

	<div id="section-seo" class="page-header">
		<h3><?=Yii::t('app', 'SEO data')?></h3>
	</div>

	<div class="form-group">
		<label for="form-page_meta_title" class="col-md-3 control-label"><?=Yii::t('app', 'No index')?>:</label>
		<div class="col-md-6">
			<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
			<div class="checkbox">
				<label for="form-page_no_index<?=$code?>">
					<input id="form-page_no_index<?=$code?>" type="checkbox" name="page_lang[page_no_index][<?=$code?>]" value="1"> <?=$lang?>
				</label>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="form-group">
		<label for="form-page_meta_title" class="col-md-3 control-label">Meta title:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-page_meta_title_<?=$code?>" aria-controls="form-page_meta_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-page_meta_title_<?=$code?>">
						<input class="form-control" type="text" name="page_lang[page_meta_title][<?=$code?>]" value="">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-page_meta_keywords" class="col-md-3 control-label">Meta keywords:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-page_meta_keywords_<?=$code?>" aria-controls="form-page_meta_keywords_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-page_meta_keywords_<?=$code?>">
						<textarea class="form-control" name="page_lang[page_meta_keywords][<?=$code?>]" rows="2"></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-page_meta_description" class="col-md-3 control-label">Meta description:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-page_meta_description_<?=$code?>" aria-controls="form-page_meta_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-page_meta_description_<?=$code?>">
						<textarea class="form-control" name="page_lang[page_meta_description][<?=$code?>]" rows="4"></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Add btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<div class="btn-group-vertical btn-insert-link" role="group" aria-label="<?=Yii::t('app', 'Insert/remove a link label')?>">
  <button id="typograph-link-btn" title="<?=Yii::t('app', 'Typography text btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-text-size"></i></button>
  <button id="insert-link-btn" title="<?=Yii::t('app', 'Insert a link btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
  <button id="remove-link-btn" title="<?=Yii::t('app', 'Remove all links btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
</div>

<!-- Insert Link Modal -->
<div class="modal fade" id="insertLinkModal" tabindex="-1" role="dialog" aria-labelledby="insertLinkModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="<?=Yii::t('app', 'Close')?>"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="insertLinkModalLabel"><?=Yii::t('app', 'Insert a link label')?></h4>
			</div>
			<div class="modal-body">
			<form id="insert-link">
				<div class="form-group">
					<label for="link-text" class="control-label"><?=Yii::t('app', 'Link text')?>:</label>
					<input type="text" class="form-control" id="link-text">
				</div>
				<div class="form-group">
					<label for="link-url" class="control-label"><?=Yii::t('app', 'Link tip')?>:</label>
					<input type="text" class="form-control" id="link-url">
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label for="link-blank">
							<input id="link-blank" type="checkbox" value="1"> <?=Yii::t('app', 'Open in new tab')?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="link-title" class="control-label"><?=Yii::t('app', 'Link tip (title)')?>:</label>
					<input type="text" class="form-control" id="link-title">
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=Yii::t('app', 'Cancel btn')?></button>
				<button id="do-insert" type="button" class="btn btn-primary"><?=Yii::t('app', 'Insert btn')?></button>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var form = $("#manage-page"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	$('#form-page_type').on('change', function() {
		var value = $(this).val();

		if (value == 'about') {
			$('.page-about').removeClass('hidden');
			$('.page-text').addClass('hidden');
            $('.page-faq').addClass('hidden');
            $('#add-content').removeClass('hidden');
            $('#add-content-faq').removeClass('hidden');
		} else if(value == 'faq') {
            $('.page-faq').removeClass('hidden');
            $('.page-text').addClass('hidden');
            $('.page-about').addClass('hidden');
            $('#add-content').removeClass('hidden');
            $('#add-content-faq').removeClass('hidden');
        } else {
			$('.page-faq').addClass('hidden');
			$('.page-about').addClass('hidden');
			$('.page-text').removeClass('hidden');
		}
	}).change();

	$('#typograph-link-btn').on('click', function(e) {
		var input = focused,
			text = input.val(),
			btn = $(this);

		if (focused != null) {
			btn.prop('disabled', true);
			
			$.ajax({type: "POST", url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/typograph', data: $.param({text: focused.val()}), cache: false, dataType: "json",
				success: function(data) {
					btn.prop('disabled', false);
					
					if (data.error == 'Y') {
						bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					}
					else {
						if (focused.hasClass('form-redactor')) {
							$('#' + focused[0].id).redactor('code.set', data.text);
						}
						else {
							focused.val(data.text);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					btn.prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});
	
	$('#insert-link-btn').on('click', function(e) {
		var input = focused,
			len = input.val().length,
			start = input[0].selectionStart,
			end = input[0].selectionEnd,
			selectedText = input.val().substring(start, end);

		$('#link-text').val(selectedText);
		$('#link-url').val('');
		$('#link-title').val('');
		$('#link-blank').prop('checked', false);

		if (focused != null) {
			$('#insertLinkModal').modal('show');
		}
	});

	$('#remove-link-btn').on('click', function(e) {
		if (focused != null) {
			var input = focused,
				new_value = input.val().replace(/<a[^>]*>([\s\S]*?)<\/a>/ig, '$1');

			input.val(new_value);
		}
	});

	$('#insert-link').submit(function() {
		var link_url = $.trim($('#link-url').val()),
			link_text = $.trim($('#link-text').val()),
			link_title = $.trim($('#link-title').val()),
			link_blank = $('#link-blank').prop('checked');

		if (link_url == '' || link_text == '') {
			$('#insertLinkModal').modal('hide');
			return false;
		}

		var new_link = $('<a href="' + link_url + '"/>');
		new_link.text(link_text);

		if (link_title != '') {
			new_link.attr('title', link_title);
		}

		if (link_blank) {
			new_link.attr('target', '_blank');
		}

		var len = focused.val().length,
			start = focused[0].selectionStart,
			end = focused[0].selectionEnd,
			selectedText = focused.val().substring(start, end);

		focused.val(focused.val().substring(0, start) + new_link[0].outerHTML + focused.val().substring(end, len));
		focused = null;

		$('#insertLinkModal').modal('hide');

		return false;
	});

	$('#do-insert').click(function() {
		$('#insert-link').submit();

		return false;
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				// error for lang fields
				if ($(this).parent().hasClass('tab-pane')) {
					var that = this,
						tab = $(this).parent().parent().prev().children(':eq(' + $(this).parent().index() + ')').children(),
						alert_callback = function() {
							if (that.id == 'form-page_description') {
								setTimeout(function() {
									tab.click();
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									tab.click();
									$(that).focus();
								}, 21);
							}
						};

					if ($(this).parent()[0].id.indexOf('form-page_title') === 0) {
						bootbox.alert("<?=Yii::t('pages', 'Enter a page title!')?>", alert_callback);
					}
				}
				else {
					var that = this,
						alert_callback = function() {
							if (that.id == 'form-redactor') {
								setTimeout(function() {
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									$(that).focus();
								}, 21);
							}
						};

					switch (this.id) {
						case 'form-page_title':
							bootbox.alert("<?=Yii::t('pages', 'Enter a page title!')?>", alert_callback);
							break;
					}
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});
});
</script>