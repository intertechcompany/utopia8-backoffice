<?php
/* @var $this AdminController */
?>
<?php
	function showCategoriesTree($categories, $category_ids = array(), $level = 0, $active = 1)
	{
		$html = '';

		foreach ($categories as $category) {
			if ($category['active'] && $active) {
				$category_active = ' data-active="1"';
				$subcategory_active = 1;
			} else {
				$category_active = ' data-active="0"';
				$subcategory_active = 0;
			}
			
			if (in_array($category['category_id'], $category_ids)) {
				$html .= '<option value="' . $category['category_id'] . '"' . $category_active . ' selected>' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_name']) . '</option>';
			} else {
				$html .= '<option value="' . $category['category_id'] . '"' . $category_active . '>' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_name']) . '</option>';
			}

			if (!empty($category['sub'])) {
				$html .= showCategoriesTree($category['sub'], $category_ids, $level + 1, $subcategory_active);
			}
		}

		return $html;
	}

	$properties_url_data = array();

	if (!empty($sort)) {
		$properties_url_data['sort'] = $sort;
		$properties_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$properties_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$properties_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('properties', $properties_url_data);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-property" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="property[property_id]" value="<?=$property['property_id']?>">

	<div class="page-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>

	<div class="form-group">
		<label for="form-property_title" class="col-md-3 control-label"><?=Yii::t('properties', 'Property title')?>:</label>
		<div class="col-md-6 control-required">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-property_title_<?=$code?>" aria-controls="form-property_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-property_title_<?=$code?>">
						<input class="form-control" type="text" name="property_lang[property_title][<?=$code?>]" value="<?=CHtml::encode($property[$code]['property_title'])?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

    <div class="form-group">
        <label for="form-property_title" class="col-md-3 control-label">Название в выпадающем списке:</label>
        <div class="col-md-6">
            <div class="lang-tabs" role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                        <li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
                            <a href="#form-property_selector_<?=$code?>" aria-controls="form-property_selector_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                        <div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-property_selector_<?=$code?>">
                            <input class="form-control" type="text" name="property_lang[property_selector][<?=$code?>]" value="<?=CHtml::encode($property[$code]['property_selector'])?>">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

	<div class="form-group">
		<label for="form-property_top" class="col-md-3 control-label"><?=Yii::t('properties', 'Top property')?>:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-property_top">
					<input id="form-property_top" type="checkbox" name="property[property_top]" value="1"<?php if ($property['property_top'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-property_filter" class="col-md-3 control-label"><?=Yii::t('properties', 'Property in filter')?>:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-property_filter">
					<input id="form-property_filter" type="checkbox" name="property[property_filter]" value="1"<?php if ($property['property_filter'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-property_variant" class="col-md-3 control-label"><?=Yii::t('properties', 'Property variant')?>:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-property_variant">
					<input id="form-property_variant" type="checkbox" name="property[property_variant]" value="1"<?php if ($property['property_variant'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-property_option" class="col-md-3 control-label"><?=Yii::t('properties', 'Property option')?>:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-property_option">
					<input id="form-property_option" type="checkbox" name="property[property_option]" value="1"<?php if ($property['property_option'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-property_cart" class="col-md-3 control-label"><?=Yii::t('properties', 'Property cart')?>:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-property_cart">
					<input id="form-property_cart" type="checkbox" name="property[property_cart]" value="1"<?php if ($property['property_cart'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-property_color" class="col-md-3 control-label"><?=Yii::t('properties', 'Property color')?>:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-property_color">
					<input id="form-property_color" type="checkbox" name="property[property_color]" value="1"<?php if ($property['property_color'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-property_size" class="col-md-3 control-label"><?=Yii::t('properties', 'Property size')?>:</label>
		<div class="col-md-6">
			<div class="checkbox">
				<label for="form-property_size">
					<input id="form-property_size" type="checkbox" name="property[property_size]" value="1"<?php if ($property['property_size'] == 1) { ?> checked<?php } ?>>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-product_categories" class="col-md-3 control-label"><?=Yii::t('properties', 'Categories')?>:</label>
		<div class="col-md-6">
			<select id="form-product_categories" class="form-control select2" name="categories[]" data-placeholder="<?=Yii::t('properties', 'Select a category')?>" multiple>
				<?php if (!empty($categories_tree)) { ?>
				<?=showCategoriesTree($categories_tree, $property_categories)?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-filter_categories" class="col-md-3 control-label"><?=Yii::t('properties', 'Categories')?> фильтров:</label>
		<div class="col-md-6">
			<select id="form-filter_categories" class="form-control select2" name="filter_categories[]" data-placeholder="<?=Yii::t('properties', 'Select a category')?>" multiple>
				<?php if (!empty($categories_tree)) { ?>
				<?=showCategoriesTree($categories_tree, $property_filters)?>
				<?php } ?>
			</select>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<div class="btn-group-vertical btn-insert-link" role="group" aria-label="<?=Yii::t('app', 'Insert/remove a link label')?>">
  <button id="typograph-link-btn" title="<?=Yii::t('app', 'Typography text btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-text-size"></i></button>
  <button id="insert-link-btn" title="<?=Yii::t('app', 'Insert a link btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
  <button id="remove-link-btn" title="<?=Yii::t('app', 'Remove all links btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
</div>

<!-- Insert Link Modal -->
<div class="modal fade" id="insertLinkModal" tabindex="-1" role="dialog" aria-labelledby="insertLinkModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="<?=Yii::t('app', 'Close')?>"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="insertLinkModalLabel"><?=Yii::t('app', 'Insert a link label')?></h4>
			</div>
			<div class="modal-body">
			<form id="insert-link">
				<div class="form-group">
					<label for="link-text" class="control-label"><?=Yii::t('app', 'Link text')?>:</label>
					<input type="text" class="form-control" id="link-text">
				</div>
				<div class="form-group">
					<label for="link-url" class="control-label"><?=Yii::t('app', 'Link tip')?>:</label>
					<input type="text" class="form-control" id="link-url">
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label for="link-blank">
							<input id="link-blank" type="checkbox" value="1"> <?=Yii::t('app', 'Open in new tab')?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="link-title" class="control-label"><?=Yii::t('app', 'Link tip (title)')?>:</label>
					<input type="text" class="form-control" id="link-title">
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=Yii::t('app', 'Cancel btn')?></button>
				<button id="do-insert" type="button" class="btn btn-primary"><?=Yii::t('app', 'Insert btn')?></button>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var form = $("#manage-property"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	$('#typograph-link-btn').on('click', function(e) {
		var input = focused,
			text = input.val(),
			btn = $(this);

		if (focused != null) {
			btn.prop('disabled', true);
			
			$.ajax({type: "POST", url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/typograph', data: $.param({text: focused.val()}), cache: false, dataType: "json",
				success: function(data) {
					btn.prop('disabled', false);
					
					if (data.error == 'Y') {
						bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					}
					else {
						if (focused.hasClass('form-redactor')) {
							$('#' + focused[0].id).redactor('code.set', data.text);
						}
						else {
							focused.val(data.text);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					btn.prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});
	
	$('#insert-link-btn').on('click', function(e) {
		var input = focused,
			len = input.val().length,
			start = input[0].selectionStart,
			end = input[0].selectionEnd,
			selectedText = input.val().substring(start, end);

		$('#link-text').val(selectedText);
		$('#link-url').val('');
		$('#link-title').val('');
		$('#link-blank').prop('checked', false);

		if (focused != null) {
			$('#insertLinkModal').modal('show');
		}
	});

	$('#remove-link-btn').on('click', function(e) {
		if (focused != null) {
			var input = focused,
				new_value = input.val().replace(/<a[^>]*>([\s\S]*?)<\/a>/ig, '$1');

			input.val(new_value);
		}
	});

	$('#insert-link').submit(function() {
		var link_url = $.trim($('#link-url').val()),
			link_text = $.trim($('#link-text').val()),
			link_title = $.trim($('#link-title').val()),
			link_blank = $('#link-blank').prop('checked');

		if (link_url == '' || link_text == '') {
			$('#insertLinkModal').modal('hide');
			return false;
		}

		var new_link = $('<a href="' + link_url + '"/>');
		new_link.text(link_text);

		if (link_title != '') {
			new_link.attr('title', link_title);
		}

		if (link_blank) {
			new_link.attr('target', '_blank');
		}

		var len = focused.val().length,
			start = focused[0].selectionStart,
			end = focused[0].selectionEnd,
			selectedText = focused.val().substring(start, end);

		focused.val(focused.val().substring(0, start) + new_link[0].outerHTML + focused.val().substring(end, len));
		focused = null;

		$('#insertLinkModal').modal('hide');

		return false;
	});

	$('#do-insert').click(function() {
		$('#insert-link').submit();

		return false;
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				// error for lang fields
				if ($(this).parent().hasClass('tab-pane')) {
					var that = this,
						tab = $(this).parent().parent().prev().children(':eq(' + $(this).parent().index() + ')').children(),
						alert_callback = function() {
							if (that.id == 'form-property_description') {
								setTimeout(function() {
									tab.click();
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									tab.click();
									$(that).focus();
								}, 21);
							}
						};

					if ($(this).parent()[0].id.indexOf('form-property_title') === 0) {
						bootbox.alert("<?=Yii::t('properties', 'Enter a property title!')?>", alert_callback);
					}
				}
				else {
					var that = this,
						alert_callback = function() {
							if (that.id == 'form-redactor') {
								setTimeout(function() {
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									$(that).focus();
								}, 21);
							}
						};

					switch (this.id) {
						case 'form-property_title':
							bootbox.alert("<?=Yii::t('properties', 'Enter a property title!')?>", alert_callback);
							break;
					}
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});
});
</script>