<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('brands', 'Brands h1')?></h1>

<?php
	$brand_url_data = array();

	if ($sort != 'default') {
		$brand_url_data['sort'] = $sort;
		$brand_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$brand_url_data['keyword'] = $keyword;
	}

	if (!empty($show_all)) {
		$brand_url_data['show_all'] = $show_all;
	}

	if (!empty($page)) {
		$brand_url_data['page'] = $page + 1;
	}

	$brand_new_url = $this->createUrl('brand', array_merge(array('id' => 'new'), $brand_url_data));

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/brand';
?>
<p class="text-center"><a class="btn btn-success" href="<?=$brand_new_url?>"><?=Yii::t('brands', 'Add brand btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('brands', 'ID | brand name placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<!-- <br><div class="checkbox" style="margin-top: 10px"><label for="show_all"><input id="show_all" type="checkbox" name="show_all" value="1"<?php if (!empty($show_all)) { ?> checked<?php } ?>> Показать все</label></div> -->
		<?php if ($sort != 'default' || !empty($show_all) || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('brands')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($brands)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-brands" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="brand_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array('keyword' => $keyword);
					} else {
						$sort_data = array();
					}
				?>
				<th style="width: 4%"></th>
				<th style="width: 8%">
					<?php if ($sort == 'brand_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('brands', array_merge(array('sort' => 'brand_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'brand_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('brands', array_merge(array('sort' => 'brand_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('brands', array_merge(array('sort' => 'brand_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<!-- <th style="width: 16%">
					<?=Yii::t('brands', 'Photo')?>
				</th> -->
				<th>
					<?php if ($sort == 'brand_name' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('brands', array_merge(array('sort' => 'brand_name', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('brands', 'Brand name col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'brand_name' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('brands', array_merge(array('sort' => 'brand_name', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('brands', 'Brand name col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('brands', array_merge(array('sort' => 'brand_name', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('brands', 'Brand name col')?></a>
					<?php } ?>
				</th>
				<!-- <th>
					<?=Yii::t('brands', 'Brand country col')?>
				</th>
				<th width="10%">Скидка</th> -->
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($brands as $id => $brand) { ?>
			<?php
				$brand_id = $brand['brand_id'];

				$brand_url = $this->createUrl('brand', array_merge(array('id' => $brand_id), $brand_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$brand_id?>">
				</td>
				<td>
					<a href="<?=$brand_url?>"><?=$brand_id?></a>
				</td>
				<!-- <td>
					<?php if (!empty($brand['brand_logo'])) { ?>
					<?php
						$photo = json_decode($brand['brand_logo'], true);
					?>
					<a href="<?=$brand_url?>"><img src="<?=$assetsUrl . '/' . $brand_id . '/' . $photo['small']['path']?>" width="<?=$photo['small']['size']['w']?>" height="<?=$photo['small']['size']['h']?>" alt="" style="max-width: 70px; height: auto"></a>
					<?php } ?>
				</td> -->
				<td>
					<a href="<?=$brand_url?>"><?=CHtml::encode($brand['brand_name'])?></a>
				</td>
				<!-- <td>
					<a href="<?=$brand_url?>"><?=CHtml::encode($brand['brand_country'])?></a>
				</td>
				<td>
					<?=$brand['brand_discount'] ? $brand['brand_discount'] . '%' : '—'?>
				</td> -->
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$brand_id?>">
						<div class="btn-group">
							<?php if ($brand['active']) { ?>
							<a title="<?=Yii::t('brands', 'Active')?>" class="active-btn btn btn-default btn-sm btn-success" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ok"></span></a>
							<?php } else { ?>
							<a title="<?=Yii::t('brands', 'Blocked')?>" class="block-btn btn btn-default btn-sm btn-danger" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ban-circle"></span></a>
							<?php } ?>
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$brand_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="active"><?=Yii::t('app', 'Activate selected')?></option>
								<option value="block"><?=Yii::t('app', 'Block selected')?></option>
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if (empty($show_all) && $total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}
			
			submit_form = true;
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-brands').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-brands').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-brands").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>