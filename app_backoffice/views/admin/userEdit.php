<?php
/* @var $this AdminController */
?>
<?php
	$users_url_data = array();

	if (!empty($sort)) {
		$users_url_data['sort'] = $sort;
		$users_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$users_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$users_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('users', $users_url_data);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-user" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="user[user_id]" value="<?=$user['user_id']?>">

	<div class="user-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>

	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label">ID:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$user['user_id']?></p>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-4">
			<select id="form-active" name="user[active]" class="form-control">
				<option value="0"<?php if ($user['active'] == 0) { ?> selected<?php } ?>><?=Yii::t('users', 'Blocked')?></option>
				<option value="1"<?php if ($user['active'] == 1) { ?> selected<?php } ?>><?=Yii::t('users', 'Active')?></option>
			</select>
		</div>
	</div>

	<?php /* <div class="form-group">
		<label for="form-user_login" class="col-md-3 control-label"><?=Yii::t('users', 'User login')?>:</label>
		<div class="col-md-4 control-required">
			<input id="form-user_login" class="form-control" type="text" name="user[user_login]" value="<?=CHtml::encode($user['user_login'])?>">
		</div>
	</div> */ ?>

	<div class="form-group">
		<label for="form-user_email" class="col-md-3 control-label">Email:</label>
		<div class="col-md-4 control-required">
			<input id="form-user_email" class="form-control" type="text" name="user[user_email]" value="<?=CHtml::encode($user['user_email'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_phone" class="col-md-3 control-label"><?=Yii::t('users', 'Phone')?>:</label>
		<div class="col-md-4">
			<input id="form-user_phone" class="form-control" type="text" name="user[user_phone]" value="<?=CHtml::encode($user['user_phone'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_password" class="col-md-3 control-label"><?=Yii::t('users', 'User password')?>:</label>
		<div class="col-md-4">
			<input id="form-user_password" class="form-control" type="text" name="user[user_password]" value="">
		</div>
		<div class="col-md-5 form-control-static">
			<small><?=Yii::t('users', 'leave blank if you do not need to change password')?></small>
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_first_name" class="col-md-3 control-label"><?=Yii::t('users', 'User name')?>:</label>
		<div class="col-md-4">
			<input id="form-user_first_name" class="form-control" type="text" name="user[user_first_name]" value="<?=CHtml::encode($user['user_first_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_last_name" class="col-md-3 control-label"><?=Yii::t('users', 'User last name')?>:</label>
		<div class="col-md-4">
			<input id="form-user_last_name" class="form-control" type="text" name="user[user_last_name]" value="<?=CHtml::encode($user['user_last_name'])?>">
		</div>
	</div>

	<?php /* <div class="form-group">
		<label for="form-user_zip" class="col-md-3 control-label"><?=Yii::t('users', 'Zip')?>:</label>
		<div class="col-md-4">
			<input id="form-user_zip" class="form-control" type="text" name="user[user_zip]" value="<?=CHtml::encode($user['user_zip'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_city" class="col-md-3 control-label"><?=Yii::t('users', 'City')?>:</label>
		<div class="col-md-4">
			<input id="form-user_city" class="form-control" type="text" name="user[user_city]" value="<?=CHtml::encode($user['user_city'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_address" class="col-md-3 control-label"><?=Yii::t('users', 'Address 1')?>:</label>
		<div class="col-md-4">
			<input id="form-user_address" class="form-control" type="text" name="user[user_address]" value="<?=CHtml::encode($user['user_address'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_address_2" class="col-md-3 control-label"><?=Yii::t('users', 'Address 2')?>:</label>
		<div class="col-md-4">
			<input id="form-user_address_2" class="form-control" type="text" name="user[user_address_2]" value="<?=CHtml::encode($user['user_address_2'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-discount" class="col-md-3 control-label"><?=Yii::t('users', 'Discount')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-discount" class="form-control" type="text" name="user[discount]" value="<?=CHtml::encode($user['discount'])?>">
				<span class="input-group-addon">%</span>
			</div>
		</div>
	</div> */ ?>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<div class="page-header">
	<h3><?=Yii::t('users', 'User orders')?></h3>
</div>

<?php if (empty($orders)) { ?>
<p><?=Yii::t('users', 'No orders')?></p>
<?php } else { ?>
<table class="table-data table table-striped">
	<thead>
		<tr>
			<th style="width: 2%"></th>
			<th style="width: 8%">#</th>
			<th style="width: 12%"><?=Yii::t('orders', 'Created col')?></th>
			<th style="width: 17%"><?=Yii::t('orders', 'Name col')?></th>
			<th style="width: 17%"><?=Yii::t('orders', 'Phone col')?></th>
			<th style="width: 10%"><?=Yii::t('orders', 'Price col')?></th>
			<th style="width: 10%"><?=Yii::t('orders', 'Margin col')?></th>
			<th style="width: 9%"><?=Yii::t('orders', 'Status col')?></th>
			<th width="14%"></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($orders as $id => $order) { ?>
		<?php
			$order_id = $order['order_id'];
			$order_url = $this->createUrl('order', array('id' => $order_id));

			$status_class = '';

			switch ($order['status']) {
				case 'processing':
					$status_class = ' label-warning';
					$status = Yii::t('orders', 'Status in processing');
					break;
				case 'paid':
					$status_class = ' label-success';
					$status = Yii::t('orders', 'Status paid');
					break;
				case 'completed':
					$status_class = ' label-success';
					$status = Yii::t('orders', 'Status completed');
					break;
				case 'cancelled':
					$status_class = ' label-default';
					$status = Yii::t('orders', 'Status cancelled');
					break;
				case 'payment_error':
					$status_class = ' label-danger';
					$status = Yii::t('orders', 'Status payment error');
					break;
				default:
					$status_class = ' label-info';
					$status = Yii::t('orders', 'Status new');
			}
		?>
		<tr<?php if ($order['is_new']) { ?> class="warning"<?php } ?>>
			<td>
				<input type="checkbox" name="selected[]" value="<?=$order_id?>">
			</td>
			<td>
				<a href="<?=$order_url?>"><?=$order_id?></a>
			</td>
			<td>
				<?php $created = new DateTime($order['created'], new DateTimezone(Yii::app()->timeZone)); ?>
				<a href="<?=$order_url?>"><?=$created->format('d.m.Y')?></a>
			</td>
			<td><a href="<?=$order_url?>"><?=CHtml::encode($order['first_name'] . ' ' . $order['last_name'])?></a></td>
			<td><a href="<?=$order_url?>"><?=CHtml::encode($order['phone'])?></a></td>
			<td><a href="<?=$order_url?>">€<?=CHtml::encode($order['price'])?></a></td>
			<td><a href="<?=$order_url?>">€<?=CHtml::encode($order['margin'])?></a></td>
			<td><span class="label<?=$status_class?>"><?=$status?></span></td>
			<td class="text-right" style="border-right: none;">
				<span class="edit-btns" data-id="<?=$order_id?>">
					<div class="btn-group">
						<a title="<?=Yii::t('orders', 'Download invoice')?>" class="btn btn-default btn-sm" href="<?=$this->createUrl('admin/invoice', array('id' => $order['order_id']))?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-download-alt"></span></a>
						<a title="<?=Yii::t('orders', 'Send invoice')?>" class="btn btn-default btn-sm send" href="#" data-toggle="tooltip" data-placement="top" data-id="<?=$order['order_id']?>" data-url="<?=$this->createUrl('admin/invoice', array('id' => $order['order_id']))?>"><span class="glyphicon glyphicon-send"></span></a>
						<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$order_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
					</div>
				</span>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php } ?>

<div class="modal fade" id="invoice-sent" tabindex="-1" role="dialog" aria-labelledby="invoice-sent-label">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="invoice-sent-label"><?=Yii::t('orders', 'Send invoice')?> <span></span></h4>
		</div>
		<div class="modal-body">
			<form action="" method="post" novalidate>
				<input type="hidden" name="action" value="send">
				<div class="form-group">
					<label for="invoice-email" class="control-label">Email:</label>
					<input type="email" class="form-control" id="invoice-email" name="email">
				</div>
				<div class="form-group">
					<label for="invoice-message" class="control-label"><?=Yii::t('orders', 'Message')?>:</label>
					<textarea class="form-control" id="invoice-message" name="message"></textarea>
				</div>
				<div class="form-group text-right" style="margin-bottom: 0">
					<button class="btn btn-primary"><?=Yii::t('orders', 'Send')?></button>
				</div>
			</form>
		</div>
	</div>
	</div>
</div>

<script>
var is_user = true;

$(document).ready(function(){
	var form = $("#manage-user"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				var that = this,
					alert_callback = function() {
						if (that.id == 'form-redactor') {
							setTimeout(function() {
								$(that).redactor('focus.setStart');
							}, 21);
						}
						else {
							setTimeout(function() {
								$(that).focus();
							}, 21);
						}
					};

				switch (this.id) {
					case 'form-user_login':
						bootbox.alert("<?=Yii::t('users', 'Enter a user login!')?>", alert_callback);
						break;
					case 'form-user_email':
						bootbox.alert("<?=Yii::t('users', 'Enter a user email!')?>", alert_callback);
						break;
					case 'form-user_password':
						bootbox.alert("<?=Yii::t('users', 'Enter a user password!')?>", alert_callback);
						break;
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});

	var invoice_xhr,
		is_invoice_xhr = false;

	$('.send').on('click', function(e) {
		e.preventDefault();

		$('#invoice-sent').find('form').attr('action', $(this).data('url'));
		$('#invoice-sent').find('h4 span').text('#' + $(this).data('id'));
		$('#invoice-sent').modal('show');
	});

	$('#invoice-sent').on('submit', 'form', function(e) {
		e.preventDefault();

		if (is_invoice_xhr) {
			return false;
		}

		var form = $(this),
			form_data = form.serialize();
		
		is_invoice_xhr = true;
		form.find(':input').prop('disabled', true);

		invoice_xhr = $.ajax({
			type: 'POST', 
			url: form.attr('action'), 
			data: form_data, 
			cache: false, 
			dataType: 'json',
			success: function(data) {
				is_invoice_xhr = false;
				form.find(':input').prop('disabled', false);
				form.find('.has-error').removeClass('has-error').find('.help-block').remove();

				if (data.error) {
					$('#invoice-email').after('<span class="help-block">' + data.errorMessage + '</span>').parent().addClass('has-error');
					return;
				}

				form.addClass('hidden').after('<h4>' + data.message + '</h4>');
			},
			error: function(jqXHR, textStatus, errorThrown) { 
				is_invoice_xhr = false;
				form.find(':input').prop('disabled', false);
				
				alert('Request error!');
				// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
			}
		});
	}).on('hidden.bs.modal', function (e) {
		$(this).find('.has-error').removeClass('has-error').find('.help-block').remove();
		$(this).find('input[type="email"], textarea').val('');
		$(this).find('form').removeClass('hidden').next('h4').remove();

		if (is_invoice_xhr) {
			is_invoice_xhr = false;
			invoice_xhr.abort();
		}
	});
});
</script>