<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('orders', 'Orders h1')?></h1>

<?php
	$order_url_data = array();
	$sort_data = array();

	if ($sort != 'default') {
		$order_url_data['sort'] = $sort;
		$order_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$order_url_data['keyword'] = $keyword;
		$sort_data['keyword'] = $keyword;
	}

	if (!empty($date_from)) {
		$order_url_data['date_from'] = $date_from;
		$sort_data['date_from'] = $date_from;
	}

	if (!empty($date_to)) {
		$order_url_data['date_to'] = $date_to;
		$sort_data['date_to'] = $date_to;
	}

	if (!empty($page)) {
		$order_url_data['page'] = $page + 1;
	}

	$order_new_url = $this->createUrl('order', array_merge(array('id' => 'new'), $order_url_data));
?>
<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('orders', 'Order # | name | email placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<input style="width: 110px;" class="form-control input-sm datepicker" data-date-format="DD.MM.YYYY" type="text" name="date_from" placeholder="Дата от" value="<?=CHtml::encode($date_from)?>">
		<input style="width: 110px;" class="form-control input-sm datepicker" data-date-format="DD.MM.YYYY" type="text" name="date_to" placeholder="Дата по" value="<?=CHtml::encode($date_to)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<?php if ($sort != 'default' || !empty($keyword) || !empty($date_from) || !empty($date_to)) { ?>
		<br><a href="<?=$this->createUrl('orders')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($orders)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-course-orders" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="order_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<th style="width: 2%"></th>
				<th style="width: 8%">
					<?php if ($sort == 'order_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('orders', array_merge(array('sort' => 'order_id', 'direction' => 'desc'), $sort_data))?>">#</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'order_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('orders', array_merge(array('sort' => 'order_id', 'direction' => 'asc'), $sort_data))?>">#</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('orders', array_merge(array('sort' => 'order_id', 'direction' => 'asc'), $sort_data))?>">#</a>
					<?php } ?>
				</th>
				<th style="width: 13%">
					<?php if ($sort == 'created' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('orders', array_merge(array('sort' => 'created', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('orders', 'Created col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'created' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('orders', array_merge(array('sort' => 'created', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('orders', 'Created col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('orders', array_merge(array('sort' => 'created', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('orders', 'Created col')?></a>
					<?php } ?>
				</th>
				<th style="width: 16%"><?=Yii::t('orders', 'Name col')?></th>
				<th style="width: 14%"><?=Yii::t('orders', 'Phone col')?></th>
				<th style="width: 10%"><?=Yii::t('orders', 'Price col')?></th>
				<th style="width: 10%"><?=Yii::t('orders', 'Status col')?></th>
				<th width="12%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($orders as $id => $order) { ?>
			<?php
				$order_id = $order['order_id'];
				$order_url = $this->createUrl('order', array_merge(array('id' => $order_id), $order_url_data));

				$status_class = '';

				switch ($order['status']) {
					case 'processing':
						$status_class = ' label-warning';
						$status = Yii::t('orders', 'Status in processing');
						break;
					case 'paid':
						$status_class = ' label-success';
						$status = Yii::t('orders', 'Status paid');
						break;
					case 'completed':
						$status_class = ' label-success';
						$status = Yii::t('orders', 'Status completed');
						break;
					case 'cancelled':
						$status_class = ' label-default';
						$status = Yii::t('orders', 'Status cancelled');
						break;
					case 'payment_error':
						$status_class = ' label-danger';
						$status = Yii::t('orders', 'Status payment error');
						break;
					default:
						$status_class = ' label-info';
						$status = Yii::t('orders', 'Status new');
				}

				$order['discount_value'] = (float) $order['discount_value'];
				$order['delivery_price'] = (float) $order['delivery_price'];
			?>
			<tr<?php if ($order['is_new']) { ?> class="warning"<?php } ?>>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$order_id?>">
				</td>
				<td>
					<a href="<?=$order_url?>"><?=$order_id?></a>
				</td>
				<td>
					<?php $created = new DateTime($order['created'], new DateTimezone(Yii::app()->timeZone)); ?>
					<a href="<?=$order_url?>"><?=$created->format('d.m.Y H:i')?></a>
				</td>
				<td><a href="<?=$order_url?>"><?=CHtml::encode($order['first_name'] . ' ' . $order['last_name'])?></a></td>
				<td><a href="<?=$order_url?>"><?=CHtml::encode($order['phone'])?></a></td>
				<td>
					<a href="<?=$order_url?>"><?=Currency::format($order['price'] + $order['discount_value'], $order['currency'])?></a>
					<?php if ($order['discount_value']) { ?>
					<!-- <br><small><?=Currency::format($order['discount_value'], $order['currency'])?></small> -->
					<br><span class="label label-danger">скидка</span>
					<?php } ?>
					<?php if (!empty($order['delivery_price'])) { ?>
					<!-- <br><small><?=Currency::format($order['delivery_price'], $order['delivery_currency'])?></small> -->
					<?php } ?>
				</td>
				<td><span class="label<?=$status_class?>"><?=$status?></span></td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$order_id?>">
						<div class="btn-group">
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$order_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="9">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<div class="modal fade" id="invoice-sent" tabindex="-1" role="dialog" aria-labelledby="invoice-sent-label">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="invoice-sent-label"><?=Yii::t('orders', 'Send invoice')?> <span></span></h4>
		</div>
		<div class="modal-body">
			<form action="" method="post" novalidate>
				<input type="hidden" name="action" value="send">
				<div class="form-group">
					<label for="invoice-email" class="control-label">Email:</label>
					<input type="email" class="form-control" id="invoice-email" name="email">
				</div>
				<div class="form-group">
					<label for="invoice-message" class="control-label"><?=Yii::t('orders', 'Message')?>:</label>
					<textarea class="form-control" id="invoice-message" name="message"></textarea>
				</div>
				<div class="form-group text-right" style="margin-bottom: 0">
					<button class="btn btn-primary"><?=Yii::t('orders', 'Send')?></button>
				</div>
			</form>
		</div>
	</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			del_result = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}
			
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-course-orders').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					del_result = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-course-orders').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-course-orders").submit(function() {
			if (del_result) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						del_result = true;
						that.submit();
					}
				});
				
				return false;
			}
		});

		var invoice_xhr,
			is_invoice_xhr = false;

		$('.send').on('click', function(e) {
			e.preventDefault();

			$('#invoice-sent').find('form').attr('action', $(this).data('url'));
			$('#invoice-sent').find('h4 span').text('#' + $(this).data('id'));
			$('#invoice-sent').modal('show');
		});

		$('#invoice-sent').on('submit', 'form', function(e) {
			e.preventDefault();

			if (is_invoice_xhr) {
				return false;
			}

			var form = $(this),
				form_data = form.serialize();
			
			is_invoice_xhr = true;
			form.find(':input').prop('disabled', true);

			invoice_xhr = $.ajax({
				type: 'POST', 
				url: form.attr('action'), 
				data: form_data, 
				cache: false, 
				dataType: 'json',
				success: function(data) {
					is_invoice_xhr = false;
					form.find(':input').prop('disabled', false);
					form.find('.has-error').removeClass('has-error').find('.help-block').remove();

					if (data.error) {
						$('#invoice-email').after('<span class="help-block">' + data.errorMessage + '</span>').parent().addClass('has-error');
						return;
					}

					form.addClass('hidden').after('<h4>' + data.message + '</h4>');
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					is_invoice_xhr = false;
					form.find(':input').prop('disabled', false);
					
					alert('Request error!');
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}).on('hidden.bs.modal', function (e) {
			$(this).find('.has-error').removeClass('has-error').find('.help-block').remove();
			$(this).find('input[type="email"], textarea').val('');
			$(this).find('form').removeClass('hidden').next('h4').remove();

			if (is_invoice_xhr) {
				is_invoice_xhr = false;
				invoice_xhr.abort();
			}
		});
	});
</script>