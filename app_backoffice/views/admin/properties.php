<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('properties', 'Properties h1')?></h1>

<?php
	$property_url_data = array();

	if ($sort != 'default') {
		$property_url_data['sort'] = $sort;
		$property_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$property_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$property_url_data['page'] = $page + 1;
	}

	$property_new_url = $this->createUrl('property', array_merge(array('id' => 'new'), $property_url_data));

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/property';
?>
<p class="text-center"><a class="btn btn-success" href="<?=$property_new_url?>"><?=Yii::t('properties', 'Add property btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('properties', 'ID | property title placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<?php if ($sort != 'default' || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('properties')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($properties)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-properties" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="property_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array('keyword' => $keyword);
					} else {
						$sort_data = array();
					}
				?>
				<th style="width: 4%"></th>
				<th style="width: 8%">
					<?php if ($sort == 'property_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('properties', array_merge(array('sort' => 'property_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'property_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('properties', array_merge(array('sort' => 'property_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('properties', array_merge(array('sort' => 'property_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th style="width: 24%">
					<?php if ($sort == 'property_title' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('properties', array_merge(array('sort' => 'property_title', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('properties', 'Property title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'property_title' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('properties', array_merge(array('sort' => 'property_title', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('properties', 'Property title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('properties', array_merge(array('sort' => 'property_title', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('properties', 'Property title col')?></a>
					<?php } ?>
				</th>
				<th style="width: 24%">
					<?=Yii::t('properties', 'Categories col')?>
				</th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($properties as $id => $property) { ?>
			<?php
				$property_id = $property['property_id'];

				$property_url = $this->createUrl('property', array_merge(array('id' => $property_id), $property_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$property_id?>">
				</td>
				<td>
					<a href="<?=$property_url?>"><?=$property_id?></a>
				</td>
				<td>
					<a href="<?=$property_url?>"><?=CHtml::encode($property['property_title'])?></a>
					<?php if (!empty($property['property_top'])) { ?>
					&nbsp;<span class="label label-success">top</span>
					<?php } ?>
					<?php if (!empty($property['property_filter'])) { ?>
					&nbsp;<span class="label label-info">filter</span>
					<?php } ?>
					<?php if (!empty($property['property_variant'])) { ?>
					&nbsp;<span class="label label-warning">variants</span>
					<?php } ?>
					<?php if (!empty($property['property_option'])) { ?>
					&nbsp;<span class="label label-default">options</span>
					<?php } ?>
					<?php if (!empty($property['property_cart'])) { ?>
					&nbsp;<span class="label label-danger">cart</span>
					<?php } ?>
					<?php if (!empty($property['property_color'])) { ?>
					&nbsp;<span class="label label-default">color</span>
					<?php } ?>
					<?php if (!empty($property['property_size'])) { ?>
					&nbsp;<span class="label label-primary">size</span>
					<?php } ?>
					<?php if (!empty($property['property_hide'])) { ?>
					&nbsp;<span class="label label-default">hide</span>
					<?php } ?>
					<?php if (!empty($property['property_special'])) { ?>
					&nbsp;<span class="label label-info">special</span>
					<?php } ?>
					<?php if (!empty($property['property_icon'])) { ?>
					&nbsp;<span class="label label-warning">icon</span>
					<?php } ?>
				</td>
				<td>
					<?php if (!empty($property['categories'])) { ?>
					<?php
						$property_categories = array();

						foreach ($property['categories'] as $category_id) {
							if (isset($categories[$category_id])) {
								$property_categories[] = $categories[$category_id]['category_name'];
							}
						}

						usort($property_categories, 'strcmp');
					?>
					<?=implode(', ', $property_categories)?>
					<?php } ?>
				</td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$property_id?>">
						<div class="btn-group">
							<a title="<?=Yii::t('properties', 'Values list btn')?>" class="btn btn-default btn-sm" href="<?=$this->createUrl('propertyvalues', array('id' => $property_id))?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-list"></span></a>
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$property_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-properties').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-properties").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>