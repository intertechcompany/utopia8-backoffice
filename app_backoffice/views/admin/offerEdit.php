<?php
	/* @var $this AdminController */

	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$offers_url_data = array();

	if (!empty($sort)) {
		$offers_url_data['sort'] = $sort;
		$offers_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$offers_url_data['keyword'] = $keyword;
	}

	if (!empty($date_from)) {
		$offers_url_data['date_from'] = $date_from;
	}

	if (!empty($date_to)) {
		$offers_url_data['date_to'] = $date_to;
	}

	if (!empty($page)) {
		$offers_url_data['page'] = $page + 1;
	}

	$back_link = $this->createUrl('offers', $offers_url_data);
?>
<h1><?=Yii::t('offers', 'Edit offer h1')?> #<?=$offer['offer_id']?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-offer" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="offer[offer_id]" value="<?=$offer['offer_id']?>">
		
	<div class="page-header">
		<h3><?=Yii::t('offers', 'Offer details')?></h3>
	</div>

	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label"><?=Yii::t('offers', 'Offer #')?>:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$offer['offer_id']?></p>
		</div>
	</div>

	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label"><?=Yii::t('offers', 'Total amount')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<span class="input-group-addon">€</span>
				<input id="form-price" class="form-control" type="text" name="offer[price]" value="<?=$offer['price']?>" disabled>
			</div>
		</div>
	</div>

	<?php if (!empty($offer['user_id'])) { ?>
	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label"><?=Yii::t('offers', 'Margin')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<span class="input-group-addon">€</span>
				<input id="form-margin" class="form-control" type="text" name="offer[margin]" value="<?=$offer['margin']?>" disabled>
			</div>
		</div>
	</div>
	<?php } ?>

	<?php if (!empty($offer['discount'])) { ?>
	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label"><?=Yii::t('offers', 'Discount')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-discount" class="form-control" type="text" name="offer[discount]" value="<?=$offer['discount']?>" disabled>
				<span class="input-group-addon">%</span>
			</div>
		</div>
	</div>
	<?php } ?>

	<div class="form-group">
		<label for="form-first_name" class="col-md-3 control-label"><?=Yii::t('offers', 'First name')?>:</label>
		<div class="col-md-4">
			<input id="form-first_name" class="form-control" type="text" name="offer[first_name]" value="<?=CHtml::encode($offer['first_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-last_name" class="col-md-3 control-label"><?=Yii::t('offers', 'Last name')?>:</label>
		<div class="col-md-4">
			<input id="form-last_name" class="form-control" type="text" name="offer[last_name]" value="<?=CHtml::encode($offer['last_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-email" class="col-md-3 control-label">Email:</label>
		<div class="col-md-4">
			<input id="form-email" class="form-control" type="text" name="offer[email]" value="<?=CHtml::encode($offer['email'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-phone" class="col-md-3 control-label"><?=Yii::t('offers', 'Phone')?>:</label>
		<div class="col-md-4">
			<input id="form-phone" class="form-control" type="text" name="offer[phone]" value="<?=CHtml::encode($offer['phone'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-zip" class="col-md-3 control-label"><?=Yii::t('offers', 'Zip')?>:</label>
		<div class="col-md-4">
			<input id="form-zip" class="form-control" type="text" name="offer[zip]" value="<?=CHtml::encode($offer['zip'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-city" class="col-md-3 control-label"><?=Yii::t('offers', 'City')?>:</label>
		<div class="col-md-4">
			<input id="form-city" class="form-control" type="text" name="offer[city]" value="<?=CHtml::encode($offer['city'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-address" class="col-md-3 control-label"><?=Yii::t('offers', 'Address 1')?>:</label>
		<div class="col-md-6">
			<textarea id="form-address" class="form-control" name="offer[address]" rows="2"><?=CHtml::encode($offer['address'])?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label for="form-address_2" class="col-md-3 control-label"><?=Yii::t('offers', 'Address 2')?>:</label>
		<div class="col-md-6">
			<textarea id="form-address_2" class="form-control" name="offer[address_2]" rows="2"><?=CHtml::encode($offer['address_2'])?></textarea>
		</div>
	</div>

	<?php if (!empty($offer['delivery_to_address'])) { ?>
	<div class="page-header">
		<h3>Данные доставки</h3>
	</div>

	<div class="form-group">
		<label for="form-delivery_first_name" class="col-md-3 control-label"><?=Yii::t('offers', 'First name')?>:</label>
		<div class="col-md-4">
			<input id="form-delivery_first_name" class="form-control" type="text" name="offer[delivery_first_name]" value="<?=CHtml::encode($offer['delivery_first_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-delivery_last_name" class="col-md-3 control-label"><?=Yii::t('offers', 'Last name')?>:</label>
		<div class="col-md-4">
			<input id="form-delivery_last_name" class="form-control" type="text" name="offer[delivery_last_name]" value="<?=CHtml::encode($offer['delivery_last_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-delivery_email" class="col-md-3 control-label">Email:</label>
		<div class="col-md-4">
			<input id="form-delivery_email" class="form-control" type="text" name="offer[delivery_email]" value="<?=CHtml::encode($offer['delivery_email'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-delivery_phone" class="col-md-3 control-label"><?=Yii::t('offers', 'Phone')?>:</label>
		<div class="col-md-4">
			<input id="form-delivery_phone" class="form-control" type="text" name="offer[delivery_phone]" value="<?=CHtml::encode($offer['delivery_phone'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-delivery_zip" class="col-md-3 control-label"><?=Yii::t('offers', 'Zip')?>:</label>
		<div class="col-md-4">
			<input id="form-delivery_zip" class="form-control" type="text" name="offer[delivery_zip]" value="<?=CHtml::encode($offer['delivery_zip'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-delivery_city" class="col-md-3 control-label"><?=Yii::t('offers', 'City')?>:</label>
		<div class="col-md-4">
			<input id="form-delivery_city" class="form-control" type="text" name="offer[delivery_city]" value="<?=CHtml::encode($offer['delivery_city'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-delivery_address" class="col-md-3 control-label"><?=Yii::t('offers', 'Address 1')?>:</label>
		<div class="col-md-6">
			<textarea id="form-delivery_address" class="form-control" name="offer[delivery_address]" rows="2"><?=CHtml::encode($offer['delivery_address'])?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label for="form-delivery_address_2" class="col-md-3 control-label"><?=Yii::t('offers', 'Address 2')?>:</label>
		<div class="col-md-6">
			<textarea id="form-delivery_address_2" class="form-control" name="offer[delivery_address_2]" rows="2"><?=CHtml::encode($offer['delivery_address_2'])?></textarea>
		</div>
	</div>
	<?php } ?>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<?php if (!empty($products)) { ?>
<div class="page-header">
	<h3><?=Yii::t('offers', 'Offer products')?></h3>
</div>

<table class="table-data table table-striped">
	<thead>
		<tr>
			<th style="width: 2%"></th>
			<th style="width: 5%">ID</th>
			<th style="width: 10%"><?=Yii::t('offers', 'Photo')?></th>
			<th style="width: 11%"><?=Yii::t('offers', 'Product code')?></th>
			<th><?=Yii::t('offers', 'Title')?></th>
			<?php if (!empty($offer['user_id'])) { ?>
			<th style="width: 22%"><?=Yii::t('offers', 'Price seller')?></th>
			<?php } else { ?>
			<th style="width: 22%"><?=Yii::t('offers', 'Price')?></th>
			<?php } ?>
			<th style="width: 12%"><?=Yii::t('offers', 'Product price')?></th>
			<th style="width: 12%"><?=Yii::t('offers', 'Quantity')?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($products as $product) { ?>
		<?php 
			$product_image = '';

			if (!empty($product['product_alias'])) {
				$product_url = $this->createUrl('admin/product', array('id' => $product['product_id']));

				if (!empty($product['product_photo'])) {
					$product_image = json_decode($product['product_photo'], true);
					$product_image_size = $product_image['size']['catalog'];
					$product_image = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog'];
					$product_image = '<a href="' . $product_url . '"><img src="' . $product_image . '" width="' . $product_image_size['w'] . '" height="' . $product_image_size['h'] . '" style="max-width: 100px; max-height: 100px; width: auto; height: auto;"></a>';
				}

				$product_title = '<a href="' . $product_url . '">' . CHtml::encode($product['product_title']) . '</a>';

				$variants = '';

				if (!empty($product['variant'])) {
					$product['product_sku'] = $product['variant']['variant_sku'];
					$product['product_price'] = $product['variant']['variant_price'];

					if (!empty($product['variant']['values'])) {
						$variant_values = array();

						foreach ($product['variant']['values'] as $value) {
							$variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
						}
					
						$variants = '<br><small>' . implode("<br>\n", $variant_values) . '</small>';
					}
				}
			} else {
				$product_title = CHtml::encode($product['title']);

				if (!empty($product['variant_title'])) {
					$variants = '<br><small>' . str_replace("\n", "<br>\n", CHtml::encode($product['variant_title'])) . '</small>';
				}
			}

			if ($product['product_price_type'] == 'package') {
				$product['product_price'] = $product['product_price'] * $product['product_pack_size'];
				$product['product_price'] = round($product['product_price'], 2);
			}
		?>
		<tr>
			<td></td>
			<td><?=$product['product_id']?></td>
			<td><?=$product_image?></td>
			<td><?=CHtml::encode($product['product_sku'])?></td>
			<td>
				<?=$product_title?>
				<?=$variants?>
			</td>
			<?php if (!empty($offer['user_id'])) { ?>
			<td>€<?=$product['price']?> / €<?=$product['seller_price']?></td>
			<?php } else { ?>
			<td>€<?=$product['price']?></td>
			<?php } ?>
			<td>€<?=$product['product_price']?></td>
			<td><?=$product['quantity']?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php } ?>

<div class="form-horizontal">
	<div class="form-group">
		<div class="col-md-9">
			<a href="<?=$this->createUrl('admin/invoiceoffer', array('id' => $offer['offer_id']))?>" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;<?=Yii::t('offers', 'Download invoice')?></a>
			&nbsp;<a href="#" class="btn btn-default" data-toggle="modal" data-target="#invoice-sent"><span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;<?=Yii::t('offers', 'Send invoice')?></a>
		</div>
	</div>
</div>

<div class="modal fade" id="invoice-sent" tabindex="-1" role="dialog" aria-labelledby="invoice-sent-label">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="invoice-sent-label"><?=Yii::t('offers', 'Send invoice')?> <span></span></h4>
		</div>
		<div class="modal-body">
			<form action="<?=$this->createUrl('admin/invoiceoffer', array('id' => $offer['offer_id']))?>" method="post" novalidate>
				<input type="hidden" name="action" value="send">
				<div class="form-group">
					<label for="invoice-email" class="control-label">Email:</label>
					<input type="email" class="form-control" id="invoice-email" name="email">
				</div>
				<div class="form-group">
					<label for="invoice-message" class="control-label"><?=Yii::t('offers', 'Message')?>:</label>
					<textarea class="form-control" id="invoice-message" name="message"></textarea>
				</div>
				<div class="form-group text-right" style="margin-bottom: 0">
					<button class="btn btn-primary"><?=Yii::t('offers', 'Send')?></button>
				</div>
			</form>
		</div>
	</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var focused,
		form = $("#manage-offer"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				var that = this,
					alert_callback = function() {
						if (that.id == 'form-redactor') {
							setTimeout(function() {
								$(that).redactor('focus.setStart');
							}, 21);
						}
						else {
							setTimeout(function() {
								$(that).focus();
							}, 21);
						}
					};

				switch (this.id) {
					case 'form-firstname':
						bootbox.alert("Введите имя!", alert_callback);
						break;
					case 'form-mail':
						bootbox.alert("Введите email!", alert_callback);
						break;
					case 'form-phone':
						bootbox.alert("Введите телефон!", alert_callback);
						break;
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});

	var invoice_xhr,
		is_invoice_xhr = false;

	$('#invoice-sent').on('submit', 'form', function(e) {
		e.preventDefault();

		if (is_invoice_xhr) {
			return false;
		}

		var invoice_form = $(this),
			invoice_form_data = invoice_form.serialize();
		
		is_invoice_xhr = true;
		invoice_form.find(':input').prop('disabled', true);

		invoice_xhr = $.ajax({
			type: 'POST', 
			url: invoice_form.attr('action'), 
			data: invoice_form_data, 
			cache: false, 
			dataType: 'json',
			success: function(data) {
				is_invoice_xhr = false;
				invoice_form.find(':input').prop('disabled', false);
				invoice_form.find('.has-error').removeClass('has-error').find('.help-block').remove();

				if (data.error) {
					$('#invoice-email').after('<span class="help-block">' + data.errorMessage + '</span>').parent().addClass('has-error');
					return;
				}

				invoice_form.addClass('hidden').after('<h4>' + data.message + '</h4>');
			},
			error: function(jqXHR, textStatus, errorThrown) { 
				is_invoice_xhr = false;
				invoice_form.find(':input').prop('disabled', false);
				
				alert('Request error!');
				// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
			}
		});
	}).on('hidden.bs.modal', function (e) {
		$(this).find('.has-error').removeClass('has-error').find('.help-block').remove();
		$(this).find('input[type="email"], textarea').val('');
		$(this).find('form').removeClass('hidden').next('h4').remove();

		if (is_invoice_xhr) {
			is_invoice_xhr = false;
			invoice_xhr.abort();
		}
	});
});
</script>