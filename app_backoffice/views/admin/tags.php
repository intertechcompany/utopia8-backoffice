<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('tags', 'Tags h1')?></h1>

<?php
    $tag_url_data = array();

    if ($sort != 'default') {
        $tag_url_data['sort'] = $sort;
        $tag_url_data['direction'] = $direction;
    }

    if (!empty($keyword)) {
        $tag_url_data['keyword'] = $keyword;
    }

    if (!empty($page)) {
        $tag_url_data['page'] = $page + 1;
    }

    $tag_new_url = $this->createUrl('tag', array_merge(array('id' => 'new'), $tag_url_data));

    $has_rights = Yii::app()->getUser()->hasAccess($this->route, true);
?>
<?php if ($has_rights) { ?>
<p class="text-center"><a class="btn btn-success" href="<?=$tag_new_url?>"><?=Yii::t('tags', 'Add tag btn')?></a></p>
<?php } ?>

<form class="search-form form-inline text-center" method="get">
    <div class="form-group">
        <input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('tags', 'ID | tag name placeholder')?>" value="<?=CHtml::encode($keyword)?>">
        <button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
        <?php if ($sort != 'default' || !empty($keyword)) { ?>
        <br><a href="<?=$this->createUrl('tags')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
        <?php } ?>
    </div>
</form>

<?php if (!empty($tags)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-tags" class="form-inline" method="post">
    <input id="entity-id" type="hidden" name="tag_id" value="">
    <input id="entity-action" type="hidden" name="action" value="">

    <table class="table-data table table-striped">
        <thead>
            <tr>
                <?php
                    if (!empty($keyword)) {
                        $sort_data = array('keyword' => $keyword);
                    } else {
                        $sort_data = array();
                    }
                ?>
                <th style="width: 4%"></th>
                <th style="width: 10%">
                    <?php if ($sort == 'tag_id' && $direction == 'asc') { ?>
                    <a href="<?=$this->createUrl('tags', array_merge(array('sort' => 'tag_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
                    <?php } elseif ($sort == 'tag_id' && $direction == 'desc') { ?>
                    <a href="<?=$this->createUrl('tags', array_merge(array('sort' => 'tag_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
                    <?php } else { ?>
                    <a href="<?=$this->createUrl('tags', array_merge(array('sort' => 'tag_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
                    <?php } ?>
                </th>
                <th>
                    <?php if ($sort == 'tag_name' && $direction == 'asc') { ?>
                    <a href="<?=$this->createUrl('tags', array_merge(array('sort' => 'tag_name', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('tags', 'Tag name col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
                    <?php } elseif ($sort == 'tag_name' && $direction == 'desc') { ?>
                    <a href="<?=$this->createUrl('tags', array_merge(array('sort' => 'tag_name', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('tags', 'Tag name col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
                    <?php } else { ?>
                    <a href="<?=$this->createUrl('tags', array_merge(array('sort' => 'tag_name', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('tags', 'Tag name col')?></a>
                    <?php } ?>
                </th>
                <th width="14%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tags as $id => $tag) { ?>
            <?php
                $tag_id = $tag['tag_id'];

                $tag_url = $this->createUrl('tag', array_merge(array('id' => $tag_id), $tag_url_data));
            ?>
            <tr>
                <td>
                    <input type="checkbox" name="selected[]" value="<?=$tag_id?>">
                </td>
                <td>
                    <a href="<?=$tag_url?>"><?=$tag_id?></a>
                </td>
                <td>
                    <a href="<?=$tag_url?>"><?=CHtml::encode($tag['tag_name'])?></a>
                </td>
                <td class="text-right" style="border-right: none;">
                    <span class="edit-btns" data-id="<?=$tag_id?>">
                        <div class="btn-group">
                            <?php if ($has_rights) { ?>
                            <a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$tag_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
                            <?php } else { ?>
                            <a title="<?=Yii::t('app', 'View btn')?>" class="btn btn-default btn-sm" href="<?=$tag_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <?php } ?>
                        </div>
                    </span>
                </td>
            </tr>
            <?php } ?>
        </tbody>
        <?php if ($has_rights) { ?>
        <tfoot>
            <tr class="tBot">
                <td colspan="8">
                    <div class="bulk-actions clearfix">
                        <div class="form-group">
                            <span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
                        </div>
                        <div class="form-group">
                            <select id="bulkAction" class="form-control input-sm" name="bulkAction">
                                <option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
                            </select>
                            <strong id="topMsg"></strong>
                        </div>
                        <button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
                    </div>
                </td>
            </tr>
        </tfoot>
        <?php } ?>
    </table>
    <?php if ($total['pages'] > 1) { ?>
    <div class="pages text-center">
        <?php
            $this->widget('LinkPager', array(
                'pages' => $pages,
                'maxButtonCount' => 7,
                'htmlOptions' => array(
                    'class' => 'pagination',
                ),
            ));
        ?>
    </div>
    <?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
    $(document).ready(function(){
        var checkboxes = $(".table-data input[type=checkbox]"),
            submit_form = false;

        $(".block-btn, .active-btn").click( function(){
            if($(this).hasClass("block-btn")){
                $('#entity-action').val('active');
            } else {
                $('#entity-action').val('block');
            }

            submit_form = true;
            $('#entity-id').val($(this).parent().parent().attr("data-id"));
            $('#manage-tags').submit();

            return false;
        });

        $(".delete-btn").click( function(){
            var that = $(this);

            bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
                if (result) {
                    submit_form = true;

                    $('#entity-action').val('delete');
                    $('#entity-id').val(that.parent().parent().attr("data-id"));
                    $('#manage-tags').submit();
                }
            });

            return false;
        });

        $(".check-toggle").click( function(){
            if($(this).hasClass("checked"))
            {
                checkboxes.prop('checked', false);
            }
            else
            {
                checkboxes.prop('checked', true);
            }

            $(this).toggleClass("checked");
        });

        $("#manage-tags").submit(function() {
            if (submit_form) {
                return true;
            }

            if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
                return false;

            if ($("#bulkAction").val() == 'delete') {
                var that = $(this);

                bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
                    if (result) {
                        submit_form = true;
                        that.submit();
                    }
                });

                return false;
            }
        });
    });
</script>
