<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('products', 'Products h1')?></h1>

<?php
	$product_url_data = array();

	if ($sort != 'default') {
		$product_url_data['sort'] = $sort;
		$product_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$product_url_data['keyword'] = $keyword;
	}
	
	if (!empty($show_all)) {
		$product_url_data['show_all'] = $show_all;
	}

	if (!empty($page)) {
		$product_url_data['page'] = $page + 1;
	}

	$product_new_url = $this->createUrl('product', array_merge(array('id' => 'new'), $product_url_data));

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/product';
?>
<p class="text-center"><a class="btn btn-success" href="<?=$product_new_url?>"><?=Yii::t('products', 'Add product btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('products', 'ID | product title placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<!-- <br><div class="checkbox" style="margin-top: 10px"><label for="show_all"><input id="show_all" type="checkbox" name="show_all" value="1"<?php if (!empty($show_all)) { ?> checked<?php } ?>> Показать все</label></div> -->
		<?php if ($sort != 'default' || !empty($show_all) || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('products')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($products)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-products" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="product_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array('keyword' => $keyword);
					} else {
						$sort_data = array();
					}
				?>
				<th style="width: 4%"></th>
				<th style="width: 6%">
					<?php if ($sort == 'product_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'product_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th style="width: 11%">
					<?=Yii::t('products', 'Product photo col')?>
				</th>
				<th style="width: 25%">
					<?php if ($sort == 'product_title' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_title', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('products', 'Product title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'product_title' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_title', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('products', 'Product title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_title', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('products', 'Product title col')?></a>
					<?php } ?>
				</th>
				<th style="width: 11%">
					<?=Yii::t('products', 'Price')?>
				</th>
				<th style="width: 11%">
					Наличие
				</th>
				<th style="width: 12%">
					<?php if ($sort == 'product_rating' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_rating', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('products', 'Rating')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'product_rating' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_rating', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('products', 'Rating')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('products', array_merge(array('sort' => 'product_rating', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('products', 'Rating')?></a>
					<?php } ?>
				</th>
				<th width="20%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($products as $id => $product) { ?>
			<?php
				$product_id = $product['product_id'];

				$product_url = $this->createUrl('product', array_merge(array('id' => $product_id), $product_url_data));
				$product['product_price'] = (float) $product['product_price'];
				$product['product_price_old'] = (float) $product['product_price_old'];
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$product_id?>">
				</td>
				<td>
					<a href="<?=$product_url?>"><?=$product_id?></a>
				</td>
				<td>
					<?php if (!empty($product['product_photo'])) { ?>
					<?php
						$photo = json_decode($product['product_photo'], true);
					?>
					<a href="<?=$product_url?>"><img src="<?=$assetsUrl . '/' . $product_id . '/' . $photo['path']['catalog']['1x']?>" width="<?=$photo['size']['catalog']['1x']['w']?>" height="<?=$photo['size']['catalog']['1x']['h']?>" alt="" style="max-width: 70px; max-height: 70px; width: auto; height: auto"></a>
					<?php } ?>
				</td>
				<td>
					<?php if (!empty($product['brand_name'])) { ?>
					<small><?=CHtml::encode($product['brand_name'])?></small><br>
					<?php } ?>
					<a href="<?=$product_url?>"><?=CHtml::encode($product['product_title'])?></a><br>
					<?php if ($product['product_newest']) { ?>
					<span class="label label-info">new</span>
					<?php } ?>
					<?php if ($product['product_bestseller']) { ?>
					<span class="label label-success">bestseller</span>
					<?php } ?>
					<?php if ($product['product_sale']) { ?>
					<span class="label label-warning">sale</span>
					<?php } ?>
				</td>
				<td>
					<?=Yii::app()->numberFormatter->formatCurrency($product['product_price'], 'UAH')?>
					<?php if (!empty($product['product_price_old'])) { ?>
					<!-- <br><small><s><?=Yii::app()->numberFormatter->formatCurrency($product['product_price_old'], 'UAH')?></s></small> -->
					<?php } ?>
					<br><input class="form-control input-sm text-center" type="text" name="price[<?=$product_id?>]" value="<?=CHtml::encode($product['product_price_old'])?>" style="width: 50px; height: 26px">
				</td>
				<td>
					<?php if ($product['product_instock'] == 'in_stock') { ?>
					<span class="label label-success">в наличии</span>
					<!-- <br><small>Остаток: <?=$product['product_stock_qty']?></small> -->
					<?php if (!empty($product['stores'])) { ?>
					<?php foreach ($product['stores'] as $store) { ?>
					<br><small><?=CHtml::encode($store['store_name'])?>: <?=$store['quantity']?></small>
					<?php } ?>
					<?php } ?>
					<?php } elseif ($product['product_instock'] == 'out_of_stock') { ?>
					<span class="label label-danger">нет в наличии</span>
					<?php } elseif ($product['product_instock'] == 'preorder') { ?>
					<span class="label label-warning">предзаказ</span>
					<?php } ?>
				</td>
				<td>
					<input class="form-control input-sm text-center" type="text" name="product[<?=$product_id?>]" value="<?=CHtml::encode($product['product_rating'])?>" style="width: 50px">
				</td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$product_id?>">
						<div class="btn-group">
							<?php if ($product['active']) { ?>
							<a title="<?=Yii::t('products', 'Active')?>" class="active-btn btn btn-default btn-sm btn-success" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ok"></span></a>
							<?php } else { ?>
							<a title="<?=Yii::t('products', 'Blocked')?>" class="block-btn btn btn-default btn-sm btn-danger" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ban-circle"></span></a>
							<?php } ?>
							<a title="<?=Yii::t('app', 'Copy btn')?>" class="copy-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-duplicate"></span></a>
							<a title="<?=Yii::t('app', 'Preview btn')?>" class="btn btn-default btn-sm" href="<?=Yii::app()->params->url . 'product/' . $product['product_alias']?>" target="_blank" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-new-window"></span></a>
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$product_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="save"><?=Yii::t('app', 'Save positions option')?></option>
								<option value="active"><?=Yii::t('app', 'Activate selected')?></option>
								<option value="block"><?=Yii::t('app', 'Block selected')?></option>
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if (empty($show_all) && $total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}
			
			submit_form = true;
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-products').submit();
			
			return false;
		});

		$(".copy-btn").click( function(){
			submit_form = true;
			$('#entity-action').val('copy');
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-products').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-products').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-products").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>