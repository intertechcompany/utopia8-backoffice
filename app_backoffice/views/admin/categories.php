<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('categories', 'Categories h1')?></h1>

<?php
	$category_url_data = array();

	if (!empty($parent_id)) {
		$category_url_data['parent_id'] = $parent_id;
	}

	$category_new_url = $this->createUrl('category', array_merge(array('id' => 'new'), $category_url_data));

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/category';
?>
<p class="text-center" style="margin-bottom: 20px"><a class="btn btn-success" href="<?=$category_new_url?>"><?=Yii::t('categories', 'Add category btn')?></a></p>

<?php if (!empty($category)) { ?>
<?php
	$categories_path = array();

	foreach ($category['parents'] as $parent_category_id) {
		if ($parent_category_id == $category['category_id']) {
			$categories_path[] = $all_categories[$parent_category_id]['category_name'];
		}
		else {
			$categories_path[] = '<a href="' . $this->createUrl('categories', array('parent_id' => $all_categories[$parent_category_id]['category_id'])) . '">' . $all_categories[$parent_category_id]['category_name'] . '</a>';
		}
	}
?>
<p class="text-center"><?=Yii::t('categories', 'Path')?>: <a href="<?=$this->createUrl('categories')?>"><?=Yii::t('categories', 'All categories')?></a> / <?=implode(' / ', $categories_path)?></p>
<?php } else { ?>
<p class="text-center"><?=Yii::t('categories', 'Path')?>: <a href="<?=$this->createUrl('categories')?>"><?=Yii::t('categories', 'All categories')?></a></p>
<?php } ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=count($categories)?></strong></p>

<?php if (!empty($categories)) { ?>
<form id="manage-categories" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="category_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<th style="width: 4%"></th>
				<th style="width: 10%">ID</th>
				<th style="width: 58%"><?=Yii::t('categories', 'Category title col')?></th>
				<!-- <th style="width: 16%"><?=Yii::t('categories', 'Discount col')?></th> -->
				<th style="width: 10%"><?=Yii::t('categories', 'Position col')?></th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($categories as $id => $category) { ?>
			<?php
				$category_id = $category['category_id'];

				$subcategory_url = $this->createUrl('categories', array('parent_id' => $category_id));
				$category_url = $this->createUrl('category', array_merge(array('id' => $category_id), $category_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$category_id?>">
				</td>
				<td>
					<a href="<?=$subcategory_url?>"><?=$category_id?></a>
				</td>
				<td>
					<a href="<?=$subcategory_url?>"><?=CHtml::encode($category['category_name'])?></a>
					<?php if ($category['category_top']) { ?>
					<span class="label label-success">top</span>
					<?php } ?>
					<?php if ($category['category_home']) { ?>
					<span class="label label-warning">на главной</span>
					<?php } ?>
				</td>
				<!-- <td>
					<?php if (!empty($category['category_discount'])) { ?>
					<a href="<?=$subcategory_url?>"><?=CHtml::encode($category['category_discount'])?>%</a>
					<?php } else { ?>
					&mdash;
					<?php } ?>
				</td> -->
				<td>
					<input class="form-control input-sm text-center" type="text" name="category[<?=$category_id?>]" value="<?=CHtml::encode($category['category_position'])?>" style="width: 50px">
				</td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$category_id?>">
						<div class="btn-group">
							<?php if ($category['active']) { ?>
							<a title="<?=Yii::t('categories', 'Active')?>" class="active-btn btn btn-default btn-sm btn-success" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ok"></span></a>
							<?php } else { ?>
							<a title="<?=Yii::t('categories', 'Blocked')?>" class="block-btn btn btn-default btn-sm btn-danger" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ban-circle"></span></a>
							<?php } ?>
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$category_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="save"><?=Yii::t('app', 'Save positions option')?></option>
								<option value="active"><?=Yii::t('app', 'Activate selected')?></option>
								<option value="block"><?=Yii::t('app', 'Block selected')?></option>
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}
			
			submit_form = true;
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-categories').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-categories').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-categories").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>