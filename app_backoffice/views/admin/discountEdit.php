<?php
/* @var $this AdminController */
?>
<?php
	$discounts_url_data = array();

	if (!empty($sort)) {
		$discounts_url_data['sort'] = $sort;
		$discounts_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$discounts_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$discounts_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('discounts', $discounts_url_data);

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/discount';
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-discount" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="discount[discount_id]" value="<?=$discount['discount_id']?>">
		
	<div class="page-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>
	
	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label">ID:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$discount['discount_id']?></p>
		</div>
	</div>
		
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-3">
			<select id="form-active" name="discount[active]" class="form-control">
				<option value="0"<?php if ($discount['active'] == 0) { ?> selected<?php } ?>><?=Yii::t('discounts', 'Blocked')?></option>
				<option value="1"<?php if ($discount['active'] == 1) { ?> selected<?php } ?>><?=Yii::t('discounts', 'Active')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-discount_code" class="col-md-3 control-label"><?=Yii::t('discounts', 'Discount code')?>:</label>
		<div class="col-md-3">
			<input id="form-discount_code" class="form-control" type="text" name="discount[discount_code]" value="<?=CHtml::encode($discount['discount_code'])?>" readonly>
		</div>
	</div>

	<div class="form-group">
		<label for="form-discount_type" class="col-md-3 control-label"><?=Yii::t('discounts', 'Discount type')?>:</label>
		<div class="col-md-2">
			<select id="form-discount_type" name="discount[discount_type]" class="form-control">
				<option value="percentage"<?php if ($discount['discount_type'] == 'percentage') { ?> selected<?php } ?>><?=Yii::t('discounts', 'Percent')?></option>
				<option value="flat"<?php if ($discount['discount_type'] == 'flat') { ?> selected<?php } ?>><?=Yii::t('discounts', 'Value')?></option>
			</select>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-discount_value" class="col-md-3 control-label"><?=Yii::t('discounts', 'Discount value')?>:</label>
		<div class="col-md-2 control-required">
			<input id="form-discount_value" class="form-control" type="text" name="discount[discount_value]" value="<?=CHtml::encode($discount['discount_value'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label"><?=Yii::t('discounts', 'Activated')?>:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$discount['activated']?></p>
		</div>
	</div>

	<div class="form-group">
		<label for="form-discount_allowed_uses" class="col-md-3 control-label"><?=Yii::t('discounts', 'Allowed uses')?>:</label>
		<div class="col-md-1">
			<input id="form-discount_allowed_uses" class="form-control" type="text" name="discount[discount_allowed_uses]" value="<?=CHtml::encode($discount['discount_allowed_uses'])?>">
		</div>
		<div class="col-md-8 form-control-static">
			<small>0 — безлимитное использование</small>
		</div>
	</div>

	<div class="form-group">
		<label for="form-discount_start" class="col-md-3 control-label"><?=Yii::t('discounts', 'Date start')?>:</label>
		<div class="col-md-2">
			<?php
				if ($discount['discount_start'] != '0000-00-00') {
					$discount_start_date = new DateTime($discount['discount_start'], new DateTimeZone(Yii::app()->timeZone));
					$discount['discount_start'] = $discount_start_date->format('d.m.Y');
				}
				else {
					$discount['discount_start'] = '';
				}
			?>
			<input id="form-discount_start" class="form-control datepicker" data-date-format="DD.MM.YYYY" type="text" name="discount[discount_start]" value="<?=$discount['discount_start']?>">
		</div>
		<div class="col-md-7 form-control-static">
			<small>оставьте пустым, если нет необходимости в начальной дате</small>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-discount_end" class="col-md-3 control-label"><?=Yii::t('discounts', 'Date end')?>:</label>
		<div class="col-md-2">
			<?php
				if ($discount['discount_end'] != '0000-00-00') {
					$discount_end_date = new DateTime($discount['discount_end'], new DateTimeZone(Yii::app()->timeZone));
					$discount['discount_end'] = $discount_end_date->format('d.m.Y');
				}
				else {
					$discount['discount_end'] = '';
				}
			?>
			<input id="form-discount_end" class="form-control datepicker" data-date-format="DD.MM.YYYY" type="text" name="discount[discount_end]" value="<?=$discount['discount_end']?>">
		</div>
		<div class="col-md-7 form-control-static">
			<small>оставьте пустым, если нет необходимости в конечной дате</small>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-discount_comment" class="col-md-3 control-label"><?=Yii::t('discounts', 'Comment')?>:</label>
		<div class="col-md-6">
			<textarea id="form-discount_comment" class="form-control" type="text" name="discount[discount_comment]" rows="3"><?=CHtml::encode($discount['discount_comment'])?></textarea>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<div class="btn-group-vertical btn-insert-link" role="group" aria-label="<?=Yii::t('app', 'Insert/remove a link label')?>">
  <button id="typograph-link-btn" title="<?=Yii::t('app', 'Typography text btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-text-size"></i></button>
  <button id="insert-link-btn" title="<?=Yii::t('app', 'Insert a link btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
  <button id="remove-link-btn" title="<?=Yii::t('app', 'Remove all links btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
</div>

<!-- Insert Link Modal -->
<div class="modal fade" id="insertLinkModal" tabindex="-1" role="dialog" aria-labelledby="insertLinkModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="<?=Yii::t('app', 'Close')?>"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="insertLinkModalLabel"><?=Yii::t('app', 'Insert a link label')?></h4>
			</div>
			<div class="modal-body">
			<form id="insert-link">
				<div class="form-group">
					<label for="link-text" class="control-label"><?=Yii::t('app', 'Link text')?>:</label>
					<input type="text" class="form-control" id="link-text">
				</div>
				<div class="form-group">
					<label for="link-url" class="control-label"><?=Yii::t('app', 'Link tip')?>:</label>
					<input type="text" class="form-control" id="link-url">
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label for="link-blank">
							<input id="link-blank" type="checkbox" value="1"> <?=Yii::t('app', 'Open in new tab')?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="link-title" class="control-label"><?=Yii::t('app', 'Link tip (title)')?>:</label>
					<input type="text" class="form-control" id="link-title">
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=Yii::t('app', 'Cancel btn')?></button>
				<button id="do-insert" type="button" class="btn btn-primary"><?=Yii::t('app', 'Insert btn')?></button>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	var form = $("#manage-discount"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	$('#typograph-link-btn').on('click', function(e) {
		var input = focused,
			text = input.val(),
			btn = $(this);

		if (focused != null) {
			btn.prop('disabled', true);
			
			$.ajax({type: "POST", url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/typograph', data: $.param({text: focused.val()}), cache: false, dataType: "json",
				success: function(data) {
					btn.prop('disabled', false);
					
					if (data.error == 'Y') {
						bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					}
					else {
						if (focused.hasClass('form-redactor')) {
							$('#' + focused[0].id).redactor('code.set', data.text);
						}
						else {
							focused.val(data.text);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					btn.prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});
	
	$('#insert-link-btn').on('click', function(e) {
		var input = focused,
			len = input.val().length,
			start = input[0].selectionStart,
			end = input[0].selectionEnd,
			selectedText = input.val().substring(start, end);

		$('#link-text').val(selectedText);
		$('#link-url').val('');
		$('#link-title').val('');
		$('#link-blank').prop('checked', false);

		if (focused != null) {
			$('#insertLinkModal').modal('show');
		}
	});

	$('#remove-link-btn').on('click', function(e) {
		if (focused != null) {
			var input = focused,
				new_value = input.val().replace(/<a[^>]*>([\s\S]*?)<\/a>/ig, '$1');

			input.val(new_value);
		}
	});

	$('#insert-link').submit(function() {
		var link_url = $.trim($('#link-url').val()),
			link_text = $.trim($('#link-text').val()),
			link_title = $.trim($('#link-title').val()),
			link_blank = $('#link-blank').prop('checked');

		if (link_url == '' || link_text == '') {
			$('#insertLinkModal').modal('hide');
			return false;
		}

		var new_link = $('<a href="' + link_url + '"/>');
		new_link.text(link_text);

		if (link_title != '') {
			new_link.attr('title', link_title);
		}

		if (link_blank) {
			new_link.attr('target', '_blank');
		}

		var len = focused.val().length,
			start = focused[0].selectionStart,
			end = focused[0].selectionEnd,
			selectedText = focused.val().substring(start, end);

		focused.val(focused.val().substring(0, start) + new_link[0].outerHTML + focused.val().substring(end, len));
		focused = null;

		$('#insertLinkModal').modal('hide');

		return false;
	});

	$('#do-insert').click(function() {
		$('#insert-link').submit();

		return false;
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				// error for lang fields
				if ($(this).parent().hasClass('tab-pane')) {
					var that = this,
						tab = $(this).parent().parent().prev().children(':eq(' + $(this).parent().index() + ')').children(),
						alert_callback = function() {
							if (that.id == 'form-discount_description') {
								setTimeout(function() {
									tab.click();
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									tab.click();
									$(that).focus();
								}, 21);
							}
						};

					if ($(this).parent()[0].id.indexOf('form-discount_name') === 0) {
						bootbox.alert("<?=Yii::t('discounts', 'Enter a discount title!')?>", alert_callback);
					}
				}
				else {
					var that = this,
						alert_callback = function() {
							if (that.id == 'form-redactor') {
								setTimeout(function() {
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									$(that).focus();
								}, 21);
							}
						};

					switch (this.id) {
						case 'form-discount_name':
							bootbox.alert("<?=Yii::t('discounts', 'Enter a discount title!')?>", alert_callback);
							break;
					}
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});
});
</script>