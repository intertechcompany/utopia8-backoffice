<?php
	/* @var $this AdminController */

	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$orders_url_data = array();

	if (!empty($sort)) {
		$orders_url_data['sort'] = $sort;
		$orders_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$orders_url_data['keyword'] = $keyword;
	}

	if (!empty($date_from)) {
		$orders_url_data['date_from'] = $date_from;
	}

	if (!empty($date_to)) {
		$orders_url_data['date_to'] = $date_to;
	}

	if (!empty($page)) {
		$orders_url_data['page'] = $page + 1;
	}

	$order['discount_value'] = (float) $order['discount_value'];

	$back_link = $this->createUrl('orders', $orders_url_data);
?>
<h1><?=Yii::t('orders', 'Edit order h1')?> #<?=$order['order_id']?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-course-order" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="order[order_id]" value="<?=$order['order_id']?>">
		
	<div class="page-header">
		<h3><?=Yii::t('orders', 'Order details')?></h3>
	</div>

	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label"><?=Yii::t('orders', 'Order #')?>:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$order['order_id']?></p>
		</div>
	</div>

	<?php if ($order['discount_id']) { ?>
	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label"><?=Yii::t('orders', 'Order amount')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-price" class="form-control" type="text" name="order[price]" value="<?=$order['price']?>" disabled>
				<span class="input-group-addon"><?=Currency::sign($order['currency'])?></span>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label"><?=Yii::t('orders', 'Discount')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-price" class="form-control" type="text" name="order[price]" value="<?=number_format($order['discount_value'], 2, '.', '')?>" disabled>
				<span class="input-group-addon"><?=Currency::sign($order['currency'])?></span>
			</div>
		</div>
		<div class="col-md-7 form-control-static">
			<b>
				<?=$order['discount_code']?> 
				<?php if ($order['discount_type'] == 'percentage') { ?>
				-<?=$order['value']?>%
				<?php } else { ?>
				-<?=$order['value']?><?=Currency::sign($order['currency'])?>
				<?php } ?>
			</b>
			<a href="<?=$this->createUrl('admin/discount', ['id' => $order['discount_id']])?>" target="_blank">посмотреть</a>
		</div>
	</div>
	<?php } ?>

	<div class="form-group">
		<label for="form-price" class="col-md-3 control-label"><?=Yii::t('orders', 'Total amount')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-price" class="form-control" type="text" name="order[price]" value="<?=number_format($order['price'] + $order['discount_value'], 2, '.', '')?>" disabled>
				<span class="input-group-addon"><?=Currency::sign($order['currency'])?></span>
			</div>
		</div>
	</div>

	<?php /* $order['delivery_price'] = (float) $order['delivery_price']; ?>
	<?php if (!empty($order['delivery_price'])) { ?>
	<div class="form-group">
		<label for="form-delivery_price" class="col-md-3 control-label">Стоимость доставки:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-delivery_price" class="form-control" type="text" name="order[delivery_price]" value="<?=$order['delivery_price']?>" disabled>
				<span class="input-group-addon"><?=Currency::sign($order['delivery_currency'])?></span>
			</div>
		</div>
	</div>
	<?php } */ ?>

	<div class="form-group">
		<label for="form-status" class="col-md-3 control-label"><?=Yii::t('orders', 'Status')?>:</label>
		<div class="col-md-3">
			<select id="form-status" name="order[status]" class="form-control">
				<option value="new" style="color: #5bc0de"<?php if ($order['status'] == 'new') { ?> selected<?php } ?>><?=Yii::t('orders', 'Status new')?></option>
				<option value="processing" style="color: #f0ad4e"<?php if ($order['status'] == 'processing') { ?> selected<?php } ?>><?=Yii::t('orders', 'Status in processing')?></option>
				<option value="paid" style="color: #5cb85c"<?php if ($order['status'] == 'paid') { ?> selected<?php } ?>><?=Yii::t('orders', 'Status paid')?></option>
				<option value="completed" style="color: #5cb85c"<?php if ($order['status'] == 'completed') { ?> selected<?php } ?>><?=Yii::t('orders', 'Status completed')?></option>
				<option value="cancelled" style="color: #777"<?php if ($order['status'] == 'cancelled') { ?> selected<?php } ?>><?=Yii::t('orders', 'Status cancelled')?></option>
				<option value="payment_error" style="color: #d9534f"<?php if ($order['status'] == 'payment_error') { ?> selected<?php } ?>><?=Yii::t('orders', 'Status payment error')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-delivery" class="col-md-3 control-label"><?=Yii::t('orders', 'Delivery')?>:</label>
		<div class="col-md-3">
			<select id="form-delivery" name="order[delivery]" class="form-control">
				<option value="1"<?php if ($order['delivery'] == 1) { ?> selected<?php } ?>>Самовывоз</option>
				<option value="2"<?php if ($order['delivery'] == 2) { ?> selected<?php } ?>>НП на отделение</option>
				<option value="3"<?php if ($order['delivery'] == 3) { ?> selected<?php } ?>>НП адресная</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-payment" class="col-md-3 control-label"><?=Yii::t('orders', 'Payment')?>:</label>
		<div class="col-md-3">
			<select id="form-payment" name="order[payment]" class="form-control">
				<option value="1"<?php if ($order['payment'] == 1) { ?> selected<?php } ?>>На карту по реквизитам</option>
				<option value="2"<?php if ($order['payment'] == 2) { ?> selected<?php } ?>>Картой Visa/Mastercard</option>
				<option value="3"<?php if ($order['payment'] == 3) { ?> selected<?php } ?>>При получении</option>
			</select>
		</div>
	</div>

	<div class="page-header">
		<h3>Личные данные</h3>
	</div>

	<div class="form-group">
		<label for="form-first_name" class="col-md-3 control-label"><?=Yii::t('orders', 'First name')?>:</label>
		<div class="col-md-4">
			<input id="form-first_name" class="form-control" type="text" name="order[first_name]" value="<?=CHtml::encode($order['first_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-last_name" class="col-md-3 control-label"><?=Yii::t('orders', 'Last name')?>:</label>
		<div class="col-md-4">
			<input id="form-last_name" class="form-control" type="text" name="order[last_name]" value="<?=CHtml::encode($order['last_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-email" class="col-md-3 control-label">Email:</label>
		<div class="col-md-4">
			<input id="form-email" class="form-control" type="text" name="order[email]" value="<?=CHtml::encode($order['email'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-phone" class="col-md-3 control-label"><?=Yii::t('orders', 'Phone')?>:</label>
		<div class="col-md-4">
			<input id="form-phone" class="form-control" type="text" name="order[phone]" value="<?=CHtml::encode($order['phone'])?>">
		</div>
	</div>

	<div class="page-header">
		<h3>Данные доставки</h3>
	</div>

	<!-- <div id="delivery-np"<?php if ($order['delivery'] == 3) { ?> class="hidden"<?php } ?>> -->
	<div>
		<div class="form-group">
			<label for="form-np_city" class="col-md-3 control-label">Город:</label>
			<div class="col-md-4">
				<input id="form-np_city" class="form-control" type="text" name="order[np_city]" value="<?=CHtml::encode($order['np_city'])?>">
			</div>
		</div>

		<div class="form-group">
			<label for="form-np_department" class="col-md-3 control-label">Отделение:</label>
			<div class="col-md-6">
				<input id="form-np_department" class="form-control" type="text" name="order[np_department]" value="<?=CHtml::encode($order['np_department'])?>">
			</div>
		</div>

		<div class="form-group">
			<label for="form-address" class="col-md-3 control-label">Адрес:</label>
			<div class="col-md-6">
				<input id="form-address" class="form-control" type="text" name="order[address]" value="<?=CHtml::encode($order['address'])?>">
			</div>
		</div>
		
		<?php /* 
		<div class="form-group">
			<label for="form-np_address" class="col-md-3 control-label">Улица:</label>
			<div class="col-md-6">
				<input id="form-np_address" class="form-control" type="text" name="order[np_address]" value="<?=CHtml::encode($order['np_address'])?>">
			</div>
		</div>
		
		<div class="form-group">
			<label for="form-np_building" class="col-md-3 control-label">Дом:</label>
			<div class="col-md-2">
				<input id="form-np_building" class="form-control" type="text" name="order[np_building]" value="<?=CHtml::encode($order['np_building'])?>">
			</div>
		</div>
		
		<div class="form-group">
			<label for="form-np_apartment" class="col-md-3 control-label">Квартира:</label>
			<div class="col-md-2">
				<input id="form-np_apartment" class="form-control" type="text" name="order[np_apartment]" value="<?=CHtml::encode($order['np_apartment'])?>">
			</div>
		</div>
		*/ ?>
	</div>

	<?php /* <div id="delivery-worldwide"<?php if ($order['delivery'] == 1 || $order['delivery'] == 2) { ?> class="hidden"<?php } ?>>
		<div class="form-group">
			<label for="form-country" class="col-md-3 control-label"><?=Yii::t('orders', 'Country')?>:</label>
			<div class="col-md-4">
				<input id="form-country" class="form-control" type="text" name="order[country]" value="<?=CHtml::encode($order['country'])?>">
			</div>
		</div>

		<div class="form-group">
			<label for="form-zip" class="col-md-3 control-label"><?=Yii::t('orders', 'Zip')?>:</label>
			<div class="col-md-4">
				<input id="form-zip" class="form-control" type="text" name="order[zip]" value="<?=CHtml::encode($order['zip'])?>">
			</div>
		</div>

		<div class="form-group">
			<label for="form-region" class="col-md-3 control-label"><?=Yii::t('orders', 'Region')?>:</label>
			<div class="col-md-4">
				<input id="form-region" class="form-control" type="text" name="order[region]" value="<?=CHtml::encode($order['region'])?>">
			</div>
		</div>

		<div class="form-group">
			<label for="form-city" class="col-md-3 control-label"><?=Yii::t('orders', 'City')?>:</label>
			<div class="col-md-4">
				<input id="form-city" class="form-control" type="text" name="order[city]" value="<?=CHtml::encode($order['city'])?>">
			</div>
		</div>

		<div class="form-group">
			<label for="form-address" class="col-md-3 control-label"><?=Yii::t('orders', 'Address')?>:</label>
			<div class="col-md-6">
				<textarea id="form-address" class="form-control" name="order[address]" rows="2"><?=CHtml::encode($order['address'])?></textarea>
			</div>
		</div>
	</div> */ ?>

	<hr>

	<div class="form-group">
		<label for="form-zip" class="col-md-3 control-label">Способ коммуникации:</label>
		<div class="col-md-4">
			<input id="form-zip" class="form-control" type="text" name="order[zip]" value="<?=CHtml::encode($order['zip'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-comment" class="col-md-3 control-label">Комментарии:</label>
		<div class="col-md-6">
			<textarea id="form-comment" class="form-control" name="order[comment]" rows="4"><?=CHtml::encode($order['comment'])?></textarea>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<?php if (!empty($products)) { ?>
<div class="page-header">
	<h3><?=Yii::t('orders', 'Order products')?></h3>
</div>

<table class="table-data table table-striped">
	<thead>
		<tr>
			<th style="width: 2%"></th>
			<th style="width: 5%">ID</th>
			<th style="width: 10%"><?=Yii::t('orders', 'Photo')?></th>
			<th style="width: 15%"><?=Yii::t('orders', 'Product code')?></th>
			<th><?=Yii::t('orders', 'Title')?></th>
			<th style="width: 22%"><?=Yii::t('orders', 'Price')?></th>
			<!-- <th style="width: 12%"><?=Yii::t('orders', 'Product price')?></th> -->
			<th style="width: 12%"><?=Yii::t('orders', 'Quantity')?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($products as $product) { ?>
		<?php 
			$product_image = '';

			if (!empty($product['product_alias'])) {
				$product_url = $this->createUrl('admin/product', array('id' => $product['product_id']));

				if (!empty($product['product_photo'])) {
					$product_image = json_decode($product['product_photo'], true);
					$product_image_size = $product_image['size']['catalog']['1x'];
					$product_image = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['1x'];
					$product_image = '<a href="' . $product_url . '"><img src="' . $product_image . '" width="' . $product_image_size['w'] . '" height="' . $product_image_size['h'] . '" style="max-width: 100px; max-height: 100px; width: auto; height: auto;"></a>';
				}

				$product_title = '<a href="' . $product_url . '">' . CHtml::encode($product['product_title']) . '</a>';

				$variants = '';

				if (!empty($product['variant'])) {
					$product['product_sku'] = $product['variant']['variant_sku'];
					$product['product_price'] = $product['variant']['variant_price'];

					if (!empty($product['variant']['values'])) {
						$variant_values = array();

						foreach ($product['variant']['values'] as $value) {
							$variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
						}
					
						$variants = '<br><small>' . implode("<br>\n", $variant_values) . '</small>';
					}
				}

				$options = '';
				if (!empty($product['options']) && is_array($product['options'])) {
					foreach ($product['options'] as $option) {
						$options .= '<br><small>' . str_replace("\n", "<br>\n", CHtml::encode($option['option_title'])) . '</small>';
					}
				}
			} else {
				$product_title = CHtml::encode($product['title']);

				if (!empty($product['variant_title'])) {
					$variants = '<br><small>' . str_replace("\n", "<br>\n", CHtml::encode($product['variant_title'])) . '</small>';
				}
			}

			if ($product['product_price_type'] == 'package') {
				$product['product_price'] = $product['product_price'] * $product['product_pack_size'];
				$product['product_price'] = round($product['product_price'], 2);
			}
		?>
		<tr>
			<td></td>
			<td><?=$product['product_id']?></td>
			<td><?=$product_image?></td>
			<td><?=CHtml::encode($product['product_sku'])?></td>
			<td>
				<?=$product_title?>
				<?=$variants?>
				<?=$options?>
			</td>
			<td><?=Currency::format($product['price'], $order['currency'])?></td>
			<!-- <td><?=$product['product_price']?>€</td> -->
			<td><?=$product['quantity']?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php } ?>

<script>
$(document).ready(function(){
	$('#form-delivery').on('change', function(e) {
		var value = $(this).val();

        if (value == 1) {
			$('#delivery-np').addClass('hidden');
			$('#delivery-worldwide').addClass('hidden');
		} else if (value == 2) {
			$('#delivery-np').removeClass('hidden');
			$('#delivery-worldwide').addClass('hidden');
		} else if (value == 3) {
			$('#delivery-np').addClass('hidden');
			$('#delivery-worldwide').removeClass('hidden');
		}
	}).change();
	
	var focused,
		form = $("#manage-course-order"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				var that = this,
					alert_callback = function() {
						if (that.id == 'form-redactor') {
							setTimeout(function() {
								$(that).redactor('focus.setStart');
							}, 21);
						}
						else {
							setTimeout(function() {
								$(that).focus();
							}, 21);
						}
					};

				switch (this.id) {
					case 'form-firstname':
						bootbox.alert("Введите имя!", alert_callback);
						break;
					case 'form-mail':
						bootbox.alert("Введите email!", alert_callback);
						break;
					case 'form-phone':
						bootbox.alert("Введите телефон!", alert_callback);
						break;
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});

	var invoice_xhr,
			is_invoice_xhr = false;

	$('#invoice-sent').on('submit', 'form', function(e) {
		e.preventDefault();

		if (is_invoice_xhr) {
			return false;
		}

		var invoice_form = $(this),
			invoice_form_data = invoice_form.serialize();
		
		is_invoice_xhr = true;
		invoice_form.find(':input').prop('disabled', true);

		invoice_xhr = $.ajax({
			type: 'POST', 
			url: invoice_form.attr('action'), 
			data: invoice_form_data, 
			cache: false, 
			dataType: 'json',
			success: function(data) {
				is_invoice_xhr = false;
				invoice_form.find(':input').prop('disabled', false);
				invoice_form.find('.has-error').removeClass('has-error').find('.help-block').remove();

				if (data.error) {
					$('#invoice-email').after('<span class="help-block">' + data.errorMessage + '</span>').parent().addClass('has-error');
					return;
				}

				invoice_form.addClass('hidden').after('<h4>' + data.message + '</h4>');
			},
			error: function(jqXHR, textStatus, errorThrown) { 
				is_invoice_xhr = false;
				invoice_form.find(':input').prop('disabled', false);
				
				alert('Request error!');
				// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
			}
		});
	}).on('hidden.bs.modal', function (e) {
		$(this).find('.has-error').removeClass('has-error').find('.help-block').remove();
		$(this).find('input[type="email"], textarea').val('');
		$(this).find('form').removeClass('hidden').next('h4').remove();

		if (is_invoice_xhr) {
			is_invoice_xhr = false;
			invoice_xhr.abort();
		}
	});
}); 
</script>