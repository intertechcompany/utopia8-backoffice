<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('discounts', 'Discounts h1')?></h1>

<?php
	$discount_url_data = array();

	if ($sort != 'default') {
		$discount_url_data['sort'] = $sort;
		$discount_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$discount_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$discount_url_data['page'] = $page + 1;
	}

	$discount_new_url = $this->createUrl('discount', array_merge(array('id' => 'new'), $discount_url_data));

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/discount';
?>
<p class="text-center"><a class="btn btn-success" href="<?=$discount_new_url?>"><?=Yii::t('discounts', 'Add discount btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('discounts', 'ID | discount name placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<?php if ($sort != 'default' || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('discounts')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($discounts)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-discounts" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="discount_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array('keyword' => $keyword);
					} else {
						$sort_data = array();
					}
				?>
				<th style="width: 3%"></th>
				<th style="width: 5%">
					<?php if ($sort == 'discount_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('discounts', array_merge(array('sort' => 'discount_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'discount_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('discounts', array_merge(array('sort' => 'discount_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('discounts', array_merge(array('sort' => 'discount_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th style="width: 14%">
					<?=Yii::t('discounts', 'Discount code col')?>
				</th>
				<th style="width: 14%">
					<?=Yii::t('discounts', 'Discount value col')?>
				</th>
				<th style="width: 22%">
					<?=Yii::t('discounts', 'Duration col')?>
				</th>
				<th style="width: 10%">
					<?=Yii::t('discounts', 'Activated col')?>
				</th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($discounts as $id => $discount) { ?>
			<?php
				$discount_id = $discount['discount_id'];

				$discount_url = $this->createUrl('discount', array_merge(array('id' => $discount_id), $discount_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$discount_id?>">
				</td>
				<td>
					<a href="<?=$discount_url?>"><?=$discount_id?></a>
				</td>
				<td>
					<a href="<?=$discount_url?>"><?=CHtml::encode($discount['discount_code'])?></a>
				</td>
				<td>
					<?php if ($discount['discount_type'] == 'percentage') { ?>
					<a href="<?=$discount_url?>"><?=CHtml::encode($discount['discount_value'])?>%</a>
					<?php } else { ?>
					<a href="<?=$discount_url?>"><?=CHtml::encode($discount['discount_value'])?></a>
					<?php } ?>
				</td>
				<td>
					<?php
						$range = [];
						
						if ($discount['discount_start'] != '0000-00-00') {
							$range[] = 'с ' . date('d.m.Y', strtotime($discount['discount_start']));
						}
						
						if ($discount['discount_end'] != '0000-00-00') {
							$range[] = ' по ' . date('d.m.Y', strtotime($discount['discount_end']));
						}
					?>
					
					<?php if (!empty($range)) { ?>
					<a href="<?=$discount_url?>"><?=implode('', $range)?></a>
					<?php } else { ?>
					—
					<?php } ?>
				</td>
				<td>
					<?=$discount['activated']?> / 
					<?php if ($discount['discount_allowed_uses']) { ?>
					<?=CHtml::encode($discount['discount_allowed_uses'])?>
					<?php } else { ?>
					—
					<?php } ?>
				</td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$discount_id?>">
						<div class="btn-group">
							<?php if ($discount['active']) { ?>
							<a title="<?=Yii::t('discounts', 'Active')?>" class="active-btn btn btn-default btn-sm btn-success" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ok"></span></a>
							<?php } else { ?>
							<a title="<?=Yii::t('discounts', 'Blocked')?>" class="block-btn btn btn-default btn-sm btn-danger" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-ban-circle"></span></a>
							<?php } ?>
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$discount_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="active"><?=Yii::t('app', 'Activate selected')?></option>
								<option value="block"><?=Yii::t('app', 'Block selected')?></option>
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}
			
			submit_form = true;
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-discounts').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-discounts').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-discounts").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>