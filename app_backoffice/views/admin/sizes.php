<?php
/* @var $this AdminController */
?>
<h1><?=Yii::t('sizes', 'Sizes h1')?></h1>

<?php
	$size_url_data = array();

	if ($sort != 'default') {
		$size_url_data['sort'] = $sort;
		$size_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$size_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$size_url_data['page'] = $page + 1;
	}

	$size_new_url = $this->createUrl('size', array_merge(array('id' => 'new'), $size_url_data));
?>
<p class="text-center"><a class="btn btn-success" href="<?=$size_new_url?>"><?=Yii::t('sizes', 'Add size btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('sizes', 'ID | size title placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<?php if ($sort != 'default' || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('sizes')?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($sizes)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-sizes" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="size_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array('keyword' => $keyword);
					} else {
						$sort_data = array();
					}
				?>
				<th style="width: 4%"></th>
				<th style="width: 10%">
					<?php if ($sort == 'size_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'size_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th>
					<?php if ($sort == 'size_name' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_name', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('sizes', 'Size title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'size_name' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_name', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('sizes', 'Size title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_name', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('sizes', 'Size title col')?></a>
					<?php } ?>
				</th>
				<!-- <th style="width: 30%">
					<?=Yii::t('sizes', 'Size place')?>
				</th>
				<th style="width: 12%">
					<?php if ($sort == 'size_position' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_position', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('sizes', 'Size position col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'size_position' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_position', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('sizes', 'Size position col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('sizes', array_merge(array('sort' => 'size_position', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('sizes', 'Size position col')?></a>
					<?php } ?>
				</th> -->
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($sizes as $id => $size) { ?>
			<?php
				$size_id = $size['size_id'];

				$size_url = $this->createUrl('size', array_merge(array('id' => $size_id), $size_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$size_id?>">
				</td>
				<td>
					<a href="<?=$size_url?>"><?=$size_id?></a>
				</td>
				<td>
					<a href="<?=$size_url?>"><?=CHtml::encode($size['size_name'])?></a>
				</td>
				<!-- <td>
					<input class="form-control input-sm text-center" type="text" name="size[<?=$size_id?>]" value="<?=CHtml::encode($size['size_position'])?>" style="width: 50px">
				</td> -->
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$size_id?>">
						<div class="btn-group">
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$size_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<!-- <option value="save"><?=Yii::t('app', 'Save positions option')?></option> -->
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;

		$(".block-btn, .active-btn").click( function(){
			if($(this).hasClass("block-btn")){
				$('#entity-action').val('active');
			} else {
				$('#entity-action').val('block');
			}
			
			submit_form = true;
			$('#entity-id').val($(this).parent().parent().attr("data-id"));
			$('#manage-sizes').submit();
			
			return false;
		});
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-sizes').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-sizes").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>