<?php
/* @var $this AdminController */
?>
<?php
	$managers_url_data = array();

	if (!empty($sort)) {
		$managers_url_data['sort'] = $sort;
		$managers_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$managers_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$managers_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('managers', $managers_url_data);

	$has_rights = Yii::app()->getUser()->hasAccess($this->route, true);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-manager" class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="manager[manager_id]" value="<?=$manager['manager_id']?>">

	<div class="manager-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>

	<div class="form-group">
		<label for="form-id" class="col-md-3 control-label">ID:</label>
		<div class="col-md-5">
			<p class="form-control-static"><?=$manager['manager_id']?></p>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-4">
			<select id="form-active" name="manager[active]" class="form-control">
				<option value="0"<?php if ($manager['active'] == 0) { ?> selected<?php } ?>><?=Yii::t('managers', 'Blocked')?></option>
				<option value="1"<?php if ($manager['active'] == 1) { ?> selected<?php } ?>><?=Yii::t('managers', 'Active')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_login" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager login')?>:</label>
		<div class="col-md-4 control-required">
			<input id="form-manager_login" class="form-control" type="text" name="manager[manager_login]" value="<?=CHtml::encode($manager['manager_login'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_password" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager password')?>:</label>
		<div class="col-md-4">
			<input id="form-manager_password" class="form-control" type="text" name="manager[manager_password]" value="">
		</div>
		<div class="col-md-5 form-control-static">
			<small><?=Yii::t('managers', 'leave blank if you do not need to change password')?></small>
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_email" class="col-md-3 control-label">Email:</label>
		<div class="col-md-4">
			<input id="form-manager_email" class="form-control" type="text" name="manager[manager_email]" value="<?=CHtml::encode($manager['manager_email'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_first_name" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager name')?>:</label>
		<div class="col-md-4">
			<input id="form-manager_first_name" class="form-control" type="text" name="manager[manager_first_name]" value="<?=CHtml::encode($manager['manager_first_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_middle_name" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager middle name')?>:</label>
		<div class="col-md-4">
			<input id="form-manager_middle_name" class="form-control" type="text" name="manager[manager_middle_name]" value="<?=CHtml::encode($manager['manager_middle_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_last_name" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager last name')?>:</label>
		<div class="col-md-4">
			<input id="form-manager_last_name" class="form-control" type="text" name="manager[manager_last_name]" value="<?=CHtml::encode($manager['manager_last_name'])?>">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_city" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager roles')?>:</label>
		<div class="col-md-6">
			<select id="form-manager_roles" class="form-control select2" name="roles[]" multiple data-placeholder="Выберите роли...">
				<?php if (!empty($roles)) { ?>
				<?php foreach ($roles as $role) { ?>
				<option value="<?=$role['role_id']?>"<?php if (in_array($role['role_id'], $manager['roles'])) { ?> selected<?php } ?>><?=CHtml::encode($role['role_name'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"<?php if (!$has_rights) { ?> disabled<?php } ?>><?=Yii::t('app', 'Save btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<script>
var is_manager = true;

$(document).ready(function(){
	<?php if (!$has_rights) { ?>
	$('form').find(':input').prop('disabled', true);
	<?php } else {
        ?>
	
	var form = $("#manage-manager"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				var that = this,
					alert_callback = function() {
						if (that.id == 'form-redactor') {
							setTimeout(function() {
								$(that).redactor('focus.setStart');
							}, 21);
						}
						else {
							setTimeout(function() {
								$(that).focus();
							}, 21);
						}
					};

				switch (this.id) {
					case 'form-manager_login':
						bootbox.alert("<?=Yii::t('managers', 'Enter a manager login!')?>", alert_callback);
						break;
					case 'form-manager_email':
						bootbox.alert("<?=Yii::t('managers', 'Enter a manager email!')?>", alert_callback);
						break;
					case 'form-manager_password':
						bootbox.alert("<?=Yii::t('managers', 'Enter a manager password!')?>", alert_callback);
						break;
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});

	var invoice_xhr,
		is_invoice_xhr = false;

	$('.send').on('click', function(e) {
		e.preventDefault();

		$('#invoice-sent').find('form').attr('action', $(this).data('url'));
		$('#invoice-sent').find('h4 span').text('#' + $(this).data('id'));
		$('#invoice-sent').modal('show');
	});

	$('#invoice-sent').on('submit', 'form', function(e) {
		e.preventDefault();

		if (is_invoice_xhr) {
			return false;
		}

		var form = $(this),
			form_data = form.serialize();
		
		is_invoice_xhr = true;
		form.find(':input').prop('disabled', true);

		invoice_xhr = $.ajax({
			type: 'POST', 
			url: form.attr('action'), 
			data: form_data, 
			cache: false, 
			dataType: 'json',
			success: function(data) {
				is_invoice_xhr = false;
				form.find(':input').prop('disabled', false);
				form.find('.has-error').removeClass('has-error').find('.help-block').remove();

				if (data.error) {
					$('#invoice-email').after('<span class="help-block">' + data.errorMessage + '</span>').parent().addClass('has-error');
					return;
				}

				form.addClass('hidden').after('<h4>' + data.message + '</h4>');
			},
			error: function(jqXHR, textStatus, errorThrown) { 
				is_invoice_xhr = false;
				form.find(':input').prop('disabled', false);
				
				alert('Request error!');
				// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
			}
		});
	}).on('hidden.bs.modal', function (e) {
		$(this).find('.has-error').removeClass('has-error').find('.help-block').remove();
		$(this).find('input[type="email"], textarea').val('');
		$(this).find('form').removeClass('hidden').next('h4').remove();

		if (is_invoice_xhr) {
			is_invoice_xhr = false;
			invoice_xhr.abort();
		}
	});

	<?php } ?>
});
</script>