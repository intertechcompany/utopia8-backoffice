<?php
/* @var $this AdminController */
?>
<?php
	$users_url_data = array();

	if (!empty($sort)) {
		$users_url_data['sort'] = $sort;
		$users_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$users_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$users_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('users', $users_url_data);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-user" class="form-horizontal" method="post" enctype="multipart/form-data">
	<div class="user-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>
	
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-4">
			<select id="form-active" name="user[active]" class="form-control">
				<option value="0"><?=Yii::t('users', 'Blocked')?></option>
				<option value="1"><?=Yii::t('users', 'Active')?></option>
			</select>
		</div>
	</div>

	<?php /* <div class="form-group">
		<label for="form-user_login" class="col-md-3 control-label"><?=Yii::t('users', 'User login')?>:</label>
		<div class="col-md-4 control-required">
			<input id="form-user_login" class="form-control" type="text" name="user[user_login]" value="">
		</div>
	</div> */ ?>

	<div class="form-group">
		<label for="form-user_email" class="col-md-3 control-label">Email:</label>
		<div class="col-md-4 control-required">
			<input id="form-user_email" class="form-control" type="text" name="user[user_email]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_phone" class="col-md-3 control-label"><?=Yii::t('users', 'Phone')?>:</label>
		<div class="col-md-4">
			<input id="form-user_phone" class="form-control" type="text" name="user[user_phone]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_password" class="col-md-3 control-label"><?=Yii::t('users', 'User password')?>:</label>
		<div class="col-md-4 control-required">
			<input id="form-user_password" class="form-control" type="text" name="user[user_password]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_first_name" class="col-md-3 control-label"><?=Yii::t('users', 'User name')?>:</label>
		<div class="col-md-4">
			<input id="form-user_first_name" class="form-control" type="text" name="user[user_first_name]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_last_name" class="col-md-3 control-label"><?=Yii::t('users', 'User last name')?>:</label>
		<div class="col-md-4">
			<input id="form-user_last_name" class="form-control" type="text" name="user[user_last_name]" value="">
		</div>
	</div>

	<?php /* <div class="form-group">
		<label for="form-user_zip" class="col-md-3 control-label"><?=Yii::t('users', 'Zip')?>:</label>
		<div class="col-md-4">
			<input id="form-user_zip" class="form-control" type="text" name="user[user_zip]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_city" class="col-md-3 control-label"><?=Yii::t('users', 'City')?>:</label>
		<div class="col-md-4">
			<input id="form-user_city" class="form-control" type="text" name="user[user_city]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_address" class="col-md-3 control-label"><?=Yii::t('users', 'Address 1')?>:</label>
		<div class="col-md-4">
			<input id="form-user_address" class="form-control" type="text" name="user[user_address]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-user_address_2" class="col-md-3 control-label"><?=Yii::t('users', 'Address 2')?>:</label>
		<div class="col-md-4">
			<input id="form-user_address_2" class="form-control" type="text" name="user[user_address_2]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-discount" class="col-md-3 control-label"><?=Yii::t('users', 'Discount')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-discount" class="form-control" type="text" name="user[discount]" value="">
				<span class="input-group-addon">%</span>
			</div>
		</div>
	</div> */ ?>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Add btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<script>
var is_user = true;

$(document).ready(function(){
	var form = $("#manage-user"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				var that = this,
					alert_callback = function() {
						if (that.id == 'form-redactor') {
							setTimeout(function() {
								$(that).redactor('focus.setStart');
							}, 21);
						}
						else {
							setTimeout(function() {
								$(that).focus();
							}, 21);
						}
					};

				switch (this.id) {
					case 'form-user_login':
						bootbox.alert("<?=Yii::t('users', 'Enter a user login!')?>", alert_callback);
						break;
					case 'form-user_email':
						bootbox.alert("<?=Yii::t('users', 'Enter a user email!')?>", alert_callback);
						break;
					case 'form-user_password':
						bootbox.alert("<?=Yii::t('users', 'Enter a user password!')?>", alert_callback);
						break;
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});
});
</script>