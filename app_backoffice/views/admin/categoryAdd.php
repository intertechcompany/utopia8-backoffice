<?php
/* @var $this AdminController */
	function showCategoriesTree($categories, $category_id = 0, $selected_id = array(), $level = 0, $disable_children = false)
	{
		$html = '';

		foreach ($categories as $category) {
			if ($category['category_id'] == $category_id || $disable_children) {
				$disable_children = true;
				$html .= '<option value="' . $category['category_id'] . '" disabled>' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_name']) . '</option>';
			} elseif (in_array($category['category_id'], $selected_id)) {
				$html .= '<option value="' . $category['category_id'] . '" selected>' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_name']) . '</option>';
			} else {
				$html .= '<option value="' . $category['category_id'] . '">' . str_repeat('&nbsp;&nbsp;', $level) . ' ' . CHtml::encode($category['category_name']) . '</option>';
			}

			if (!empty($category['sub'])) {
				$html .= showCategoriesTree($category['sub'], $category_id, $selected_id, $level + 1, $disable_children);
			}

			$disable_children = false;
		}

		return $html;
	}

	function showBaseCategoriesTree($categories, $category_id = 0, $level = 0, $path = '')
	{
		$html = '';

		foreach ($categories as $category) {
			if ((is_array($category_id) && in_array($category['category_id'], $category_id)) || $category['category_id'] == $category_id) {
				$html .= '<option value="' . $category['category_id'] . '" selected>' . CHtml::encode($path) . CHtml::encode($category['category_alias']) . '</option>';
			} else {
				$html .= '<option value="' . $category['category_id'] . '">' . CHtml::encode($path) . CHtml::encode($category['category_alias']) . '</option>';
			}

			if (!empty($category['sub'])) {
				$path = !empty($path) ? $path . ' / ' . $category['category_alias'] . ' / ' : $category['category_alias'] . ' / ';

				$html .= showBaseCategoriesTree($category['sub'], $category_id, $level + 1, $path);
			}
		}

		return $html;
	}

	$categories_url_data = array();

	if (!empty($parent_id)) {
		$categories_url_data['parent_id'] = $parent_id;
	}

	$back_link = $this->createUrl('categories', $categories_url_data);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-category" class="form-horizontal" method="post" enctype="multipart/form-data">
	<div class="page-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>
		
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-3">
			<select id="form-active" name="category[active]" class="form-control">
				<option value="0"><?=Yii::t('categories', 'Blocked')?></option>
				<option value="1"><?=Yii::t('categories', 'Active')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-category_alias" class="col-md-3 control-label"><?=Yii::t('app', 'Alias (URL code)')?>:</label>
		<div class="col-md-6">
			<div class="input-group">
				<span class="input-group-addon">/catalog/</span>
				<input id="form-category_alias" class="form-control" type="text" name="category[category_alias]" value="">
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-category_position" class="col-md-3 control-label"><?=Yii::t('categories', 'Position')?>:</label>
		<div class="col-md-1">
			<input id="form-category_position" class="form-control" type="text" name="category[category_position]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-category_name" class="col-md-3 control-label"><?=Yii::t('categories', 'Category title')?>:</label>
		<div class="col-md-6 control-required">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-category_name_<?=$code?>" aria-controls="form-category_name_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-category_name_<?=$code?>">
						<input class="form-control" type="text" name="category_lang[category_name][<?=$code?>]" value="">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

    <div class="form-group">
        <label for="form-category_name" class="col-md-3 control-label">Категория H1:</label>
        <div class="col-md-6">
            <div class="lang-tabs" role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                        <li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
                            <a href="#form-category_title_<?=$code?>" aria-controls="form-category_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                        <div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-category_title_<?=$code?>">
                            <input class="form-control" type="text" name="category_lang[category_title][<?=$code?>]" value="">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

	<?php /* <div class="form-group">
		<label for="form-category_top" class="col-md-3 control-label">Top категория:</label>
		<div class="col-md-3">
			<div class="checkbox">
				<label for="form-category_top">
					<input id="form-category_top" type="checkbox" name="category[category_top]" value="1">
				</label>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label for="form-category_home" class="col-md-3 control-label">На главной:</label>
		<div class="col-md-3">
			<div class="checkbox">
				<label for="form-category_home">
					<input id="form-category_home" type="checkbox" name="category[category_home]" value="1">
				</label>
			</div>
		</div>
	</div> */ ?>

	<?php /* <div class="form-group">
		<label for="form-category_type" class="col-md-3 control-label"><?=Yii::t('categories', 'Category type')?>:</label>
		<div class="col-md-3">
			<select id="form-category_type" name="category[category_type]" class="form-control">
				<option value="categories"><?=Yii::t('categories', 'View categories')?></option>
				<option value="collections"><?=Yii::t('categories', 'View collections')?></option>
				<option value="products"><?=Yii::t('categories', 'View products')?></option>
			</select>
		</div>
	</div> */ ?>

	<div class="form-group">
		<label for="form-category_photo" class="col-md-3 control-label"><?=Yii::t('categories', 'Photo')?>:</label>
		<div class="col-md-7">
			<input id="form-category_photo" type="file" name="category[category_photo]">
			<small><?=Yii::t('app', 'Image file requirements', array('{types}' => 'jpg, gif, png', '{size}' => '10 MB', '{dimension}' => '364x364 px (@2x 728x728 px)'))?></small>
		</div>
	</div>

    <!-- <div class="form-group">
        <label for="form-page_type" class="col-md-3 control-label">Тип категории:</label>
        <div class="col-md-3">
            <select id="form-category_type" name="category[category_type]" class="form-control">
                <option value="products">Товары</option>
                <option value="tags">По тэгам</option>
            </select>
        </div>
    </div> -->

    <div class="form-group">
        <label for="form-tags" class="col-md-3 control-label">Тэги:</label>
        <div class="col-md-6">
            <select id="form-tags" class="form-control select2" style="width: 100%;" name="category[tags][]" multiple data-placeholder="Выберите тэги из списка...">
                <?php if (!empty($tags)) { ?>
                    <?php foreach ($tags as $tag) { ?>
                        <option value="<?=$tag['tag_id']?>"><?=CHtml::encode($tag['tag_name'])?></option>
                    <?php } ?>
                <?php } ?>
            </select>
        </div>
    </div>

	<div class="form-group">
		<label for="form-parent_id" class="col-md-3 control-label"><?=Yii::t('categories', 'Parent category')?>:</label>
		<div class="col-md-4">
			<select id="form-parent_id" name="category[parent_id]" class="form-control">
				<option value="0">---</option>
				<?php if (!empty($categories_tree)) { echo showCategoriesTree($categories_tree, 0, array($parent_id)); } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-categories" class="col-md-3 control-label">Категории 1С:</label>
		<div class="col-md-9">
			<select id="form-categories" class="form-control select2" name="category[base_categories][]" multiple data-placeholder="Выберите категории из списка...">
				<?php if (!empty($base_categories_tree)) { ?>
				<?=showBaseCategoriesTree($base_categories_tree)?>
				<?php } ?>
			</select>
		</div>
	</div>

	<?php /* <div class="form-group">
		<label for="form-related_id" class="col-md-3 control-label"><?=Yii::t('categories', 'Related products category')?>:</label>
		<div class="col-md-6">
			<select id="form-related_id" name="category[related_id][]" class="form-control select2" multiple data-placeholder="<?=Yii::t('categories', 'Select a category from the list...')?>">
				<?php if (!empty($categories_tree)) { echo showCategoriesTree($categories_tree); } ?>
			</select>
		</div>
	</div> */ ?>

	<!-- <div class="form-group">
		<label for="form-category_discount" class="col-md-3 control-label"><?=Yii::t('categories', 'Category discount')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-category_discount" class="form-control" type="text" name="category[category_discount]" value="">
				<span class="input-group-addon">%</span>
			</div>
		</div>
	</div> -->

	<?php /* <div class="form-group">
		<label for="form-category_delivery" class="col-md-3 control-label"><?=Yii::t('categories', 'Category delivery time')?>:</label>
		<div class="col-md-2">
			<div class="input-group">
				<input id="form-category_delivery" class="form-control" type="text" name="category[category_delivery]" value="">
				<span class="input-group-addon"><?=Yii::t('categories', 'weeks')?></span>
			</div>
		</div>
	</div>

    <div class="form-group">
        <label for="form-category_fullname" class="col-md-3 control-label"><?=Yii::t('categories', 'Category fullname')?>:</label>
        <div class="col-md-6">
            <div class="lang-tabs" role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                        <li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
                            <a href="#form-category_fullname_<?=$code?>" aria-controls="form-category_fullname_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                        <div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-category_fullname_<?=$code?>">
                            <input class="form-control" type="text" name="category_lang[category_fullname][<?=$code?>]" value="">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
	</div>

	<div class="form-group">
		<label for="form-category_faq" class="col-md-3 control-label">FAQ таблица размеров:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-category_faq_<?=$code?>" aria-controls="form-category_faq_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-category_faq_<?=$code?>">
						<textarea class="form-control form-redactor" name="category_lang[category_faq][<?=$code?>]" rows="6"></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div> */ ?>

	<div class="form-group">
		<label for="form-category_description" class="col-md-3 control-label"><?=Yii::t('categories', 'Description')?>:</label>
		<div class="col-md-9">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-category_description_<?=$code?>" aria-controls="form-category_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-category_description_<?=$code?>">
						<textarea class="form-control form-redactor" name="category_lang[category_description][<?=$code?>]" rows="6"></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div id="section-seo" class="page-header">
		<h3><?=Yii::t('app', 'SEO data')?></h3>
	</div>

	<div class="form-group">
		<label for="form-category_meta_title" class="col-md-3 control-label"><?=Yii::t('app', 'No index')?>:</label>
		<div class="col-md-6">
			<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
			<div class="checkbox">
				<label for="form-category_no_index<?=$code?>">
					<input id="form-category_no_index<?=$code?>" type="checkbox" name="category_lang[category_no_index][<?=$code?>]" value="1"> <?=$lang?>
				</label>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="form-group">
		<label for="form-category_meta_title" class="col-md-3 control-label">Meta title:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-category_meta_title_<?=$code?>" aria-controls="form-category_meta_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-category_meta_title_<?=$code?>">
						<input class="form-control" type="text" name="category_lang[category_meta_title][<?=$code?>]" value="">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-category_meta_keywords" class="col-md-3 control-label">Meta keywords:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-category_meta_keywords_<?=$code?>" aria-controls="form-category_meta_keywords_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-category_meta_keywords_<?=$code?>">
						<textarea class="form-control" name="category_lang[category_meta_keywords][<?=$code?>]" rows="2"></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label for="form-category_meta_description" class="col-md-3 control-label">Meta description:</label>
		<div class="col-md-6">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-category_meta_description_<?=$code?>" aria-controls="form-category_meta_description_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-category_meta_description_<?=$code?>">
						<textarea class="form-control" name="category_lang[category_meta_description][<?=$code?>]" rows="4"></textarea>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Add btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<div class="btn-group-vertical btn-insert-link" role="group" aria-label="<?=Yii::t('app', 'Insert/remove a link label')?>">
  <button id="typograph-link-btn" title="<?=Yii::t('app', 'Typography text btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-text-size"></i></button>
  <button id="insert-link-btn" title="<?=Yii::t('app', 'Insert a link btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
  <button id="remove-link-btn" title="<?=Yii::t('app', 'Remove all links btn')?>" type="button" class="btn btn-default"><i class="glyphicon glyphicon-link"></i></button>
</div>

<!-- Insert Link Modal -->
<div class="modal fade" id="insertLinkModal" tabindex="-1" role="dialog" aria-labelledby="insertLinkModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="<?=Yii::t('app', 'Close')?>"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="insertLinkModalLabel"><?=Yii::t('app', 'Insert a link label')?></h4>
			</div>
			<div class="modal-body">
			<form id="insert-link">
				<div class="form-group">
					<label for="link-text" class="control-label"><?=Yii::t('app', 'Link text')?>:</label>
					<input type="text" class="form-control" id="link-text">
				</div>
				<div class="form-group">
					<label for="link-url" class="control-label"><?=Yii::t('app', 'Link tip')?>:</label>
					<input type="text" class="form-control" id="link-url">
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label for="link-blank">
							<input id="link-blank" type="checkbox" value="1"> <?=Yii::t('app', 'Open in new tab')?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="link-title" class="control-label"><?=Yii::t('app', 'Link tip (title)')?>:</label>
					<input type="text" class="form-control" id="link-title">
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=Yii::t('app', 'Cancel btn')?></button>
				<button id="do-insert" type="button" class="btn btn-primary"><?=Yii::t('app', 'Insert btn')?></button>
			</div>
		</div>
	</div>
</div>

<script>
var is_table = true;

$(document).ready(function(){
	var form = $("#manage-category"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

    $('#form-category_type').on('change', function() {
        var value = $(this).val();

        if (value === 'tags') {
            $('.category-tags').show();
        } else {
            $('.category-tags').hide();
        }
    }).change();

	$('#typograph-link-btn').on('click', function(e) {
		var input = focused,
			text = input.val(),
			btn = $(this);

		if (focused != null) {
			btn.prop('disabled', true);
			
			$.ajax({type: "POST", url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/typograph', data: $.param({text: focused.val()}), cache: false, dataType: "json",
				success: function(data) {
					btn.prop('disabled', false);
					
					if (data.error == 'Y') {
						bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					}
					else {
						if (focused.hasClass('form-redactor')) {
							$('#' + focused[0].id).redactor('code.set', data.text);
						}
						else {
							focused.val(data.text);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) { 
					btn.prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					
					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});
	
	$('#insert-link-btn').on('click', function(e) {
		var input = focused,
			len = input.val().length,
			start = input[0].selectionStart,
			end = input[0].selectionEnd,
			selectedText = input.val().substring(start, end);

		$('#link-text').val(selectedText);
		$('#link-url').val('');
		$('#link-title').val('');
		$('#link-blank').prop('checked', false);

		if (focused != null) {
			$('#insertLinkModal').modal('show');
		}
	});

	$('#remove-link-btn').on('click', function(e) {
		if (focused != null) {
			var input = focused,
				new_value = input.val().replace(/<a[^>]*>([\s\S]*?)<\/a>/ig, '$1');

			input.val(new_value);
		}
	});

	$('#insert-link').submit(function() {
		var link_url = $.trim($('#link-url').val()),
			link_text = $.trim($('#link-text').val()),
			link_title = $.trim($('#link-title').val()),
			link_blank = $('#link-blank').prop('checked');

		if (link_url == '' || link_text == '') {
			$('#insertLinkModal').modal('hide');
			return false;
		}

		var new_link = $('<a href="' + link_url + '"/>');
		new_link.text(link_text);

		if (link_title != '') {
			new_link.attr('title', link_title);
		}

		if (link_blank) {
			new_link.attr('target', '_blank');
		}

		var len = focused.val().length,
			start = focused[0].selectionStart,
			end = focused[0].selectionEnd,
			selectedText = focused.val().substring(start, end);

		focused.val(focused.val().substring(0, start) + new_link[0].outerHTML + focused.val().substring(end, len));
		focused = null;

		$('#insertLinkModal').modal('hide');

		return false;
	});

	$('#do-insert').click(function() {
		$('#insert-link').submit();

		return false;
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				// error for lang fields
				if ($(this).parent().hasClass('tab-pane')) {
					var that = this,
						tab = $(this).parent().parent().prev().children(':eq(' + $(this).parent().index() + ')').children(),
						alert_callback = function() {
							if (that.id == 'form-category_description') {
								setTimeout(function() {
									tab.click();
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									tab.click();
									$(that).focus();
								}, 21);
							}
						};

					if ($(this).parent()[0].id.indexOf('form-category_name') === 0) {
						bootbox.alert("<?=Yii::t('categories', 'Enter an category name!')?>", alert_callback);
					}
					else if ($(this).parent()[0].id.indexOf('form-category_lastname') === 0) {
						bootbox.alert("<?=Yii::t('categories', 'Enter an category last name!')?>", alert_callback);
					}
				}
				else {
					var that = this,
						alert_callback = function() {
							if (that.id == 'form-redactor') {
								setTimeout(function() {
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									$(that).focus();
								}, 21);
							}
						};

					switch (this.id) {
						case 'form-category_name':
							bootbox.alert("<?=Yii::t('categories', 'Enter an category name!')?>", alert_callback);
							break;
						case 'form-category_lastname':
							bootbox.alert("<?=Yii::t('categories', 'Enter an category last name!')?>", alert_callback);
							break;
					}
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});
});
</script>