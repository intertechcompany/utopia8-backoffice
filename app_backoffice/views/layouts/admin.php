<?php /* @var $this AdminController */ ?>
<!DOCTYPE html>
<html lang="<?=Yii::app()->language?>">
<head>
	<title><?=!empty($this->pageTitle) ? CHtml::encode($this->pageTitle . ' | ' . Yii::app()->name) : CHtml::encode(Yii::app()->name)?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php $assetUrl = Yii::app()->assetManager->getBaseUrl() . '/'; ?>

	<!-- Bootstrap -->
	<link href="<?=$assetUrl?>bootstrap_static/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=$assetUrl?>bootstrap_static/css/non-responsive.css" rel="stylesheet">
	<link href="<?=$assetUrl?>bootstrap_static/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	<link href="<?=$assetUrl?>bootstrap_static/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	
	<!-- Imperavi redactor -->
	<link href="<?=$assetUrl?>bootstrap_static/css/redactor.css" rel="stylesheet">
	
	<!-- Select2 -->
	<link href="<?=$assetUrl?>bootstrap_static/css/select2.css" rel="stylesheet">

	<!-- Tokenfield -->
	<link href="<?=$assetUrl?>bootstrap_static/css/bootstrap-tokenfield.css" rel="stylesheet">
	<link href="<?=$assetUrl?>bootstrap_static/css/tokenfield-typeahead.css" rel="stylesheet">

	<!-- Main styles -->
	<link href="<?=$assetUrl?>bootstrap_static/css/style.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="<?=$assetUrl?>bootstrap_static/js/html5shiv.js"></script>
	  <script src="<?=$assetUrl?>bootstrap_static/js/respond.min.js"></script>
	<![endif]-->
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<!-- <script src="http://yandex.st/jquery/1.11.1/jquery.min.js"></script> -->
	<script src="<?=$assetUrl?>bootstrap_static/js/jquery-1.11.2.min.js"></script>
	
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?=$assetUrl?>bootstrap_static/js/bootstrap.min.js"></script>
	
	<!-- Bootstrap plugins -->
	<script src="<?=$assetUrl?>bootstrap_static/js/moment.min.js"></script>
	<script src="<?=$assetUrl?>bootstrap_static/js/bootstrap-colorpicker.min.js"></script>
	<?php if (Yii::app()->language != 'en') { ?>
	<script src="<?=$assetUrl?>bootstrap_static/js/moment_<?=Yii::app()->language?>.js"></script>
	<?php } ?>
	<script src="<?=$assetUrl?>bootstrap_static/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?=$assetUrl?>bootstrap_static/js/bootbox.min.js"></script>
	<script>
		bootbox.addLocale('uk', {
			OK : 'ОК',
			CANCEL : 'Відмінити',
			CONFIRM : 'Підтвердити'
		});

		bootbox.setDefaults({
			locale: '<?=Yii::app()->language?>'
		});
	</script>

	<!-- Imperavi redactor -->
	<script src="<?=$assetUrl?>bootstrap_static/js/redactor.js"></script>
	<?php if (Yii::app()->language != 'en') { ?>
	<script src="<?=$assetUrl?>bootstrap_static/js/lang/<?=Yii::app()->language?>.js"></script>
	<?php } ?>
	<script src="<?=$assetUrl?>bootstrap_static/js/plugins/table/table.js"></script>
	<script src="<?=$assetUrl?>bootstrap_static/js/plugins/video/video.js"></script>
	<script src="<?=$assetUrl?>bootstrap_static/js/plugins/counter/counter.js"></script>

	<!-- Select2 -->
	<script src="<?=$assetUrl?>bootstrap_static/js/select2.min.js"></script>
	<?php if (Yii::app()->language != 'en') { ?>
	<script src="<?=$assetUrl?>bootstrap_static/js/select2_<?=Yii::app()->language?>.js"></script>
	<?php } ?>

	<!-- Tokenfield -->
	<script src="<?=$assetUrl?>bootstrap_static/js/bootstrap-tokenfield.js"></script>
	
	<!-- Twitter Typeahead -->
	<script src="<?=$assetUrl?>bootstrap_static/js/typeahead.bundle.min.js"></script>

	<!-- jQuery UI -->
	<script src="<?=$assetUrl?>bootstrap_static/js/jquery-ui.min.js"></script>
	
	<!-- jQuery Fileupload -->
	<script src="<?=$assetUrl?>bootstrap_static/js/fileupload.js"></script>

	<style>
		.info-blocks {
			margin-top: -15px;
		}

		.info-block {
			position: relative;
			border-top: 1px solid #eee;
			padding: 15px 0;
		}

		.info-block:first-child {
			border-top: 0;
		}

		.info-block .form-group:last-child {
			margin-bottom: 0;
		}

		.i-placeholder {
			height: 241px;
			background-color: #f5f5f5;
		}

		.add-info-block {
			margin-top: 10px;
		}

		.del-info-block {
			position: relative;
		}

		.del-info-block-tip {
			display: inline-block;
			margin-top: -3px;
			margin-left: 5px;
			vertical-align: middle;
		}

		.info-block .btn-sm {
			position: absolute;
			top: 15px;
			left: 15px;
			z-index: 1;
			cursor: move;
		}
	</style>
</head>
<body>
	<div class="wrap">
		<nav class="top-menu navbar navbar-default navbar-static-top" role="navigation">
		  <div class="container">
			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#top-menu">
				  <span class="sr-only">Toggle navigation</span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?=$this->createUrl('admin/index')?>"><?=Yii::app()->name?></a>
			  </div>

			  <?php if (!Yii::app()->user->isGuest) { ?>
			  <?php $this->widget('application.components.AdminMenu.AdminMenu'); ?>
			  <?php } ?>
			</div>
		</nav>
			
		<div class="container">
			<?php if (Yii::app()->user->hasFlash('error_msg')) { ?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><?=Yii::t('app', 'Close')?></span></button>
				<?=Yii::app()->user->getFlash('error_msg')?>
			</div>
			<?php } ?>

			<?php if (Yii::app()->user->hasFlash('success_msg')) { ?>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><?=Yii::t('app', 'Close')?></span></button>
				<?=Yii::app()->user->getFlash('success_msg')?>
			</div>
			<?php } ?>

			<?php echo $content; ?>
		</div>
	</div>
	<!-- /.wrap -->
	
	<div class="footer">
		<div class="container">
			<small><?=Yii::app()->name?> &copy; <?=date('Y')?> All rights reserved</small>
		</div>
	</div>

	<script>
		var	form_changed = false,
			form_submit = false,
			focused,
			files_counter = 0,
			files_active = 0,
			files_xhr = [];

		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip({container: 'body'});

			$('.form-colorpicker').colorpicker({
				format: 'hex',
				template: '<div class="colorpicker dropdown-menu">' +
					'<div class="colorpicker-saturation"><i><b></b></i></div>' +
					'<div class="colorpicker-hue"><i></i></div>' +
					// '<div class="colorpicker-alpha"><i></i></div>' +
					// '<div class="colorpicker-color"><div /></div>' +
					'<div class="colorpicker-selectors"></div>' +
					'</div>'
			});
			
			$('.datepicker').datetimepicker({
				pickTime: false,
				language: '<?=Yii::app()->language?>'
			});
		
			$('.datetimepicker').datetimepicker({
				pickTime: true,
				language: '<?=Yii::app()->language?>'
			});

			var redactor_config = {
				toolbarFixed: false,
				replaceDivs: true,
				replaceStyles: [],
				minHeight: 200,
				maxHeight: 300,
				formatting: ['p', 'h1', 'h2', 'h3', 'h4'],
				plugins: ['table'],
				// plugins: ['table', 'video'],
				imageUpload: '<?=Yii::app()->getBaseUrl()?>/ajax/uploadImage',
				// fileUpload: '<?=Yii::app()->getBaseUrl()?>/ajax/upload_file.php',
				lang: '<?=Yii::app()->language?>',
				focusCallback: function(e) {
					focused = this.$element;
				}
			};

			$('.form-redactor').each(function() {
				if ($(this).hasClass('form-redactor-counter')) {
					$(this).redactor($.extend({}, redactor_config, {
						plugins: ['counter'],
						initCallback: function() {
							this.$editor.after('<div style="padding-top: 5px">Символов: <span></span> | Слов: <span></span> | Пробелов: <span></span></div>');
							this.$editor.trigger('keyup.redactor-limiter');
						},
						counterCallback: function(data) {
							this.$editor.next().children(':eq(0)').text(data.characters);
							this.$editor.next().children(':eq(1)').text(data.words);
							this.$editor.next().children(':eq(2)').text(data.spaces);
						}
					}));
				} else if (typeof is_page !== 'undefined') {
					$(this).redactor($.extend({}, redactor_config, {
						formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
						formattingAdd: [{
							tag: 'p',
							title: 'Large text',
							class: 'big-font'
						}]
					}));
				}
				else if (typeof is_table !== 'undefined') {
					$(this).redactor($.extend({}, redactor_config, {
						plugins: ['table']
					}));
				}
				else {
					$(this).redactor(redactor_config)
				}
			});

			$('.select2').select2({
				minimumResultsForSearch: 20,
				templateSelection: function(data, container) {
					return $.trim(data.text);
				},
				templateResult: function(state) {
					if (!state.id) {
						return state.text;
					}
					
					var option = $(state.element);

					if (option.data('active') || typeof option.data('active') === 'undefined') {
						return state.text;
					} else {
						var $state = $('<span style="opacity: .5">' + state.text + '</span>');
						return $state;
					}
				}
			});

			$('#form-colors, #form-products').select2({
				ajax: {
					url: "/ajax/products",
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							keyword: params.term, // search term
							page: params.page
						};
					},
					processResults: function (data, params) {
						// parse the results into the format expected by Select2
						// since we are using custom formatting functions we do not need to
						// alter the remote JSON data, except to indicate that infinite
						// scrolling can be used
						params.page = params.page || 1;

						return {
							results: $.map(data.items, function (item) {
								return {
									id: item.product_id,
									text: item.product_title + ' (код: ' + item.product_alias + ')'
								};
							}),
							pagination: {
								more: (params.page * 10) < data.total
							}
						};
					},
					cache: true
				},
				minimumInputLength: 3
			});

			var tabs = $('.lang-tabs').children('.nav-tabs'),
				tabs_active_index = 0,
                tab_handler = function() {
                    tabs_active_index = $(this).parent().index();

                    tabs.off('click.tabs');
                    tabs.find('li:eq(' + tabs_active_index + ') a').click();
                    tabs.on('click.tabs', 'a', tab_handler);
                };

            tabs.on('click.tabs', 'a', tab_handler);

			$('.info-blocks').each(function() {
				var id = $(this)[0].id,
					group = $(this).data('group'),
					infoblocks = $(this),
					infoblock_html = infoblocks.find('.info-block-start').html(),
					infoblock_index = infoblocks.find('.info-block').length,
					infoblock_first = infoblock_index - 1,
					regexp_quote = function(str) {
						return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
					};

				$('#add-' + id).find('button').click(function() {
					var replace_id = new RegExp(regexp_quote('-' + infoblock_first), "g"),
						replace_input = new RegExp(regexp_quote('[' + infoblock_first + ']'), "g"),
						new_id = '-' + infoblock_index,
						new_input = '[' + infoblock_index + ']',
						new_infoblock_html = $('<div class="info-block"/>').html(infoblock_html.replace(replace_id, new_id).replace(replace_input, new_input));

					new_infoblock_html.find('.form-group:first')
						.append('<div class="col-md-3 form-control-static del-info-block"><a class="text-muted" href="#"><small><span class="glyphicon glyphicon-remove"></span><span class="del-info-block-tip"> <?=Yii::t('app', 'Remove block')?></span></small></a></div>');
					
					infoblocks.append(new_infoblock_html);

					var last_block = infoblocks.find('.info-block:last');

					last_block.find('.form-colorpicker').colorpicker({
						format: 'hex',
						template: '<div class="colorpicker dropdown-menu">' +
							'<div class="colorpicker-saturation"><i><b></b></i></div>' +
							'<div class="colorpicker-hue"><i></i></div>' +
							'<div class="colorpicker-selectors"></div>' +
							'</div>'
					});
					
					last_block.find('.datepicker').datetimepicker({
						pickTime: false,
						language: '<?=Yii::app()->language?>'
					});
				
					last_block.find('.datetimepicker').datetimepicker({
						pickTime: true,
						language: '<?=Yii::app()->language?>'
					});

					if (typeof is_page !== 'undefined') {
						last_block.find('.infoblock-redactor').redactor($.extend({}, redactor_config, {
							formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
							formattingAdd: [{
								tag: 'p',
								title: 'Large text',
								class: 'big-font'
							}]
						}));
					}
					else if (typeof is_table !== 'undefined') {
						last_block.find('.infoblock-redactor').redactor($.extend({}, redactor_config, {
							plugins: ['table']
						}));
					}
					else {
						last_block.find('.infoblock-redactor').redactor(redactor_config)
					}

					last_block.find('.select2').select2({
						minimumResultsForSearch: 20,
						templateSelection: function(data, container) {
							return $.trim(data.text);
						}
					});

					tabs.off('click.tabs');
					tabs = $('.lang-tabs').children('.nav-tabs');
                    tabs.on('click.tabs', 'a', tab_handler);
					
					if (tabs_active_index != 0) {
						last_block.find('.nav-tabs:first').find('a:eq(' + tabs_active_index + ')').click();
					}

					infoblock_index++;

					return false;
				});

				// editor
				if (typeof is_page !== 'undefined') {
					$('.infoblock-redactor').redactor($.extend({}, redactor_config, {
						formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
						formattingAdd: [{
							tag: 'p',
							title: 'Large text',
							class: 'big-font'
						}]
					}));
				}
				else if (typeof is_table !== 'undefined') {
					$('.infoblock-redactor').redactor($.extend({}, redactor_config, {
						plugins: ['table']
					}));
				}
				else {
					$('.infoblock-redactor').redactor(redactor_config)
				}

				infoblocks.on('click', '.del-info-block', function() {
					$(this).parent().parent().remove();

					tabs.off('click.tabs');
					tabs = $('.lang-tabs').children('.nav-tabs');
                    tabs.on('click.tabs', 'a', tab_handler);

					return false;
				});

				infoblocks.sortable({
					handle: '.btn-sm',
					items: '.info-block',
					placeholder: 'i-placeholder',
					cursor: 'move',
					zIndex: 1019,
					opacity: .7,
					update: function(event, ui) {
						infoblocks.find('.info-block').each(function(index) {
							$(this).find(':input').each(function() {
								var name = $(this).attr('name');
								name.replace(/\[\d+\]/g, '[' + index + ']');
								$(this).attr('name', name.replace(/\[\d+\]/g, '[' + index + ']'));
								// $(this).attr('name', 'page_content[' + group + '][' + index + '][' + $(this).data('name') + ']')
							});
						});
					}
				});
			});

			if ($('#upload-container').length) {
				var upload_container = $('#upload-container'),
					fileupload_config = {
						url: upload_container.data('url'),
						dropZone: null,
						dataType: 'json',
						add: function (e, data) {
							if (!data.files.length) {
								return;
							}

							files_counter++;

							var validate_extension = /\.(jpe?g|png)$/i.test(data.files[0].name),
								validate_size = typeof data.files[0].size == 'undefined' || data.files[0].size <= 10 * 1024 * 1024, // 10 MB
								file_name = data.files[0].name;
								// file_size = typeof data.files[0].size == 'undefined' ? '' : bytesToSize(data.files[0].size);
				
							var upload_html = '<div class="upload-file-details">' +
								'<span class="upload-file-name">' + file_name + '</span>' +
								'<a class="upload-remove text-muted" href="#" style="margin-left: 10px"><small><span class="glyphicon glyphicon-remove"></span></small></a>' +
								'</div>',
								upload_el = $('<div id="upload-' + files_counter + '" class="upload-file" style="margin-bottom: 10px"/>').html(upload_html);

							upload_container.append(upload_el);
							
							if (!validate_extension) {
								upload_el.append('<div class="text-danger">Изображение должно быть в формате jpg или png!</div>');
							} else if (!validate_size) {
								upload_el.append('<div class="text-danger">Размер файла должен быть не более 10 MiB!</div>');
							} else {
								files_counter++;
								files_active++;

								upload_el.append('<div class="progress" style="width: 300px;height: 6px;margin: 10px 0;">' +
  								'<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">' + 
								'</div>' +
								'</div>');
								data.context = upload_el;
								files_xhr[files_counter] = data.submit();
							}
						},
						done: function (e, data) {
							files_active--;
							files_xhr[data.context[0].id.split('-')[1]] = null;
				
							if (typeof data.result == 'undefined') {
								error = true;
								data.context.append('<div class="text-danger">Ошибка загрузки файла!</div>');
								data.context.find('.progress').children().removeClass('progress-bar-striped active');
							} else {
								if (data.result.error) {
									data.context.append('<div class="text-danger">' + data.result.message + '</div>');
									data.context.find('.progress').children().removeClass('progress-bar-striped active');
								} else {
									var last_position = $('#photos-list').children('li:last').length ? $('#photos-list').children('li:last').find('input[type="hidden"]').val() : 0;
									
									var sort_el = '<li>';
									sort_el += '<div class="pl-thumb ui-sortable-handle">';
									sort_el += '<input type="hidden" name="gallery_position[' + data.result.file_id + ']" value="' + data.result.file_position + '">';
									sort_el += '<img src="' + data.result.file_url + '" alt="" width="' + data.result.file_width + '" height="' + data.result.file_height + '">';
									sort_el += '</div>';
									sort_el += '<div class="pl-thumb-del text-center">';
									sort_el += '<div class="pl-thumb-el checkbox">';
									sort_el += '<label for="form-del-photo-' + data.result.file_id + '">';
									sort_el += '<input id="form-del-photo-' + data.result.file_id + '" type="checkbox" name="del_gallery[]" value="' + data.result.file_id + '"> <small>del</small>';
									sort_el += '</label>';
									sort_el += '</div>';
									sort_el += '</div>';
									sort_el += '</li>';

									$('#photos-list').append(sort_el);
									data.context.remove();
								}
							}
						},
						formData: function (form) {
							return form.serializeArray();
						},
						fail: function (e, data) {
							files_active--;
							files_xhr[data.context[0].id.split('-')[1]] = null;
							
							if (data.context.data('remove')) {
								data.context.remove();
							} else {
								data.context.find('.progress').children().removeClass('progress-bar-striped active');
								data.context.append('<div class="text-danger">Произошла ошибка во время загрузки файла!</div>');
							}
						},
						progress: function (e, data) {
							var progress = parseInt(data.loaded / data.total * 100, 10);
				
							if (progress == 100) {
								data.context.find('.progress').children().css({width: progress + '%'}).addClass('progress-bar-striped active');
							} else {
								data.context.find('.progress').children().css({width: progress + '%'});
							}
						}
					};
				
				upload_container.parent().find('input[type="file"]').fileupload(fileupload_config);
				upload_container.on('click', '.upload-remove', function(e) {
					e.preventDefault();

					var id = $(this).parent().parent()[0].id.split('-')[1];

					if (files_xhr[id]) {
						$(this).parent().parent().data('remove', 1);
						files_xhr[id].abort();
					} else {
						$(this).parent().parent().remove();
					}
				});
			}
		});
	</script>
</body>
</html>