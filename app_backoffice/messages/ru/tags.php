<?php
    return array(
        'Tag has been deleted! success' => 'Тэг удален!',
        'Tags have been deleted! success' => 'Тэги удалены!',
        'Tags h1' => 'Тэги',
        'Tag has been successfully added! success' => 'Тэг успешно добавлен!',
        'Tag has been successfully saved! success' => 'Тэг успешно сохранен!',
        'Add tag h1' => 'Добавить тэг',
        'Edit tag h1' => 'Редактировать тэг',
        'Add tag btn' => 'Добавить тэг',
        'ID | tag name placeholder' => 'ID | тэг...',
        'Tag name col' => 'Тэг',
        'Tag name' => 'Тэг',
        'Enter a tag name!' => 'Введите тэг!',
        'Enter a tag name in all languages!' => 'Введите тэг на всех языках!',
    );
