<?php
	return array(
		'Role has been deleted! success' => 'Роль удалена!',
		'Roles have been deleted! success' => 'Роли удалены!',
		'Roles h1' => 'Роли',
		'Role has been successfully added! success' => 'Роль успешно добавлена!',
		'Role has been successfully saved! success' => 'Роль успешно сохранена!',
		'Add role h1' => 'Добавить роль',
		'Edit role h1' => 'Редактировать роль',
		'Add role btn' => 'Добавить роль',
		'ID | role name placeholder'  => 'ID | название роли...',
		'Role name col' => 'Название роли',
		'Role name' => 'Название',
		'Role rights' => 'Права',
		'Enter a role name!' => 'Введите название роли!',
	);