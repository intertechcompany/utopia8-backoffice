<?php
	return array(
		'Translation has been deleted! success' => 'Перевод был удален!',
		'Translations have been deleted! success' => 'Переводы были удалены!',
		'Translations h1' => 'Переводы',
		'Translation has been successfully added! success' => 'Перевод успешно добавлен!',
		'Translation has been successfully saved! success' => 'Перевод успешно сохранен!',
		'Add translation h1' => 'Добавить перевод',
		'Edit translation h1' => 'Редактировать перевод',
		'Add translation btn' => 'Добавить перевод',
		'ID | translation code placeholder' => 'ID | код перевода | часть фразы...',
		'Group:' => 'Группа:',
		'All' => 'Все',
		'Translation code col' => 'Код',
		'Translation value col' => 'Перевод',
		'Translation code' => 'Код перевода',
		'Translation code tip' => '<strong>Не менять без надобности!</strong><br>Формат кода: <strong>group.entityType.translationCode</strong>, где:<br>
			<span style="display: block; margin: 6px 0;"><strong>group [обязательно]</strong> &mdash; идентификатор группы. Например, название страницы (для статей &mdash; blog), глобальный идентификатор (для общих вещей, например, знак валюты &mdash; global), действие (для динамических AJAX запросов &mdash; ajax)</span>
			<span style="display: block; margin: 6px 0;"><strong>entityType [не обязательно]</strong> &mdash; тип перевода. Например, для кнопок &mdash; btn; для ссылок &mdash; link; для подсказок &mdash; tip; для HTML блоков &mdash; html; etc.</span>
			<span style="display: block; margin: 6px 0;"><strong>translationCode [обязательно]</strong> &mdash; идентификатор перевода (например, сообщение &laquo;неверный email&raquo; можно идентифицировать как invalidEmail)</span>',
		'Translation tip' => 'Подсказка',
		'Translation type' => 'Тип перевода',
		'Type text' => 'Текст',
		'Translation value' => 'Перевод',
		'Enter a translation code!' => 'Введите код перевода!',
		'Enter a translation value!' => 'Введите значение перевода!',
		'Invalid translation type!' => 'Неверный тип перевода!',
		'Enter a translation value in base language ({lang})!' => 'Введите перевод на базовом языке ({lang})!',
	);