<?php
	return array(
		'A care has been deleted! success' => 'Уход удален!',
		'Cares have been deleted! success' => 'Уходы удалены!',
		'Cares h1' => 'Уходы',
		'A care has been successfully added! success' => 'Уход успешно добавлен!',
		'A care has been successfully saved! success' => 'Уход успешно сохранен!',
		'Add care h1' => 'Добавить уход',
		'Edit care h1' => 'Редактировать уход',
		'Add care btn' => 'Добавить уход',
		'ID | care title placeholder'  => 'ID | название ухода...',
		'Care title col' => 'Название ухода',
		'Care position col' => 'Позиция',
		'Care title' => 'Название ухода',
		'Position' => 'Позиция',
		'Enter a care name in all languages!' => 'Введите название ухода на всех языках!',
	);