<?php
	return array(
		'A reclamation has been deleted! success' => 'Возврат удален!',
		'Reclamations have been deleted! success' => 'Возвраты удалены!',
		'Reclamations h1' => 'Возвраты',
		'A reclamation has been successfully added! success' => 'Возврат успешно добавлен!',
		'A reclamation has been successfully saved! success' => 'Возврат успешно сохранен!',
		'Add reclamation h1' => 'Добавить возврат',
		'Edit reclamation h1' => 'Редактировать возврат',
		'Add reclamation btn' => 'Добавить возврат',
		'ID | reclamation title placeholder'  => 'ID | название возврата...',
		'Reclamation title col' => 'Название возврата',
		'Reclamation position col' => 'Позиция',
		'Reclamation title' => 'Название возврата',
		'Position' => 'Позиция',
		'Enter a reclamation name in all languages!' => 'Введите название возврата на всех языках!',
	);