<?php
	return array(
		'A size has been deleted! success' => 'Размерная сетка удалена!',
		'Sizes have been deleted! success' => 'Размерные сетки удалены!',
		'Sizes h1' => 'Размерные сетки',
		'A size has been successfully added! success' => 'Размерная сетка успешно добавлена!',
		'A size has been successfully saved! success' => 'Размерная сетка успешно сохранена!',
		'Add size h1' => 'Добавить размерную сетку',
		'Edit size h1' => 'Редактировать размерную сетку',
		'Add size btn' => 'Добавить размерную сетку',
		'ID | size title placeholder'  => 'ID | название размерной сетки...',
		'Size title col' => 'Название размерной сетки',
		'Size position col' => 'Позиция',
		'Size title' => 'Название размерной сетки',
		'Position' => 'Позиция',
		'Enter a size name in all languages!' => 'Введите название размерной сетки на всех языках!',
	);