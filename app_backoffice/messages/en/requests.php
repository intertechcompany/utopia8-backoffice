<?php
	return array(
		'Request has been blocked! success' => 'Request has been blocked!',
		'Request has been activated! success' => 'Request has been activated!',
		'Request has been deleted! success' => 'Request has been deleted!',
		'Requests have been deleted! success' => 'Requests have been deleted!',
		'Requests h1' => 'Requests',
		'Request has been successfully added! success' => 'Request has been successfully added!',
		'Request has been successfully saved! success' => 'Request has been successfully saved!',
		'Add request h1' => 'Add request',
		'Edit request h1' => 'Edit request',
		'Add request btn' => 'Add request',
		'Request ID | Invoice code placeholder'  => 'Request ID | Invoice code...',
		'Date col' => 'Date',
		'Invoice code col' => 'Invoice code',
		'Client col' => 'Client',
		'Service col' => 'Service',
		'Amount col' => 'Amount',
		'Status col' => 'Status',
		'Status paid' => 'Paid',
		'Status pending' => 'Pending',
		'Status not paid' => 'Not paid',
		'Active' => 'Active',
		'Blocked' => 'Blocked',
		'Request ID' => 'Request ID',
		'Status' => 'Status',
		'Request type' => 'Request type',
		'Transfer' => 'Transfer',
		'Individual request' => 'Individual request',
		'Invoice title' => 'Invoice',
		'Payment link' => 'Payment link',
		'Invoice code' => 'Invoice code',
		'Interface language' => 'Interface language',
		'Amount' => 'Amount',
		'Service description' => 'Service description',
		'Request details title' => 'Request details',
		'Request name' => 'Name',
		'Request phone' => 'Phone',
		'Request comment' => 'Comment',
		'Enter an invoice code!' => 'Enter an invoice code!',
		'Enter a total amount!' => 'Enter a total amount!',
		'Enter a service description!' => 'Enter a service description!',
		'Invalid state value!' => 'Invalid state value!',
		'Invalid payment status!' => 'Invalid payment status!',
		'Invalid request type!' => 'Invalid request type!',
		'Invoice is required field!' => 'Invoice is required field!',
		'Interface language is invalid!' => 'Interface language is invalid!',
	);