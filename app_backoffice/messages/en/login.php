<?php
	return array(
		'Log In h1' => 'Log In',
		'Login or email address input' => 'Login or email address',
		'Password input' => 'Password',
		'Log In btn' => 'Log In',
		'Please enter an email and password! err' => 'Please enter an email and password!',
		'Invalid email or password! err' => 'Invalid email or password!',
	);