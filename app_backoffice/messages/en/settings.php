<?php
	return array(
		'Settings h1' => 'Settings',
		'Common settings' => 'Common settings',
		'Assets version (for developers)' => 'Assets version (for developers)',
		'Phone' => 'Phone',
		'Google map' => 'Google map',
		'Latitude' => 'Latitude',
		'Longtitude' => 'Longtitude',
		'Social networks' => 'Social networks',
		'SEO optimization' => 'SEO optimization',
		'No index site' => 'No index site',
		'Google Analytics code' => 'Google Analytics code',
		'Yandex Metrika code' => 'Yandex Metrika code',
		'Field \'Assets version\' is required!' => 'Field \'Assets version\' is required!',
		'\'No index site\' value is invalid!' => '\'No index site\' value is invalid!',
	);