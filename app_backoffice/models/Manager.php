<?php
class Manager extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getManager()
	{
		$manager = Yii::app()->db
			->createCommand("SELECT * FROM manager WHERE manager_id = :id AND active = 1")
			->bindValue(':id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryRow();
		
		return $manager;
	}

	public function getManagerRights()
	{
		$rights = [];
		
		$manager_roles = Yii::app()->db
			->createCommand("SELECT r.role_id, r.role_name, r.role_rights 
							 FROM manager_role as mr 
							 JOIN role as r 
							 ON mr.role_id = r.role_id 
							 WHERE mr.manager_id = :manager_id")
			->bindValue(':manager_id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($manager_roles)) {
			// merge roles access
			foreach ($manager_roles as $manager_role) {
				if (empty($manager_role['role_rights'])) {
					continue;
				}

				$role_rights = json_decode($manager_role['role_rights'], true);

				foreach ($role_rights as $route => $right) {
					if (isset($rights[$route]['write'])) {
						continue;
					}

					$rights[$route] = $right;
				}
			}
		}

		return $rights;
	}

	public function saveToken($token, $id)
	{
		$rs = Yii::app()->db
				->createCommand("UPDATE manager SET manager_token = :token WHERE manager_id = :id")
				->bindValue(':token', $token, PDO::PARAM_STR)
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->execute();
				
		return (bool) $rs;
	}
	
	public function isValidToken($id, $token)
	{
		$manager = Yii::app()->db
			->createCommand("SELECT saved FROM manager WHERE manager_id = :id AND active = 1 AND manager_token = :token")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->bindValue(':token', $token, PDO::PARAM_STR)
			->queryRow();

		if (!empty($manager)) {
			$is_valid = true;
			Yii::app()->user->setState('saved', $manager['saved']);
		}
		else {
			$is_valid = false;
			Yii::app()->user->setState('saved', null);
		}
			
		return $is_valid;
	}
	
	public function getManagersAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$manager_id = (int) $func_args[1];
			$manager_login = trim($func_args[1]);
			$manager_name = addcslashes($func_args[1], '%_');

			$total_managers = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM manager WHERE manager_id = :id OR manager_login = :login OR manager_first_name LIKE :manager_name")
				->bindValue(':id', $manager_id, PDO::PARAM_INT)
				->bindValue(':login', $manager_login, PDO::PARAM_STR)
				->bindValue(':manager_name', '%' . $manager_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_managers = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM manager")
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_managers,
			'pages' => ceil($total_managers / $per_page),
		);
	}

	public function getManagersAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'manager_id':
				$order_by = ($direction == 'asc') ? 'manager_id' : 'manager_id DESC';
				break;
			case 'manager_name':
				$order_by = ($direction == 'asc') ? 'manager_first_name' : 'manager_first_name DESC';
				break;
			default:
				$order_by = 'manager_id';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$manager_id = (int) $func_args[4];
			$manager_login = trim($func_args[4]);
			$manager_name = addcslashes($func_args[4], '%_');

			$managers = Yii::app()->db
				->createCommand("SELECT * FROM manager WHERE manager_id = :id OR manager_login = :login OR manager_first_name LIKE :manager_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':id', $manager_id, PDO::PARAM_INT)
				->bindValue(':login', $manager_login, PDO::PARAM_STR)
				->bindValue(':manager_name', '%' . $manager_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
            $managers = Yii::app()->db
                ->createCommand("SELECT * FROM manager ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
                ->queryAll();
		}
		
		if (!empty($managers)) {
			foreach ($managers as $id => $manager) {
				$managers[$id]['roles'] = Yii::app()->db
					->createCommand("SELECT r.role_name 
									 FROM manager_role as mr 
									 JOIN role as r 
									 ON mr.role_id = r.role_id
									 WHERE mr.manager_id = :manager_id
									 ORDER BY r.role_name")
					->bindValue(':manager_id', (int) $manager['manager_id'], PDO::PARAM_INT)
					->queryColumn();
			}
		}
			
		return $managers;
	}

	public function getManagerByIdAdmin($id)
	{
		$manager = Yii::app()->db
			->createCommand("SELECT * FROM manager WHERE manager_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		// get manager roles
		if (!empty($manager)) {
			$manager['roles'] = Yii::app()->db
				->createCommand("SELECT role_id FROM manager_role WHERE manager_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryColumn();
		}
			
		return $manager;
	}

	public function getManagerByLogin($login)
	{
		$manager = Yii::app()->db
			->createCommand("SELECT * FROM manager WHERE manager_login = :login")
			->bindParam(':login', $login, PDO::PARAM_STR)
			->queryRow();
			
		return $manager;
	}

	public function getManagersListAdmin()
	{
		$managers = Yii::app()->db
			->createCommand("SELECT manager_id, manager_first_name FROM manager ORDER BY manager_name")
			->queryAll();
			
		return $managers;
	}

	public function issetManagerByLogin($manager_login, $manager_id)
	{
		if (!empty($manager_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM manager WHERE manager_login = :manager_login AND manager_id != :id")
				->bindValue(':manager_login', $manager_login, PDO::PARAM_STR)
				->bindValue(':id', $manager_id, PDO::PARAM_INT)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM manager WHERE manager_login = :manager_login")
				->bindValue(':manager_login', $manager_login, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function save($model, $roles)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'manager_id',
		);

		// integer attributes
		$int_attributes = array(
			'active',
		);

		// date attributes
		$date_attributes = array();

		if (empty($model->manager_id)) {
			// insert manager
			$insert_manager = array(
				'created' => $today,
				'saved' => $today,
			);

			$model->manager_password = CPasswordHelper::hashPassword($model->manager_password, 10);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_manager[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$insert_manager[$field] = $date->format('Y-m-d');
				}
				else {
					$insert_manager[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('manager', $insert_manager)->execute();

				if ($rs) {
					$model->manager_id = (int) Yii::app()->db->getLastInsertID();

					$this->saveRoles($model, $roles);
					
					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_manager = array(
				'saved' => $today,
			);
			
			if (!empty($model->manager_password)) {
				$model->manager_password = CPasswordHelper::hashPassword($model->manager_password, 10);
				$model->manager_token = '';
			}
			else {
				$skip_attributes[] = 'manager_password';
				$skip_attributes[] = 'manager_token';
			}

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_manager[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$update_manager[$field] = $date->format('Y-m-d');
				}
				else {
					$update_manager[$field] = $value;
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "manager_id = :manager_id" , 
					"params" => array(
						"manager_id" => $model->manager_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('manager', $update_manager, $update_criteria)->execute();

				if ($rs) {
					$this->saveRoles($model, $roles);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function saveRoles($model, $roles)
	{
		$rs_inserted = 0;
		$model->manager_id = (int) $model->manager_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete roles
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "manager_id = :manager_id" , 
				"params" => array(
					"manager_id" => $model->manager_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('manager_role', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($roles)) {
			$insert_roles = array();

			foreach ($roles as $role) {
				$role = (int) $role;

				if (!empty($role)) {
					$insert_roles[] = array(
						'manager_id' => $model->manager_id,
						'role_id' => $role,
					);
				}
			}

			if (!empty($insert_roles)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('manager_role', $insert_roles)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	public function toggle($manager_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_manager = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "manager_id = :manager_id" , 
				"params" => array(
					"manager_id" => $manager_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('manager', $update_manager, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($manager_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "manager_id = :manager_id" , 
				"params" => array(
					"manager_id" => $manager_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('manager', $delete_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @param string the password hash
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password, $password_hash)
	{
		return CPasswordHelper::verifyPassword($password, $password_hash);
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @return string hash
	 */
	public function hashPassword($password)
	{
		return CPasswordHelper::hashPassword($password, 10);
	}
}