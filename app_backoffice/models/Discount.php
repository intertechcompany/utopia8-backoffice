<?php
class Discount extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getDiscountsAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$discount_id = (int) $func_args[1];
			$discount_code = addcslashes($func_args[1], '%_');

			$total_discounts = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM discount as d WHERE d.discount_id = :id OR d.discount_code = :discount_code")
				->bindValue(':id', $discount_id, PDO::PARAM_INT)
				->bindValue(':discount_code', $discount_code, PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_discounts = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM discount as d")
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_discounts,
			'pages' => ceil($total_discounts / $per_page),
		);
	}

	public function getDiscountsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'discount_id':
				$order_by = ($direction == 'asc') ? 'd.discount_id' : 'd.discount_id DESC';
				break;
			default:
				$order_by = 'd.discount_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$discount_id = (int) $func_args[4];
			$discount_code = addcslashes($func_args[4], '%_');

			$discounts = Yii::app()->db
				->createCommand("SELECT d.* FROM discount as d WHERE d.discount_id = :id OR d.discount_code = :discount_code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':id', $discount_id, PDO::PARAM_INT)
				->bindValue(':discount_code', $discount_code, PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$discounts = Yii::app()->db
				->createCommand("SELECT d.* FROM discount as d ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->queryAll();
		}
			
		return $discounts;
	}

	public function getDiscountByIdAdmin($id)
	{
		$discount = Yii::app()->db
			->createCommand("SELECT * FROM discount WHERE discount_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();
			
		return $discount;
	}

	public function save($model)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'discount_id',
			'discount_code',
			'quantity',
		);

		// integer attributes
		$int_attributes = array(
			'active',
			'discount_value',
			'discount_allowed',
		);

		// date attributes
		$date_attributes = array(
			'discount_start',
			'discount_end',
		);

		if (empty($model->discount_id)) {
			for ($i = 0; $i < $model->quantity; $i++) {
				// insert discount
				$insert_discount = array(
					'created' => $today,
					'saved' => $today,
					'discount_code' => empty($model->discount_code) ? $this->generateCode() : $model->discount_code,
				);

				foreach ($model as $field => $value) {
					if (in_array($field, $skip_attributes)) {
						continue;
					}
					elseif (in_array($field, $int_attributes)) {
						$insert_discount[$field] = (int) $value;
					}
					elseif (in_array($field, $date_attributes)) {
						if (empty($value)) {
							$insert_discount[$field] = '0000-00-00';
						}
						else {
							$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
							$insert_discount[$field] = $date->format('Y-m-d');
						}
					}
					else {
						$insert_discount[$field] = $value;
					}
				}

				try {
					$rs = $builder->createInsertCommand('discount', $insert_discount)->execute();

					/* if ($rs) {
						$model->discount_id = (int) Yii::app()->db->getLastInsertID();

						return true;
					} */
				}
				catch (CDbException $e) {
					// ...
				}
			}

			if (!empty($rs)) {
				return true;
			}
		}
		else {
			$update_discount = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_discount[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_discount[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_discount[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_discount[$field] = $value;
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "discount_id = :discount_id" , 
					"params" => array(
						"discount_id" => $model->discount_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('discount', $update_discount, $update_criteria)->execute();

				if ($rs) {
					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function generateCode($length = 8, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[mt_rand(0, $max)];
		}

		return $str;
	}

	public function toggle($discount_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_discount = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "discount_id = :discount_id" , 
				"params" => array(
					"discount_id" => $discount_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('discount', $update_discount, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($discount_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$discount = $this->getDiscountByIdAdmin($discount_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "discount_id = :discount_id" , 
				"params" => array(
					"discount_id" => $discount_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('discount', $delete_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}