<?php
class Product extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getProductsAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$product_id = (int) $func_args[1];
			$product_sku = $func_args[1];
			$product_title = addcslashes($func_args[1], '%_');

			// try to get brands
			$brands = Yii::app()->db
				->createCommand("SELECT brand_id FROM brand_lang WHERE brand_name LIKE :brand_name")
				->bindValue(':brand_name', '%' . $product_title . '%', PDO::PARAM_STR)
				->queryColumn();
			
			$brands_sql = !empty($brands) ? ' OR p.brand_id IN (' . implode(',', $brands) . ')' : '';

			$total_products = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM product as p JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code WHERE p.product_id = :id OR p.product_alias = :product_sku OR p.product_sku = :product_sku OR pl.product_title LIKE :product_title {$brands_sql}")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $product_id, PDO::PARAM_INT)
				->bindValue(':product_sku', $product_sku, PDO::PARAM_STR)
				->bindValue(':product_title', '%' . $product_title . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_products = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM product as p JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_products,
			'pages' => ceil($total_products / $per_page),
		);
	}

	public function getProductsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'product_id':
				$order_by = ($direction == 'asc') ? 'p.product_id' : 'p.product_id DESC';
				break;
			case 'product_title':
				$order_by = ($direction == 'asc') ? 'pl.product_title' : 'pl.product_title DESC';
				break;
			case 'product_rating':
				$order_by = ($direction == 'asc') ? 'p.product_rating' : 'p.product_rating DESC';
				break;
			default:
				$order_by = 'p.product_id DESC';
		}

		$func_args = func_get_args();

		$product_columns = array(
			'p.product_id',
			'p.active',
			'p.product_alias',
			'p.product_newest',
			'p.product_bestseller',
			'p.product_sale',
			'p.product_price',
			'p.product_price_old',
			'p.product_instock',
			'p.product_stock_qty',
			'p.product_rating',
			'p.product_photo',
			'pl.product_title',
			'bl.brand_name',
		);

		if (!empty($func_args[4])) {
			$product_id = (int) $func_args[4];
			$product_sku = $func_args[4];
			$product_title = addcslashes($func_args[4], '%_');

			// try to get brands
			$brands = Yii::app()->db
				->createCommand("SELECT brand_id FROM brand_lang WHERE brand_name LIKE :brand_name")
				->bindValue(':brand_name', '%' . $product_title . '%', PDO::PARAM_STR)
				->queryColumn();

			$brands_sql = !empty($brands) ? ' OR p.brand_id IN (' . implode(',', $brands) . ')' : '';

			$products = Yii::app()->db
				->createCommand("SELECT " . implode(',', $product_columns) . " 
								 FROM product as p 
								 JOIN product_lang as pl 
								 ON p.product_id = pl.product_id AND pl.language_code = :code 
								 LEFT JOIN brand_lang as bl 
								 ON p.brand_id = bl.brand_id AND bl.language_code = :code 
								 WHERE p.product_id = :id OR p.product_alias = :product_sku OR p.product_sku = :product_sku OR pl.product_title LIKE :product_title {$brands_sql} 
								 ORDER BY " . $order_by . " 
								 LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $product_id, PDO::PARAM_INT)
				->bindValue(':product_sku', $product_sku, PDO::PARAM_STR)
				->bindValue(':product_title', '%' . $product_title . '%', PDO::PARAM_STR)
				->queryAll();
		} else {
			$products = Yii::app()->db
				->createCommand("SELECT " . implode(',', $product_columns) . " 
								 FROM product as p 
								 JOIN product_lang as pl 
								 ON p.product_id = pl.product_id AND pl.language_code = :code 
								 LEFT JOIN brand_lang as bl 
								 ON p.brand_id = bl.brand_id AND bl.language_code = :code 
								 ORDER BY " . $order_by . " 
								 LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}

		if (!empty($products)) {
			$products = $this->getProductsStores($products);
		}
			
		return $products;
	}

	public function getProductsStores($products)
	{
		$product_ids = array_column($products, 'product_id');

		$stores_data = Yii::app()->db
			->createCommand("SELECT ps.product_id, ps.quantity, sl.store_name 
							 FROM product_store as ps 
							 JOIN store as s
							 ON ps.store_id = s.store_id  
							 JOIN store_lang as sl 
							 ON s.store_id = sl.store_id AND sl.language_code = :code 
							 WHERE ps.product_id IN (" . implode(',', $product_ids) . ")
							 ORDER BY s.store_position")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		$product_stores = [];
		
		foreach ($stores_data as $store) {
			$product_id = $store['product_id'];

			if (!isset($product_stores[$product_id])) {
				$product_stores[$product_id] = [];	
			}

			$product_stores[$product_id][] = [
				'store_name' => $store['store_name'],
				'quantity' => $store['quantity'],
			];
		}

		foreach ($products as $index => $product) {
			$product_id = $product['product_id'];
			
			if (isset($product_stores[$product_id])) {
				$products[$index]['stores'] = $product_stores[$product_id];
			} else {
				$products[$index]['stores'] = [];
			}
		}

		return $products;
	}

	public function getProductByIdAdmin($id)
	{
		$product = Yii::app()->db
			->createCommand("SELECT * FROM product WHERE product_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($product)) {
			// product langs
			$product_langs = Yii::app()->db
				->createCommand("SELECT * FROM product_lang WHERE product_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($product_langs)) {
				foreach ($product_langs as $product_lang) {
					$code = $product_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$product[$code] = $product_lang;
					}
				}
			}

			$product['stores'] = Yii::app()->db
				->createCommand("SELECT ps.quantity, sl.store_name 
								FROM product_store as ps 
								JOIN store as s
								ON ps.store_id = s.store_id  
								JOIN store_lang as sl 
								ON s.store_id = sl.store_id AND sl.language_code = :code 
								WHERE ps.product_id = :id
								ORDER BY s.store_position")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			$product['colors'] = Yii::app()->db
				->createCommand("SELECT p.product_id, p.product_alias, pl.product_title 
								 FROM product_color as pc 
								 JOIN product as p 
								 ON pc.ref_id = p.product_id 
								 JOIN product_lang as pl 
								 ON p.product_id = pl.product_id AND pl.language_code = :code 
								 WHERE pc.product_id = :id
								 ORDER BY pl.product_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			// product collections
			$product['collections'] = Yii::app()->db
				->createCommand("SELECT collection_id FROM collection_product WHERE product_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryColumn();

			// product categories
			$product['categories'] = Yii::app()->db
				->createCommand("SELECT category_id FROM category_product WHERE product_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryColumn();
			
			// product badges
			$product['badges'] = Yii::app()->db
				->createCommand("SELECT badge_id FROM product_badge WHERE product_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryColumn();
			
			// product tags
			$product['tags'] = Yii::app()->db
				->createCommand("SELECT tag_id FROM product_tag WHERE product_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryColumn();
		}

		return $product;
	}

	public function getProductByAliasAdmin($alias)
	{
        $product = Yii::app()->db
            ->createCommand("SELECT * FROM product WHERE product_alias = :alias LIMIT 1")
            ->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();
			
		return $product;
    }

	public function getProductProperties($id)
	{
		$product_properties = Yii::app()->db
			->createCommand("SELECT property_id, value_id FROM property_product WHERE product_id = :id")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryAll();

		return $product_properties;
	}

	public function getProductVariants($id)
	{
		$variants = array();

		$variants_list = Yii::app()->db
			->createCommand("SELECT * FROM product_variant WHERE product_id = :id ORDER BY position")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($variants_list)) {
			// collect variant ids
			$variant_ids = array();

			foreach ($variants_list as $variant) {
				$variant_id = $variant['variant_id'];
				
				$variant_ids[] = $variant_id;
				$variants[$variant_id] = $variant;
				$variants[$variant_id]['values'] = array();
				$variants[$variant_id]['photos'] = array();
				$variants[$variant_id]['stores'] = array();
			}

			// get variant values
			$variant_values = Yii::app()->db
				->createCommand("SELECT * FROM product_variant_value WHERE product_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			foreach ($variant_values as $variant_value) {
				$variant_id = $variant_value['variant_id'];

				if (isset($variants[$variant_id])) {
					$variants[$variant_id]['values'][] = $variant_value['value_id'];
				}
			}

			// get variant photos
			$variant_photos = Yii::app()->db
				->createCommand("SELECT * FROM product_variant_photo WHERE product_id = :id ORDER BY position")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			foreach ($variant_photos as $variant_photo) {
				$variant_id = $variant_photo['variant_id'];

				if (isset($variants[$variant_id])) {
					$variants[$variant_id]['photos'][] = $variant_photo;
				}
			}
			
			// get variant stores
			$variant_stores = Yii::app()->db
				->createCommand("SELECT pvs.variant_id, pvs.quantity, sl.store_name 
								 FROM product_variant_store as pvs 
								 JOIN store as s
								 ON pvs.store_id = s.store_id  
								 JOIN store_lang as sl 
								 ON s.store_id = sl.store_id AND sl.language_code = :code 
								 WHERE pvs.product_id = :id 
								 ORDER BY s.store_position")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			foreach ($variant_stores as $variant_store) {
				$variant_id = $variant_store['variant_id'];

				if (isset($variants[$variant_id])) {
					$variants[$variant_id]['stores'][] = $variant_store;
				}
			}
		}

		return $variants;
	}

	public function getVariantsBySku($variant_code)
	{
		$variant = Yii::app()->db
			->createCommand("SELECT * 
							 FROM product_variant 
							 WHERE variant_sku = :variant_code")
			->bindValue(':variant_code', $variant_code, PDO::PARAM_STR)
			->queryRow();

		return $variant;
	}

	public function getVariantsByIds($variant_ids)
	{
		$variants = array();

		$variants_list = Yii::app()->db
			->createCommand("SELECT * 
							 FROM product_variant 
							 WHERE variant_id IN (" . implode(',', $variant_ids) . ")")
			->queryAll();

		if (!empty($variants_list)) {
			foreach ($variants_list as $variant) {
				$variant_id = $variant['variant_id'];

				$variants[$variant_id] = $variant;
				$variants[$variant_id]['values'] = array();
			}

			// get properties and their values
			$variant_ids = array_keys($variants);

			$variant_values = Yii::app()->db
				->createCommand("SELECT pvv.variant_id, p.property_id, pl.property_title, v.value_id, vl.value_title 
								 FROM product_variant_value as pvv 
								 JOIN property_value as v 
								 ON pvv.value_id = v.value_id 
								 JOIN property_value_lang as vl 
								 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
								 JOIN property as p 
								 ON v.property_id = p.property_id 
								 JOIN property_lang as pl 
								 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1
								 WHERE pvv.variant_id IN (" . implode(',', $variant_ids) . ")
								 ORDER BY p.property_top DESC, pl.property_title, v.value_top DESC, v.value_position, vl.value_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();

			if (!empty($variant_values)) {
				foreach ($variant_values as $variant_value) {
					$variant_id = $variant_value['variant_id'];

					if (isset($variants[$variant_id])) {
						$variants[$variant_id]['values'][] = $variant_value;
					}
				}
			}
		}
			
		return $variants;
	}

	public function getProductOptions($id)
	{
		$options = Yii::app()->db
			->createCommand("SELECT po.* 
							 FROM product_option as po 
							 JOIN property_value as pv 
							 ON po.value_id = pv.value_id 
							 JOIN property_value_lang as pvl 
							 ON pv.value_id = pvl.value_id AND pvl.language_code = :code 
							 JOIN property as p 
							 ON pv.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 WHERE po.product_id = :id 
							 ORDER BY p.property_top DESC, pl.property_title, pv.value_top DESC, pv.value_position, pvl.value_title")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		return $options;
	}

	public function getGalleryPhotos($id)
	{
		$gallery_photos = Yii::app()->db
			->createCommand("SELECT * FROM product_photo WHERE product_id = :id ORDER BY position")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryAll();
			
		return $gallery_photos;
	}

	public function issetProductByAlias($product_id, $product_alias)
	{
		if (!empty($product_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM product WHERE product_id != :product_id AND product_alias LIKE :alias")
				->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
				->bindValue(':alias', $product_alias, PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM product WHERE product_alias LIKE :alias")
				->bindValue(':alias', $product_alias, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function addStockQty($order_product)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$product_id = $order_product['product_id'];
		$variant_id = $order_product['variant_id'];
		$quantity = $order_product['quantity'];
		
		if (!empty($variant_id)) {
			$variant_qty = $order_product['variant_stock_qty'] + $quantity;
			
			$update = array(
				'saved' => $today,
				'variant_instock' => ($variant_qty > 0) ? 'in_stock' : 'out_of_stock',
				'variant_stock_qty' => $variant_qty,
			);
	
			$criteria = new CDbCriteria(
				array(
					"condition" => "variant_id = :id" , 
					"params" => array(
						"id" => (int) $variant_id,
					)
				)
			);
			
			try {
				$variant_rs = $builder->createUpdateCommand('product_variant', $update, $criteria)->execute();
	
				if ($variant_rs) {
					return true;
				}
			} catch (CDbException $e) {
				
			}
		} else {
			$product_qty = $order_product['product_stock_qty'] + $quantity;
			
			$update = array(
				'saved' => $today,
				'product_instock' => ($product_qty > 0) ? 'in_stock' : 'out_of_stock',
				'product_stock_qty' => $product_qty,
			);
	
			$criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :id" , 
					"params" => array(
						"id" => (int) $product_id,
					)
				)
			);
			
			try {
				$product_rs = $builder->createUpdateCommand('product', $update, $criteria)->execute();
	
				if ($product_rs) {
					return true;
				}
			} catch (CDbException $e) {
				
			}
		}

		return false;
	}

	public function updateProductsStockStatus($products)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');
		
		foreach ($products as $product_id => $product_type) {
			$to_last_size = false;
			
			if ($product_type == 'variants') {
				// get in stock value
				$variants_summary = Yii::app()->db
					->createCommand("SELECT v.variant_id, vv.variant_instock, SUM(v.variant_stock_qty) as variant_stock_qty 
						FROM product_variant as v
						JOIN (
							SELECT MIN(variant_instock) as variant_instock 
							FROM product_variant 
							WHERE product_id = :pid
						) as vv
						WHERE v.product_id = :pid 
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryRow();
				
				$update_product = array(
					'saved' => $today,
					'product_instock' => $variants_summary['variant_instock'],
					'product_stock_qty' => $variants_summary['variant_stock_qty'],
				);

				$variants_available = Yii::app()->db
					->createCommand("SELECT COUNT(*) 
						FROM product_variant as v
						WHERE v.product_id = :pid AND v.variant_stock_qty > 0
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryScalar();

				$to_last_size = ($variants_available == 1) ? true : false;
			} else {
				$product_summary = Yii::app()->db
					->createCommand("SELECT p.product_instock, p.product_stock_qty 
						FROM product as p
						WHERE p.product_id = :pid 
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryRow();
				
				$update_product = array(
					'saved' => $today,
					'product_instock' => ($product_summary['product_stock_qty'] > 0) ? 'in_stock' : 'out_of_stock',
					'product_stock_qty' => $product_summary['product_stock_qty'],
				);

				$to_last_size = ($product_summary['product_stock_qty'] == 1) ? true : false;
			}

			// update product
			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id" , 
					"params" => array(
						"product_id" => $product_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();
			
				if (Yii::app()->params->settings['last_size'] && $rs) {
					if ($to_last_size) {
						// move to last size
						$insert_category = [
							'product_id' => $product_id,
							'category_id' => Yii::app()->params->settings['last_size_cid'],
						];

						try {
							$builder->createMultipleInsertCommand('category_product', $insert_category)->execute();
						} catch (CDbException $e) {
							
						}
					} else {
						// remove from last size
						$delete_criteria = new CDbCriteria(
							array(
								"condition" => "product_id = :product_id AND category_id = :category_id" , 
								"params" => array(
									"product_id" => $product_id,
									"category_id" => Yii::app()->params->settings['last_size_cid'],
								)
							)
						);
						
						try {
							$builder->createDeleteCommand('category_product', $delete_criteria)->execute();
						}
						catch (CDbException $e) {
							// ...
						}
					}
				}
			} catch (CDbException $e) {
				// ...
			}
		}
	}

	public function add($product, $brand_id, $categories, $group_id)
    {
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$insert_product = [
			'created' => $today,
			'saved' => $today,
			'active' => 0,
			'product_alias' => $product['id'],
			'category_id' => !empty($categories) ? $categories[0] : 0,
			'brand_id' => $brand_id,
			'product_sku' => isset($product['article']) ? $product['article'] : '',
			'product_crm_id' => $product['id'],
			'product_1c_id' => $product['externalId'],
			'product_price' => $product['minPrice'],
			'product_price_type' => $product['price_type'],
			'product_instock' => $product['quantity'] > 0 ? 'in_stock' : 'out_of_stock',
			'product_stock_type' => $product['stock_type'],
			'product_stock_qty' => $product['quantity'],
			'offer_id' => $product['offer_id'],
			'base_category_id' => $group_id,
		];
		
		try {
			$rs = $builder->createInsertCommand('product', $insert_product)->execute();

			if ($rs) {
				$product_id = (int) Yii::app()->db->getLastInsertID();
				
				foreach (Yii::app()->params->langs as $language_code => $language_name) {
					// save details
					$insert_product_lang = array(
						'product_id' => $product_id,
						'language_code' => $language_code,
						'product_visible' => 1,
						'created' => $today,
						'saved' => $today,
						'product_title' => $product['name'],
					);

					$rs = $builder->createInsertCommand('product_lang', $insert_product_lang)->execute();

					if (!$rs) {
						$delete_criteria = new CDbCriteria(
							array(
								"condition" => "product_id = :product_id" , 
								"params" => array(
									"product_id" => $product_id,
								)
							)
						);
						
						$builder->createDeleteCommand('product', $delete_criteria)->execute();

						return false;
					}
				}

				$model = new ProductForm;
				$model->product_id = $product_id;
				
				$this->saveCategories($model, $categories);
				$this->saveStores($model, $product['stores']);

				return $product_id;
            }
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}
	
	public function update($product_id, $product, $brand_id, $categories, $group_id)
    {
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_product = [
			'saved' => $today,
			'category_id' => !empty($categories) ? $categories[0] : 0,
			'brand_id' => $brand_id,
			'product_price' => $product['minPrice'],
			'product_price_type' => $product['price_type'],
			'product_instock' => $product['quantity'] > 0 ? 'in_stock' : 'out_of_stock',
			'product_stock_type' => $product['stock_type'],
			'product_stock_qty' => $product['quantity'],
			'offer_id' => $product['offer_id'],
			'base_category_id' => $group_id,
		];

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => (int) $product_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();

			if ($rs) {
				$model = new ProductForm;
				$model->product_id = $product_id;
				
				$this->saveCategories($model, $categories);
				$this->saveStores($model, $product['stores']);

				(new Sphinx)->updateProduct($product_id);

				return $product_id;
            }
		} catch (CDbException $e) {
			// ...
		}

		return false;
    }

	public function save($model, $model_lang, $variants, $bulk_variants, $options, $properties, $del_gallery_photos, $gallery_photos_position, $gallery_photos)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'product_id',
			'product_photo',
			'product_video',
			'product_sku',
			'product_crm_id',
			'product_1c_id',
			'product_price_type',
			'product_price',
			// 'product_price_old',
			'product_instock',
			'product_stock_type',
			'product_stock_qty',
			'categories',
			'tags',
			'badges',
			'colors',
		);

		// integer attributes
		$int_attributes = array(
			'category_id',
			'collection_id',
			'product_newest',
			'product_bestseller',
			'product_sale',
			'product_rating',
			'product_stock_qty',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_product_video',
		);

		// photos attributes
		$save_images = array();

		// videos attributes
		$save_videos = array(
			'product_video',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get not empty title
		foreach (Yii::app()->params->langs as $language_code => $language_name) {
			if (!empty($model_lang->product_title[$language_code])) {
				$product_title = $model_lang->product_title[$language_code];
				break;
			}
		}

		// get alias
		$model->product_alias = empty($model->product_alias) ? URLify::filter($product_title, 200) : URLify::filter($model->product_alias, 200);

		while ($this->issetProductByAlias($model->product_id, $model->product_alias)) {
			$model->product_alias = $model->product_alias . '-' . uniqid();
		}

		if (empty($model->product_id)) {
			// insert product
			$insert_product = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_product[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_product[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_product[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_product[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('product', $insert_product)->execute();

				if ($rs) {
					$model->product_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array(
						'product_no_index',
					);

					if (!empty($model->brand_id)) {
						$brand = Brand::model()->getBrandByIdAdmin($model->brand_id);
					}

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_product_lang = array(
							'product_id' => $model->product_id,
							'language_code' => $language_code,
							'product_visible' => !empty($model_lang->product_title[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
							'product_brand' => !empty($brand[$language_code]) ? trim($brand[$language_code]['brand_name'] . ' ' . $model_lang->product_title[$language_code]) : $model_lang->product_title[$language_code],
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_product_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_product_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('product_lang', $insert_product_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "product_id = :product_id" , 
									"params" => array(
										"product_id" => $model->product_id,
									)
								)
							);
							
							$builder->createDeleteCommand('product', $delete_criteria)->execute();

							return false;
						}
					}

					// save videos
					$this->saveVideos($model, $save_videos);

					$this->saveCategories($model, $model->categories);
					$this->saveTags($model, $model->tags);
					$this->saveBadges($model, $model->badges);
					$this->saveColors($model, $model->colors);

					// save variants
					// $this->saveVariants($model, $variants);
					// $this->saveBulkVariants($model, $bulk_variants);

					// save options
					$this->saveOptions($model, $options);

					// save properties
					$this->saveProperties($model, $properties);

					// save gallery photos
					// $this->saveGalleryPhotos($model->product_id, $gallery_photos);

					// delete gallery photos
					$this->deleteGalleryPhotos($del_gallery_photos);
					
					// save photos by positions
					$this->saveGalleryPhotosByPosition($model->product_id, $gallery_photos_position);
					
					// update thumbnail
					$this->updateThumbnail($model->product_id);

					(new Sphinx)->updateProduct($model->product_id);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_product = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_product[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_product[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_product[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_product[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_product[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id" , 
					"params" => array(
						"product_id" => $model->product_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();

				if ($rs) {
					// delete files
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$file_path = Yii::app()->assetManager->basePath . DS . 'product' . DS . $model->product_id . DS . $model->$del_attribute;

							if (is_file($file_path)) {
								CFileHelper::removeDirectory(dirname($file_path));
							}
						}
					}

					$int_attributes = array(
						'product_no_index',
					);

					if (!empty($model->brand_id)) {
						$brand = Brand::model()->getBrandByIdAdmin($model->brand_id);
					}

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_product_lang = array(
							'product_visible' => !empty($model_lang->product_title[$language_code]) ? 1 : 0,
							'saved' => $today,
							'product_brand' => !empty($brand[$language_code]) ? trim($brand[$language_code]['brand_name'] . ' ' . $model_lang->product_title[$language_code]) : $model_lang->product_title[$language_code],
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'product_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_product_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_product_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "product_id = :product_id AND language_code = :lang" , 
								"params" => array(
									"product_id" => (int) $model->product_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('product_lang', $update_product_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					// save videos
					$this->saveVideos($model, $save_videos);

					$this->saveCategories($model, $model->categories);
					$this->saveTags($model, $model->tags);
					$this->saveBadges($model, $model->badges);
					$this->saveColors($model, $model->colors);

					// save variants
					$this->saveVariants($model, $variants);
					// $this->saveBulkVariants($model, $bulk_variants);

					// save options
					$this->saveOptions($model, $options);

					// save properties
					$this->saveProperties($model, $properties);

					// delete gallery photos
					$this->deleteGalleryPhotos($del_gallery_photos);

					// update gallery photos positions
					$this->saveGalleryPhotosPosition($gallery_photos_position);

					// save gallery photos
					// $this->saveGalleryPhotos($model->product_id, $gallery_photos);

					// update thumbnail
					$this->updateThumbnail($model->product_id);

					(new Sphinx)->updateProduct($model->product_id);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function saveVideos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_videos_rs = array();

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$video_id = uniqid();
				$video_parts = explode('.', strtolower($model->$attribute->getName()));
				$video_extension = array_pop($video_parts);
				$video_name = implode('.', $video_parts);
				$video_name = str_replace('-', '_', URLify::filter($video_name, 60, '', true));
				$video_full_name = $video_name . '.' . $video_extension;

				$video_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $model->product_id . DS . $video_id . DS;
				$video_path = $video_dir . $video_full_name;

				$dir_rs = true;

				if (!is_dir($video_dir)) {
					$dir_rs = CFileHelper::createDirectory($video_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_video_rs = $model->$attribute->saveAs($video_path);

					if ($save_video_rs) {
						$video_save_name = $video_id . '/' . $video_full_name;

                        $videoConverter = new Video();
                        $videoData = $videoConverter->convert($video_full_name, $video_dir);

						$save_videos_rs[$attribute] = json_encode(array(
							'file' => $video_save_name,
                            'params' => $videoData['params'],
                            'web_file' => $video_id . '/' . $videoData['file'],
						));
					}
				}
			}
		}

		if (!empty($save_videos_rs)) {
			$update_product = array(
				'saved' => $today,
			);

			$update_product = array_merge($update_product, $save_videos_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id" , 
					"params" => array(
						"product_id" => $model->product_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	private function saveCollections($model, $collections)
	{
		$rs_inserted = 0;
		$model->product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete collections
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => $model->product_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('collection_product', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($collections)) {
			$insert_collections = array();

			foreach ($collections as $collection) {
				$collection = (int) $collection;

				if (!empty($collection)) {
					$insert_collections[] = array(
						'product_id' => $model->product_id,
						'collection_id' => $collection,
					);
				}
			}

			if (!empty($insert_collections)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('collection_product', $insert_collections)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	private function saveCategories($model, $categories)
	{
		$rs_inserted = 0;
		$model->product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete categories
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => $model->product_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('category_product', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($categories)) {
			$insert_categories = array();

			foreach ($categories as $category) {
				$category = (int) $category;

				if (!empty($category)) {
					$insert_categories[] = array(
						'product_id' => $model->product_id,
						'category_id' => $category,
					);
				}
			}

			if (!empty($insert_categories)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('category_product', $insert_categories)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	private function saveStores($model, $stores)
	{
		$rs_inserted = 0;
		$model->product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete stores
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => $model->product_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('product_store', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($stores)) {
			$insert_stores = array();

			foreach ($stores as $store_id => $quantity) {
				$insert_stores[] = array(
					'product_id' => $model->product_id,
					'store_id' => (int) $store_id,
					'quantity' => (int) $quantity,
				);
			}

			if (!empty($insert_stores)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('product_store', $insert_stores)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	private function saveTags($model, $tags)
	{
		$rs_inserted = 0;
		$model->product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete tags
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" ,
				"params" => array(
					"product_id" => $model->product_id,
				)
			)
		);

		try {
			$builder->createDeleteCommand('product_tag', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($tags)) {
			$insert_tags = array();

			foreach ($tags as $tag) {
				$tag = (int) $tag;

				if (!empty($tag)) {
					$insert_tags[] = array(
						'product_id' => $model->product_id,
						'tag_id' => $tag,
					);
				}
			}

			if (!empty($insert_tags)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('product_tag', $insert_tags)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}
	
	private function saveBadges($model, $badges)
	{
		$rs_inserted = 0;
		$model->product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete badges
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" ,
				"params" => array(
					"product_id" => $model->product_id,
				)
			)
		);

		try {
			$builder->createDeleteCommand('product_badge', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($badges)) {
			$insert_badges = array();

			foreach ($badges as $badge) {
				$badge = (int) $badge;

				if (!empty($badge)) {
					$insert_badges[] = array(
						'product_id' => $model->product_id,
						'badge_id' => $badge,
					);
				}
			}

			if (!empty($insert_badges)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('product_badge', $insert_badges)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
    }
	
	private function saveColors($model, $colors)
	{
		$rs_inserted = 0;
		$model->product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// get previous color refs
		$refs = Yii::app()->db
			->createCommand("SELECT ref_id FROM product_color WHERE product_id = :product_id")
			->bindValue(':product_id', $model->product_id, PDO::PARAM_INT)
			->queryColumn();
		
		$refs[] = $model->product_id;
		
		// delete colors
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id IN (" . implode(',', $refs) . ")",
			)
		);

		/* $delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" ,
				"params" => array(
					"product_id" => $model->product_id,
				)
			)
		); */

		try {
			$builder->createDeleteCommand('product_color', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($colors)) {
			$insert_colors = array();

			foreach ($colors as $color) {
				$color = (int) $color;

				if (!empty($color)) {
					$insert_colors[] = array(
						'product_id' => $model->product_id,
						'ref_id' => $color,
					);
				}
			}

			if (!empty($insert_colors)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('product_color', $insert_colors)->execute();
				} catch (CDbException $e) {
					return false;
				}
			}

			// add related colors
			$colors_copy = $colors;
			$colors_copy[] = $model->product_id;

			foreach ($colors as $product_id) {
				$insert_colors = array();

				foreach ($colors_copy as $color) {
					if ($color == $product_id) {
						continue;
					}
					
					$color = (int) $color;
	
					if (!empty($color)) {
						$insert_colors[] = array(
							'product_id' => $product_id,
							'ref_id' => $color,
						);
					}
				}
	
				if (!empty($insert_colors)) {
					try {
						$rs_inserted = $builder->createMultipleInsertCommand('product_color', $insert_colors)->execute();
					} catch (CDbException $e) {
						return false;
					}
				}
			}
		}

		return $rs_inserted;
    }

	private function saveVariants($model, $variants)
	{
		if ($model->product_price_type != 'variants') {
			return false;
		}
		
		$product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		if (!empty($variants)) {
			$index = 1;

			foreach ($variants as $i => $variant) {
				$variant_id = !empty($variant['variant_id']) ? $variant['variant_id'] : 0;

				if (!empty($variant_id)) {
					// update variant
					$this->updateVariant($product_id, $variant_id, $variant, $index);
				} else {
					// add variant
					$variant_id = $this->addVariant($product_id, $variant, $index);
				}

				$index++;
			}

			// get min price and in stock value
			$variants_summary = Yii::app()->db
				->createCommand("SELECT v.variant_id, v.variant_price, v.variant_price_old, vv.variant_instock, vv.variant_weight, SUM(v.variant_stock_qty) as variant_stock_qty 
					FROM product_variant as v
					JOIN (
						SELECT MIN(variant_instock) as variant_instock, MIN(variant_weight) as variant_weight 
						FROM product_variant 
						WHERE product_id = :pid
					) as vv
					WHERE v.product_id = :pid
					ORDER BY v.variant_price 
					LIMIT 1")
				->bindValue(':pid', $product_id, PDO::PARAM_INT)
				->queryRow();
			
			// update product
			$update_product = array(
				'saved' => date('Y-m-d H:i:s'),
				// 'product_price' => (float) $variants_summary['variant_price'],
				'product_price_old' => (float) $variants_summary['variant_price_old'],
				// 'product_instock' => $variants_summary['variant_instock'],
				// 'product_stock_qty' => $variants_summary['variant_stock_qty'],
				// 'product_weight' => $variants_summary['variant_weight'],
			);
	
			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id" , 
					"params" => array(
						"product_id" => $product_id,
					)
				)
			);
	
			try {
				$rs = (bool) $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();
			} catch (CDbException $e) {
				// ...
			}
		}

		return true;
	}

	private function __saveVariants($model, $variants)
	{
		if ($model->product_price_type != 'variants') {
			return false;
		}
		
		$product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		if (!empty($variants)) {
			$index = 1;

			foreach ($variants as $i => $variant) {
				// delete variant
				if (!empty($variant['del_variant'])) {
					$this->deleteVariant($product_id, $variant['del_variant']);
					continue;
				}

				$variant_id = !empty($variant['variant_id']) ? $variant['variant_id'] : 0;

				if (!empty($variant['values']) && is_array($variant['values'])) {
					$variant['values'] = array_filter($variant['values'], function($v) {
						return !empty($v);
					});
				}

				// invalid variant
				if (empty($variant['values'])) { // empty($variant['variant_price'])
					if (!empty($variant_id)) {
						$this->deleteVariant($product_id, $variant_id);
					}

					continue;
				}

				if (!empty($variant_id)) {
					// update variant
					$this->updateVariant($product_id, $variant_id, $variant, $index);

					if (!empty($variant['delete'])) {
						// delete variant photos
						$this->deleteVariantPhotos($variant['delete']);
					}

					if (!empty($variant['position'])) {
						// update variant photos positions
						$this->saveVariantPhotosPosition($variant['position']);
					}
				} else {
					// add variant
					$variant_id = $this->addVariant($product_id, $variant, $index);
				}

				$photos = CUploadedFile::getInstancesByName('variant[' . $i . '][photos]');
				$this->saveVariantPhotos($product_id, $variant_id, $photos);

				$index++;
			}

			// get min price and in stock value
			$variants_summary = Yii::app()->db
				->createCommand("SELECT v.variant_id, v.variant_price, v.variant_price_old, vv.variant_instock, vv.variant_weight, SUM(v.variant_stock_qty) as variant_stock_qty 
					FROM product_variant as v
					JOIN (
						SELECT MIN(variant_instock) as variant_instock, MIN(variant_weight) as variant_weight 
						FROM product_variant 
						WHERE product_id = :pid
					) as vv
					WHERE v.product_id = :pid
					ORDER BY v.variant_price 
					LIMIT 1")
				->bindValue(':pid', $product_id, PDO::PARAM_INT)
				->queryRow();
			
			// update product
			$update_product = array(
				'saved' => date('Y-m-d H:i:s'),
				'product_price' => (float) $variants_summary['variant_price'],
				'product_price_old' => (float) $variants_summary['variant_price_old'],
				'product_instock' => $variants_summary['variant_instock'],
				'product_stock_qty' => $variants_summary['variant_stock_qty'],
				'product_weight' => $variants_summary['variant_weight'],
			);
	
			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id" , 
					"params" => array(
						"product_id" => $product_id,
					)
				)
			);
	
			try {
				$rs = (bool) $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();
			} catch (CDbException $e) {
				// ...
			}
		}

		return true;
	}

	private function saveBulkVariants($model, $bulk_variants)
	{
		if (empty($bulk_variants)) {
			return false;
		}

		set_time_limit(1800);

		$product_id = (int) $model->product_id;
		$product_sku = $model->product_sku;
		$product_price = (float) $model->product_price;
		$product_price_old = (float) $model->product_price_old;

		$variant_combinations = $this->getVariantCombinations($bulk_variants);

		// get all current variants for product
		$variant_combinations_map = Yii::app()->db
			->createCommand("SELECT GROUP_CONCAT(value_id ORDER BY value_id ASC SEPARATOR '_') FROM product_variant_value WHERE product_id = :product_id GROUP BY variant_id")
			->bindValue(':product_id', $product_id, PDO::PARAM_INT)
			->queryColumn();

		// get latest position
		$position = Yii::app()->db
			->createCommand("SELECT MAX(position) FROM product_variant WHERE product_id = :product_id")
			->bindValue(':product_id', $product_id, PDO::PARAM_INT)
			->queryScalar();

		// add new variants
		foreach ($variant_combinations as $variant_combination) {
			sort($variant_combination, SORT_NUMERIC);

			$values = implode('_', $variant_combination);

			if (!in_array($values, $variant_combinations_map)) {
				$variant = array(
					'variant_sku' => $product_sku,
					'variant_price' => $product_price,
					'variant_price_old' => $product_price_old,
					'variant_price_type' => 'item',
					'variant_instock' => 'in_stock',
					'variant_stock_qty' => 1,
					'variant_weight' => 0,
					'variant_length' => 0,
					'variant_width' => 0,
					'variant_pack_size' => 0,
					'variant_pack_qty' => 0,
					'values' => $variant_combination,
				);
				$position++;
				
				$this->addVariant($product_id, $variant, $position);
			}
		}
	}

	private function getVariantCombinations($arrays)
	{
		$result = array(array());

		foreach ($arrays as $property => $property_values) {
			$tmp = array();
			
			foreach ($result as $result_item) {
				foreach ($property_values as $property_value) {
					$tmp[] = array_merge($result_item, array($property => $property_value));
				}
			}
			
			$result = $tmp;
		}

		return $result;
	}

	public function addVariant($product_id, $variant, $index)
	{
		$today = date('Y-m-d H:i:s');
		$builder = Yii::app()->db->schema->commandBuilder;

		$insert_variant = array(
			'product_id' => $product_id,
			'created' => $today,
			'saved' => $today,
			'variant_sku' => $variant['variant_sku'],
			'variant_price' => (float) $variant['variant_price'],
			'variant_price_old' => (float) $variant['variant_price_old'],
			'variant_price_type' => $variant['variant_price_type'],
			'variant_instock' => $variant['variant_instock'],
			'variant_stock_qty' => (int) $variant['variant_stock_qty'],
			'variant_weight' => (float) $variant['variant_weight'],
			// 'variant_length' => (int) $variant['variant_length'],
			// 'variant_width' => (int) $variant['variant_width'],
			// 'variant_pack_size' => (float) $variant['variant_pack_size'],
			// 'variant_pack_qty' => (int) $variant['variant_pack_qty'],
			'position' => $index,
		);

		try {
			$rs = $builder->createInsertCommand('product_variant', $insert_variant)->execute();

			if ($rs) {
				$variant_id = (int) Yii::app()->db->getLastInsertID();

				// add values
				$insert_values = array();

				foreach ($variant['values'] as $value_id) {
					$insert_values[] = array(
						'product_id' => $product_id,
						'variant_id' => $variant_id,
						'value_id' => (int) $value_id,
					);
				}

				$builder->createMultipleInsertCommand('product_variant_value', $insert_values)->execute();

				return $variant_id;
			}
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function updateVariant($product_id, $variant_id, $variant, $index, $is_api = false)
	{
		$today = date('Y-m-d H:i:s');
		$builder = Yii::app()->db->schema->commandBuilder;

		if ($is_api) {
			$update_variant = array(
				'saved' => $today,
				'variant_sku' => $variant['variant_sku'],
				'variant_price' => (float) $variant['variant_price'],
				// 'variant_price_old' => (float) $variant['variant_price_old'],
				'variant_price_type' => $variant['variant_price_type'],
				'variant_instock' => $variant['variant_instock'],
				'variant_stock_qty' => (int) $variant['variant_stock_qty'],
				'variant_weight' => (float) $variant['variant_weight'],
				
				// 'variant_length' => (int) $variant['variant_length'],
				// 'variant_width' => (int) $variant['variant_width'],
				// 'variant_pack_size' => (float) $variant['variant_pack_size'],
				// 'variant_pack_qty' => (int) $variant['variant_pack_qty'],
				'position' => $index,
			);
		} else {
			$update_variant = array(
				'saved' => $today,
				// 'variant_sku' => $variant['variant_sku'],
				// 'variant_price' => (float) $variant['variant_price'],
				'variant_price_old' => (float) $variant['variant_price_old'],
				// 'variant_price_type' => $variant['variant_price_type'],
				// 'variant_instock' => $variant['variant_instock'],
				// 'variant_stock_qty' => (int) $variant['variant_stock_qty'],
				// 'variant_weight' => (float) $variant['variant_weight'],
				
				// 'variant_length' => (int) $variant['variant_length'],
				// 'variant_width' => (int) $variant['variant_width'],
				// 'variant_pack_size' => (float) $variant['variant_pack_size'],
				// 'variant_pack_qty' => (int) $variant['variant_pack_qty'],
				// 'position' => $index,
			);
		}

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id AND variant_id = :variant_id" , 
				"params" => array(
					"product_id" => (int) $product_id,
					"variant_id" => (int) $variant_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('product_variant', $update_variant, $update_criteria)->execute();

			if ($rs) {
                if ($is_api) {
                    // remove previous values
                    $this->deleteVariantValues($product_id, $variant_id);

                    // add values
                    $insert_values = array();

                    foreach ($variant['values'] as $value_id) {
                        $insert_values[] = array(
                            'product_id' => $product_id,
                            'variant_id' => $variant_id,
                            'value_id' => (int) $value_id,
                        );
                    }

                    $builder->createMultipleInsertCommand('product_variant_value', $insert_values)->execute();
                }

				return true;
			}
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function saveVariantStores($product_id, $variant_id, $stores)
	{
		$rs_inserted = 0;
		$product_id = (int) $product_id;
		$variant_id = (int) $variant_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete stores
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id AND variant_id = :variant_id" , 
				"params" => array(
					"product_id" => $product_id,
					"variant_id" => $variant_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('product_variant_store', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($stores)) {
			$insert_stores = array();

			foreach ($stores as $store_id => $quantity) {
				$insert_stores[] = array(
					'product_id' => $product_id,
					'variant_id' => $variant_id,
					'store_id' => (int) $store_id,
					'quantity' => (int) $quantity,
				);
			}

			if (!empty($insert_stores)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('product_variant_store', $insert_stores)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	private function deleteVariantPhotos($del_photos)
	{
		if (empty($del_photos)) {
			return false;
		}
		
		$delete_counter = 0;
		
		// delete photos
		$builder = Yii::app()->db->schema->commandBuilder;

		foreach ($del_photos as $photo_id) {
			// get delete data
			$delete_data = Yii::app()->db
				->createCommand("SELECT * FROM product_variant_photo WHERE photo_id = :pid")
				->bindValue(':pid', (int) $photo_id, PDO::PARAM_INT)
				->queryRow();

			if (!empty($delete_data)) {
				$delete_criteria = new CDbCriteria(
					array(
						"condition" => "photo_id = :photo_id" , 
						"params" => array(
							"photo_id" => (int) $photo_id,
						)
					)
				);

				try {
					$rs = $builder->createDeleteCommand('product_variant_photo', $delete_criteria)->execute();

					if ($rs) {
						$delete_counter++;

						// delete file
						$file_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $delete_data['product_id'] . DS . 'variant' . DS . $delete_data['variant_id'] . DS;
						
						$photo_path = json_decode($delete_data['photo_path'], true);

						$file_path = $file_dir . $photo_path['thumb']['1x'];

						if (is_file($file_path) && is_dir(dirname($file_path))) {
							CFileHelper::removeDirectory(dirname($file_path));
						}
					}
				}
				catch (CDbException $e) {
					// ...
				}
			}
		}

		if ($delete_counter) {
			return $delete_counter;
		}
		else {
			return false;	
		}
	}

	private function saveVariantPhotosPosition($photos_position)
	{
		if (empty($photos_position)) {
			return false;
		}

		$update_counter = 0;
		
		// update photos positions
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		foreach ($photos_position as $photo_id => $position) {
			$update = array(
				'saved' => $today,
				'position' => $position,
			);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "photo_id = :photo_id" , 
					"params" => array(
						"photo_id" => $photo_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('product_variant_photo', $update, $update_criteria)->execute();

				if ($rs) {
					$update_counter++;	
				}
			}
			catch (CDbException $e) {
				return false;
			}
		}

		if (count($photos_position) == $update_counter) {
			return true;
		}
		else {
			return false;	
		}
	}

	private function saveVariantPhotos($product_id, $variant_id, $photos)
	{
		if (empty($product_id) || empty($variant_id) || empty($photos)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$photos_counter = 0;

		foreach ($photos as $photo) {
			$model = new AddFileForm('photo');
			$model->photo = $photo;

			if ($model->validate()) {
				$save_image_rs = false;
				
				$image_id = uniqid();
				$image_parts = explode('.', $model->photo->getName());
				$image_extension = strtolower(array_pop($image_parts));
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $product_id . DS . 'variant' . DS . $variant_id . DS . $image_id . DS;
				$image_path = $image_dir . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}				

				if ($dir_rs) {
					$save_image_rs = $model->photo->saveAs($image_path);

					if ($save_image_rs) {
						$saved = false;

						$image_file_large = $image_dir . $image_name . '_l.' . $image_extension;
						$image_file_large_2x = $image_dir . $image_name . '_l@2x.' . $image_extension;
						$image_file_catalog = $image_dir . $image_name . '_c.' . $image_extension;
						$image_file_catalog_2x = $image_dir . $image_name . '_c@2x.' . $image_extension;
						$image_file_catalog_alt = $image_dir . $image_name . '_c_alt.' . $image_extension;
						$image_file_catalog_alt_2x = $image_dir . $image_name . '_c_alt@2x.' . $image_extension;
						$image_file_thumb = $image_dir . $image_name . '_t.' . $image_extension;
						$image_file_thumb_2x = $image_dir . $image_name . '_t@2x.' . $image_extension;

						// 470*506 (940*1012 retina) - large
						// 230x230 (460*460 retina) - catalog #1
						// 270x404 (540*808 retina) - catalog #2
						// 100x100 (200*200 retina) - thumb
						
						// resize file
						if (extension_loaded('imagick')) {
							$imagine = new Imagine\Imagick\Imagine();
						}
						elseif (extension_loaded('gd') && function_exists('gd_info')) {
							$imagine = new Imagine\Gd\Imagine();
						}

						// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
						$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
						// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
						$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

						$image_obj = $imagine->open($image_path);
						$original_image_size = $image_obj->getSize();

						if ($original_image_size->getWidth() < 470 || $original_image_size->getHeight() < 506) {
							continue;
						}

						// large
						$resized_image_large = $image_obj->thumbnail(new Imagine\Image\Box(470, 506), $mode_inset)
							->save($image_file_large, array('quality' => 80))
							->getSize();

						if ($original_image_size->getWidth() >= 940 && $original_image_size->getHeight() >= 1012) {
							$resized_image_large_2x = $image_obj->thumbnail(new Imagine\Image\Box(940, 1012), $mode_inset)
								->save($image_file_large_2x, array('quality' => 80))
								->getSize();
						} else {
							$resized_image_large_2x = null;
						}

						// catalog
						$resized_image_catalog_2x = $image_obj->thumbnail(new Imagine\Image\Box(460, 460), $mode_inset)
							->save($image_file_catalog_2x, array('quality' => 80))
							->getSize();
						$resized_image_catalog = $image_obj->thumbnail(new Imagine\Image\Box(230, 230), $mode_inset)
							->save($image_file_catalog, array('quality' => 80))
							->getSize();

						// catalog alt
						$resized_image_catalog_alt = $image_obj->thumbnail(new Imagine\Image\Box(270, 404), $mode_inset)
							->save($image_file_catalog_alt, array('quality' => 80))
							->getSize();
						
						if ($original_image_size->getWidth() >= 540 && $original_image_size->getHeight() >= 808) {
							$resized_image_catalog_alt_2x = $image_obj->thumbnail(new Imagine\Image\Box(540, 808), $mode_inset)
								->save($image_file_catalog_alt_2x, array('quality' => 80))
								->getSize();
						} else {
							$resized_image_catalog_alt_2x = null;
						}

						// thumb
						$resized_image_thumb = $image_obj->thumbnail(new Imagine\Image\Box(100, 100), $mode_inset)
							->save($image_file_thumb, array('quality' => 80))
							->getSize();

						$resized_image_thumb_2x = $image_obj->thumbnail(new Imagine\Image\Box(200, 200), $mode_inset)
							->save($image_file_thumb_2x, array('quality' => 80))
							->getSize();
						

						$save_path = json_encode(array(
							'large' => array(
								'1x' => $image_id . '/' . $image_name . '_l.' . $image_extension,
								'2x' => ($resized_image_large_2x === null) 
									? null 
									: $image_id . '/' . $image_name . '_l@2x.' . $image_extension,
							),
							'catalog' => array(
								'1x' => $image_id . '/' . $image_name . '_c.' . $image_extension,
								'2x' => ($resized_image_catalog_2x === null) 
									? null 
									: $image_id . '/' . $image_name . '_c@2x.' . $image_extension,
							),
							'catalog_alt' => array(
								'1x' => $image_id . '/' . $image_name . '_c_alt.' . $image_extension,
								'2x' => ($resized_image_catalog_alt_2x === null) 
									? null 
									: $image_id . '/' . $image_name . '_c_alt@2x.' . $image_extension,
							),
							'thumb' => array(
								'1x' => $image_id . '/' . $image_name . '_t.' . $image_extension,
								'2x' => ($resized_image_thumb_2x === null) 
									? null 
									: $image_id . '/' . $image_name . '_t@2x.' . $image_extension,
							),
						));

						$save_size = json_encode(array(
							'large' => array(
								'1x' => array(
									'w' => $resized_image_large->getWidth(),
									'h' => $resized_image_large->getHeight(),
								),
								'2x' => ($resized_image_large_2x === null) ? null : array(
									'w' => $resized_image_large_2x->getWidth(),
									'h' => $resized_image_large_2x->getHeight(),
								),
							),
							'catalog' => array(
								'1x' => array(
									'w' => $resized_image_catalog->getWidth(),
									'h' => $resized_image_catalog->getHeight(),
								),
								'2x' => ($resized_image_catalog_2x === null) ? null : array(
									'w' => $resized_image_catalog_2x->getWidth(),
									'h' => $resized_image_catalog_2x->getHeight(),
								),
							),
							'catalog_alt' => array(
								'1x' => array(
									'w' => $resized_image_catalog_alt->getWidth(),
									'h' => $resized_image_catalog_alt->getHeight(),
								),
								'2x' => ($resized_image_catalog_alt_2x === null) ? null : array(
									'w' => $resized_image_catalog_alt_2x->getWidth(),
									'h' => $resized_image_catalog_alt_2x->getHeight(),
								),
							),
							'thumb' => array(
								'1x' => array(
									'w' => $resized_image_thumb->getWidth(),
									'h' => $resized_image_thumb->getHeight(),
								),
								'2x' => ($resized_image_thumb_2x === null) ? null : array(
									'w' => $resized_image_thumb_2x->getWidth(),
									'h' => $resized_image_thumb_2x->getHeight(),
								),
							),
						));

						if (is_file($image_file_large)) {
							// get max position
							$max_position = Yii::app()->db
								->createCommand("SELECT MAX(position) FROM product_variant_photo")
								->queryScalar();

							$max_position += 1;

							// insert photo
							$insert_photo = array(
								'photo_id' => null,
								'created' => $today,
								'saved' => $today,
								'product_id' => $product_id,
								'variant_id' => $variant_id,
								'photo_path' => $save_path,
								'photo_size' => $save_size,
								'position' => $max_position,
							);

							try {
								$save_image_rs = (bool) $builder->createInsertCommand('product_variant_photo', $insert_photo)->execute();
								$saved = true;
							} catch (CDbException $e) {
								
							}
						}
						
						if (!$saved) {
							// remove resized files
							if (is_file($image_file_large)) {
								unlink($image_file_large);
							}
							
							if (is_file($image_file_large_2x)) {
								unlink($image_file_large_2x);
							}

							if (is_file($image_file_catalog)) {
								unlink($image_file_catalog);
							}
							
							if (is_file($image_file_catalog_2x)) {
								unlink($image_file_catalog_2x);
							}
							
							if (is_file($image_file_catalog_alt)) {
								unlink($image_file_catalog_alt);
							}
							
							if (is_file($image_file_catalog_alt_2x)) {
								unlink($image_file_catalog_alt_2x);
							}

							if (is_file($image_file_thumb)) {
								unlink($image_file_thumb);
							}
							
							if (is_file($image_file_thumb_2x)) {
								unlink($image_file_thumb_2x);
							}
						}

						// remove original image
						unlink($image_path);
					} else {
						continue;
					}
				}

				if ($save_image_rs) {
					$photos_counter++;
				}
			}
		}

		if ($photos_counter) {
			return $photos_counter;
		} else {
			return false;
		}
	}

	public function deleteVariant($product_id, $variant_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id AND variant_id = :variant_id" , 
				"params" => array(
					"product_id" => (int) $product_id,
					"variant_id" => (int) $variant_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('product_variant', $delete_criteria)->execute();
			$builder->createDeleteCommand('product_variant_photo', $delete_criteria)->execute();
			$builder->createDeleteCommand('product_variant_store', $delete_criteria)->execute();
			$this->deleteVariantValues($product_id, $variant_id);

			// remove variant directory
			if (is_dir($assetPath . DS . 'product' . DS . $product_id . DS . 'variant' . DS . $variant_id)) {
				CFileHelper::removeDirectory($assetPath . DS . 'product' . DS . $product_id . DS . 'variant' . DS . $variant_id);
			}

			return true;
		} catch (CDbException $e) {
			// ...
		}

		return true;
	}

	private function deleteVariantValues($product_id, $variant_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;

		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id AND variant_id = :variant_id" , 
				"params" => array(
					"product_id" => (int) $product_id,
					"variant_id" => (int) $variant_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('product_variant_value', $delete_criteria)->execute();

			return true;
		} catch (CDbException $e) {
			// ...
		}

		return true;
	}

	public function makeVariantsFromProperties()
	{
		$variants_counter = 0;

		$products = Yii::app()->db
			->createCommand("SELECT product_id, product_instock, product_stock_qty, product_price, product_price_old FROM product WHERE product_price_type = 'item'")
			->queryAll();

		foreach ($products as $product) {
			// get size values
			$sizes = Yii::app()->db
				->createCommand("SELECT pp.value_id, vl.value_title 
								 FROM property_product as pp 
								 JOIN property as p 
								 ON pp.property_id = p.property_id 
								 JOIN property_value_lang as vl 
								 ON pp.value_id = vl.value_id AND vl.language_code = :code
								 WHERE pp.product_id = :product_id AND p.property_size = 1 
								 ORDER BY vl.value_title")
				->bindValue(':product_id', (int) $product['product_id'], PDO::PARAM_INT)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
			
			if (!empty($sizes)) {
				usort($sizes, function($a, $b){
					return strnatcmp($a['value_title'], $b['value_title']);
				});

				// add variants
				foreach ($sizes as $index => $size) {
					$variant = [
						'variant_sku' => '',
						'variant_price' => $product['product_price'],
						'variant_price_old' => $product['product_price_old'],
						'variant_price_type' => 'item',
						'variant_instock' => $product['product_instock'],
						'variant_stock_qty' => $product['product_stock_qty'],
						'variant_weight' => 0,
						'variant_length' => 0,
						'variant_width' => 0,
						'variant_pack_size' => 0,
						'variant_pack_qty' => 0,
						'values' => [
							$size['value_id'],
						],
					];

					$v_id = $this->addVariant($product['product_id'], $variant, $index + 1);

					if ($v_id) {
						$variants_counter++;
					}
				}

				$update_criteria = new CDbCriteria([
					'condition' => 'product_id = :product_id',
					'params' => [
						'product_id' => (int) $product['product_id'],
					],
				]);

				$update_product = [
					'saved' => date('Y-m-d H:i:s'),
					'product_price_type' => 'variants',
				];
		
				try {
					Yii::app()->db->schema->commandBuilder->createUpdateCommand('product', $update_product, $update_criteria)->execute();
				} catch (CDbException $e) {
					// ...
				}
			}
		}

		return $variants_counter ? true : false;
	}

	private function saveOptions($model, $options)
	{
		$product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		if (!empty($options)) {
			foreach ($options as $option) {
				// delete option
				if (!empty($option['del_option'])) {
					$this->deleteOption($product_id, $option['del_option']);
					continue;
				}

				$option_id = !empty($option['option_id']) ? $option['option_id'] : 0;

				// invalid option
				if (empty($option['value_id'])) {
					if (!empty($option_id)) {
						$this->deleteOption($product_id, $option_id);
					}

					continue;
				}

				if (!empty($option_id)) {
					// update option
					$this->updateOption($product_id, $option_id, $option);
				} else {
					// add option
					$this->addOption($product_id, $option);
				}
			}
		}

		return true;
	}

	private function addOption($product_id, $option)
	{
		$today = date('Y-m-d H:i:s');
		$builder = Yii::app()->db->schema->commandBuilder;

		$insert_option = array(
			'product_id' => $product_id,
			'created' => $today,
			'saved' => $today,
			'option_price' => (float) $option['option_price'],
			'value_id' => (int) $option['value_id'],
		);

		try {
			$rs = $builder->createInsertCommand('product_option', $insert_option)->execute();

			if ($rs) {
				$option_id = (int) Yii::app()->db->getLastInsertID();

				return true;
			}
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}

	private function updateOption($product_id, $option_id, $option)
	{
		$today = date('Y-m-d H:i:s');
		$builder = Yii::app()->db->schema->commandBuilder;

		$update_option = array(
			'saved' => $today,
			'option_price' => (float) $option['option_price'],
			'value_id' => (int) $option['value_id'],
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id AND option_id = :option_id" , 
				"params" => array(
					"product_id" => (int) $product_id,
					"option_id" => (int) $option_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('product_option', $update_option, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}

	private function deleteOption($product_id, $option_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;

		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id AND option_id = :option_id" , 
				"params" => array(
					"product_id" => (int) $product_id,
					"option_id" => (int) $option_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('product_option', $delete_criteria)->execute();

			return true;
		} catch (CDbException $e) {
			// ...
		}

		return true;
	}

	private function saveProperties($model, $properties)
	{
		$rs_inserted = 0;
		$model->product_id = (int) $model->product_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete properties
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => $model->product_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('property_product', $delete_criteria)->execute();
		} catch (CDbException $e) {
			// ...
		}

		if (!empty($properties)) {
			// get valid properties
			$values_list = array_unique($properties);
			$values_list = array_filter($values_list, function($v) {
				$v = (int) $v;
				return $v > 0;
			});
			
			if (empty($values_list)) {
				return false;
			}

			$properties = PropertyValue::model()->getPropertyValuesAdmin($model->category_id, $values_list);

			$insert_properties = array();

			foreach ($properties as $property) {
				$property_id = (int) $property['property_id'];
				$value_id = (int) $property['value_id'];

				if (!empty($property)) {
					$insert_properties[] = array(
						'product_id' => $model->product_id,
						'property_id' => $property_id,
						'value_id' => $value_id,
					);
				}
			}

			if (!empty($insert_properties)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('property_product', $insert_properties)->execute();
				} catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	public function updateProperties($product_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		
		$variant_values = Yii::app()->db
			->createCommand("SELECT pvv.*, pv.property_id  
							FROM product_variant_value as pvv 
							JOIN property_value as pv
							ON pvv.value_id = pv.value_id 
							WHERE pvv.product_id = :product_id")
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();

		if (empty($variant_values)) {
			return;
		}

		$property_ids = array_column($variant_values, 'property_id');
		$value_ids = array_column($variant_values, 'value_id');

		$product_values = Yii::app()->db
			->createCommand("SELECT value_id  
							FROM property_product as pp 
							WHERE pp.product_id = :product_id AND pp.property_id IN (" . implode(',', $property_ids) . ")")
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryColumn();
		
		$delete_values = [];
		$add_values = [];

		foreach ($product_values as $value_id) {
			if (!in_array($value_id, $value_ids)) {
				$delete_values[] = $value_id;
			}
		}
		
		foreach ($variant_values as $value) {
			if (!in_array($value['value_id'], $product_values)) {
				$add_values[$value['property_id']] = $value['value_id'];
			}
		}

		if (!empty($delete_values)) {
			// delete properties
			$delete_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id AND value_id IN (" . implode(',', $delete_values) . ")" , 
					"params" => array(
						"product_id" => (int) $product_id,
					)
				)
			);
			
			try {
				$builder->createDeleteCommand('property_product', $delete_criteria)->execute();
			} catch (CDbException $e) {
				// ...
			}
		}

        if (!empty($add_values)) {
            $insert_properties = [];

            foreach ($add_values as $property_id => $value_id) {
                $insert_properties[] = array(
                    'product_id' => $product_id,
                    'property_id' => $property_id,
                    'value_id' => $value_id,
                );
            }

            try {
                $builder->createMultipleInsertCommand('property_product', $insert_properties)->execute();
            } catch (CDbException $e) {
                // ...
            }
        }
    }

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_parts = explode('.', strtolower($model->$attribute->getName()));
				$image_extension = array_pop($image_parts);
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;

				$image_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $model->product_id . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;
						
						if ($attribute == 'product_photo') {
							$image_files = array();

							$image_file_original = $image_dir . $image_name . '_o.' . $image_extension; // original
							$image_save_name_original = $image_id . '/' . $image_name . '_o.' . $image_extension;

							$image_file_details = $image_dir . $image_name . '_d.' . $image_extension; // details page
							$image_save_name_details = $image_id . '/' . $image_name . '_d.' . $image_extension;

							$image_file_list = $image_dir . $image_name . '_l.' . $image_extension; // catalog list
							$image_save_name_list = $image_id . '/' . $image_name . '_l.' . $image_extension;

							$image_file_cart = $image_dir . $image_name . '_c.' . $image_extension; // cart
							$image_save_name_cart = $image_id . '/' . $image_name . '_c.' . $image_extension;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							if ($original_image_size->getWidth() < 250) {
								continue;
							}

							$resized_image = $image_obj->save($image_file_original, array('quality' => 80));

							$image_files['original'] = array(
								'path' => $image_save_name_original,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// details image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 740) {
								$resized_image = $image_obj->resize($original_image_size->widen(740))
									->save($image_file_details, array('quality' => 80));	
							}
							else {
								$resized_image = $image_obj->resize($original_image_size->widen(370))
									->save($image_file_details, array('quality' => 80));
							}

							$image_files['details'] = array(
								'path' => $image_save_name_details,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// list image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 540) {
								$resized_image = $image_obj->resize($original_image_size->widen(540))
									->save($image_file_list, array('quality' => 80));	
							}
							else {
								$resized_image = $image_obj->resize($original_image_size->widen(270))
									->save($image_file_list, array('quality' => 80));
							}

							$image_files['list'] = array(
								'path' => $image_save_name_list,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// cart image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 180) {
								$resized_image = $image_obj->resize($original_image_size->widen(180))
									->save($image_file_cart, array('quality' => 80));	
							}
							else {
								$resized_image = $image_obj->resize($original_image_size->widen(90))
									->save($image_file_cart, array('quality' => 80));
							}

							$image_files['cart'] = array(
								'path' => $image_save_name_cart,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							if (is_file($image_file_original) && is_file($image_file_details) && is_file($image_file_list) && is_file($image_file_cart)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						}
						else {
							$image_file = $image_dir . $image_name;
							$image_save_name = $image_id . '/' . $image_name;

							if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_inset)
									->save($image_file, array('quality' => 80));
							}
							else {
								$resized_image = $image_obj->save($image_file, array('quality' => 80));	
							}

							$image_size = array(
								'w' => $resized_image->getSize()->getWidth(),
								'h' => $resized_image->getSize()->getHeight(),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode(array_merge(
									array(
										'file' => $image_save_name,
									),
									$image_size
								));
							}
							else {
								// remove resized files
								if (is_file($image_file)) {
									unlink($image_file);
								}
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_product = array(
				'saved' => $today,
			);

			$update_product = array_merge($update_product, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id" , 
					"params" => array(
						"product_id" => $model->product_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	private function deleteGalleryPhotos($del_photos)
	{
		if (empty($del_photos)) {
			return false;
		}
		
		$delete_counter = 0;
		
		// delete photos
		$builder = Yii::app()->db->schema->commandBuilder;

		foreach ($del_photos as $photo_id) {
			// get delete data
			$delete_data = Yii::app()->db
				->createCommand("SELECT * FROM product_photo WHERE photo_id = :pid")
				->bindValue(':pid', (int) $photo_id, PDO::PARAM_INT)
				->queryRow();

			if (!empty($delete_data)) {
				$delete_criteria = new CDbCriteria(
					array(
						"condition" => "photo_id = :photo_id" , 
						"params" => array(
							"photo_id" => (int) $photo_id,
						)
					)
				);

				try {
					$rs = $builder->createDeleteCommand('product_photo', $delete_criteria)->execute();

					if ($rs) {
						$delete_counter++;

						// delete file
						$file_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $delete_data['product_id'] . DS;
						
						$photo_path = json_decode($delete_data['photo_path'], true);

						$file_path = $file_dir . $photo_path['thumb']['1x'];

						if (is_file($file_path) && is_dir(dirname($file_path))) {
							CFileHelper::removeDirectory(dirname($file_path));
						}
					}
				}
				catch (CDbException $e) {
					// ...
				}
			}
		}

		if ($delete_counter) {
			return $delete_counter;
		}
		else {
			return false;	
		}
	}

	private function saveGalleryPhotosByPosition($product_id, $photos_position)
	{
		if (empty($photos_position)) {
			return false;
		}

		$update_counter = 0;
		
		// update photos positions
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		foreach ($photos_position as $photo_id => $position) {
			$update = array(
				'saved' => $today,
				'product_id' => (int) $product_id,
			);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "photo_id = :photo_id" , 
					"params" => array(
						"photo_id" => $photo_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('product_photo', $update, $update_criteria)->execute();

				if ($rs) {
					$dest_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $product_id;

					if (!is_dir($dest_dir)) {
						CFileHelper::createDirectory($dest_dir, 0777, true);
					}
					
					// get photo
					$photo = Yii::app()->db
						->createCommand("SELECT photo_path FROM product_photo WHERE photo_id = :photo_id")
						->bindValue(':photo_id', (int) $photo_id, PDO::PARAM_INT)
						->queryRow();

					$photo_path = json_decode($photo['photo_path'], true);
					$path_parts = explode('/', $photo_path['large']['1x']);

					// move folder
					$src_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . '0' . DS . $path_parts[0];
					$dest_dir .= DS . $path_parts[0];
					
					CFileHelper::copyDirectory($src_dir, $dest_dir);

					if (is_dir($dest_dir)) {
						CFileHelper::removeDirectory($src_dir);
					}
					
					$update_counter++;
				}
			}
			catch (CDbException $e) {
				return false;
			}
		}

		if (count($photos_position) == $update_counter) {
			return true;
		}
		else {
			return false;	
		}
	}

	private function saveGalleryPhotosPosition($photos_position)
	{
		if (empty($photos_position)) {
			return false;
		}

		$update_counter = 0;
		
		// update photos positions
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		foreach ($photos_position as $photo_id => $position) {
			$update = array(
				'saved' => $today,
				'position' => $position + 1,
			);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "photo_id = :photo_id" , 
					"params" => array(
						"photo_id" => $photo_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('product_photo', $update, $update_criteria)->execute();

				if ($rs) {
					$update_counter++;	
				}
			}
			catch (CDbException $e) {
				return false;
			}
		}

		if (count($photos_position) == $update_counter) {
			return true;
		}
		else {
			return false;	
		}
	}

	public function addGalleryPhoto($model)
	{
		if (empty($model->photo)) {
			return false;
		}

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_image_rs = false;
		$product_id = !empty($model->product_id) ? $model->product_id : 0;
		
		$image_id = uniqid();
		$image_parts = explode('.', $model->photo->getName());
		$image_extension = strtolower(array_pop($image_parts));
		$image_name = implode('.', $image_parts);
		$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
		$image_full_name = $image_name . '.' . $image_extension;
		
		$image_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $product_id . DS . $image_id . DS;
		$image_path = $image_dir . $image_full_name;

		$dir_rs = true;

		if (!is_dir($image_dir)) {
			$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
		}				

		if ($dir_rs) {
			$save_image_rs = $model->photo->saveAs($image_path);

			if ($save_image_rs) {
				$saved = false;

				$image_file_original = $image_dir . $image_name . '_o.' . $image_extension;
				$image_file_large = $image_dir . $image_name . '_l.' . $image_extension;
				$image_file_large_2x = $image_dir . $image_name . '_l@2x.' . $image_extension;
				$image_file_catalog = $image_dir . $image_name . '_c.' . $image_extension;
				$image_file_catalog_2x = $image_dir . $image_name . '_c@2x.' . $image_extension;
				$image_file_catalog_alt = $image_dir . $image_name . '_c_alt.' . $image_extension;
				$image_file_catalog_alt_2x = $image_dir . $image_name . '_c_alt@2x.' . $image_extension;
				$image_file_thumb = $image_dir . $image_name . '_t.' . $image_extension;
				$image_file_thumb_2x = $image_dir . $image_name . '_t@2x.' . $image_extension;

				// 480*xxx (960*xxx retina) - large
				// 360*xxx (720*xxx retina) - catalog #1
				// 120*xxx (240*xxx retina) - catalog #2
				// 60*xxx (120*xxx retina) - thumb
				
				// resize file
				if (extension_loaded('imagick')) {
					$imagine = new Imagine\Imagick\Imagine();
				}
				elseif (extension_loaded('gd') && function_exists('gd_info')) {
					$imagine = new Imagine\Gd\Imagine();
				}

				// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
				$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
				// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
				$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

				$image_obj = $imagine->open($image_path);
				$original_image_size = $image_obj->getSize();

				if ($original_image_size->getWidth() < 360) { //  || $original_image_size->getHeight() < 600
					return false;
				}

				// original
				copy($image_path, $image_file_original);

				// large
				/* $resized_image_large = $imagine->open($image_path)
					->thumbnail(new Imagine\Image\Box(550, 648), $mode_outbound)
					->save($image_file_large, array('quality' => 80))
					->getSize(); */
				$resized_image_large = $imagine->open($image_path)
					->resize($original_image_size->widen(480))
					->save($image_file_large, array('quality' => 80))
					->getSize();

				if ($original_image_size->getWidth() >= 960) {
					/* $resized_image_large_2x = $imagine->open($image_path)
						->thumbnail(new Imagine\Image\Box(1100, 1296), $mode_outbound)
						->save($image_file_large_2x, array('quality' => 80))
						->getSize(); */
					$resized_image_large_2x = $imagine->open($image_path)
						->resize($original_image_size->widen(960))
						->save($image_file_large_2x, array('quality' => 80))
						->getSize();
				} else {
					$resized_image_large_2x = null;
				}

				// catalog
				$resized_image_catalog_2x = $imagine->open($image_path)
					->resize($original_image_size->widen(720))
					->save($image_file_catalog_2x, array('quality' => 80))
					->getSize();
				$resized_image_catalog = $imagine->open($image_path)
					->resize($original_image_size->widen(360))
					->save($image_file_catalog, array('quality' => 80))
					->getSize();

				// catalog alt
				$resized_image_catalog_alt_2x = $imagine->open($image_path)
					->resize($original_image_size->widen(240))
					->save($image_file_catalog_alt_2x, array('quality' => 80))
					->getSize();
				$resized_image_catalog_alt = $imagine->open($image_path)
					->resize($original_image_size->widen(120))
					->save($image_file_catalog_alt, array('quality' => 80))
					->getSize();

				// thumb
				$resized_image_thumb = $imagine->open($image_path)
					->resize($original_image_size->widen(60))
					->save($image_file_thumb, array('quality' => 80))
					->getSize();

				$resized_image_thumb_2x = $imagine->open($image_path)
					->resize($original_image_size->widen(120))
					->save($image_file_thumb_2x, array('quality' => 80))
					->getSize();

				$save_path = json_encode(array(
					'original' => array(
						'1x' => $image_id . '/' . $image_name . '_o.' . $image_extension,
					),
					'large' => array(
						'1x' => $image_id . '/' . $image_name . '_l.' . $image_extension,
						'2x' => ($resized_image_large_2x === null) 
							? null 
							: $image_id . '/' . $image_name . '_l@2x.' . $image_extension,
					),
					'catalog' => array(
						'1x' => $image_id . '/' . $image_name . '_c.' . $image_extension,
						'2x' => ($resized_image_catalog_2x === null) 
							? null 
							: $image_id . '/' . $image_name . '_c@2x.' . $image_extension,
					),
					'catalog_alt' => array(
						'1x' => $image_id . '/' . $image_name . '_c_alt.' . $image_extension,
						'2x' => ($resized_image_catalog_alt_2x === null) 
							? null 
							: $image_id . '/' . $image_name . '_c_alt@2x.' . $image_extension,
					),
					'thumb' => array(
						'1x' => $image_id . '/' . $image_name . '_t.' . $image_extension,
						'2x' => ($resized_image_thumb_2x === null) 
							? null 
							: $image_id . '/' . $image_name . '_t@2x.' . $image_extension,
					),
				));

				$save_size = json_encode(array(
					'large' => array(
						'1x' => array(
							'w' => $resized_image_large->getWidth(),
							'h' => $resized_image_large->getHeight(),
						),
						'2x' => ($resized_image_large_2x === null) ? null : array(
							'w' => $resized_image_large_2x->getWidth(),
							'h' => $resized_image_large_2x->getHeight(),
						),
					),
					'catalog' => array(
						'1x' => array(
							'w' => $resized_image_catalog->getWidth(),
							'h' => $resized_image_catalog->getHeight(),
						),
						'2x' => ($resized_image_catalog_2x === null) ? null : array(
							'w' => $resized_image_catalog_2x->getWidth(),
							'h' => $resized_image_catalog_2x->getHeight(),
						),
					),
					'catalog_alt' => array(
						'1x' => array(
							'w' => $resized_image_catalog_alt->getWidth(),
							'h' => $resized_image_catalog_alt->getHeight(),
						),
						'2x' => ($resized_image_catalog_alt_2x === null) ? null : array(
							'w' => $resized_image_catalog_alt_2x->getWidth(),
							'h' => $resized_image_catalog_alt_2x->getHeight(),
						),
					),
					'thumb' => array(
						'1x' => array(
							'w' => $resized_image_thumb->getWidth(),
							'h' => $resized_image_thumb->getHeight(),
						),
						'2x' => ($resized_image_thumb_2x === null) ? null : array(
							'w' => $resized_image_thumb_2x->getWidth(),
							'h' => $resized_image_thumb_2x->getHeight(),
						),
					),
				));

				if (is_file($image_file_large)) {
					// get max position
					$max_position = Yii::app()->db
						->createCommand("SELECT MAX(position) FROM product_photo WHERE product_id = :product_id")
						->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
						->queryScalar();

					$max_position += 1;

					// insert photo
					$insert_photo = array(
						'photo_id' => null,
						'created' => $today,
						'saved' => $today,
						'product_id' => $product_id,
						'photo_path' => $save_path,
						'photo_size' => $save_size,
						'position' => $max_position,
					);

					try {
						$save_image_rs = (bool) $builder->createInsertCommand('product_photo', $insert_photo)->execute();
						$file_id = (int) Yii::app()->db->getLastInsertID();
						$saved = true;

						return [
							'file_id' => $file_id,
							'file_position' => $max_position,
							'file_url' => Yii::app()->assetManager->baseUrl . '/product/' . $product_id . '/' . $image_id . '/' . $image_name . '_t.' . $image_extension,
							'file_width' => $resized_image_thumb->getWidth(),
							'file_height' => $resized_image_thumb->getHeight(),
						];
					} catch (CDbException $e) {
						
					}
				}
				
				if (!$saved) {
					// remove resized files
					if (is_file($image_file_large)) {
						unlink($image_file_large);
					}
					
					if (is_file($image_file_large_2x)) {
						unlink($image_file_large_2x);
					}

					if (is_file($image_file_catalog)) {
						unlink($image_file_catalog);
					}
					
					if (is_file($image_file_catalog_2x)) {
						unlink($image_file_catalog_2x);
					}
					
					if (is_file($image_file_catalog_alt)) {
						unlink($image_file_catalog_alt);
					}
					
					if (is_file($image_file_catalog_alt_2x)) {
						unlink($image_file_catalog_alt_2x);
					}

					if (is_file($image_file_thumb)) {
						unlink($image_file_thumb);
					}
					
					if (is_file($image_file_thumb_2x)) {
						unlink($image_file_thumb_2x);
					}
				}

				// remove original image
				unlink($image_path);
			}
		}

		return false;
	}

	public function updateImages()
	{
		ini_set('memory_limit', '512M');
		set_time_limit(3 * 60 * 60);

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));
		
		/* $total_products = Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM product_photo WHERE title != '1' AND created < '2020-05-10 00:00:00'") // WHERE product_id = 21228
			->queryScalar();

		$products_offset = 0;

		while ($products_offset < $total_products) {
			$products = Yii::app()->db
				->createCommand("SELECT * 
								 FROM product_photo 
								 WHERE title != '1' AND created < '2020-05-10 00:00:00'
								 ORDER BY photo_id
								 LIMIT {$products_offset},500")
				->queryAll();

			foreach ($products as $product) {
				$this->updatePhoto($product);
			}
			
			$products_offset += 500;
		} */

		$total_products = Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM product WHERE product_photo != ''")
			->queryScalar();

		$products_offset = 0;

		while ($products_offset < $total_products) {
			$products = Yii::app()->db
				->createCommand("SELECT product_id 
								 FROM product 
								 WHERE product_photo != ''
								 ORDER BY product_id
								 LIMIT {$products_offset},500")
				->queryColumn();

			foreach ($products as $product_id) {
				$this->updateThumbnail($product_id);
			}
			
			$products_offset += 500;
		}
	}

	private function updatePhoto($data)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_image_rs = false;
		$product_id = $data['product_id'];

		$photo_data = json_decode($data['photo_path'], true);
		$original_photo = $photo_data['original']['1x'];

		list($image_id, $image_original_name) = explode('/', $original_photo);
		$image_original_name = str_replace('_o.', '.', $image_original_name);
		
		$image_parts = explode('.', $image_original_name);
		$image_extension = strtolower(array_pop($image_parts));
		$image_name = implode('.', $image_parts);
		$image_full_name = $image_name . '.' . $image_extension;
		
		$image_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $product_id . DS . $image_id . DS;
		$image_path = Yii::app()->assetManager->basePath . DS . 'product' . DS . $product_id . DS . $original_photo;

		$dir_rs = true;			

		if ($dir_rs) {
			$saved = false;

			$image_file_original = $image_dir . $image_name . '_o.' . $image_extension;
			$image_file_large = $image_dir . $image_name . '_l.' . $image_extension;
			$image_file_large_u = $image_dir . $image_name . 'u_l.' . $image_extension;
			$image_file_large_2x = $image_dir . $image_name . '_l@2x.' . $image_extension;
			$image_file_large_2x_u = $image_dir . $image_name . 'u_l@2x.' . $image_extension;
			$image_file_catalog = $image_dir . $image_name . '_c.' . $image_extension;
			$image_file_catalog_u = $image_dir . $image_name . 'u_c.' . $image_extension;
			$image_file_catalog_2x = $image_dir . $image_name . '_c@2x.' . $image_extension;
			$image_file_catalog_2x_u = $image_dir . $image_name . 'u_c@2x.' . $image_extension;
			$image_file_catalog_alt = $image_dir . $image_name . '_c_alt.' . $image_extension;
			$image_file_catalog_alt_u = $image_dir . $image_name . 'u_c_alt.' . $image_extension;
			$image_file_catalog_alt_2x = $image_dir . $image_name . '_c_alt@2x.' . $image_extension;
			$image_file_catalog_alt_2x_u = $image_dir . $image_name . 'u_c_alt@2x.' . $image_extension;
			$image_file_thumb = $image_dir . $image_name . '_t.' . $image_extension;
			$image_file_thumb_u = $image_dir . $image_name . 'u_t.' . $image_extension;
			$image_file_thumb_2x = $image_dir . $image_name . '_t@2x.' . $image_extension;
			$image_file_thumb_2x_u = $image_dir . $image_name . 'u_t@2x.' . $image_extension;

			// 480*xxx (960*xxx retina) - large
			// 360*xxx (720*xxx retina) - catalog #1
			// 120*xxx (240*xxx retina) - catalog #2
			// 60*xxx (120*xxx retina) - thumb
			
			// resize file
			if (extension_loaded('imagick')) {
				$imagine = new Imagine\Imagick\Imagine();
			}
			elseif (extension_loaded('gd') && function_exists('gd_info')) {
				$imagine = new Imagine\Gd\Imagine();
			}

			// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
			$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
			// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
			$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			$image_obj = $imagine->open($image_path);
			$original_image_size = $image_obj->getSize();

			if ($original_image_size->getWidth() < 360) { //  || $original_image_size->getHeight() < 600
				return false;
			}

			// large
			@unlink($image_file_large);
			$resized_image_large = $imagine->open($image_path)
				->resize($original_image_size->widen(480))
				->save($image_file_large_u, array('quality' => 80))
				->getSize();

			if ($original_image_size->getWidth() >= 960) {
				@unlink($image_file_large_2x);
				$resized_image_large_2x = $imagine->open($image_path)
					->resize($original_image_size->widen(960))
					->save($image_file_large_2x_u, array('quality' => 80))
					->getSize();
			} else {
				$resized_image_large_2x = null;
			}

			// catalog
			@unlink($image_file_catalog_2x);
			$resized_image_catalog_2x = $imagine->open($image_path)
				->resize($original_image_size->widen(720))
				->save($image_file_catalog_2x_u, array('quality' => 80))
				->getSize();
			@unlink($image_file_catalog);
			$resized_image_catalog = $imagine->open($image_path)
				->resize($original_image_size->widen(360))
				->save($image_file_catalog_u, array('quality' => 80))
				->getSize();

			// catalog alt
			@unlink($image_file_catalog_alt_2x);
			$resized_image_catalog_alt_2x = $imagine->open($image_path)
				->resize($original_image_size->widen(240))
				->save($image_file_catalog_alt_2x_u, array('quality' => 80))
				->getSize();
			@unlink($image_file_catalog_alt);
			$resized_image_catalog_alt = $imagine->open($image_path)
				->resize($original_image_size->widen(120))
				->save($image_file_catalog_alt_u, array('quality' => 80))
				->getSize();

			// thumb
			@unlink($image_file_thumb);
			$resized_image_thumb = $imagine->open($image_path)
				->resize($original_image_size->widen(60))
				->save($image_file_thumb_u, array('quality' => 80))
				->getSize();

			@unlink($image_file_thumb_2x);
			$resized_image_thumb_2x = $imagine->open($image_path)
				->resize($original_image_size->widen(120))
				->save($image_file_thumb_2x_u, array('quality' => 80))
				->getSize();

			$save_path = json_encode(array(
				'original' => array(
					'1x' => $image_id . '/' . $image_name . '_o.' . $image_extension,
				),
				'large' => array(
					'1x' => $image_id . '/' . $image_name . 'u_l.' . $image_extension,
					'2x' => ($resized_image_large_2x === null) 
						? null 
						: $image_id . '/' . $image_name . 'u_l@2x.' . $image_extension,
				),
				'catalog' => array(
					'1x' => $image_id . '/' . $image_name . 'u_c.' . $image_extension,
					'2x' => ($resized_image_catalog_2x === null) 
						? null 
						: $image_id . '/' . $image_name . 'u_c@2x.' . $image_extension,
				),
				'catalog_alt' => array(
					'1x' => $image_id . '/' . $image_name . 'u_c_alt.' . $image_extension,
					'2x' => ($resized_image_catalog_alt_2x === null) 
						? null 
						: $image_id . '/' . $image_name . 'u_c_alt@2x.' . $image_extension,
				),
				'thumb' => array(
					'1x' => $image_id . '/' . $image_name . 'u_t.' . $image_extension,
					'2x' => ($resized_image_thumb_2x === null) 
						? null 
						: $image_id . '/' . $image_name . 'u_t@2x.' . $image_extension,
				),
			));

			$save_size = json_encode(array(
				'large' => array(
					'1x' => array(
						'w' => $resized_image_large->getWidth(),
						'h' => $resized_image_large->getHeight(),
					),
					'2x' => ($resized_image_large_2x === null) ? null : array(
						'w' => $resized_image_large_2x->getWidth(),
						'h' => $resized_image_large_2x->getHeight(),
					),
				),
				'catalog' => array(
					'1x' => array(
						'w' => $resized_image_catalog->getWidth(),
						'h' => $resized_image_catalog->getHeight(),
					),
					'2x' => ($resized_image_catalog_2x === null) ? null : array(
						'w' => $resized_image_catalog_2x->getWidth(),
						'h' => $resized_image_catalog_2x->getHeight(),
					),
				),
				'catalog_alt' => array(
					'1x' => array(
						'w' => $resized_image_catalog_alt->getWidth(),
						'h' => $resized_image_catalog_alt->getHeight(),
					),
					'2x' => ($resized_image_catalog_alt_2x === null) ? null : array(
						'w' => $resized_image_catalog_alt_2x->getWidth(),
						'h' => $resized_image_catalog_alt_2x->getHeight(),
					),
				),
				'thumb' => array(
					'1x' => array(
						'w' => $resized_image_thumb->getWidth(),
						'h' => $resized_image_thumb->getHeight(),
					),
					'2x' => ($resized_image_thumb_2x === null) ? null : array(
						'w' => $resized_image_thumb_2x->getWidth(),
						'h' => $resized_image_thumb_2x->getHeight(),
					),
				),
			));

			$update_photo = array(
				'saved' => $today,
				'product_id' => $product_id,
				'photo_path' => $save_path,
				'photo_size' => $save_size,
				'title' => '1',
			);
			
			$update_criteria = new CDbCriteria(
				array(
					"condition" => "photo_id = :photo_id" , 
					"params" => array(
						"photo_id" => $data['photo_id'],
					)
				)
			);

			try {
				$builder->createUpdateCommand('product_photo', $update_photo, $update_criteria)->execute();
			} catch (CDbException $e) {
			}
		}
	}

	private function saveGalleryPhotos($product_id, $photos)
	{
		if (empty($product_id) || empty($photos)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$photos_counter = 0;

		foreach ($photos as $photo) {
			$model = new AddFileForm('photo');
			$model->photo = $photo;

			if ($model->validate()) {
				$save_image_rs = false;
				
				$image_id = uniqid();
				$image_parts = explode('.', $model->photo->getName());
				$image_extension = strtolower(array_pop($image_parts));
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'product' . DS . $product_id . DS . $image_id . DS;
				$image_path = $image_dir . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}				

				if ($dir_rs) {
					$save_image_rs = $model->photo->saveAs($image_path);

					if ($save_image_rs) {
						$saved = false;

						$image_file_original = $image_dir . $image_name . '_o.' . $image_extension;
						$image_file_large = $image_dir . $image_name . '_l.' . $image_extension;
						$image_file_large_2x = $image_dir . $image_name . '_l@2x.' . $image_extension;
						$image_file_catalog = $image_dir . $image_name . '_c.' . $image_extension;
						$image_file_catalog_2x = $image_dir . $image_name . '_c@2x.' . $image_extension;
						$image_file_catalog_alt = $image_dir . $image_name . '_c_alt.' . $image_extension;
						$image_file_catalog_alt_2x = $image_dir . $image_name . '_c_alt@2x.' . $image_extension;
						$image_file_thumb = $image_dir . $image_name . '_t.' . $image_extension;
						$image_file_thumb_2x = $image_dir . $image_name . '_t@2x.' . $image_extension;

						// 470*506 (940*1012 retina) - large
						// 230x230 (460*460 retina) - catalog #1
						// 270x404 (540*808 retina) - catalog #2
						// 100x100 (200*200 retina) - thumb
						
						// resize file
						if (extension_loaded('imagick')) {
							$imagine = new Imagine\Imagick\Imagine();
						}
						elseif (extension_loaded('gd') && function_exists('gd_info')) {
							$imagine = new Imagine\Gd\Imagine();
						}

						// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
						$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
						// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
						$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

						$image_obj = $imagine->open($image_path);
						$original_image_size = $image_obj->getSize();

						if ($original_image_size->getWidth() < 550 || $original_image_size->getHeight() < 648) {
							continue;
						}

						// original
						copy($image_path, $image_file_original);

						// large
						$resized_image_large = $image_obj->thumbnail(new Imagine\Image\Box(550, 648), $mode_inset)
							->save($image_file_large, array('quality' => 80))
							->getSize();

						if ($original_image_size->getWidth() >= 1100 && $original_image_size->getHeight() >= 1296) {
							$resized_image_large_2x = $image_obj->thumbnail(new Imagine\Image\Box(1100, 1296), $mode_inset)
								->save($image_file_large_2x, array('quality' => 80))
								->getSize();
						} else {
							$resized_image_large_2x = null;
						}

						// catalog
						$resized_image_catalog_2x = $image_obj->thumbnail(new Imagine\Image\Box(728, 856), $mode_inset)
							->save($image_file_catalog_2x, array('quality' => 80))
							->getSize();
						$resized_image_catalog = $image_obj->thumbnail(new Imagine\Image\Box(364, 428), $mode_inset)
							->save($image_file_catalog, array('quality' => 80))
							->getSize();

						// catalog alt
						$resized_image_catalog_alt = $image_obj->thumbnail(new Imagine\Image\Box(154, 180), $mode_inset)
							->save($image_file_catalog_alt, array('quality' => 80))
							->getSize();

						if ($original_image_size->getWidth() >= 308 && $original_image_size->getHeight() >= 360) {
							$resized_image_catalog_alt_2x = $image_obj->thumbnail(new Imagine\Image\Box(308, 360), $mode_inset)
								->save($image_file_catalog_alt_2x, array('quality' => 80))
								->getSize();
						} else {
							$resized_image_catalog_alt_2x = null;
						}

						// thumb
						$resized_image_thumb = $image_obj->thumbnail(new Imagine\Image\Box(100, 100), $mode_inset)
							->save($image_file_thumb, array('quality' => 80))
							->getSize();

						$resized_image_thumb_2x = $image_obj->thumbnail(new Imagine\Image\Box(200, 200), $mode_inset)
							->save($image_file_thumb_2x, array('quality' => 80))
							->getSize();

						$save_path = json_encode(array(
							'original' => array(
								'1x' => $image_id . '/' . $image_name . '_o.' . $image_extension,
							),
							'large' => array(
								'1x' => $image_id . '/' . $image_name . '_l.' . $image_extension,
								'2x' => ($resized_image_large_2x === null) 
									? null 
									: $image_id . '/' . $image_name . '_l@2x.' . $image_extension,
							),
							'catalog' => array(
								'1x' => $image_id . '/' . $image_name . '_c.' . $image_extension,
								'2x' => ($resized_image_catalog_2x === null) 
									? null 
									: $image_id . '/' . $image_name . '_c@2x.' . $image_extension,
							),
							'catalog_alt' => array(
								'1x' => $image_id . '/' . $image_name . '_c_alt.' . $image_extension,
								'2x' => ($resized_image_catalog_alt_2x === null) 
									? null 
									: $image_id . '/' . $image_name . '_c_alt@2x.' . $image_extension,
							),
							'thumb' => array(
								'1x' => $image_id . '/' . $image_name . '_t.' . $image_extension,
								'2x' => ($resized_image_thumb_2x === null) 
									? null 
									: $image_id . '/' . $image_name . '_t@2x.' . $image_extension,
							),
						));

						$save_size = json_encode(array(
							'large' => array(
								'1x' => array(
									'w' => $resized_image_large->getWidth(),
									'h' => $resized_image_large->getHeight(),
								),
								'2x' => ($resized_image_large_2x === null) ? null : array(
									'w' => $resized_image_large_2x->getWidth(),
									'h' => $resized_image_large_2x->getHeight(),
								),
							),
							'catalog' => array(
								'1x' => array(
									'w' => $resized_image_catalog->getWidth(),
									'h' => $resized_image_catalog->getHeight(),
								),
								'2x' => ($resized_image_catalog_2x === null) ? null : array(
									'w' => $resized_image_catalog_2x->getWidth(),
									'h' => $resized_image_catalog_2x->getHeight(),
								),
							),
							'catalog_alt' => array(
								'1x' => array(
									'w' => $resized_image_catalog_alt->getWidth(),
									'h' => $resized_image_catalog_alt->getHeight(),
								),
								'2x' => ($resized_image_catalog_alt_2x === null) ? null : array(
									'w' => $resized_image_catalog_alt_2x->getWidth(),
									'h' => $resized_image_catalog_alt_2x->getHeight(),
								),
							),
							'thumb' => array(
								'1x' => array(
									'w' => $resized_image_thumb->getWidth(),
									'h' => $resized_image_thumb->getHeight(),
								),
								'2x' => ($resized_image_thumb_2x === null) ? null : array(
									'w' => $resized_image_thumb_2x->getWidth(),
									'h' => $resized_image_thumb_2x->getHeight(),
								),
							),
						));

						if (is_file($image_file_large)) {
							// get max position
							$max_position = Yii::app()->db
								->createCommand("SELECT MAX(position) FROM product_photo")
								->queryScalar();

							$max_position += 1;

							// insert photo
							$insert_photo = array(
								'photo_id' => null,
								'created' => $today,
								'saved' => $today,
								'product_id' => $product_id,
								'photo_path' => $save_path,
								'photo_size' => $save_size,
								'position' => $max_position,
							);

							try {
								$save_image_rs = (bool) $builder->createInsertCommand('product_photo', $insert_photo)->execute();
								$saved = true;
							} catch (CDbException $e) {
								
							}
						}
						
						if (!$saved) {
							// remove resized files
							if (is_file($image_file_large)) {
								unlink($image_file_large);
							}
							
							if (is_file($image_file_large_2x)) {
								unlink($image_file_large_2x);
							}

							if (is_file($image_file_catalog)) {
								unlink($image_file_catalog);
							}
							
							if (is_file($image_file_catalog_2x)) {
								unlink($image_file_catalog_2x);
							}
							
							if (is_file($image_file_catalog_alt)) {
								unlink($image_file_catalog_alt);
							}
							
							if (is_file($image_file_catalog_alt_2x)) {
								unlink($image_file_catalog_alt_2x);
							}

							if (is_file($image_file_thumb)) {
								unlink($image_file_thumb);
							}
							
							if (is_file($image_file_thumb_2x)) {
								unlink($image_file_thumb_2x);
							}
						}

						// remove original image
						unlink($image_path);
					} else {
						continue;
					}
				}

				if ($save_image_rs) {
					$photos_counter++;
				}
			}
		}

		if ($photos_counter) {
			return $photos_counter;
		} else {
			return false;
		}
	}

	private function updateThumbnail($product_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;

		$product = $this->getProductByIdAdmin($product_id);
		
		// get first image of product
		$images = Yii::app()->db
			->createCommand("SELECT photo_path, photo_size FROM product_photo WHERE product_id = :id ORDER BY position LIMIT 2")
			->bindValue(':id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();

		$first_image = isset($images[0]) ? $images[0] : array();
		$second_image = isset($images[1]) ? $images[1] : array();

		if (!empty($first_image)) {
			$product_photo = array(
				'path' => json_decode($first_image['photo_path'], true),
				'size' => json_decode($first_image['photo_size'], true),
			);
			if (!empty($second_image)) {
			    $product_photo['hover']['path'] = json_decode($second_image['photo_path'], true);
                $product_photo['hover']['size'] = json_decode($second_image['photo_size'], true);
            }
            $product_photo = json_encode($product_photo);
		} else {
			$product_photo = '';
		}

		// update product thumbnail
		if ($product['product_published'] == '0000-00-00 00:00:00' && !empty($product_photo)) {
			$update = array(
				'active' => 1,
				'saved' => date('Y-m-d H:i:s'),
				'product_published' => date('Y-m-d H:i:s'),
				'product_photo' => $product_photo,
			);
		} else {
			$update = array(
				'saved' => date('Y-m-d H:i:s'),
				'product_photo' => $product_photo,
			);
		}

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => $product_id,
				)
			)
		);

		try {
			$rs = (bool) $builder->createUpdateCommand('product', $update, $update_criteria)->execute();
		}
		catch (CDbException $e) {
			return false;
		}

		return $rs;
	}

	public function makeKeywords()
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');
		$counter = 0;
		
		$products = Yii::app()->db
			->createCommand("SELECT * 
								FROM product as p 
								JOIN product_lang as pl 
								ON p.product_id = pl.product_id  
								LEFT JOIN brand_lang as bl 
								ON p.brand_id = bl.brand_id AND bl.language_code = pl.language_code  
								ORDER BY p.product_id")
			->queryAll();
		
		foreach ($products as $product) {
			$product_id = $product['product_id'];
			$product_keyword = trim($product['brand_name'] . ' ' . $product['product_title']);

			$update_product = array(
				'saved' => $today,
				'product_brand' => $product_keyword,
			);
	
			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id AND language_code = :code" , 
					"params" => array(
						"product_id" => (int) $product_id,
						"code" => $product['language_code'],
					)
				)
			);
	
			try {
				$rs = $builder->createUpdateCommand('product_lang', $update_product, $update_criteria)->execute();

				if ($rs) {
					$counter++;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return $counter;
	}

	public function setRating($product_id, $rating)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_product = array(
			'saved' => $today,
			'product_rating' => (int) $rating,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => (int) $product_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
	
	public function setPriceOld($product_id, $price)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_product = array(
			'saved' => $today,
			'product_price_old' => (float) $price,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => (int) $product_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();

			if ($rs) {
				$variants = $this->getProductVariants($product_id);

				if (!empty($variants)) {
					foreach ($variants as $variant) {
						$update_variant = array(
							'saved' => $today,
							'variant_price_old' => (float) $price,
						);

						$update_criteria = new CDbCriteria(
							array(
								"condition" => "variant_id = :variant_id" , 
								"params" => array(
									"variant_id" => (int) $variant['variant_id'],
								)
							)
						);

						$builder->createUpdateCommand('product_variant', $update_variant, $update_criteria)->execute();
					}
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function toggle($product_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_product = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => $product_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function copy($pid)
	{
		$product_id = 0;
		$product_langs = array();

		// get product
		$product = $this->getProductByIdAdmin($pid);

		if (empty($product)) {
			return false;
		}

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// remove attributes
		unset($product['product_id']);
		unset($product['collections']);
		unset($product['categories']);

		// collect product langs
		foreach (Yii::app()->params->langs as $code => $language) {
			if (isset($product[$code])) {
				$product_langs[$code] = $product[$code];
				unset($product[$code]);
			}
		}
		
		// set actual dates
		$product['created'] = $today;
		$product['saved'] = $today;

		// disable product
		$product['active'] = 0;
		
		// generate product alias
		$product['product_alias'] = preg_replace('#\-[0-9abcdef]{13}$#u', '', $product['product_alias']);
		$product['product_alias'] = $product['product_alias'] . '-' . uniqid();

		// add product
		try {
			$rs = $builder->createInsertCommand('product', $product)->execute();

			if ($rs) {
				$product_id = (int) Yii::app()->db->getLastInsertID();

				// add product langs
				foreach ($product_langs as $code => $product_lang) {
					$product_lang['product_id'] = $product_id;

					$builder->createInsertCommand('product_lang', $product_lang)->execute();
				}

				// get product properties
				$product_properties = Yii::app()->db
					->createCommand("SELECT property_id, value_id FROM property_product WHERE product_id = :id")
					->bindValue(':id', (int) $pid, PDO::PARAM_INT)
					->queryAll();

				// insert product properties
				foreach ($product_properties as $product_property) {
					$product_property['product_id'] = $product_id;

					$builder->createInsertCommand('property_product', $product_property)->execute();
				}

				// get product options
				$product_options = Yii::app()->db
					->createCommand("SELECT * FROM product_option WHERE product_id = :id ORDER BY option_id")
					->bindValue(':id', (int) $pid, PDO::PARAM_INT)
					->queryAll();

				// insert product options
				foreach ($product_options as $product_option) {
					unset($product_option['option_id']);
					$product_option['product_id'] = $product_id;
					$product_option['created'] = $today;
					$product_option['saved'] = $today;

					$builder->createInsertCommand('product_option', $product_option)->execute();
				}

				// copy files
				$current_product_path = Yii::app()->assetManager->basePath . DS . 'product' . DS . $pid;
				$new_product_path = Yii::app()->assetManager->basePath . DS . 'product' . DS . $product_id;

				if (is_dir($current_product_path)) {
					CFileHelper::copyDirectory($current_product_path, $new_product_path);
				}

				// get product photos
				$product_photos = Yii::app()->db
					->createCommand("SELECT * FROM product_photo WHERE product_id = :id ORDER BY photo_id")
					->bindValue(':id', (int) $pid, PDO::PARAM_INT)
					->queryAll();

				// insert product photos
				foreach ($product_photos as $product_photo) {
					unset($product_photo['photo_id']);
					$product_photo['product_id'] = $product_id;
					$product_photo['created'] = $today;
					$product_photo['saved'] = $today;

					$builder->createInsertCommand('product_photo', $product_photo)->execute();
				}

				// get product variants
				$product_variants = Yii::app()->db
					->createCommand("SELECT * FROM product_variant WHERE product_id = :id ORDER BY variant_id")
					->bindValue(':id', (int) $pid, PDO::PARAM_INT)
					->queryAll();

				// insert product variants
				foreach ($product_variants as $product_variant) {
					$vid = $product_variant['variant_id'];
					unset($product_variant['variant_id']);

					$product_variant['product_id'] = $product_id;
					$product_variant['created'] = $today;
					$product_variant['saved'] = $today;

					$rs = $builder->createInsertCommand('product_variant', $product_variant)->execute();

					if ($rs) {
						$variant_id = (int) Yii::app()->db->getLastInsertID();

						// get variant values
						$variant_values = Yii::app()->db
							->createCommand("SELECT * FROM product_variant_value WHERE variant_id = :vid")
							->bindValue(':vid', (int) $vid, PDO::PARAM_INT)
							->queryAll();

						// insert variant values
						foreach ($variant_values as $variant_value) {
							$variant_value['product_id'] = $product_id;
							$variant_value['variant_id'] = $variant_id;

							$builder->createInsertCommand('product_variant_value', $variant_value)->execute();
						}

						// get variant photos
						$variant_photos = Yii::app()->db
							->createCommand("SELECT * FROM product_variant_photo WHERE variant_id = :vid ORDER BY photo_id")
							->bindValue(':vid', (int) $vid, PDO::PARAM_INT)
							->queryAll();

						// insert variant photos
						foreach ($variant_photos as $variant_photo) {
							unset($variant_photo['photo_id']);
							$variant_photo['product_id'] = $product_id;
							$variant_photo['variant_id'] = $variant_id;
							$variant_photo['created'] = $today;
							$variant_photo['saved'] = $today;

							$variant_photo_path = $new_product_path . DS . 'variant' . DS . $vid;

							$rs = $builder->createInsertCommand('product_variant_photo', $variant_photo)->execute();

							if ($rs && is_dir($variant_photo_path)) {
								rename($variant_photo_path, $new_product_path . DS . 'variant' . DS . $variant_id);
							}
						}
					}
				}
			}
		} catch (CDbException $e) {
			if ($product_id) {
				$this->delete($product_id);
			}

			return false;
		}

		return true;
	}

	public function delete($product_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$product = $this->getProductByIdAdmin($product_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "product_id = :product_id" , 
				"params" => array(
					"product_id" => $product_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('product', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('product_lang', $delete_criteria)->execute();
				$builder->createDeleteCommand('collection_product', $delete_criteria)->execute();
				$builder->createDeleteCommand('category_product', $delete_criteria)->execute();
				$builder->createDeleteCommand('property_product', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_photo', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_variant', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_variant_photo', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_variant_store', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_variant_value', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_store', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_badge', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_color', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_tag', $delete_criteria)->execute();
				$builder->createDeleteCommand('product_option', $delete_criteria)->execute();

				// remove product directory
				if (is_dir($assetPath . DS . 'product' . DS . $product_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'product' . DS . $product_id);
				}

				(new Sphinx)->deleteProduct($product_id);

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}