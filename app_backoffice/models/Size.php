<?php
class Size extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getSizesAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$size_id = (int) $func_args[1];
			$size_name = addcslashes($func_args[1], '%_');

			$total_sizes = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM size as s JOIN size_lang as sl ON s.size_id = sl.size_id AND sl.language_code = :code WHERE s.size_id = :id OR sl.size_name LIKE :size_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $size_id, PDO::PARAM_INT)
				->bindValue(':size_name', '%' . $size_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_sizes = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM size as s JOIN size_lang as sl ON s.size_id = sl.size_id AND sl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_sizes,
			'pages' => ceil($total_sizes / $per_page),
		);
	}

	public function getSizesAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'size_id':
				$order_by = ($direction == 'asc') ? 's.size_id' : 's.size_id DESC';
				break;
			case 'size_position':
				$order_by = ($direction == 'asc') ? 's.size_position' : 's.size_position DESC';
				break;
			case 'size_name':
				$order_by = ($direction == 'asc') ? 'sl.size_name' : 'sl.size_name DESC';
				break;
			default:
				$order_by = 's.size_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$size_id = (int) $func_args[4];
			$size_name = addcslashes($func_args[4], '%_');

			$sizes = Yii::app()->db
				->createCommand("SELECT s.size_id, s.size_position, sl.size_name FROM size as s JOIN size_lang as sl ON s.size_id = sl.size_id AND sl.language_code = :code WHERE s.size_id = :id OR sl.size_name LIKE :size_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $size_id, PDO::PARAM_INT)
				->bindValue(':size_name', '%' . $size_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$sizes = Yii::app()->db
				->createCommand("SELECT s.size_id, s.size_position, sl.size_name FROM size as s JOIN size_lang as sl ON s.size_id = sl.size_id AND sl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $sizes;
	}

	public function getSizesListAdmin()
	{
		$sizes = Yii::app()->db
			->createCommand("SELECT s.size_id, sl.size_name FROM size as s JOIN size_lang as sl ON s.size_id = sl.size_id AND sl.language_code = :code ORDER BY sl.size_name")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		return $sizes;
	}

	public function getSizeByIdAdmin($id)
	{
		$size = Yii::app()->db
			->createCommand("SELECT * FROM size WHERE size_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($size)) {
			// size langs
			$size_langs = Yii::app()->db
				->createCommand("SELECT * FROM size_lang WHERE size_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($size_langs)) {
				foreach ($size_langs as $size_lang) {
					$code = $size_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$size[$code] = $size_lang;
					}
				}
			}
		}
			
		return $size;
	}

	public function save($model, $model_lang)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'size_id',
		);

		// integer attributes
		$int_attributes = array(
			'size_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array();

		// photos attributes
		$save_images = array();

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get max page position
		if (empty($model->size_position)) {
			$max_position = Yii::app()->db
				->createCommand("SELECT MAX(size_position) FROM size")
				->queryScalar();

			$model->size_position = $max_position + 1;
		}

		if (empty($model->size_id)) {
			// insert size
			$insert_size = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_size[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_size[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_size[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_size[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('size', $insert_size)->execute();

				if ($rs) {
					$model->size_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_size_lang = array(
							'size_id' => $model->size_id,
							'language_code' => $language_code,
							'size_visible' => !empty($model_lang->size_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_size_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_size_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('size_lang', $insert_size_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "size_id = :size_id" , 
									"params" => array(
										"size_id" => $model->size_id,
									)
								)
							);
							
							$builder->createDeleteCommand('size', $delete_criteria)->execute();

							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_size = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_size[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_size[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_size[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_size[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_size[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "size_id = :size_id" , 
					"params" => array(
						"size_id" => $model->size_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('size', $update_size, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'size' . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_size_lang = array(
							'size_visible' => !empty($model_lang->size_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_size_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_size_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "size_id = :size_id AND language_code = :lang" , 
								"params" => array(
									"size_id" => (int) $model->size_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('size_lang', $update_size_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function setPosition($size_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_size = array(
			'saved' => $today,
			'size_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "size_id = :size_id" , 
				"params" => array(
					"size_id" => $size_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('size', $update_size, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($size_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$size = $this->getSizeByIdAdmin($size_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "size_id = :size_id" , 
				"params" => array(
					"size_id" => $size_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('size', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('size_lang', $delete_criteria)->execute();

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}