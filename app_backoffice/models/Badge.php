<?php
class Badge extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getBadgesAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$badge_id = (int) $func_args[1];
			$badge_name = addcslashes($func_args[1], '%_');

			$total_badges = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM badge as b JOIN badge_lang as bl ON b.badge_id = bl.badge_id AND bl.language_code = :code WHERE b.badge_id = :id OR bl.badge_name LIKE :badge_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $badge_id, PDO::PARAM_INT)
				->bindValue(':badge_name', '%' . $badge_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_badges = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM badge as b JOIN badge_lang as bl ON b.badge_id = bl.badge_id AND bl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_badges,
			'pages' => ceil($total_badges / $per_page),
		);
	}

	public function getBadgesAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'badge_id':
				$order_by = ($direction == 'asc') ? 'b.badge_id' : 'b.badge_id DESC';
				break;
			case 'badge_position':
				$order_by = ($direction == 'asc') ? 'b.badge_position' : 'b.badge_position DESC';
				break;
			case 'badge_name':
				$order_by = ($direction == 'asc') ? 'bl.badge_name' : 'bl.badge_name DESC';
				break;
			default:
				$order_by = 'b.badge_position, b.badge_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$badge_id = (int) $func_args[4];
			$badge_name = addcslashes($func_args[4], '%_');

			$badges = Yii::app()->db
				->createCommand("SELECT b.*, bl.badge_name FROM badge as b JOIN badge_lang as bl ON b.badge_id = bl.badge_id AND bl.language_code = :code WHERE b.badge_id = :id OR bl.badge_name LIKE :badge_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $badge_id, PDO::PARAM_INT)
				->bindValue(':badge_name', '%' . $badge_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$badges = Yii::app()->db
				->createCommand("SELECT b.*, bl.badge_name FROM badge as b JOIN badge_lang as bl ON b.badge_id = bl.badge_id AND bl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $badges;
	}

	public function getBadgesListAdmin()
	{
		$badges = Yii::app()->db
			->createCommand("SELECT b.badge_id, bl.badge_name FROM badge as b JOIN badge_lang as bl ON b.badge_id = bl.badge_id AND bl.language_code = :code ORDER BY bl.badge_name")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		return $badges;
	}

	public function getBadgeByIdAdmin($id)
	{
		$badge = Yii::app()->db
			->createCommand("SELECT * FROM badge WHERE badge_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($badge)) {
			// badge langs
			$badge_langs = Yii::app()->db
				->createCommand("SELECT * FROM badge_lang WHERE badge_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($badge_langs)) {
				foreach ($badge_langs as $badge_lang) {
					$code = $badge_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$badge[$code] = $badge_lang;
					}
				}
			}
		}
			
		return $badge;
	}

	public function save($model, $model_lang)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'badge_id',
			'badge_logo',
		);

		// integer attributes
		$int_attributes = array(
			'badge_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_badge_logo',
		);

		// photos attributes
		$save_images = array(
			'badge_logo',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get max page position
		if (empty($model->badge_position)) {
			$max_position = Yii::app()->db
				->createCommand("SELECT MAX(badge_position) FROM badge")
				->queryScalar();

			$model->badge_position = $max_position + 1;
		}

		if (empty($model->badge_id)) {
			// insert badge
			$insert_badge = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_badge[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_badge[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_badge[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_badge[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('badge', $insert_badge)->execute();

				if ($rs) {
					$model->badge_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_badge_lang = array(
							'badge_id' => $model->badge_id,
							'language_code' => $language_code,
							'badge_visible' => !empty($model_lang->badge_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_badge_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_badge_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('badge_lang', $insert_badge_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "badge_id = :badge_id" , 
									"params" => array(
										"badge_id" => $model->badge_id,
									)
								)
							);
							
							$builder->createDeleteCommand('badge', $delete_criteria)->execute();

							return false;
						}
					}

					// save photos
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_badge = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_badge[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_badge[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_badge[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_badge[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_badge[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "badge_id = :badge_id" , 
					"params" => array(
						"badge_id" => $model->badge_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('badge', $update_badge, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'badge' . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_badge_lang = array(
							'badge_visible' => !empty($model_lang->badge_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_badge_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_badge_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "badge_id = :badge_id AND language_code = :lang" , 
								"params" => array(
									"badge_id" => (int) $model->badge_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('badge_lang', $update_badge_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					// save photos
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_name = strtolower(str_replace('-', '_', URLify::filter($model->$attribute->getName(), 60, 'ru', true)));
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'badge' . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;
						
						$image_file = $image_dir . $image_name;
						$image_save_name = $image_id . '/' . $image_name;

						// resize file
						$image_obj = $imagine->open($image_path);
						$original_image_size = $image_obj->getSize();

						switch ($attribute) {
							case 'badge_logo':
                                /* if ($model->badge_place == 'slider') {
                                    if ($original_image_size->getWidth() > 1110 && $original_image_size->getHeight() > 584) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(1110, 584), $mode_outbound)
                                        ->save($image_file, array('quality' => 80));
                                    } else {
                                        $resized_image = $image_obj->save($image_file, array('quality' => 80));
                                    }
                                } else {
									if ($model->badge_type == 'horizontal' && $original_image_size->getWidth() > 1110) {
										$resized_image = $image_obj->resize($original_image_size->widen(1110))
											->save($image_file, array('quality' => 80));
									} elseif ($model->badge_type == 'vertical' && $original_image_size->getWidth() > 535) {
										$resized_image = $image_obj->resize($original_image_size->widen(535))
											->save($image_file, array('quality' => 80));
									} else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									}
								} */
								if ($original_image_size->getWidth() > 48 && $original_image_size->getHeight() > 48) {
									$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(48, 48), $mode_outbound)
									->save($image_file, array('quality' => 80));
								} else {
									$resized_image = $image_obj->save($image_file, array('quality' => 80));
								}
								break;
								/*
								if ($original_image_size->getWidth() > $original_image_size->getHeight()) {
									if ($original_image_size->getWidth() > 360) {
										$resized_image = $image_obj->resize($original_image_size->widen(360))
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									}
								}
								else {
									if ($original_image_size->getHeight() > 200) {
										$resized_image = $image_obj->resize($original_image_size->heighten(200))
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									}
								}
								*/
						}

						$image_size = array(
							'w' => $resized_image->getSize()->getWidth(),
							'h' => $resized_image->getSize()->getHeight(),
						);

						if (is_file($image_file)) {
							$save_images_rs[$attribute] = json_encode(array_merge(
								array(
									'file' => $image_save_name,
								),
								$image_size
							));
						}
						else {
							// remove resized files
							if (is_file($image_file)) {
								unlink($image_file);
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_badge = array(
				'saved' => $today,
			);

			$update_badge = array_merge($update_badge, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "badge_id = :badge_id" , 
					"params" => array(
						"badge_id" => $model->badge_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('badge', $update_badge, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	public function setPosition($badge_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_badge = array(
			'saved' => $today,
			'badge_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "badge_id = :badge_id" , 
				"params" => array(
					"badge_id" => $badge_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('badge', $update_badge, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($badge_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$badge = $this->getBadgeByIdAdmin($badge_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "badge_id = :badge_id" , 
				"params" => array(
					"badge_id" => $badge_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('badge', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('badge_lang', $delete_criteria)->execute();

				// remove badge logo
				if (!empty($badge['badge_logo'])) {
					$photo = json_decode($badge['badge_logo'], true);
				
					if (is_file($assetPath . DS . 'badge' . DS . $photo['file'])) {
						CFileHelper::removeDirectory(dirname($assetPath . DS . 'badge' . DS . $photo['file']));
					}
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}