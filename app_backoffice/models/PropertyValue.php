<?php
class PropertyValue extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getValuesAdminTotal($property_id, $per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[2])) {
			$value_id = (int) $func_args[2];
			$value_title = addcslashes($func_args[2], '%_');

			$total_values = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM property_value as pv JOIN property_value_lang as pvl ON pv.value_id = pvl.value_id AND pvl.language_code = :code WHERE pv.property_id = :property_id AND pv.value_id = :id OR pvl.value_title LIKE :value_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':property_id', (int) $property_id, PDO::PARAM_INT)
				->bindValue(':id', $value_id, PDO::PARAM_INT)
				->bindValue(':value_title', '%' . $value_title . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_values = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM property_value as pv JOIN property_value_lang as pvl ON pv.value_id = pvl.value_id AND pvl.language_code = :code WHERE pv.property_id = :property_id")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':property_id', (int) $property_id, PDO::PARAM_INT)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_values,
			'pages' => ceil($total_values / $per_page),
		);
	}

	public function getValuesAdmin($property_id, $sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'value_id':
				$order_by = ($direction == 'asc') ? 'pv.value_id' : 'pv.value_id DESC';
				break;
			case 'value_title':
				$order_by = ($direction == 'asc') ? 'pvl.value_title' : 'pvl.value_title DESC';
				break;
			case 'value_position':
				$order_by = ($direction == 'asc') ? 'pv.value_position' : 'pv.value_position DESC';
				break;
			default:
				$order_by = 'pv.value_top DESC, pv.value_position, pvl.value_title';
		}

		$func_args = func_get_args();

		if (!empty($func_args[5])) {
			$value_id = (int) $func_args[5];
			$value_title = addcslashes($func_args[5], '%_');

			$values = Yii::app()->db
				->createCommand("SELECT pv.value_id, pv.value_top, pv.value_color, pv.value_multicolor, pv.value_position, pvl.value_title FROM property_value as pv JOIN property_value_lang as pvl ON pv.value_id = pvl.value_id AND pvl.language_code = :code WHERE pv.property_id = :property_id AND pv.value_id = :id OR pvl.value_title LIKE :value_title ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':property_id', (int) $property_id, PDO::PARAM_INT)
				->bindValue(':id', $value_id, PDO::PARAM_INT)
				->bindValue(':value_title', '%' . $value_title . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$values = Yii::app()->db
				->createCommand("SELECT pv.value_id, pv.value_top, pv.value_color, pv.value_multicolor, pv.value_position, pvl.value_title FROM property_value as pv JOIN property_value_lang as pvl ON pv.value_id = pvl.value_id AND pvl.language_code = :code WHERE pv.property_id = :property_id ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':property_id', (int) $property_id, PDO::PARAM_INT)
				->queryAll();
		}
			
		return $values;
	}

	public function getValueByIdAdmin($id)
	{
		$value = Yii::app()->db
			->createCommand("SELECT * FROM property_value WHERE value_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($value)) {
			// property_value langs
			$value_langs = Yii::app()->db
				->createCommand("SELECT * FROM property_value_lang WHERE value_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($value_langs)) {
				foreach ($value_langs as $value_lang) {
					$code = $value_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$value[$code] = $value_lang;
					}
				}
			}
		}
			
		return $value;
	}

	public function getValuesListAdmin($property_id)
	{
		if (is_array($property_id)) {
			$property_ids = array();

			foreach ($property_id as $value) {
				$value = (int) $value;

				if (!empty($value)) {
					$property_ids[] = $value;
				}
			}

			$values = Yii::app()->db
				->createCommand("SELECT pv.value_id, pv.property_id, pv.value_code, pvl.value_title FROM property_value as pv JOIN property_value_lang as pvl ON pv.value_id = pvl.value_id AND pvl.language_code = :code WHERE pv.property_id IN (" . implode(',', $property_ids) . ") ORDER BY pv.value_top DESC, pv.value_position, pvl.value_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		} else {
			$values = Yii::app()->db
				->createCommand("SELECT pv.value_id, pv.property_id, pv.value_code, pvl.value_title FROM property_value as pv JOIN property_value_lang as pvl ON pv.value_id = pvl.value_id AND pvl.language_code = :code WHERE pv.property_id = :property_id ORDER BY pv.value_top DESC, pv.value_position, pvl.value_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':property_id', (int) $property_id, PDO::PARAM_INT)
				->queryAll();
		}
			
		return $values;
	}

	public function getPropertyValuesAdmin($category_id, $values = array())
	{
		if (empty($values)) {
			return array();
		}

		$values = Yii::app()->db
			->createCommand("SELECT p.property_id, v.value_id 
							 FROM property_category as pc 
							 JOIN property as p 
							 ON pc.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 JOIN property_value as v 
							 ON v.property_id = p.property_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code 
							 WHERE pc.category_id = :category_id AND v.value_id IN (" . implode(',', $values) . ")")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();

		return $values;
	}

	public function add($property_id, $value_code)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$insert_value = [
			'created' => $today,
			'saved' => $today,
			'property_id' => (int) $property_id,
			'value_code' => $value_code,
		];
		
		try {
			$rs = $builder->createInsertCommand('property_value', $insert_value)->execute();

			if ($rs) {
				$value_id = (int) Yii::app()->db->getLastInsertID();
				
				foreach (Yii::app()->params->langs as $language_code => $language_name) {
					// save details
					$insert_property_lang = array(
						'value_id' => $value_id,
						'language_code' => $language_code,
						'value_visible' => 1,
						'created' => $today,
						'saved' => $today,
						'value_title' => $value_code,
					);

					$rs = $builder->createInsertCommand('property_value_lang', $insert_property_lang)->execute();

					if (!$rs) {
						$delete_criteria = new CDbCriteria(
							array(
								"condition" => "value_id = :value_id" , 
								"params" => array(
									"value_id" => $value_id,
								)
							)
						);
						
						$builder->createDeleteCommand('property_value', $delete_criteria)->execute();

						return false;
					}
                }

				return $value_id;
            }
		} catch (CDbException $e) {
			// ...
		}

		return false;
    }

	public function save($model, $model_lang)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'value_id',
		);

		// integer attributes
		$int_attributes = array();

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array();

		// photos attributes
		$save_images = array();

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		if (empty($model->value_id)) {
			// insert property_value
			$insert_value = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_value[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_value[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_value[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_value[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('property_value', $insert_value)->execute();

				if ($rs) {
					$model->value_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array(
						'value_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_value_lang = array(
							'value_id' => $model->value_id,
							'language_code' => $language_code,
							'value_visible' => !empty($model_lang->value_title[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_value_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_value_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('property_value_lang', $insert_value_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "value_id = :value_id" , 
									"params" => array(
										"value_id" => $model->value_id,
									)
								)
							);
							
							$builder->createDeleteCommand('property_value', $delete_criteria)->execute();

							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_value = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_value[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_value[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_value[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_value[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_value[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "value_id = :value_id" , 
					"params" => array(
						"value_id" => $model->value_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('property_value', $update_value, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'value' . DS . $model->value_id . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array(
						'value_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_value_lang = array(
							'value_visible' => !empty($model_lang->value_title[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'value_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_value_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_value_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "value_id = :value_id AND language_code = :lang" , 
								"params" => array(
									"value_id" => (int) $model->value_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('property_value_lang', $update_value_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function setPosition($value_id, $position)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        $update_value = array(
            'saved' => $today,
            'value_position' => (int) $position,
        );

        $update_criteria = new CDbCriteria(
            array(
                "condition" => "value_id = :value_id" ,
                "params" => array(
                    "value_id" => $value_id,
                )
            )
        );

        try {
            $rs = $builder->createUpdateCommand('property_value', $update_value, $update_criteria)->execute();

            if ($rs) {
                return true;
            }
        }
        catch (CDbException $e) {
            // ...
        }

        return false;
    }

	public function delete($value_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$value = $this->getValueByIdAdmin($value_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "value_id = :value_id" , 
				"params" => array(
					"value_id" => $value_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('property_value', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('property_value_lang', $delete_criteria)->execute();
				$builder->createDeleteCommand('property_product', $delete_criteria)->execute();

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}