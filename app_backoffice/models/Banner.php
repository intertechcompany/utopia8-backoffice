<?php
class Banner extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getBannersAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$banner_id = (int) $func_args[1];
			$banner_name = addcslashes($func_args[1], '%_');

			$total_banners = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM banner as b JOIN banner_lang as bl ON b.banner_id = bl.banner_id AND bl.language_code = :code WHERE b.banner_id = :id OR bl.banner_name LIKE :banner_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $banner_id, PDO::PARAM_INT)
				->bindValue(':banner_name', '%' . $banner_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_banners = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM banner as b JOIN banner_lang as bl ON b.banner_id = bl.banner_id AND bl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_banners,
			'pages' => ceil($total_banners / $per_page),
		);
	}

	public function getBannersAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'banner_id':
				$order_by = ($direction == 'asc') ? 'b.banner_id' : 'b.banner_id DESC';
				break;
			case 'banner_position':
				$order_by = ($direction == 'asc') ? 'b.banner_position' : 'b.banner_position DESC';
				break;
			case 'banner_name':
				$order_by = ($direction == 'asc') ? 'bl.banner_name' : 'bl.banner_name DESC';
				break;
			default:
				$order_by = 'b.banner_place, b.banner_position';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$banner_id = (int) $func_args[4];
			$banner_name = addcslashes($func_args[4], '%_');

			$banners = Yii::app()->db
				->createCommand("SELECT b.banner_id, b.active, b.banner_position, b.banner_place, bl.banner_name FROM banner as b JOIN banner_lang as bl ON b.banner_id = bl.banner_id AND bl.language_code = :code WHERE b.banner_id = :id OR bl.banner_name LIKE :banner_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $banner_id, PDO::PARAM_INT)
				->bindValue(':banner_name', '%' . $banner_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$banners = Yii::app()->db
				->createCommand("SELECT b.banner_id, b.active, b.banner_position, b.banner_place, bl.banner_name FROM banner as b JOIN banner_lang as bl ON b.banner_id = bl.banner_id AND bl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $banners;
	}

	public function getBannerByIdAdmin($id)
	{
		$banner = Yii::app()->db
			->createCommand("SELECT * FROM banner WHERE banner_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($banner)) {
			// banner langs
			$banner_langs = Yii::app()->db
				->createCommand("SELECT * FROM banner_lang WHERE banner_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($banner_langs)) {
				foreach ($banner_langs as $banner_lang) {
					$code = $banner_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$banner[$code] = $banner_lang;
					}
				}
			}

			if (!empty($banner['products'])) {
				$banner['products'] = Yii::app()->db
					->createCommand("SELECT p.product_id, p.product_alias, pl.product_title 
									 FROM product as p 
									 JOIN product_lang as pl 
									 ON p.product_id = pl.product_id AND pl.language_code = :code 
									 WHERE p.product_id IN ({$banner['products']})
									 ORDER BY pl.product_title")
					->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
					->queryAll();
			}
		}
			
		return $banner;
	}

	public function save($model, $model_lang)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'banner_id',
			'banner_logo',
		);

		// integer attributes
		$int_attributes = array(
			'banner_url_blank',
			'banner_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_banner_logo',
		);

		// photos attributes
		$save_images = array(
			'banner_logo',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get max page position
		if (empty($model->banner_position)) {
			$max_position = Yii::app()->db
				->createCommand("SELECT MAX(banner_position) FROM banner WHERE banner_place = :banner_place")
				->bindValue(':banner_place', $model->banner_place, PDO::PARAM_STR)
				->queryScalar();

			$model->banner_position = $max_position + 1;
		}

		if (empty($model->banner_id)) {
			// insert banner
			$insert_banner = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_banner[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_banner[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_banner[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_banner[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('banner', $insert_banner)->execute();

				if ($rs) {
					$model->banner_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_banner_lang = array(
							'banner_id' => $model->banner_id,
							'language_code' => $language_code,
							'banner_visible' => !empty($model_lang->banner_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_banner_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_banner_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('banner_lang', $insert_banner_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "banner_id = :banner_id" , 
									"params" => array(
										"banner_id" => $model->banner_id,
									)
								)
							);
							
							$builder->createDeleteCommand('banner', $delete_criteria)->execute();

							return false;
						}
					}

					// save photos
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_banner = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_banner[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_banner[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_banner[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_banner[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_banner[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "banner_id = :banner_id" , 
					"params" => array(
						"banner_id" => $model->banner_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('banner', $update_banner, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'banner' . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_banner_lang = array(
							'banner_visible' => !empty($model_lang->banner_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_banner_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_banner_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "banner_id = :banner_id AND language_code = :lang" , 
								"params" => array(
									"banner_id" => (int) $model->banner_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('banner_lang', $update_banner_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					// save photos
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_name = strtolower(str_replace('-', '_', URLify::filter($model->$attribute->getName(), 60, 'ru', true)));
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'banner' . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;
						
						$image_file = $image_dir . $image_name;
						$image_save_name = $image_id . '/' . $image_name;

						// resize file
						$image_obj = $imagine->open($image_path);
						$original_image_size = $image_obj->getSize();

						switch ($attribute) {
							case 'banner_logo':
                                if ($model->banner_place == 'slider') {
                                    if ($original_image_size->getWidth() >= 1360 && $original_image_size->getHeight() >= 840) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(1360, 840), $mode_outbound)
                                        ->save($image_file, array('quality' => 80));
                                    } elseif ($original_image_size->getWidth() >= 680 && $original_image_size->getHeight() >= 420) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(680, 420), $mode_outbound)
                                        ->save($image_file, array('quality' => 80));
                                    } else {
                                        $resized_image = $image_obj->save($image_file, array('quality' => 80));
                                    }
                                } elseif ($model->banner_place == 'category') {
									if ($original_image_size->getWidth() >= 280 && $original_image_size->getHeight() >= 280) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(280, 280), $mode_outbound)
                                        ->save($image_file, array('quality' => 80));
                                    } elseif ($original_image_size->getWidth() >= 140 && $original_image_size->getHeight() >= 140) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(140, 140), $mode_outbound)
                                        ->save($image_file, array('quality' => 80));
                                    } else {
                                        $resized_image = $image_obj->save($image_file, array('quality' => 80));
                                    }
                                } elseif ($model->banner_place == 'instagram') {
									if ($original_image_size->getWidth() >= 1000 && $original_image_size->getHeight() >= 1000) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(1000, 1000), $mode_outbound)
                                        ->save($image_file, array('quality' => 80));
                                    } elseif ($original_image_size->getWidth() >= 500 && $original_image_size->getHeight() >= 500) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(500, 500), $mode_outbound)
                                        ->save($image_file, array('quality' => 80));
                                    } else {
                                        $resized_image = $image_obj->save($image_file, array('quality' => 80));
                                    }
                                } else {
									if ($original_image_size->getWidth() >= 1920) { // && $original_image_size->getHeight() > 2196
										$resized_image = $image_obj->resize($original_image_size->widen(1920))
											->save($image_file, array('quality' => 80));
										// $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(1920, 2196), $mode_outbound)
                                        // 	->save($image_file, array('quality' => 80));
                                    } elseif ($original_image_size->getWidth() >= 960) { // && $original_image_size->getHeight() > 1098
										$resized_image = $image_obj->resize($original_image_size->widen(960))
											->save($image_file, array('quality' => 80));
										// $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(960, 1098), $mode_outbound)
                                        // 	->save($image_file, array('quality' => 80));
                                    } else {
                                        $resized_image = $image_obj->save($image_file, array('quality' => 80));
                                    }
									
									/* if ($model->banner_type == 'horizontal' && $original_image_size->getWidth() > 1110) {
										$resized_image = $image_obj->resize($original_image_size->widen(1110))
											->save($image_file, array('quality' => 80));
									} elseif ($model->banner_type == 'vertical' && $original_image_size->getWidth() > 535) {
										$resized_image = $image_obj->resize($original_image_size->widen(535))
											->save($image_file, array('quality' => 80));
									} else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									} */
								}
								break;
								/*
								if ($original_image_size->getWidth() > $original_image_size->getHeight()) {
									if ($original_image_size->getWidth() > 360) {
										$resized_image = $image_obj->resize($original_image_size->widen(360))
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									}
								}
								else {
									if ($original_image_size->getHeight() > 200) {
										$resized_image = $image_obj->resize($original_image_size->heighten(200))
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									}
								}
								*/
						}

						$image_size = array(
							'w' => $resized_image->getSize()->getWidth(),
							'h' => $resized_image->getSize()->getHeight(),
						);

						if (is_file($image_file)) {
							$save_images_rs[$attribute] = json_encode(array_merge(
								array(
									'file' => $image_save_name,
								),
								$image_size
							));
						}
						else {
							// remove resized files
							if (is_file($image_file)) {
								unlink($image_file);
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_banner = array(
				'saved' => $today,
			);

			$update_banner = array_merge($update_banner, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "banner_id = :banner_id" , 
					"params" => array(
						"banner_id" => $model->banner_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('banner', $update_banner, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	public function toggle($banner_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_banner = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "banner_id = :banner_id" , 
				"params" => array(
					"banner_id" => $banner_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('banner', $update_banner, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function setPosition($banner_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_banner = array(
			'saved' => $today,
			'banner_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "banner_id = :banner_id" , 
				"params" => array(
					"banner_id" => $banner_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('banner', $update_banner, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($banner_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$banner = $this->getBannerByIdAdmin($banner_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "banner_id = :banner_id" , 
				"params" => array(
					"banner_id" => $banner_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('banner', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('banner_lang', $delete_criteria)->execute();

				// remove banner logo
				if (!empty($banner['banner_logo'])) {
					$photo = json_decode($banner['banner_logo'], true);
				
					if (is_file($assetPath . DS . 'banner' . DS . $photo['file'])) {
						CFileHelper::removeDirectory(dirname($assetPath . DS . 'banner' . DS . $photo['file']));
					}
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}