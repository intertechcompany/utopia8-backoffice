<?php

/**
 * AuthorForm class.
 * AuthorForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class AuthorForm extends CFormModel
{
	public $author_id;
	public $active;
	public $author_position;
	public $author_image;

	public $del_author_image;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'author_id',
				'isValidAuthor',
				'on' => 'edit',
			),
			array(
				'author_id',
				'safe',
				'on' => 'add',
			),
			array(
				'author_image',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'active, author_position,
				del_author_image',
				'safe',
			),
		);
	}
	
	public function isValidAuthor($attribute, $params)
	{
		$author = Author::model()->getAuthorByIdAdmin($this->$attribute);

		if (empty($author)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}