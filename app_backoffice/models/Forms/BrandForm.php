<?php

/**
 * BrandForm class.
 * BrandForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class BrandForm extends CFormModel
{
	public $brand_id;
	public $active;
	public $brand_alias;
	public $brand_crm;
	public $brand_logo;
	public $brand_photo;
	public $brand_discount;

	// unnecessary attributes
	public $del_brand_logo;
	public $del_brand_photo;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'brand_id',
				'isValidBrand',
				'on' => 'edit',
			),
			array(
				'brand_id',
				'safe',
				'on' => 'add',
			),
			array(
				'brand_logo, brand_photo',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'brand_discount',
				'filter',
				'filter' => 'intval',
			),
			array(
				'active, brand_alias, brand_crm,
				del_brand_logo, del_brand_photo',
				'safe',
			),
		);
	}
	
	public function isValidBrand($attribute, $params)
	{
		$brand = Brand::model()->getBrandByIdAdmin($this->$attribute);

		if (empty($brand)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}