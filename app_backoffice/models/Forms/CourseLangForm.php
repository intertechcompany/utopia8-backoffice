<?php

/**
 * CourseLangForm class.
 * CourseLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class CourseLangForm extends CFormModel
{
	public $course_name;
	public $course_tip;
	public $course_place;
	public $course_content;
	public $course_no_index;
	public $course_meta_title;
	public $course_meta_description;
	public $course_meta_keywords;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'course_name',
				'requiredMultiLang',
				'message' => Yii::t('course', 'Enter a course title in all languages!'),
			),
			/* array(
				'course_content',
				'filter',
				'filter' => 'json_encode',
			), */
			array(
				'course_country, course_tip, course_place, course_content, course_no_index, course_meta_title, course_meta_description, course_meta_keywords',
				'safe',
			),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		foreach (Yii::app()->params->langs as $code => $lang) {
			if (empty($lang_fields[$code])) {
				$this->addError($attribute, $params['message']);	
				
				break;
			}		
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}