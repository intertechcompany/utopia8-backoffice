<?php

/**
 * CategoryLangForm class.
 * CategoryLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class CategoryLangForm extends CFormModel
{
	public $category_name;
	public $category_title;
    public $category_fullname;
	public $category_faq;
	public $category_description;
	public $category_no_index;
	public $category_meta_title;
	public $category_meta_description;
	public $category_meta_keywords;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'category_name',
				'requiredMultiLang',
				'message' => Yii::t('category', 'Enter an category name in all languages!'),
			),
			array(
				'category_fullname, category_title, category_faq, category_description, category_no_index, category_meta_title, category_meta_description, category_meta_keywords',
				'safe',
			),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		foreach (Yii::app()->params->langs as $code => $lang) {
			if (empty($lang_fields[$code])) {
				$this->addError($attribute, $params['message']);	
				
				break;
			}		
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}