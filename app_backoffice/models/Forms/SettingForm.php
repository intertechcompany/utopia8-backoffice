<?php

/**
 * SettingForm class.
 * SettingForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class SettingForm extends CFormModel
{
	public $setting_id;
	public $setting_rev;
	public $setting_notify_mail;
	public $setting_mail;
	public $setting_phone;
	public $setting_facebook;
	public $setting_twitter;
	public $setting_instagram;
	public $setting_lat;
	public $setting_long;
	public $setting_no_index;
	public $setting_meta_title;
	public $setting_meta_keywords;
	public $setting_meta_description;
	public $setting_ga;
	public $setting_ym;
	public $setting_currency;
	public $setting_currency_eur;
	public $setting_currency_usd;
	public $setting_quick_buy;
	public $setting_stock;
	public $setting_last_size;
	public $setting_last_size_cid;
	public $setting_newest;
	public $setting_min_qty;
	public $setting_courier;
	public $setting_np_address;
	public $setting_np_department;
	public $setting_max_order_amount;
	public $setting_delivery_world;
	public $setting_instagram_token;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'setting_id',
				'compare',
				'compareValue' => 1,
				'message' => Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'),
			),
			array(
				'setting_rev',
				'required',
				'message' => Yii::t('settings', 'Field \'Assets version\' is required!'),
			),
			array(
				'setting_currency_eur, setting_currency_usd',
				'filter',
				'filter' => 'floatval',
			),
			array(
				'setting_quick_buy',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('settings', '\'Quick buy\' value is invalid!'),
			),
			array(
				'setting_stock',
				'in',
				'range' => array('none', 'order', 'payment'),
				'message' => Yii::t('settings', '\'Quick buy\' value is invalid!'),
			),
			array(
				'setting_last_size',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('settings', '\'Last size\' value is invalid!'),
			),
			array(
				'setting_currency',
				'filter',
				'filter' => 'json_encode',
			),
			array(
				'setting_newest, setting_min_qty',
				'filter',
				'filter' => 'intval',
			),
			array(
				'setting_notify_mail, setting_mail, setting_phone, setting_no_index, setting_meta_title, setting_meta_keywords, setting_meta_description, 
				setting_facebook, setting_twitter, setting_instagram,
				setting_lat, setting_long, 
				setting_ga, setting_ym,
				setting_last_size_cid, 
				setting_instagram_token, 
				setting_courier, setting_np_address, setting_np_department, setting_delivery_world, setting_max_order_amount',
				'safe',
			),
			array(
				'setting_no_index',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('settings', '\'No index site\' value is invalid!'),
			),
		);
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}