<?php

/**
 * ManagerForm class.
 * ManagerForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class ManagerForm extends CFormModel
{
	public $manager_id;
	public $active;
	public $manager_login;
	public $manager_password;
	public $manager_token;
	public $manager_email;
	public $manager_first_name;
	public $manager_middle_name;
	public $manager_last_name;
	
	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'manager_id',
				'safe',
				'on' => 'add',
			),
			array(
				'manager_id',
				'isValidManager',
				'on' => 'edit',
			),
			array(
				'manager_login',
				'required',
				'message' => Yii::t('managers', 'Enter a manager login!'),
			),
			array(
				'manager_login',
				'isValidManagerLogin',
				'skipOnError' => true,
			),
			/* array(
				'manager_email',
				'required',
				'message' => Yii::t('managers', 'Enter a manager email!'),
			),
			array(
				'manager_email',
				'isValidManagerEmail',
				'skipOnError' => true,
			), */
			array(
				'manager_password',
				'required',
				'message' => Yii::t('managers', 'Enter a manager password!'),
				'on' => 'add',
			),
			array(
				'manager_password',
				'safe',
				'on' => 'edit',
			),
			array(
				'active, manager_email, manager_first_name, manager_middle_name, manager_last_name',
				'safe',
			),
		);
	}

	public function isValidManager($attribute, $params)
	{
		$manager = Manager::model()->getManagerByIdAdmin($this->$attribute);

		if (empty($manager)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function isValidManagerLogin($attribute, $params)
	{
		$manager = Manager::model()->issetManagerByLogin($this->$attribute, $this->manager_id);

		if (!empty($manager)) {
			$this->addError($attribute, Yii::t('managers', 'This managername already exists! Come up with a different name'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}