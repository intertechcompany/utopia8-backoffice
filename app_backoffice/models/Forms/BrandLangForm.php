<?php

/**
 * BrandLangForm class.
 * BrandLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class BrandLangForm extends CFormModel
{
	public $brand_name;
	public $brand_country;
	public $brand_description;
	public $brand_no_index;
	public $brand_meta_title;
	public $brand_meta_description;
	public $brand_meta_keywords;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'brand_name',
				'requiredMultiLang',
				'message' => Yii::t('brand', 'Enter a brand name in all languages!'),
			),
			array(
				'brand_country, brand_description, brand_no_index, brand_meta_title, brand_meta_description, brand_meta_keywords',
				'safe',
			),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		foreach (Yii::app()->params->langs as $code => $lang) {
			if (empty($lang_fields[$code])) {
				$this->addError($attribute, $params['message']);	
				
				break;
			}		
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}