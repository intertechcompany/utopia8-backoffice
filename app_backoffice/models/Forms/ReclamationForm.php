<?php

/**
 * ReclamationForm class.
 * ReclamationForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class ReclamationForm extends CFormModel
{
	public $reclamation_id;
	public $reclamation_position;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'reclamation_id',
				'isValidReclamation',
				'on' => 'edit',
			),
			array(
				'reclamation_id',
				'safe',
				'on' => 'add',
			),
			array(
				'active, reclamation_position',
				'safe',
			),
		);
	}
	
	public function isValidReclamation($attribute, $params)
	{
		$reclamation = Reclamation::model()->getReclamationByIdAdmin($this->$attribute);

		if (empty($reclamation)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}