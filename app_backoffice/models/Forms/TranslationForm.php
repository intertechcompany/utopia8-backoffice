<?php

/**
 * TranslationForm class.
 * TranslationForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class TranslationForm extends CFormModel
{
	public $translation_id;
	public $translation_code;
	public $translation_group;
	public $translation_tip;
	public $translation_type;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'translation_id',
				'isValidTranslation',
				'on' => 'edit',
			),
			array(
				'translation_id',
				'safe',
				'on' => 'add',
			),
			array(
				'translation_code',
				'required',
				'message' => Yii::t('translations', 'Enter a translation code!'),
			),
			array(
				'translation_type',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('translations', 'Invalid translation type!'),
			),
			array(
				'translation_group, translation_tip',
				'safe',
			),
		);
	}
	
	public function isValidTranslation($attribute, $params)
	{
		$translation = Translation::model()->getTranslationByIdAdmin($this->$attribute);

		if (empty($translation)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}