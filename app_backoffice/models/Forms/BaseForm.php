<?php

/**
 * BaseForm class.
 * BaseForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class BaseForm extends CFormModel
{
	public $base_id;
	public $active;
	public $base_alias;
	public $base_photo;
	public $category_id;

	// unnecessary attributes
	public $del_base_photo;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'base_id',
				'isValidBase',
				'on' => 'edit',
			),
			array(
				'base_id',
				'safe',
				'on' => 'add',
			),
			array(
				'base_photo',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
                'category_id',
                'required',
                'message' => Yii::t('bases', '\'Category\' is required!'),
            ),
            array(
                'category_id',
                'isValidCategory',
                'skipOnError' => true,
            ),
			array(
				'active, base_alias, 
				del_base_photo',
				'safe',
			),
		);
	}
	
	public function isValidBase($attribute, $params)
	{
		$base = Base::model()->getBaseByIdAdmin($this->$attribute);

		if (empty($base)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function isValidCategory($attribute, $params)
    {
        $category = BaseCategory::model()->getCategoryByIdAdmin($this->$attribute);

        if (empty($category)) {
            $this->addError($attribute, Yii::t('bases', 'Category is invalid!'));
        }
    }
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}