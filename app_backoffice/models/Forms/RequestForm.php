<?php

/**
 * RequestForm class.
 * RequestForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class RequestForm extends CFormModel
{
    public $request_id;
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $message;

    private $_myErrors = array();
    private $_errorFields = array();

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array(
                'request_id',
                'isValidRequest',
                'on' => 'edit',
            ),
            array(
                'request_id',
                'safe',
                'on' => 'add',
            ),
            array(
                'first_name, last_name, email, phone, message',
                'safe',
            ),
        );
    }

    public function isValidRequest($attribute, $params)
    {
        $product = Request::model()->getRequestByIdAdmin($this->$attribute);

        if (empty($product)) {
            $this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

            return false;
        }

        return true;
    }

    public function afterValidate()
    {
        foreach ($this->attributes as $attribute => $value) {
            if ($this->hasErrors($attribute)) {
                $this->_errorFields[] = $attribute;

                foreach ($this->getErrors($attribute) as $error) {
                    $this->_myErrors[] = $error;
                }
            }
        }

        return parent::afterValidate();
    }

    public function jsonErrors()
    {
        $json_errors = array(
            'msg' => array_unique($this->_myErrors),
            'fields' => array_unique($this->_errorFields),
        );

        return $json_errors;
    }
}
