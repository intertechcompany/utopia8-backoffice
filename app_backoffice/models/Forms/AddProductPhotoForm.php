<?php

/**
 * AddProductPhotoForm class.
 * AddProductPhotoForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class AddProductPhotoForm extends CFormModel
{
	public $product_id;
	public $photo;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'product_id',
				'filter',
				'filter' => 'intval',
			),
			array(
				'photo',
				'file',
				'types' => 'jpg, jpeg, png',
				'wrongType' => 'Изображение должно быть в формате JPG, GIF или PNG!',
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => 'Размер изображения не должен превышать 10 MB!',
				'message' => 'Выберите файл!',
				'on' => 'photo',
			),
		);
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
				
		// get all errors
		/*
		foreach($this->getErrors() as $errors) {
			foreach ($errors as $error) {
				if (!empty($error)) {
					$json_errors['msg'][] = $error;
					
					break 2;
				}
			}
		}
		*/
		
		return $json_errors;
	}
}