<?php

/**
 * ProductLangForm class.
 * ProductLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class ProductLangForm extends CFormModel
{
	public $product_title;
	public $product_country;
	public $product_brand;
	public $product_cart;
	public $product_special_title;
	public $product_special_tip;
	public $product_tags;
	public $product_tip;
	public $product_description;
	public $product_faq;
	public $product_care;
	public $product_reclamation;
	public $product_no_index;
	public $product_meta_title;
	public $product_meta_description;
	public $product_meta_keywords;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'product_title',
				'requiredMultiLang',
				'message' => Yii::t('product', 'Enter a product title in all languages!'),
			),
			array(
				'product_no_index, product_country, product_brand, product_cart, product_special_title, product_special_tip, product_tags, product_tip, product_description, product_faq, product_care, product_reclamation, product_meta_title, product_meta_description, product_meta_keywords',
				'safe',
			),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		foreach (Yii::app()->params->langs as $code => $lang) {
			if (empty($lang_fields[$code])) {
				$this->addError($attribute, $params['message']);	
				
				break;
			}		
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}