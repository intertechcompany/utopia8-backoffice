<?php

/**
 * PropertyForm class.
 * PropertyForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class PropertyForm extends CFormModel
{
	public $property_id;
	public $property_top;
	public $property_filter;
	public $property_variant;
	public $property_option;
	public $property_cart;
	public $property_color;
	public $property_size;
	public $property_hide;
	public $property_special;
	public $property_icon;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'property_id',
				'isValidProperty',
				'on' => 'edit',
			),
			array(
				'property_id',
				'safe',
				'on' => 'add',
			),
			array(
				'property_top',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Top property\' value is invalid!'),
			),
			array(
				'property_filter',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property in filter\' value is invalid!'),
			),
			array(
				'property_variant',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property variant\' value is invalid!'),
			),
			array(
				'property_option',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property option\' value is invalid!'),
			),
			array(
				'property_cart',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property cart\' value is invalid!'),
			),
			array(
				'property_color',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property color\' value is invalid!'),
			),
			array(
				'property_size',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property size\' value is invalid!'),
			),
			array(
				'property_hide',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property hide\' value is invalid!'),
			),
			array(
				'property_special',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property special\' value is invalid!'),
			),
			array(
				'property_icon',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('properties', '\'Property icon\' value is invalid!'),
			),
		);
	}
	
	public function isValidProperty($attribute, $params)
	{
		$property = Property::model()->getPropertyByIdAdmin($this->$attribute);

		if (empty($property)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}