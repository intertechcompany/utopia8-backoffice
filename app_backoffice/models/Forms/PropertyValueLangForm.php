<?php

/**
 * PropertyValueLangForm class.
 * PropertyValueLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class PropertyValueLangForm extends CFormModel
{
	public $value_title;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'value_title',
				'requiredMultiLang',
				'message' => Yii::t('values', 'Enter a value title in all languages!'),
			),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		foreach (Yii::app()->params->langs as $code => $lang) {
			if (empty($lang_fields[$code])) {
				$this->addError($attribute, $params['message']);	
				
				break;
			}		
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}