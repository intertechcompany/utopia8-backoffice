<?php

/**
 * PropertyValueForm class.
 * PropertyValueForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class PropertyValueForm extends CFormModel
{
	public $property_id;
	public $value_id;
	public $value_top;
	public $value_color;
	public $value_multicolor;
	public $value_icon;
	public $value_position;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'property_id',
				'isValidProperty',
			),
			array(
				'value_id',
				'isValidValue',
				'on' => 'edit',
			),
			array(
				'value_id',
				'safe',
				'on' => 'add',
			),
			array(
				'value_top',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('values', '\'Top property\' value is invalid!'),
			),
			array(
				'value_color',
				'match',
				'allowEmpty' => true,
				'pattern' => '/^#([a-f\d]{6}|[a-f\d]{3})$/i',
				'message' => Yii::t('values', 'Invalid color format!'),
			),
			array(
				'value_multicolor',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('values', '\'Multicolor\' value is invalid!'),
			),
			array(
				'value_icon',
				'in',
				'range' => array('', 'espresso', 'filter'),
				'message' => Yii::t('values', '\'Icon\' value is invalid!'),
			),
			array(
				'value_position',
				'filter',
				'filter' => 'intval',
			),
		);
	}
	
	public function isValidProperty($attribute, $params)
	{
		$property = Property::model()->getPropertyByIdAdmin($this->$attribute);

		if (empty($property)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function isValidValue($attribute, $params)
	{
		$value = PropertyValue::model()->getValueByIdAdmin($this->$attribute);

		if (empty($value)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}