<?php

/**
 * DiscountForm class.
 * DiscountForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class DiscountForm extends CFormModel
{
	public $discount_id;
	public $active;
	public $discount_code;
	public $discount_type;
	public $discount_value;
	public $discount_allowed_uses;
	public $discount_start;
	public $discount_end;
	public $discount_comment;
	public $quantity;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'discount_id',
				'isValidDiscount',
				'on' => 'edit',
			),
			array(
				'discount_id',
				'safe',
				'on' => 'add',
			),
			array(
				'discount_type',
				'in',
				'range' => array('percentage', 'flat'),
				'message' => Yii::t('discounts', '\'Discount type\' value is invalid!'),
			),
			array(
				'discount_value, discount_allowed_uses',
				'filter',
				'filter' => 'intval',
			),
			array(
				'discount_value',
				'isValidValue',
			),
			array(
				'discount_allowed_uses',
				'numerical',
				'min' => 0,
				'tooSmall' => 'Allowed uses must be 0 or greater',
				'allowEmpty' => false,
			),
			array(
				'discount_start',
				'date',
				'allowEmpty' => true,
				'format' => 'dd.MM.yyyy',
				'message' => Yii::t('discounts', 'Start date format is invalid!'),
			),
			array(
				'discount_end',
				'date',
				'allowEmpty' => true,
				'format' => 'dd.MM.yyyy',
				'message' => Yii::t('discounts', 'End date format is invalid!'),
			),
			array(
				'quantity',
				'numerical',
				'min' => 1,
				'tooSmall' => 'Quantity must be 1 or greater',
				'allowEmpty' => false,
				'on' => 'add',
			),
			array(
				'quantity',
				'safe',
				'on' => 'edit',
			),
			array(
				'active, discount_code, discount_comment',
				'safe',
			),
		);
	}
	
	public function isValidDiscount($attribute, $params)
	{
		$discount = Discount::model()->getDiscountByIdAdmin($this->$attribute);

		if (empty($discount)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function isValidValue($attribute, $params)
	{
		if ($this->discount_type == 'percentage') {
			if ($this->$attribute < 1 || $this->$attribute > 100) {
				$this->addError($attribute, Yii::t('discounts', 'Discount value must be between 1 and 100!'));
			}
		} else {
			if ($this->$attribute < 1) {
				$this->addError($attribute, Yii::t('discounts', 'Discount value must be 1 or greater!'));
			}
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}