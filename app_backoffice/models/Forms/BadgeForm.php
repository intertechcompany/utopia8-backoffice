<?php

/**
 * BadgeForm class.
 * BadgeForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class BadgeForm extends CFormModel
{
	public $badge_id;
	public $badge_logo;
	public $badge_position;
	public $badge_icon_only;

	// unnecessary attributes
	public $del_badge_logo;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'badge_id',
				'isValidBadge',
				'on' => 'edit',
			),
			array(
				'badge_id',
				'safe',
				'on' => 'add',
			),
			array(
				'badge_logo',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'badge_icon_only',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('badges', '\'Icon only\' value is invalid!'),
			),
			array(
				'active, badge_position,
				del_badge_logo',
				'safe',
			),
		);
	}
	
	public function isValidBadge($attribute, $params)
	{
		$badge = Badge::model()->getBadgeByIdAdmin($this->$attribute);

		if (empty($badge)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}