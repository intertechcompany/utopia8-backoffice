<?php

/**
 * StoreForm class.
 * StoreForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class StoreForm extends CFormModel
{
	public $store_id;
	public $active;
	public $store_position;
	public $store_code;
	public $store_type;
	public $store_lat;
	public $store_long;
	public $store_image;

	public $del_store_image;
	
	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'store_id',
				'safe',
				'on' => 'add',
			),
			array(
				'store_id',
				'isValidStore',
				'on' => 'edit',
			),
			array(
				'store_code',
				'required',
				'message' => Yii::t('stores', 'Enter a store code!'),
			),
			array(
				'store_type',
				'in',
				'range' => array('warehouse', 'preorder', 'dropship'),
				'message' => Yii::t('stores', '\'Type\' value is invalid!'),
			),
			array(
				'store_image',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'active, store_position, store_lat, store_long,
				del_store_image',
				'safe',
			),
		);
	}

	public function isValidStore($attribute, $params)
	{
		$store = Store::model()->getStoreByIdAdmin($this->$attribute);

		if (empty($store)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}