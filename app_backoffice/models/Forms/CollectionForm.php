<?php

/**
 * CollectionForm class.
 * CollectionForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class CollectionForm extends CFormModel
{
	public $collection_id;
	public $active;
	public $collection_alias;
	public $collection_rating;
	public $collection_description_full;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'collection_id',
				'isValidCollection',
				'on' => 'edit',
			),
			array(
				'collection_id',
				'safe',
				'on' => 'add',
			),
			array(
				'collection_description_full',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('collections', '\'Show full description\' value is invalid!'),
			),
			array(
				'active, collection_alias, collection_rating',
				'safe',
			),
		);
	}
	
	public function isValidCollection($attribute, $params)
	{
		$collection = Collection::model()->getCollectionByIdAdmin($this->$attribute);

		if (empty($collection)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}