<?php

/**
 * UserForm class.
 * UserForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class UserForm extends CFormModel
{
	public $user_id;
	public $active;
	/* public $user_login; */
	public $user_password;
	public $user_token;
	public $user_email;
	public $user_phone;
	public $user_first_name;
	public $user_last_name;
	public $user_zip;
	public $user_city;
	public $user_address;
	public $user_address_2;
	public $discount;
	
	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'user_id',
				'safe',
				'on' => 'add',
			),
			array(
				'user_id',
				'isValidUser',
				'on' => 'edit',
			),
			/* array(
				'user_login',
				'required',
				'message' => Yii::t('users', 'Enter a user name!'),
			),
			array(
				'user_login',
				'isValidUserLogin',
				'skipOnError' => true,
			), */
			array(
				'user_email',
				'required',
				'message' => Yii::t('users', 'Enter a user email!'),
			),
			array(
				'user_email',
				'isValidUserEmail',
				'skipOnError' => true,
			),
			array(
				'user_password',
				'required',
				'message' => Yii::t('users', 'Enter a user password!'),
				'on' => 'add',
			),
			array(
				'user_password',
				'safe',
				'on' => 'edit',
			),
			array(
				'active, user_email, user_phone, user_first_name, user_last_name, user_zip, user_city, user_address, user_address_2, discount',
				'safe',
			),
		);
	}

	public function isValidUser($attribute, $params)
	{
		$user = User::model()->getUserByIdAdmin($this->$attribute);

		if (empty($user)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function isValidUserLogin($attribute, $params)
	{
		$user = User::model()->issetUserByLogin($this->$attribute, $this->user_id);

		if (!empty($user)) {
			$this->addError($attribute, Yii::t('users', 'This username already exists! Come up with a different name'));

			return false;
		}

		return true;
	}

	public function isValidUserEmail($attribute, $params)
	{
		$user = User::model()->issetUserByEmail($this->$attribute, $this->user_id);

		if (!empty($user)) {
			$this->addError($attribute, Yii::t('users', 'This email already exists! Come up with a different email'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}