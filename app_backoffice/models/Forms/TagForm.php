<?php

/**
 * TagForm class.
 * TagForm is the data structure for keeping
 * write mail form data. It is used by the 'tag' action of 'AjaxController'.
 */
class TagForm extends CFormModel
{
    public $tag_id;
    public $tag_alias;
    public $tag_image;

    // unnecessary attributes
	public $del_tag_image;

    private $_myErrors = array();
    private $_errorFields = array();

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array(
                'tag_id',
                'isValidTag',
                'on' => 'edit',
            ),
            array(
                'tag_id',
                'safe',
                'on' => 'add',
            ),
            array(
				'tag_image',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
            array(
                'tag_alias, del_tag_image',
                'safe',
            ),
        );
    }

    public function isValidTag($attribute, $params)
    {
        $tag = Tag::model()->getTagByIdAdmin($this->$attribute);

        if (empty($tag)) {
            $this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

            return false;
        }

        return true;
    }

    public function afterValidate()
    {
        foreach ($this->attributes as $attribute => $value) {
            if ($this->hasErrors($attribute)) {
                $this->_errorFields[] = $attribute;

                foreach ($this->getErrors($attribute) as $error) {
                    $this->_myErrors[] = $error;
                }
            }
        }

        return parent::afterValidate();
    }

    public function jsonErrors()
    {
        $json_errors = array(
            'msg' => array_unique($this->_myErrors),
            'fields' => array_unique($this->_errorFields),
        );

        return $json_errors;
    }
}
