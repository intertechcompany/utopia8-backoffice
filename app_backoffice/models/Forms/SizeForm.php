<?php

/**
 * SizeForm class.
 * SizeForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class SizeForm extends CFormModel
{
	public $size_id;
	public $size_position;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'size_id',
				'isValidSize',
				'on' => 'edit',
			),
			array(
				'size_id',
				'safe',
				'on' => 'add',
			),
			array(
				'active, size_position',
				'safe',
			),
		);
	}
	
	public function isValidSize($attribute, $params)
	{
		$size = Size::model()->getSizeByIdAdmin($this->$attribute);

		if (empty($size)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}