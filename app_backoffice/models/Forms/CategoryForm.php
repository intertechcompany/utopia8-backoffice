<?php

/**
 * CategoryForm class.
 * CategoryForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class CategoryForm extends CFormModel
{
	public $category_id;
	public $parent_id;
	public $related_id;
	public $active;
	public $category_top;
	public $category_home;
	public $category_type;
	public $category_alias;
	public $category_photo;
	public $category_position;
	public $category_discount;
	public $category_delivery;
	public $base_categories;
	public $tags;

	// unnecessary attributes
	public $del_category_photo;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'category_id',
				'isValidCategory',
				'on' => 'edit',
			),
			array(
				'category_id',
				'safe',
				'on' => 'add',
			),
			array(
				'category_type',
				'in',
				'range' => array('categories', 'collections', 'products', 'tags'),
				'message' => Yii::t('categories', 'Invalid value for field "Category type"!'),
			),
			array(
				'parent_id',
				'isValidListCategory',
			),
			array(
				'category_top',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('categories', '\'Top\' value is invalid!'),
			),
			array(
				'category_home',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('categories', '\'Home\' value is invalid!'),
			),
			array(
				'parent_id',
				'compare',
				'compareAttribute' => 'category_id',
				'operator' => '!=',
				'message' => Yii::t('categories', 'Parent category should not be a current category!'),
				'on' => 'edit',
			),
			array(
				'related_id',
				'filter',
				'filter' => array($this, 'filterRelatedCategories'),
			),
			array(
				'category_discount',
				'filter',
				'filter' => 'intval',
			),
            array(
                'tags',
                'isValidTags',
            ),
			array(
				'category_photo',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'base_categories',
				'isValidBaseCategories',
			),
			array(
				'active, category_alias, category_position, category_delivery, 
				del_category_photo',
				'safe',
			),
		);
	}
	
	public function isValidCategory($attribute, $params)
	{
		$category = Category::model()->getCategoryByIdAdmin($this->$attribute);

		if (empty($category)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function isValidListCategory($attribute, $params)
	{
		$this->$attribute = (int) $this->$attribute;

		if (!empty($this->$attribute)) {
			// validate only if subcategory
			$this->isValidCategory($attribute, $params);
		}

		return true;
	}

    public function isValidTags($attribute, $params)
    {
        if (empty($this->$attribute)) {
            return true;
        }

        foreach ($this->$attribute as $value) {
            $tag = Tag::model()->getTagByIdAdmin($value);

            if (empty($tag)) {
                $this->addError($attribute, Yii::t('products', 'Tag is invalid!'));

                return false;
            }
        }

        return true;
    }

	public function filterRelatedCategories($related_ids)
	{
		$category_ids = array();

		$category_model = Category::model();

		if (empty($related_ids)) {
			return '';
		} else {
			foreach ($related_ids as $category_id) {
				$category_id = (int) $category_id;

				$category = $category_model->getCategoryByIdAdmin($category_id);

				if (!empty($category) && $category_id != $this->category_id) {
					$category_ids[] = $category_id;
				}
			}

			return implode(',', $category_ids);			
		}
	}

	public function isValidBaseCategories($attribute, $params)
	{
		if (empty($this->$attribute)) {
			return true;
		}

        foreach ($this->$attribute as $value) {
            $category = BaseCategory::model()->getCategoryByIdAdmin($value);

            if (empty($category)) {
                $this->addError($attribute, Yii::t('categories', 'Category is invalid!'));

                return false;
            }
        }

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}