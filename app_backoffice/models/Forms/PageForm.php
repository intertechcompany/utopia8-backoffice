<?php

/**
 * PageForm class.
 * PageForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class PageForm extends CFormModel
{
	public $page_id;
	public $active;
	public $page_alias;
	public $page_type;
	// public $page_menu;
	public $page_position;
	public $page_photo;
	public $page_content;
	public $page_content_faq;
	public $page_content_faq_main;
	public $menu;

	// unnecessary attributes
	public $del_page_photo;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'page_id',
				'isValidPage',
				'on' => 'edit',
			),
			array(
				'page_id',
				'safe',
				'on' => 'add',
			),
			array(
				'page_type',
				'in',
				'range' => array('text', 'contact', 'delivery', 'about', 'faq'),
				'message' => Yii::t('pages', '\'Page type\' value is invalid!'),
			),
			/* array(
				'page_menu',
				'in',
				'range' => array('top', 'bottom1', 'bottom2', 'bottom3'),
				'message' => Yii::t('pages', '\'Page menu\' value is invalid!'),
			), */
			array(
				'page_photo',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'menu',
				'filter',
				'filter' => array($this, 'filterMenu'),
			),
			array(
				'active, page_alias, page_position, page_content, del_page_photo, page_content_faq, page_content_faq_main',
				'safe',
			),
		);
	}
	
	public function isValidPage($attribute, $params)
	{
		$page = Page::model()->getPageByIdAdmin($this->$attribute);

		if (empty($page)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function filterMenu($menu)
	{
		if (empty($menu)) {
			return [];
		}
		
		$filtered_menu = [];

		foreach ($menu as $menu_item) {
			if (in_array($menu_item, ['top', 'bottom'])) {
				$filtered_menu[] = $menu_item;
			}
		}

		return $filtered_menu;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}