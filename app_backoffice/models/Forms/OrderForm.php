<?php

/**
 * OrderForm class.
 * OrderForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class OrderForm extends CFormModel
{
	public $order_id;
	public $status;
	public $delivery;
	public $payment;
	public $first_name;
	public $last_name;
	public $full_name;
	public $email;
	public $phone;
	public $country;
	public $zip;
	public $region;
	public $city;
	public $address;
	public $np_city;
	public $np_address;
	public $np_department;
	public $np_building;
	public $np_apartment;
	public $comment;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'order_id',
				'isValidOrder',
				'on' => 'edit',
			),
			array(
				'order_id',
				'safe',
				'on' => 'add',
			),
			array(
				'status',
				'in',
				'range' => array(
					'new',
					'processing',
					'paid',
					'completed',
					'cancelled',
					'payment_error',
				),
				'message' => Yii::t('orders', '\'Status\' value is invalid!'),
			),
			array(
				'delivery',
				'in',
				'range' => array(1,2,3),
				'message' => Yii::t('orders', '\'Delivery\' value is invalid!'),
			),
			array(
				'payment',
				'in',
				'range' => array(1,2,3),
				'message' => Yii::t('orders', '\'Payment\' value is invalid!'),
			),
			array(
				'first_name, last_name, email, phone, country, zip, region, city, address, np_city, np_address, np_department, np_building, np_apartment, comment',
				'safe',
			),
			array(
				'full_name',
				'filter',
				'filter' => array($this, 'fullName'),
			),
		);
	}
	
	public function isValidOrder($attribute, $params)
	{
		$product = Order::model()->getOrderByIdAdmin($this->$attribute);

		if (empty($product)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function fullName($full_name)
	{
		$full_name = trim($this->first_name . ' ' . $this->last_name);

		return $full_name;
	}

	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}