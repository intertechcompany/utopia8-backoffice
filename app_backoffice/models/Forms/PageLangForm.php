<?php

/**
 * PageLangForm class.
 * PageLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class PageLangForm extends CFormModel
{
	public $page_title;
	public $page_description;
	public $page_vacancy;
	public $page_content;
	public $page_content_faq;
	public $page_content_faq_main;
	public $page_no_index;
	public $page_meta_title;
	public $page_meta_description;
	public $page_meta_keywords;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'page_title',
				'requiredMultiLang',
				'message' => Yii::t('page', 'Enter a page title in all languages!'),
			),
			array(
				'page_intro, page_description, page_vacancy, page_content, page_content_faq, page_content_faq_main, page_no_index, page_meta_title, page_meta_description, page_meta_keywords',
				'safe',
			),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		foreach (Yii::app()->params->langs as $code => $lang) {
			if (empty($lang_fields[$code])) {
				$this->addError($attribute, $params['message']);	
				
				break;
			}		
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}