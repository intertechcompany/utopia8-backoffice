<?php

/**
 * TagLangForm class.
 * TagLangForm is the data structure for keeping
 * write mail form data. It is used by the 'tag' action of 'AjaxController'.
 */
class TagLangForm extends CFormModel
{
    public $tag_name;
    public $tag_no_index;
	public $tag_meta_title;
	public $tag_meta_description;
	public $tag_meta_keywords;

    private $_myErrors = array();
    private $_errorFields = array();

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array(
                'tag_name',
                'requiredMultiLang',
                'message' => Yii::t('tag', 'Enter a tag name in all languages!'),
            ),
            array(
				'tag_no_index, tag_meta_title, tag_meta_description, tag_meta_keywords',
				'safe',
			),
        );
    }

    public function requiredMultiLang($attribute, $params)
    {
        $lang_fields = $this->$attribute;

        foreach (Yii::app()->params->langs as $code => $lang) {
            if (empty($lang_fields[$code])) {
                $this->addError($attribute, $params['message']);

                break;
            }
        }
    }

    public function afterValidate()
    {
        foreach ($this->attributes as $attribute => $value) {
            if ($this->hasErrors($attribute)) {
                $this->_errorFields[] = $attribute;

                foreach ($this->getErrors($attribute) as $error) {
                    $this->_myErrors[] = $error;
                }
            }
        }

        return parent::afterValidate();
    }

    public function jsonErrors()
    {
        $json_errors = array(
            'msg' => array_unique($this->_myErrors),
            'fields' => array_unique($this->_errorFields),
        );

        return $json_errors;
    }
}
