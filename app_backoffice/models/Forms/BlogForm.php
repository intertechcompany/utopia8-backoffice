<?php

/**
 * BlogForm class.
 * BlogForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class BlogForm extends CFormModel
{
	public $blog_id;
	public $active;
	public $blog_alias;
	public $blog_published;
	public $blog_photo;
	public $category_id;

	// unnecessary attributes
	public $del_blog_photo;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'blog_id',
				'isValidBlog',
				'on' => 'edit',
			),
			array(
				'blog_id',
				'safe',
				'on' => 'add',
			),
			array(
				'blog_published',
				'date',
				'allowEmpty' => true,
				'format' => 'dd.MM.yyyy',
				'message' => Yii::t('blogs', 'Published date format is invalid!'),
			),
			array(
				'blog_photo',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
                'category_id',
                'required',
                'message' => Yii::t('blogs', '\'Category\' is required!'),
            ),
            array(
                'category_id',
                'isValidCategory',
                'skipOnError' => true,
            ),
			array(
				'active, blog_alias, 
				del_blog_photo',
				'safe',
			),
		);
	}
	
	public function isValidBlog($attribute, $params)
	{
		$blog = Blog::model()->getBlogByIdAdmin($this->$attribute);

		if (empty($blog)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function isValidCategory($attribute, $params)
    {
        $category = BlogCategory::model()->getCategoryByIdAdmin($this->$attribute);

        if (empty($category)) {
            $this->addError($attribute, Yii::t('blogs', 'Category is invalid!'));
        }
    }
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}