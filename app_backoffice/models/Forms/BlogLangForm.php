<?php

/**
 * BlogLangForm class.
 * BlogLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class BlogLangForm extends CFormModel
{
	public $blog_title;
	public $blog_intro;
	public $blog_description;
	public $blog_no_index;
	public $blog_meta_title;
	public $blog_meta_description;
	public $blog_meta_keywords;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'blog_title',
				'requiredMultiLang',
				'message' => Yii::t('blog', 'Enter a blog title in all languages!'),
			),
			array(
				'blog_intro, blog_description, blog_no_index, blog_meta_title, blog_meta_description, blog_meta_keywords',
				'safe',
			),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		foreach (Yii::app()->params->langs as $code => $lang) {
			if (empty($lang_fields[$code])) {
				$this->addError($attribute, $params['message']);	
				
				break;
			}		
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}