<?php

/**
 * BaseCategoryForm class.
 * BaseCategoryForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class BaseCategoryForm extends CFormModel
{
    public $category_id;
    public $parent_id;
    public $related_id;
    public $active;
    public $category_alias;
    public $category_crm_id;
    public $category_1c_id;
    public $category_photo;
    public $category_position;
    public $care_id;
    public $reclamation_id;
    public $size_id;

    // unnecessary attributes
    public $del_category_photo;

    private $_myErrors = array();
    private $_errorFields = array();

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array(
                'category_id',
                'isValidCategory',
                'on' => 'edit',
            ),
            array(
                'category_id',
                'safe',
                'on' => 'add',
            ),
            array(
                'parent_id',
                'isValidListCategory',
            ),
            array(
                'parent_id',
                'compare',
                'compareAttribute' => 'category_id',
                'operator' => '!=',
                'message' => Yii::t('categories', 'Parent category should not be a current category!'),
                'on' => 'edit',
            ),
            array(
                'category_photo',
                'file',
                'allowEmpty' => true,
                'types' => 'jpg, jpeg, gif, png',
                'wrongType' => Yii::t('app', 'Image wrong extension type!'),
                'maxSize' => 10 * 1024 * 1024, // 10 MB
                'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
            ),
            array(
                'active, category_crm_id, category_1c_id, category_alias, category_position,
                care_id, reclamation_id, size_id, 
                del_category_photo',
                'safe',
            ),
        );
    }

    public function isValidCategory($attribute, $params)
    {
        $category = BaseCategory::model()->getCategoryByIdAdmin($this->$attribute);

        if (empty($category)) {
            $this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));
        }
    }

    public function isValidListCategory($attribute, $params)
    {
        $this->$attribute = (int) $this->$attribute;

        if (!empty($this->$attribute)) {
            // validate only if subcategory
            $this->isValidCategory($attribute, $params);
        }

        return true;
    }

    public function filterRelatedCategories($related_ids)
    {
        $category_ids = array();

        $category_model = Category::model();

        if (empty($related_ids)) {
            return '';
        } else {
            foreach ($related_ids as $category_id) {
                $category_id = (int) $category_id;

                $category = $category_model->getCategoryByIdAdmin($category_id);

                if (!empty($category) && $category_id != $this->category_id) {
                    $category_ids[] = $category_id;
                }
            }

            return implode(',', $category_ids);
        }
    }

    public function afterValidate()
    {
        foreach ($this->attributes as $attribute => $value) {
            if ($this->hasErrors($attribute)) {
                $this->_errorFields[] = $attribute;

                foreach ($this->getErrors($attribute) as $error) {
                    $this->_myErrors[] = $error;
                }
            }
        }

        return parent::afterValidate();
    }

    public function jsonErrors()
    {
        $json_errors = array(
            'msg' => array_unique($this->_myErrors),
            'fields' => array_unique($this->_errorFields),
        );

        return $json_errors;
    }
}
