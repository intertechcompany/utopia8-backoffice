<?php
class Brand extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getBrandsAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$brand_id = (int) $func_args[1];
			$brand_name = addcslashes($func_args[1], '%_');

			$total_brands = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM brand as b JOIN brand_lang as bl ON b.brand_id = bl.brand_id AND bl.language_code = :code WHERE b.brand_id = :id OR bl.brand_name LIKE :brand_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $brand_id, PDO::PARAM_INT)
				->bindValue(':brand_name', '%' . $brand_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_brands = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM brand as b JOIN brand_lang as bl ON b.brand_id = bl.brand_id AND bl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_brands,
			'pages' => ceil($total_brands / $per_page),
		);
	}

	public function getBrandsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'brand_id':
				$order_by = ($direction == 'asc') ? 'b.brand_id' : 'b.brand_id DESC';
				break;
			case 'brand_name':
				$order_by = ($direction == 'asc') ? 'bl.brand_name' : 'bl.brand_name DESC';
				break;
			default:
				$order_by = 'b.brand_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$brand_id = (int) $func_args[4];
			$brand_name = addcslashes($func_args[4], '%_');

			$brands = Yii::app()->db
				->createCommand("SELECT b.brand_id, b.active, b.brand_logo, b.brand_discount, bl.brand_name, bl.brand_country FROM brand as b JOIN brand_lang as bl ON b.brand_id = bl.brand_id AND bl.language_code = :code WHERE b.brand_id = :id OR bl.brand_name LIKE :brand_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $brand_id, PDO::PARAM_INT)
				->bindValue(':brand_name', '%' . $brand_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$brands = Yii::app()->db
				->createCommand("SELECT b.brand_id, b.active, b.brand_logo, b.brand_discount, bl.brand_name, bl.brand_country FROM brand as b JOIN brand_lang as bl ON b.brand_id = bl.brand_id AND bl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $brands;
	}

	public function getBrandByIdAdmin($id)
	{
		$brand = Yii::app()->db
			->createCommand("SELECT * FROM brand WHERE brand_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($brand)) {
			// brand langs
			$brand_langs = Yii::app()->db
				->createCommand("SELECT * FROM brand_lang WHERE brand_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($brand_langs)) {
				foreach ($brand_langs as $brand_lang) {
					$code = $brand_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$brand[$code] = $brand_lang;
					}
				}
			}
		}
			
		return $brand;
	}

	public function getBrandsListAdmin()
	{
		$brands = Yii::app()->db
			->createCommand("SELECT b.brand_id, bl.brand_name FROM brand as b JOIN brand_lang as bl ON b.brand_id = bl.brand_id AND bl.language_code = :code ORDER BY bl.brand_name")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();
			
		return $brands;
	}

	public function getBrandsMap()
	{
		$brands = array();

		$brands_list = Yii::app()->db
			->createCommand("SELECT b.brand_id, b.brand_crm, bl.brand_name 
							 FROM brand as b 
							 JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code 
							 ORDER BY b.brand_id")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		if (!empty($brands_list)) {
			foreach ($brands_list as $brand) {
				$brand_code = $brand['brand_crm'];
				$brands[$brand_code] = $brand;
			}
		}

		return $brands;
	}

    public function issetBrandByAlias($brand_id, $brand_alias)
	{
		if (!empty($brand_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM brand WHERE brand_id != :brand_id AND brand_alias LIKE :alias")
				->bindValue(':brand_id', (int) $brand_id, PDO::PARAM_INT)
				->bindValue(':alias', $brand_alias, PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM brand WHERE brand_alias LIKE :alias")
				->bindValue(':alias', $brand_alias, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function add($brand_name)
    {
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');
		
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$insert_brand = [
			'created' => $today,
			'saved' => $today,
			'active' => 1,
			'brand_alias' => URLify::filter($brand_name, 200),
			'brand_crm' => $brand_name,
		];
		
		try {
			$rs = $builder->createInsertCommand('brand', $insert_brand)->execute();

			if ($rs) {
				$brand_id = (int) Yii::app()->db->getLastInsertID();
				
				foreach (Yii::app()->params->langs as $language_code => $language_name) {
					// save details
					$insert_brand_lang = array(
						'brand_id' => $brand_id,
						'language_code' => $language_code,
						'brand_visible' => 1,
						'created' => $today,
						'saved' => $today,
						'brand_name' => $brand_name,
					);

					$rs = $builder->createInsertCommand('brand_lang', $insert_brand_lang)->execute();

					if (!$rs) {
						$delete_criteria = new CDbCriteria(
							array(
								"condition" => "brand_id = :brand_id" , 
								"params" => array(
									"brand_id" => $brand_id,
								)
							)
						);
						
						$builder->createDeleteCommand('brand', $delete_criteria)->execute();

						return false;
					}
                }
                
                $brand = Yii::app()->db
                    ->createCommand("SELECT b.brand_id, b.brand_crm, bl.brand_name 
                                     FROM brand as b 
                                     JOIN brand_lang as bl 
                                     ON b.brand_id = bl.brand_id AND bl.language_code = :code 
                                     WHERE b.brand_id = :id
                                     LIMIT 1")
                    ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                    ->bindValue(':id', (int) $brand_id, PDO::PARAM_INT)
					->queryRow();

				return $brand;
            }
		} catch (CDbException $e) {
			// ...
		}

		return false;
    }

	public function save($model, $model_lang)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'brand_id',
			'brand_logo',
			'brand_photo',
		);

		// integer attributes
		$int_attributes = array();

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_brand_logo',
			'del_brand_photo',
		);

		// photos attributes
		$save_images = array(
			'brand_logo',
			'brand_photo',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get not empty title
		foreach (Yii::app()->params->langs as $language_code => $language_name) {
			if (!empty($model_lang->brand_name[$language_code])) {
				$brand_name = $model_lang->brand_name[$language_code];
				break;
			}
		}

		// get alias
		$model->brand_alias = empty($model->brand_alias) ? URLify::filter($brand_name, 200) : URLify::filter($model->brand_alias, 200);

		while ($this->issetBrandByAlias($model->brand_id, $model->brand_alias)) {
			$model->brand_alias = $model->brand_alias . '-' . uniqid();
		}

		if (empty($model->brand_id)) {
			// insert brand
			$insert_brand = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_brand[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_brand[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_brand[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_brand[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('brand', $insert_brand)->execute();

				if ($rs) {
					$model->brand_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array(
						'brand_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_brand_lang = array(
							'brand_id' => $model->brand_id,
							'language_code' => $language_code,
							'brand_visible' => !empty($model_lang->brand_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_brand_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_brand_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('brand_lang', $insert_brand_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "brand_id = :brand_id" , 
									"params" => array(
										"brand_id" => $model->brand_id,
									)
								)
							);
							
							$builder->createDeleteCommand('brand', $delete_criteria)->execute();

							return false;
						}
					}

					// save photos
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_brand = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_brand[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_brand[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_brand[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_brand[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_brand[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "brand_id = :brand_id" , 
					"params" => array(
						"brand_id" => $model->brand_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('brand', $update_brand, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'brand' . DS . $model->brand_id . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array(
						'brand_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_brand_lang = array(
							'brand_visible' => !empty($model_lang->brand_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'brand_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_brand_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_brand_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "brand_id = :brand_id AND language_code = :lang" , 
								"params" => array(
									"brand_id" => (int) $model->brand_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('brand_lang', $update_brand_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					// save photos
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_parts = explode('.', strtolower($model->$attribute->getName()));
				$image_extension = array_pop($image_parts);
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'brand' . DS . $model->brand_id . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;
						
						if ($attribute == 'brand_logo') {
							$image_files = array();

							$image_file_original = $image_dir . $image_name . '_o.' . $image_extension; // original
							$image_save_name_original = $image_id . '/' . $image_name . '_o.' . $image_extension;
							
							$image_file_small = $image_dir . $image_name . '_s.' . $image_extension; // small
							$image_save_name_small = $image_id . '/' . $image_name . '_s.' . $image_extension;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							$resized_image = $image_obj->save($image_file_original, array('quality' => 80));

							$image_files['original'] = array(
								'path' => $image_save_name_original,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// small image
							$image_obj = $imagine->open($image_path);

							/* if ($original_image_size->getWidth() > $original_image_size->getHeight()) {
								$resized_image = $image_obj->resize($original_image_size->widen(140))
									->save($image_file_small, array('quality' => 80));
							} else {
								$resized_image = $image_obj->resize($original_image_size->heighten(60))
									->save($image_file_small, array('quality' => 80));
							} */

							if ($original_image_size->getHeight() > 40) {
								$resized_image = $image_obj->resize($original_image_size->heighten(40))
									->save($image_file_small, array('quality' => 80));
							} else {
								$resized_image = $image_obj->save($image_file_small, array('quality' => 80));
							}

							$image_files['small'] = array(
								'path' => $image_save_name_small,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							if (is_file($image_file_original) && is_file($image_file_small)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						} elseif ($attribute == 'brand_photo') {
							$image_files = array();

							$image_file_original = $image_dir . $image_name . '_o.' . $image_extension; // original
							$image_save_name_original = $image_id . '/' . $image_name . '_o.' . $image_extension;
							
							$image_file_small = $image_dir . $image_name . '_s.' . $image_extension; // small
							$image_save_name_small = $image_id . '/' . $image_name . '_s.' . $image_extension;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							$resized_image = $image_obj->save($image_file_original, array('quality' => 80));

							$image_files['original'] = array(
								'path' => $image_save_name_original,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// small image
							$image_obj = $imagine->open($image_path);

							/* if ($original_image_size->getWidth() > $original_image_size->getHeight()) {
								$resized_image = $image_obj->resize($original_image_size->widen(140))
									->save($image_file_small, array('quality' => 80));
							} else {
								$resized_image = $image_obj->resize($original_image_size->heighten(60))
									->save($image_file_small, array('quality' => 80));
							} */

							if ($original_image_size->getWidth() > 600) {
								$resized_image = $image_obj->resize($original_image_size->widen(600))
									->save($image_file_small, array('quality' => 80));
							} else {
								copy($image_path, $image_file_small);
							}

							$image_files['small'] = array(
								'path' => $image_save_name_small,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							if (is_file($image_file_original) && is_file($image_file_small)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						} else {
							$image_file = $image_dir . $image_name;
							$image_save_name = $image_id . '/' . $image_name;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							switch ($attribute) {
								case 'brand_logo':
									if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
										$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_outbound)
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));	
									}
									break;
							}

							$image_size = array(
								'w' => $resized_image->getSize()->getWidth(),
								'h' => $resized_image->getSize()->getHeight(),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode(array_merge(
									array(
										'file' => $image_save_name,
									),
									$image_size
								));
							}
							else {
								// remove resized files
								if (is_file($image_file)) {
									unlink($image_file);
								}
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_brand = array(
				'saved' => $today,
			);

			$update_brand = array_merge($update_brand, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "brand_id = :brand_id" , 
					"params" => array(
						"brand_id" => $model->brand_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('brand', $update_brand, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	public function toggle($brand_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_brand = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "brand_id = :brand_id" , 
				"params" => array(
					"brand_id" => $brand_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('brand', $update_brand, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($brand_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$brand = $this->getBrandByIdAdmin($brand_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "brand_id = :brand_id" , 
				"params" => array(
					"brand_id" => $brand_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('brand', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('brand_lang', $delete_criteria)->execute();

				// remove brand directory
				if (is_dir($assetPath . DS . 'brand' . DS . $brand_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'brand' . DS . $brand_id);
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}