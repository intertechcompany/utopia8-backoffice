<?php
class Care extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getCaresAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$care_id = (int) $func_args[1];
			$care_name = addcslashes($func_args[1], '%_');

			$total_cares = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM care as c JOIN care_lang as cl ON c.care_id = cl.care_id AND cl.language_code = :code WHERE c.care_id = :id OR cl.care_name LIKE :care_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $care_id, PDO::PARAM_INT)
				->bindValue(':care_name', '%' . $care_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_cares = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM care as c JOIN care_lang as cl ON c.care_id = cl.care_id AND cl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_cares,
			'pages' => ceil($total_cares / $per_page),
		);
	}

	public function getCaresAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'care_id':
				$order_by = ($direction == 'asc') ? 'c.care_id' : 'c.care_id DESC';
				break;
			case 'care_position':
				$order_by = ($direction == 'asc') ? 'c.care_position' : 'c.care_position DESC';
				break;
			case 'care_name':
				$order_by = ($direction == 'asc') ? 'cl.care_name' : 'cl.care_name DESC';
				break;
			default:
				$order_by = 'c.care_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$care_id = (int) $func_args[4];
			$care_name = addcslashes($func_args[4], '%_');

			$cares = Yii::app()->db
				->createCommand("SELECT c.care_id, c.care_position, cl.care_name FROM care as c JOIN care_lang as cl ON c.care_id = cl.care_id AND cl.language_code = :code WHERE c.care_id = :id OR cl.care_name LIKE :care_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $care_id, PDO::PARAM_INT)
				->bindValue(':care_name', '%' . $care_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$cares = Yii::app()->db
				->createCommand("SELECT c.care_id, c.care_position, cl.care_name FROM care as c JOIN care_lang as cl ON c.care_id = cl.care_id AND cl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $cares;
	}

	public function getCaresListAdmin()
	{
		$cares = Yii::app()->db
			->createCommand("SELECT c.care_id, cl.care_name FROM care as c JOIN care_lang as cl ON c.care_id = cl.care_id AND cl.language_code = :code ORDER BY cl.care_name")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		return $cares;
	}

	public function getCareByIdAdmin($id)
	{
		$care = Yii::app()->db
			->createCommand("SELECT * FROM care WHERE care_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($care)) {
			// care langs
			$care_langs = Yii::app()->db
				->createCommand("SELECT * FROM care_lang WHERE care_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($care_langs)) {
				foreach ($care_langs as $care_lang) {
					$code = $care_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$care[$code] = $care_lang;
					}
				}
			}
		}
			
		return $care;
	}

	public function save($model, $model_lang)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'care_id',
		);

		// integer attributes
		$int_attributes = array(
			'care_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array();

		// photos attributes
		$save_images = array();

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get max page position
		if (empty($model->care_position)) {
			$max_position = Yii::app()->db
				->createCommand("SELECT MAX(care_position) FROM care")
				->queryScalar();

			$model->care_position = $max_position + 1;
		}

		if (empty($model->care_id)) {
			// insert care
			$insert_care = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_care[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_care[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_care[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_care[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('care', $insert_care)->execute();

				if ($rs) {
					$model->care_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_care_lang = array(
							'care_id' => $model->care_id,
							'language_code' => $language_code,
							'care_visible' => !empty($model_lang->care_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_care_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_care_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('care_lang', $insert_care_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "care_id = :care_id" , 
									"params" => array(
										"care_id" => $model->care_id,
									)
								)
							);
							
							$builder->createDeleteCommand('care', $delete_criteria)->execute();

							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_care = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_care[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_care[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_care[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_care[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_care[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "care_id = :care_id" , 
					"params" => array(
						"care_id" => $model->care_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('care', $update_care, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'care' . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_care_lang = array(
							'care_visible' => !empty($model_lang->care_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_care_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_care_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "care_id = :care_id AND language_code = :lang" , 
								"params" => array(
									"care_id" => (int) $model->care_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('care_lang', $update_care_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function setPosition($care_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_care = array(
			'saved' => $today,
			'care_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "care_id = :care_id" , 
				"params" => array(
					"care_id" => $care_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('care', $update_care, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($care_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$care = $this->getCareByIdAdmin($care_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "care_id = :care_id" , 
				"params" => array(
					"care_id" => $care_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('care', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('care_lang', $delete_criteria)->execute();

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}