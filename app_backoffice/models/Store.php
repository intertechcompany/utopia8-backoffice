<?php
class Store extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getStoresAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$store_id = (int) $func_args[1];
			$store_code = trim($func_args[1]);
			$store_name = addcslashes($func_args[1], '%_');

			$total_stores = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM store as s JOIN store_lang as sl ON s.store_id = sl.store_id AND sl.language_code = :code WHERE s.store_id = :id OR s.store_code = :store_code OR sl.store_name LIKE :store_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $store_id, PDO::PARAM_INT)
				->bindValue(':store_code', $store_code, PDO::PARAM_STR)
				->bindValue(':store_name', '%' . $store_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_stores = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM store as s JOIN store_lang as sl ON s.store_id = sl.store_id AND sl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_stores,
			'pages' => ceil($total_stores / $per_page),
		);
	}

	public function getStoresAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'store_id':
				$order_by = ($direction == 'asc') ? 's.store_id' : 's.store_id DESC';
				break;
			case 'store_position':
				$order_by = ($direction == 'asc') ? 's.store_position' : 's.store_position DESC';
				break;
			case 'store_name':
				$order_by = ($direction == 'asc') ? 'sl.store_name' : 'sl.store_name DESC';
				break;
			default:
				$order_by = 's.store_position, s.store_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$store_id = (int) $func_args[4];
			$store_code = trim($func_args[4]);
			$store_name = addcslashes($func_args[4], '%_');

			$stores = Yii::app()->db
				->createCommand("SELECT s.*, sl.store_name FROM store as s JOIN store_lang as sl ON s.store_id = sl.store_id AND sl.language_code = :code WHERE s.store_id = :id OR s.store_code = :store_code OR sl.store_name LIKE :store_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $store_id, PDO::PARAM_INT)
				->bindValue(':store_code', $store_code, PDO::PARAM_STR)
				->bindValue(':store_name', '%' . $store_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$stores = Yii::app()->db
				->createCommand("SELECT s.*, sl.store_name FROM store as s JOIN store_lang as sl ON s.store_id = sl.store_id AND sl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $stores;
	}

	public function getStoreByIdAdmin($id)
	{
		$store = Yii::app()->db
			->createCommand("SELECT * FROM store WHERE store_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

        if (!empty($store)) {
            // store langs
            $store_langs = Yii::app()->db
                ->createCommand("SELECT * FROM store_lang WHERE store_id = :id")
                ->bindValue(':id', (int) $id, PDO::PARAM_INT)
                ->queryAll();

            if (!empty($store_langs)) {
                foreach ($store_langs as $store_lang) {
                    $code = $store_lang['language_code'];

                    if (isset(Yii::app()->params->langs[$code])) {
                        $store[$code] = $store_lang;
                    }
                }
            }
        }
			
		return $store;
	}

	public function getStoresListAdmin()
	{
		$stores = Yii::app()->db
			->createCommand("SELECT s.store_id, sl.store_name FROM store as s JOIN store_lang as sl ON s.store_id = sl.store_id AND sl.language_code = :code ORDER BY s.store_position, s.store_id DESC")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();
			
		return $stores;
	}

	public function getStoresMap()
	{
		$stores = array();

		$stores_list = Yii::app()->db
			->createCommand("SELECT s.store_id, s.store_code, s.store_type, sl.store_name 
							 FROM store as s 
							 JOIN store_lang as sl 
							 ON s.store_id = sl.store_id AND sl.language_code = :code 
							 ORDER BY s.store_id")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		if (!empty($stores_list)) {
			foreach ($stores_list as $store) {
				$store_id = $store['store_id'];
				$store_code = $store['store_code'];
				
				$stores[$store_code] = $store;
			}
		}

		return $stores;
	}

	public function save($model, $model_lang)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'store_id',
			'store_image',
		);

		// integer attributes
		$int_attributes = array(
			'store_id',
			'store_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_store_image',
		);

		// photos attributes
		$save_images = array(
			'store_image',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get max store position
		if (empty($model->store_position)) {
			$max_position = Yii::app()->db
				->createCommand("SELECT MAX(store_position) FROM store")
				->queryScalar();

			$model->store_position = $max_position + 1;
		}

		if (empty($model->store_id)) {
			// insert store
			$insert_store = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_store[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_store[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_store[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_store[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('store', $insert_store)->execute();

				if ($rs) {
					$model->store_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array(
						'store_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_store_lang = array(
							'store_id' => $model->store_id,
							'language_code' => $language_code,
							'store_visible' => !empty($model_lang->store_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_store_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_store_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('store_lang', $insert_store_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "store_id = :store_id" , 
									"params" => array(
										"store_id" => $model->store_id,
									)
								)
							);
							
							$builder->createDeleteCommand('store', $delete_criteria)->execute();

							return false;
						}
					}

					// $this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_store = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_store[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_store[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_store[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_store[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_store[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "store_id = :store_id" , 
					"params" => array(
						"store_id" => $model->store_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('store', $update_store, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'store' . DS . $model->store_id . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array(
						'store_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_store_lang = array(
							'store_visible' => !empty($model_lang->store_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'store_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_store_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_store_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "store_id = :store_id AND language_code = :lang" , 
								"params" => array(
									"store_id" => (int) $model->store_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('store_lang', $update_store_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					// $this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_parts = explode('.', strtolower($model->$attribute->getName()));
				$image_extension = array_pop($image_parts);
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				$image_full_name_2x = $image_name . '@2x.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'store' . DS . $model->store_id . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;

						if ($attribute == 'store_image') {
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();
							
							$image_file = $image_dir . $image_name . '.' . $image_extension;
							$image_save_name = $image_id . '/' . $image_name . '.' . $image_extension;
							$image_file_2x = $image_dir . $image_full_name_2x;
							$image_save_name_2x = $image_id . '/' . $image_full_name_2x;

							if ($original_image_size->getWidth() < 400 || $original_image_size->getHeight() < 400) {
								continue;
							}

							$resized_image_2x = null;

                            if ($original_image_size->getWidth() >= 800 && $original_image_size->getHeight() >= 800) {
                                $resized_image_2x = $image_obj->thumbnail(new Imagine\Image\Box(544, 544), $mode_outbound)
                                    ->save($image_file_2x, array('quality' => 80));
                            }

							$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(400, 400), $mode_outbound)
								->save($image_file, array('quality' => 80));

							$image_files = array(
								'1x' => array(
									'path' => $image_save_name,
									'size' => array(
										'w' => $resized_image->getSize()->getWidth(),
										'h' => $resized_image->getSize()->getHeight(),
									),
								),
								'2x' => empty($resized_image_2x) ? null : array(
									'path' => $image_save_name_2x,
									'size' => array(
										'w' => $resized_image_2x->getSize()->getWidth(),
										'h' => $resized_image_2x->getSize()->getHeight(),
									),
								),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						} elseif ($attribute == 'store_photo') {
							$image_files = array();

							$image_file_original = $image_dir . $image_name . '_o.' . $image_extension; // original
							$image_save_name_original = $image_id . '/' . $image_name . '_o.' . $image_extension;

							$image_file_details = $image_dir . $image_name . '_d.' . $image_extension; // details
							$image_save_name_details = $image_id . '/' . $image_name . '_d.' . $image_extension;

							$image_file_list = $image_dir . $image_name . '_s.' . $image_extension; // list
							$image_save_name_list = $image_id . '/' . $image_name . '_s.' . $image_extension;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							if ($original_image_size->getWidth() < 270 || $original_image_size->getHeight() < 326) {
								continue;
							}

							$resized_image = $image_obj->save($image_file_original, array('quality' => 80));

							$image_files['original'] = array(
								'path' => $image_save_name_original,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// details image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 740) {
								$resized_image = $image_obj->resize($original_image_size->widen(740))
									->save($image_file_details, array('quality' => 80));	
							} else {
								$resized_image = $image_obj->resize($original_image_size->widen(370))
									->save($image_file_details, array('quality' => 80));
							}

							$image_files['details'] = array(
								'path' => $image_save_name_details,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// list image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 540 && $original_image_size->getHeight() > 652) {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(540, 652), $mode_outbound)
									->save($image_file_list, array('quality' => 80));
							} else {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(270, 326), $mode_outbound)
									->save($image_file_list, array('quality' => 80));
							}

							$image_files['list'] = array(
								'path' => $image_save_name_list,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							if (is_file($image_file_original) && is_file($image_file_details) && is_file($image_file_list)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						} else {
							$image_file = $image_dir . $image_name;
							$image_save_name = $image_id . '/' . $image_name;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							switch ($attribute) {
								case 'store_photo':
									if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
										$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_outbound)
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));	
									}
									break;
							}

							$image_size = array(
								'w' => $resized_image->getSize()->getWidth(),
								'h' => $resized_image->getSize()->getHeight(),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode(array_merge(
									array(
										'file' => $image_save_name,
									),
									$image_size
								));
							}
							else {
								// remove resized files
								if (is_file($image_file)) {
									unlink($image_file);
								}
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_store = array(
				'saved' => $today,
			);

			$update_store = array_merge($update_store, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "store_id = :store_id" , 
					"params" => array(
						"store_id" => $model->store_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('store', $update_store, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	public function setPosition($store_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_store = array(
			'saved' => $today,
			'store_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "store_id = :store_id" , 
				"params" => array(
					"store_id" => $store_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('store', $update_store, $update_criteria)->execute();
			
			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function toggle($store_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_store = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "store_id = :store_id" , 
				"params" => array(
					"store_id" => $store_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('store', $update_store, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($store_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$store = $this->getStoreByIdAdmin($store_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "store_id = :store_id" , 
				"params" => array(
					"store_id" => $store_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('store', $delete_criteria)->execute();

			if ($rs) {
				// remove store directory
				if (is_dir($assetPath . DS . 'store' . DS . $store_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'store' . DS . $store_id);
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}