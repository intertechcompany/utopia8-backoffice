<?php
class Tag extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function getTagsAdminTotal($per_page = 10)
    {
        $func_args = func_get_args();

        if (!empty($func_args[1])) {
            $tag_id = (int) $func_args[1];
            $tag_name = addcslashes($func_args[1], '%_');

            $total_tags = Yii::app()->db
                ->createCommand("SELECT COUNT(*)
                                 FROM tag as t
                                 JOIN tag_lang as tl
                                 ON t.tag_id = tl.tag_id AND tl.language_code = :code
                                 WHERE t.tag_id = :id OR tl.tag_name LIKE :tag_name")
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->bindValue(':id', $tag_id, PDO::PARAM_INT)
                ->bindValue(':tag_name', '%' . $tag_name . '%', PDO::PARAM_STR)
                ->queryScalar();
        }
        else {
            $total_tags = Yii::app()->db
                ->createCommand("SELECT COUNT(*)
                                 FROM tag as t
                                 JOIN tag_lang as tl
                                 ON t.tag_id = tl.tag_id AND tl.language_code = :code")
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->queryScalar();
        }

        return array(
            'total' => (int) $total_tags,
            'pages' => ceil($total_tags / $per_page),
        );
    }

    public function getTagsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
    {
        switch ($sort) {
            case 'tag_id':
                $order_by = ($direction == 'asc') ? 't.tag_id' : 't.tag_id DESC';
                break;
            case 'tag_name':
                $order_by = ($direction == 'asc') ? 'tl.tag_name' : 'tl.tag_name DESC';
                break;
            default:
                $order_by = 't.tag_id DESC';
        }

        $func_args = func_get_args();

        if (!empty($func_args[4])) {
            $tag_id = (int) $func_args[4];
            $tag_name = addcslashes($func_args[4], '%_');

            $tags = Yii::app()->db
                ->createCommand("SELECT t.tag_id, tl.tag_name
                                FROM tag as t
                                JOIN tag_lang as tl
                                ON t.tag_id = tl.tag_id AND tl.language_code = :code
                                WHERE t.tag_id = :id OR tl.tag_name LIKE :tag_name
                                ORDER BY " . $order_by . "
                                LIMIT ".$offset.",".$per_page)
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->bindValue(':id', $tag_id, PDO::PARAM_INT)
                ->bindValue(':tag_name', '%' . $tag_name . '%', PDO::PARAM_STR)
                ->queryAll();
        }
        else {
            $tags = Yii::app()->db
                ->createCommand("SELECT t.tag_id, tl.tag_name
                                FROM tag as t
                                JOIN tag_lang as tl
                                ON t.tag_id = tl.tag_id AND tl.language_code = :code
                                ORDER BY " . $order_by . "
                                LIMIT ".$offset.",".$per_page)
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->queryAll();
        }

        return $tags;
    }

    public function getTagsListAdmin()
	{
		$tags = Yii::app()->db
			->createCommand("SELECT t.tag_id, tl.tag_name FROM tag as t JOIN tag_lang as tl ON t.tag_id = tl.tag_id AND tl.language_code = :code ORDER BY tl.tag_name")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		return $tags;
	}

    public function getTagByIdAdmin($id)
    {
        $tag = Yii::app()->db
            ->createCommand("SELECT * FROM tag WHERE tag_id = :id LIMIT 1")
            ->bindValue(':id', (int) $id, PDO::PARAM_INT)
            ->queryRow();

        if (!empty($tag)) {
            // tag langs
            $tag_langs = Yii::app()->db
                ->createCommand("SELECT * FROM tag_lang WHERE tag_id = :id")
                ->bindValue(':id', (int) $id, PDO::PARAM_INT)
                ->queryAll();

            if (!empty($tag_langs)) {
                foreach ($tag_langs as $tag_lang) {
                    $code = $tag_lang['language_code'];

                    if (isset(Yii::app()->params->langs[$code])) {
                        $tag[$code] = $tag_lang;
                    }
                }
            }
        }

        return $tag;
    }

    public function issetTagByAlias($tag_id, $tag_alias)
    {
        if (!empty($tag_id)) {
            $isset = (bool) Yii::app()->db
                ->createCommand("SELECT COUNT(*) FROM tag WHERE tag_id != :tag_id AND tag_alias LIKE :alias")
                ->bindValue(':tag_id', (int) $tag_id, PDO::PARAM_INT)
                ->bindValue(':alias', $tag_alias, PDO::PARAM_STR)
                ->queryScalar();
        }
        else {
            $isset = (bool) Yii::app()->db
                ->createCommand("SELECT COUNT(*) FROM tag WHERE tag_alias LIKE :alias")
                ->bindValue(':alias', $tag_alias, PDO::PARAM_STR)
                ->queryScalar();
        }

        return $isset;
    }

    public function save($model, $model_lang)
    {
        // import URLify library
        Yii::import('application.vendor.URLify.URLify');

        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        // skip unnecessary attributes
        $skip_attributes = array(
            'tag_id',
            'tag_image',
        );

        // integer attributes
        $int_attributes = array();

        // date attributes
        $date_attributes = array();

        // delete attributes
        $del_attributes = array(
            'del_tag_image',
        );

        // photos attributes
        $save_images = array(
            'tag_image',
        );

        $skip_attributes = array_merge($skip_attributes, $del_attributes);

        // get not empty title
        foreach (Yii::app()->params->langs as $language_code => $language_name) {
            if (!empty($model_lang->tag_name[$language_code])) {
                $tag_name = $model_lang->tag_name[$language_code];
                break;
            }
        }

        // get alias
        $model->tag_alias = empty($model->tag_alias) ? URLify::filter($tag_name, 200) : URLify::filter($model->tag_alias, 200);

        while ($this->issetTagByAlias($model->tag_id, $model->tag_alias)) {
            $model->tag_alias = $model->tag_alias . '-' . uniqid();
        }

        if (empty($model->tag_id)) {
            // insert tag
            $insert_tag = array(
                'created' => $today,
                'saved' => $today,
            );

            foreach ($model as $field => $value) {
                if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
                    continue;
                }
                elseif (in_array($field, $int_attributes)) {
                    $insert_tag[$field] = (int) $value;
                }
                elseif (in_array($field, $date_attributes)) {
                    if (empty($value)) {
                        $insert_tag[$field] = '0000-00-00';
                    }
                    else {
                        $date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
                        $insert_tag[$field] = $date->format('Y-m-d');
                    }
                }
                else {
                    $insert_tag[$field] = $value;
                }
            }

            try {
                $rs = $builder->createInsertCommand('tag', $insert_tag)->execute();

                if ($rs) {
                    $model->tag_id = (int) Yii::app()->db->getLastInsertID();

                    $int_attributes = array();

                    foreach (Yii::app()->params->langs as $language_code => $language_name) {
                        // save details
                        $insert_tag_lang = array(
                            'tag_id' => $model->tag_id,
                            'language_code' => $language_code,
                            'tag_visible' => !empty($model_lang->tag_name[$language_code]) ? 1 : 0,
                            'created' => $today,
                            'saved' => $today,
                        );

                        foreach ($model_lang->attributes as $field => $value) {
                            if (!is_array($value) || !isset($value[$language_code])) {
                                // skip non multilang fields
                                continue;
                            }
                            elseif (in_array($field, $int_attributes)) {
                                $insert_tag_lang[$field] = (int) $value[$language_code];
                            }
                            else {
                                $insert_tag_lang[$field] = trim($value[$language_code]);
                            }
                        }

                        $rs = $builder->createInsertCommand('tag_lang', $insert_tag_lang)->execute();

                        if (!$rs) {
                            $delete_criteria = new CDbCriteria(
                                array(
                                    "condition" => "tag_id = :tag_id" ,
                                    "params" => array(
                                        "tag_id" => $model->tag_id,
                                    )
                                )
                            );

                            $builder->createDeleteCommand('tag', $delete_criteria)->execute();

                            return false;
                        }
                    }

                    // save photos
					$this->savePhotos($model, $save_images);

                    return true;
                }
            }
            catch (CDbException $e) {
                // ...
            }
        }
        else {
            $update_tag = array(
                'saved' => $today,
            );

            foreach ($model as $field => $value) {
                if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
                    continue;
                }
                elseif (in_array($field, $int_attributes)) {
                    $update_tag[$field] = (int) $value;
                }
                elseif (in_array($field, $date_attributes)) {
                    if (empty($value)) {
                        $update_tag[$field] = '0000-00-00';
                    }
                    else {
                        $date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
                        $update_tag[$field] = $date->format('Y-m-d');
                    }
                }
                else {
                    $update_tag[$field] = $value;
                }
            }

            foreach ($del_attributes as $del_attribute) {
                if (!empty($model->$del_attribute)) {
                    $del_attribute = str_replace('del_', '', $del_attribute);
                    $update_tag[$del_attribute] = '';
                }
            }

            $update_criteria = new CDbCriteria(
                array(
                    "condition" => "tag_id = :tag_id" ,
                    "params" => array(
                        "tag_id" => $model->tag_id,
                    )
                )
            );

            try {
                $rs = $builder->createUpdateCommand('tag', $update_tag, $update_criteria)->execute();

                if ($rs) {
                    $int_attributes = array();

                    foreach (Yii::app()->params->langs as $language_code => $language_name) {
                        // save details
                        $update_tag_lang = array(
                            'tag_visible' => !empty($model_lang->tag_name[$language_code]) ? 1 : 0,
                            'saved' => $today,
                        );

                        foreach ($model_lang->attributes as $field => $value) {
                            if (!is_array($value) || !isset($value[$language_code])) {
                                // skip non multilang fields
                                continue;
                            }
                            elseif (in_array($field, $int_attributes)) {
                                $update_tag_lang[$field] = (int) $value[$language_code];
                            }
                            else {
                                $update_tag_lang[$field] = trim($value[$language_code]);
                            }
                        }

                        $update_lang_criteria = new CDbCriteria(
                            array(
                                "condition" => "tag_id = :tag_id AND language_code = :lang" ,
                                "params" => array(
                                    "tag_id" => (int) $model->tag_id,
                                    "lang" => $language_code,
                                )
                            )
                        );

                        $rs = $builder->createUpdateCommand('tag_lang', $update_tag_lang, $update_lang_criteria)->execute();

                        if (!$rs) {
                            return false;
                        }
                    }

                    // save photos
					$this->savePhotos($model, $save_images);

                    return true;
                }
            }
            catch (CDbException $e) {
                // ...
            }
        }

        return false;
    }

    private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_name = strtolower(str_replace('-', '_', URLify::filter($model->$attribute->getName(), 60, 'ru', true)));
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'tag' . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;
						
						$image_file = $image_dir . $image_name;
						$image_save_name = $image_id . '/' . $image_name;

						// resize file
						$image_obj = $imagine->open($image_path);
						$original_image_size = $image_obj->getSize();

						switch ($attribute) {
							case 'tag_image':
                                /* if ($model->tag_place == 'slider') {
                                    if ($original_image_size->getWidth() > 1110 && $original_image_size->getHeight() > 584) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(1110, 584), $mode_outbound)
                                        ->save($image_file, array('quality' => 80));
                                    } else {
                                        $resized_image = $image_obj->save($image_file, array('quality' => 80));
                                    }
                                } else {
									if ($model->tag_type == 'horizontal' && $original_image_size->getWidth() > 1110) {
										$resized_image = $image_obj->resize($original_image_size->widen(1110))
											->save($image_file, array('quality' => 80));
									} elseif ($model->tag_type == 'vertical' && $original_image_size->getWidth() > 535) {
										$resized_image = $image_obj->resize($original_image_size->widen(535))
											->save($image_file, array('quality' => 80));
									} else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									}
								} */
								if ($original_image_size->getWidth() > 48 && $original_image_size->getHeight() > 48) {
									$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(48, 48), $mode_outbound)
									->save($image_file, array('quality' => 80));
								} else {
									$resized_image = $image_obj->save($image_file, array('quality' => 80));
								}
								break;
								/*
								if ($original_image_size->getWidth() > $original_image_size->getHeight()) {
									if ($original_image_size->getWidth() > 360) {
										$resized_image = $image_obj->resize($original_image_size->widen(360))
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									}
								}
								else {
									if ($original_image_size->getHeight() > 200) {
										$resized_image = $image_obj->resize($original_image_size->heighten(200))
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));
									}
								}
								*/
						}

						$image_size = array(
							'w' => $resized_image->getSize()->getWidth(),
							'h' => $resized_image->getSize()->getHeight(),
						);

						if (is_file($image_file)) {
							$save_images_rs[$attribute] = json_encode(array_merge(
								array(
									'file' => $image_save_name,
								),
								$image_size
							));
						}
						else {
							// remove resized files
							if (is_file($image_file)) {
								unlink($image_file);
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_tag = array(
				'saved' => $today,
			);

			$update_tag = array_merge($update_tag, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "tag_id = :tag_id" , 
					"params" => array(
						"tag_id" => $model->tag_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('tag', $update_tag, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

    public function delete($tag_id)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $assetPath = Yii::app()->assetManager->basePath;

        $tag = $this->getTagByIdAdmin($tag_id);

        $delete_criteria = new CDbCriteria(
            array(
                "condition" => "tag_id = :tag_id" ,
                "params" => array(
                    "tag_id" => $tag_id,
                )
            )
        );

        try {
            $rs = $builder->createDeleteCommand('tag', $delete_criteria)->execute();

            if ($rs) {
                // delete related tables
                $builder->createDeleteCommand('tag_lang', $delete_criteria)->execute();

                // remove tag logo
				if (!empty($tag['tag_logo'])) {
					$photo = json_decode($tag['tag_logo'], true);
				
					if (is_file($assetPath . DS . 'tag' . DS . $photo['file'])) {
						CFileHelper::removeDirectory(dirname($assetPath . DS . 'tag' . DS . $photo['file']));
					}
				}

                return true;
            }
        }
        catch (CDbException $e) {
            // ...
        }

        return false;
    }
}
