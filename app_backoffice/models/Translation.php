<?php
class Translation extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getTranslationsAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		$search_sql = array();
		$bind_sql = array();

		if (!empty($func_args[1])) {
			$translation_id = (int) $func_args[1];
			$translation_code = trim($func_args[1]);
			$translation_value = addcslashes($func_args[1], '%_');

			$search_sql[] = '(t.translation_id = :id OR t.translation_code = :translation_code OR tl.translation_value LIKE :translation_value)';
			
			$bind_sql[] = array(
				'param' => ':id',
				'value' => $translation_id,
				'type'  => PDO::PARAM_INT,
			);

			$bind_sql[] = array(
				'param' => ':translation_code',
				'value' => $translation_code,
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':translation_value',
				'value' => '%' . $translation_value . '%',
				'type'  => PDO::PARAM_STR,
			);
		}

		if (!empty($func_args[2])) {
			$translation_group = trim($func_args[2]);

			$search_sql[] = '(t.translation_group = :group)';

			$bind_sql[] = array(
				'param' => ':group',
				'value' => $translation_group,
				'type'  => PDO::PARAM_STR,
			);
		}

		if (!empty($search_sql)) {
			$total_translations_db = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM translation as t JOIN translation_lang as tl ON t.translation_id = tl.translation_id AND tl.language_code = :code WHERE " . implode(' AND ', $search_sql))
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR);

			foreach ($bind_sql as $bind) {
				$total_translations_db->bindValue($bind['param'], $bind['value'], $bind['type']);
			}
			
			$total_translations = $total_translations_db->queryScalar();
		}
		else {
			$total_translations = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM translation as t JOIN translation_lang as tl ON t.translation_id = tl.translation_id AND tl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_translations,
			'pages' => ceil($total_translations / $per_page),
		);
	}

	public function getTranslationsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'translation_id':
				$order_by = ($direction == 'asc') ? 't.translation_id' : 't.translation_id DESC';
				break;
			case 'translation_code':
				$order_by = ($direction == 'asc') ? 't.translation_code' : 't.translation_code DESC';
				break;
			default:
				$order_by = 't.translation_id';
		}

		$func_args = func_get_args();

		$search_sql = array();
		$bind_sql = array();

		if (!empty($func_args[4])) {
			$translation_id = (int) $func_args[4];
			$translation_code = trim($func_args[4]);
			$translation_value = addcslashes($func_args[4], '%_');

			$search_sql[] = '(t.translation_id = :id OR t.translation_code = :translation_code OR tl.translation_value LIKE :translation_value)';
			
			$bind_sql[] = array(
				'param' => ':id',
				'value' => $translation_id,
				'type'  => PDO::PARAM_INT,
			);

			$bind_sql[] = array(
				'param' => ':translation_code',
				'value' => $translation_code,
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':translation_value',
				'value' => '%' . $translation_value . '%',
				'type'  => PDO::PARAM_STR,
			);
		}

		if (!empty($func_args[5])) {
			$translation_group = trim($func_args[5]);

			$search_sql[] = '(t.translation_group = :group)';

			$bind_sql[] = array(
				'param' => ':group',
				'value' => $translation_group,
				'type'  => PDO::PARAM_STR,
			);
		}

		if (!empty($search_sql)) {
			$translations_db = Yii::app()->db
				->createCommand("SELECT t.translation_id, t.translation_code, tl.translation_value FROM translation as t JOIN translation_lang as tl ON t.translation_id = tl.translation_id AND tl.language_code = :code WHERE " . implode(' AND ', $search_sql) . " ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR);
			
			foreach ($bind_sql as $bind) {
				$translations_db->bindValue($bind['param'], $bind['value'], $bind['type']);
			}
			
			$translations = $translations_db->queryAll();
		}
		else {
			$translations = Yii::app()->db
				->createCommand("SELECT t.translation_id, t.translation_code, tl.translation_value FROM translation as t JOIN translation_lang as tl ON t.translation_id = tl.translation_id AND tl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $translations;
	}
	
	public function getTranslationGroups()
	{
		$groups = Yii::app()->db
			->createCommand("SELECT translation_group FROM translation GROUP BY translation_group ORDER BY translation_group")
			->queryColumn();

		return $groups;
	}

	public function getTranslationByIdAdmin($id)
	{
		$translation = Yii::app()->db
			->createCommand("SELECT * FROM translation WHERE translation_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($translation)) {
			// translation langs
			$translation_langs = Yii::app()->db
				->createCommand("SELECT * FROM translation_lang WHERE translation_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($translation_langs)) {
				foreach ($translation_langs as $translation_lang) {
					$code = $translation_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$translation[$code] = $translation_lang;
					}
				}
			}
		}
			
		return $translation;
	}

	public function getTranslationByCode($code, $lang)
	{
		$translation = Yii::app()->db
			->createCommand("SELECT t.translation_id, t.translation_type, t.translation_code, tl.language_code, tl.translation_value FROM translation as t JOIN translation_lang as tl ON t.translation_id = tl.translation_id AND tl.language_code = :lang WHERE t.translation_code = :code")
			->bindValue(':lang', $lang, PDO::PARAM_STR)
			->bindValue(':code', $code, PDO::PARAM_STR)
			->queryRow();
			
		return $translation;
	}

    public function issetTranslationByCode($translation_id, $translation_code)
	{
		if (!empty($translation_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM translation WHERE translation_id != :translation_id AND translation_code LIKE :translation_code")
				->bindValue(':translation_id', (int) $translation_id, PDO::PARAM_INT)
				->bindValue(':translation_code', $translation_code, PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM translation WHERE translation_code LIKE :translation_code")
				->bindValue(':translation_code', $translation_code, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function save($model, $model_lang)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'translation_id',
		);

		// integer attributes
		$int_attributes = array(
			'translation_type',
		);

		// date attributes
		$date_attributes = array();

		// get not empty title
		foreach (Yii::app()->params->langs as $language_code => $language_name) {
			if (!empty($model_lang->translation_value[$language_code])) {
				$translation_value = $model_lang->translation_value[$language_code];
				break;
			}
		}

		// get code
		$model->translation_code = URLify::downcode($model->translation_code);
		$model->translation_code = preg_replace('/[^\-.\w\s]/u', '', $model->translation_code);  // remove unneeded chars
		$model->translation_code = preg_replace('/^\s+|\s+$/', '', $model->translation_code);   // trim leading/trailing spaces
		$model->translation_code = preg_replace('/[_\-\s\.]+/', '.', $model->translation_code);  // convert spaces to dots
		$model->translation_code = trim($model->translation_code, '.');

		while ($this->issetTranslationByCode($model->translation_id, $model->translation_code)) {
			$model->translation_code = $model->translation_code . '.' . uniqid();
		}

		list($translation_group) = explode('.', $model->translation_code);
		$model->translation_group = $translation_group;

		if (empty($model->translation_id)) {
			// insert translation
			$insert_translation = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_translation[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_translation[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_translation[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_translation[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('translation', $insert_translation)->execute();

				if ($rs) {
					$model->translation_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_translation_lang = array(
							'translation_id' => $model->translation_id,
							'language_code' => $language_code,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_translation_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_translation_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('translation_lang', $insert_translation_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "translation_id = :translation_id" , 
									"params" => array(
										"translation_id" => $model->translation_id,
									)
								)
							);
							
							$builder->createDeleteCommand('translation', $delete_criteria)->execute();

							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_translation = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_translation[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_translation[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_translation[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_translation[$field] = $value;
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "translation_id = :translation_id" , 
					"params" => array(
						"translation_id" => $model->translation_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('translation', $update_translation, $update_criteria)->execute();

				if ($rs) {
					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_translation_lang = array(
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_translation_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_translation_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "translation_id = :translation_id AND language_code = :lang" , 
								"params" => array(
									"translation_id" => (int) $model->translation_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('translation_lang', $update_translation_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function warmUpCache()
	{
		$builder = Yii::app()->db->schema->commandBuilder;

		Yii::app()->cache->delete('_t');

		$lang_codes = array();
		$translations_cache = array();
		$js_translation = array();

		// get all translations
		$translations = Yii::app()->db
			->createCommand("SELECT t.translation_id, t.translation_code, tl.language_code, tl.translation_value FROM translation as t JOIN translation_lang as tl ON t.translation_id = tl.translation_id ORDER BY t.translation_id")
			->queryAll();

		if (!empty($translations)) {
			foreach ($translations as $translation) {
				$code = $translation['translation_code'];
				$lang = $translation['language_code'];

				$lang_codes[$lang] = $lang;

				$translations_cache[$code][$lang] = $translation['translation_value'];

				// add js value
				if (!isset($js_translation[$lang])) {
					$js_translation[$lang] = array();
				}

				list($type, $group, $variable) = array_pad(explode('.', $code), 3, '');

				if ($type == 'javaScript') {
					if (!empty($variable)) {
						$js_translation[$lang][$group][$variable] = $translation['translation_value'];
					}
					else {
						$js_translation[$lang][$group] = $translation['translation_value'];
					}
				}
			}
		}

		Yii::app()->cache->set('_t', $translations_cache);

		$lang_path = Yii::app()->assetManager->getBasePath() . DS . 'static' . DS . 'lang';

		// prepare js
		$new_js = array();

		foreach ($lang_codes as $lang_code) {
			/*
			$json_data = array(
				'mfp_close' => isset($translations_cache['javaScript.magnificPopup.close'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.close'][$lang_code] : '',
				'mfp_loading' => isset($translations_cache['javaScript.magnificPopup.loading'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.loading'][$lang_code] : '',
				'mfp_prev' => isset($translations_cache['javaScript.magnificPopup.prev'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.prev'][$lang_code] : '',
				'mfp_next' => isset($translations_cache['javaScript.magnificPopup.next'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.next'][$lang_code] : '',
				'mfp_counter' => isset($translations_cache['javaScript.magnificPopup.counter'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.counter'][$lang_code] : '',
				'mfp_image_err' => isset($translations_cache['javaScript.magnificPopup.imageError'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.imageError'][$lang_code] : '',
				'mfp_ajax_err' => isset($translations_cache['javaScript.magnificPopup.ajaxError'][$lang_code]) ? $translations_cache['javaScript.magnificPopup.ajaxError'][$lang_code] : '',
				'pswp_close' => isset($translations_cache['javaScript.photoSwipe.close'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.close'][$lang_code] : '',
				'pswp_fullscreen' => isset($translations_cache['javaScript.photoSwipe.fullscreen'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.fullscreen'][$lang_code] : '',
				'pswp_zoom' => isset($translations_cache['javaScript.photoSwipe.zoom'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.zoom'][$lang_code] : '',
				'pswp_prev' => isset($translations_cache['javaScript.photoSwipe.prev'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.prev'][$lang_code] : '',
				'pswp_next' => isset($translations_cache['javaScript.photoSwipe.next'][$lang_code]) ? $translations_cache['javaScript.photoSwipe.next'][$lang_code] : '',
				'std_ajax_err' => isset($translations_cache['javaScript.ajaxError'][$lang_code]) ? $translations_cache['javaScript.ajaxError'][$lang_code] : '',
				'std_page_load_err' => isset($translations_cache['javaScript.pageLoadError'][$lang_code]) ? $translations_cache['javaScript.pageLoadError'][$lang_code] : '',
			);
			*/
			
			$new_js[$lang_code] = 'var _t = ' . json_encode($js_translation[$lang_code]) . ';';
		}

		$rebuild_js = false;

		// get current version of js translations
		$js_ver = Yii::app()->params->settings['js_rev'];

		if (!empty($js_ver)) {
			foreach ($lang_codes as $lang_code) {
				if (!is_file($lang_path . DS . $js_ver . DS . $lang_code . '.js')) {
					$rebuild_js = true;
					break;
				}

				// read file
				$old_js = file_get_contents($lang_path . DS . $js_ver . DS . $lang_code . '.js');

				if (strcmp($old_js, $new_js[$lang_code]) !== 0) {
					$rebuild_js = true;
					break;
				}
			}
		}
		else {
			$rebuild_js = true;
		}

		if ($rebuild_js) {
			// remove old files
			if (is_dir($lang_path . DS . $js_ver)) {
				CFileHelper::removeDirectory($lang_path . DS . $js_ver);
			}

			// rebuild js lang files
			
			// digits
			$digits = range(0, 9);
			shuffle($digits);
			
			// small letters
			$lower = range('a', 'z');
			shuffle($lower);

			// caption letters
			$upper = range('A', 'Z');
			shuffle($upper);
			
			$chars = array_merge(array_slice($lower, 0, 4), array_slice($digits, 0, 2), array_slice($upper, 0, 2));
			shuffle($chars);
			$new_js_hash = implode($chars);

			$dir_rs = CFileHelper::createDirectory($lang_path . DS . $new_js_hash, 0755, true);

			if ($dir_rs) {
				// write to files
				foreach ($lang_codes as $lang_code) {
					file_put_contents($lang_path . DS . $new_js_hash . DS . $lang_code . '.js', $new_js[$lang_code]);
				}

				$update_setting = array(
					'saved' => date('Y-m-d H:i:s'),
					'setting_js_rev' => $new_js_hash,
				);

				$update_criteria = new CDbCriteria(
					array(
						"condition" => "setting_id = :setting_id" , 
						"params" => array(
							"setting_id" => 1,
						)
					)
				);

				try {
					$builder->createUpdateCommand('setting', $update_setting, $update_criteria)->execute();
				}
				catch (CDbException $e) {
					// ...
				}
			}
		}
	}

	public function delete($translation_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$translation = $this->getTranslationByIdAdmin($translation_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "translation_id = :translation_id" , 
				"params" => array(
					"translation_id" => $translation_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('translation', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('translation_lang', $delete_criteria)->execute();

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}