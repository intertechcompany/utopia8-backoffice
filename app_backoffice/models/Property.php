<?php
class Property extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getPropertiesAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$property_id = (int) $func_args[1];
			$property_title = addcslashes($func_args[1], '%_');

			$total_properties = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM property as p JOIN property_lang as pl ON p.property_id = pl.property_id AND pl.language_code = :code WHERE p.property_id = :id OR pl.property_title LIKE :property_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $property_id, PDO::PARAM_INT)
				->bindValue(':property_title', '%' . $property_title . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_properties = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM property as p JOIN property_lang as pl ON p.property_id = pl.property_id AND pl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_properties,
			'pages' => ceil($total_properties / $per_page),
		);
	}

	public function getPropertiesAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'property_id':
				$order_by = ($direction == 'asc') ? 'p.property_id' : 'p.property_id DESC';
				break;
			case 'property_title':
				$order_by = ($direction == 'asc') ? 'pl.property_title' : 'pl.property_title DESC';
				break;
			default:
				$order_by = 'p.property_top DESC, pl.property_title';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$property_id = (int) $func_args[4];
			$property_title = addcslashes($func_args[4], '%_');

			$properties = Yii::app()->db
				->createCommand("SELECT p.*, pl.property_title FROM property as p JOIN property_lang as pl ON p.property_id = pl.property_id AND pl.language_code = :code WHERE p.property_id = :id OR pl.property_title LIKE :property_title ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $property_id, PDO::PARAM_INT)
				->bindValue(':property_title', '%' . $property_title . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$properties = Yii::app()->db
				->createCommand("SELECT p.*, pl.property_title FROM property as p JOIN property_lang as pl ON p.property_id = pl.property_id AND pl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}

		if (!empty($properties)) {
			foreach ($properties as $id => $property) {
				$properties[$id]['categories'] = $this->getPropertyCategories($property['property_id']);
			}
		}
			
		return $properties;
	}

	public function getPropertyByIdAdmin($id)
	{
		$property = Yii::app()->db
			->createCommand("SELECT * FROM property WHERE property_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($property)) {
			// property langs
			$property_langs = Yii::app()->db
				->createCommand("SELECT * FROM property_lang WHERE property_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($property_langs)) {
				foreach ($property_langs as $property_lang) {
					$code = $property_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$property[$code] = $property_lang;
					}
				}
			}
		}
			
		return $property;
	}

	public function getPropertiesListAdmin($category_id)
	{
		$properties = array();

		$properties_list = Yii::app()->db
			->createCommand("SELECT p.*, pl.property_title 
							 FROM property_category as pc 
							 JOIN property as p 
							 ON pc.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 WHERE pc.category_id = :category_id 
							 ORDER BY p.property_top DESC, pl.property_title")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($properties_list)) {
			foreach ($properties_list as $property) {
				$property_id = $property['property_id'];
				
				$properties[$property_id] = $property;
				$properties[$property_id]['values'] = array();
			}

			$properties_ids = array_column($properties_list, 'property_id');
			$values = PropertyValue::model()->getValuesListAdmin($properties_ids);

			if (!empty($values)) {
				foreach ($values as $value) {
					$property_id = $value['property_id'];
					$value_id = $value['value_id'];

					if (isset($properties[$property_id])) {
						$properties[$property_id]['values'][$value_id] = $value;
					}
				}
			}
		}

		return $properties;
	}

	public function getPropertiesMap()
	{
		$properties = array();
		$properties_map = array();

		$properties_list = Yii::app()->db
			->createCommand("SELECT p.property_id, p.property_code, pl.property_title 
							 FROM property as p 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 ORDER BY p.property_id")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		if (!empty($properties_list)) {
			foreach ($properties_list as $property) {
				$property_id = $property['property_id'];
				$property_code = $property['property_code'];
				
				$properties[$property_id] = $property_code;
				$properties_map[$property_code] = $property;
				$properties_map[$property_code]['values'] = array();
			}

			$properties_ids = array_column($properties_list, 'property_id');
			$values = PropertyValue::model()->getValuesListAdmin($properties_ids);

			if (!empty($values)) {
				foreach ($values as $value) {
					$property_id = $value['property_id'];
					$value_code = empty($value['value_code']) ? $value['value_title'] : $value['value_code'];

					if (isset($properties[$property_id])) {
						$propery_code = $properties[$property_id];
						$properties_map[$propery_code]['values'][$value_code] = $value['value_id'];
					}
				}
			}
		}

		return $properties_map;
	}

	public function getPropertiesVariants($category_id)
	{
		$properties = array();

		$properties_list = Yii::app()->db
			->createCommand("SELECT p.property_id, pl.property_title 
							 FROM property_category as pc 
							 JOIN property as p 
							 ON pc.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 WHERE pc.category_id = :category_id AND p.property_variant = 1 
							 ORDER BY p.property_top DESC, pl.property_title")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($properties_list)) {
			foreach ($properties_list as $property) {
				$property_id = $property['property_id'];
				
				$properties[$property_id] = $property;
				$properties[$property_id]['values'] = array();
			}

			$properties_ids = array_column($properties_list, 'property_id');
			$values = PropertyValue::model()->getValuesListAdmin($properties_ids);

			if (!empty($values)) {
				foreach ($values as $value) {
					$property_id = $value['property_id'];
					$value_id = $value['value_id'];

					if (isset($properties[$property_id])) {
						$properties[$property_id]['values'][$value_id] = $value;
					}
				}
			}
		}

		return $properties;
	}

	public function getPropertiesOptions($category_id)
	{
		$properties = array();

		$properties_list = Yii::app()->db
			->createCommand("SELECT p.property_id, pl.property_title 
							 FROM property_category as pc 
							 JOIN property as p 
							 ON pc.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 WHERE pc.category_id = :category_id AND p.property_option = 1 
							 ORDER BY p.property_top DESC, pl.property_title")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($properties_list)) {
			foreach ($properties_list as $property) {
				$property_id = $property['property_id'];
				
				$properties[$property_id] = $property;
				$properties[$property_id]['values'] = array();
			}

			$properties_ids = array_column($properties_list, 'property_id');
			$values = PropertyValue::model()->getValuesListAdmin($properties_ids);

			if (!empty($values)) {
				foreach ($values as $value) {
					$property_id = $value['property_id'];
					$value_id = $value['value_id'];

					if (isset($properties[$property_id])) {
						$properties[$property_id]['values'][$value_id] = $value;
					}
				}
			}
		}

		return $properties;
	}

	public function getPropertyCategories($id)
	{
		$categories = Yii::app()->db
			->createCommand("SELECT category_id FROM property_category WHERE property_id = :id")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryColumn();
			
		return $categories;
	}
	
	public function getFilterCategories($id)
	{
		$categories = Yii::app()->db
			->createCommand("SELECT category_id FROM property_filter WHERE property_id = :id")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryColumn();
			
		return $categories;
	}

	public function add($property)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$insert_property = [
			'created' => $today,
			'saved' => $today,
			'property_code' => $property['code'],
			'property_variant' => 1,
			'property_filter' => 1,
			'property_cart' => 1,
		];
		
		try {
			$rs = $builder->createInsertCommand('property', $insert_property)->execute();

			if ($rs) {
				$property_id = (int) Yii::app()->db->getLastInsertID();
				
				foreach (Yii::app()->params->langs as $language_code => $language_name) {
					// save details
					$insert_property_lang = array(
						'property_id' => $property_id,
						'language_code' => $language_code,
						'property_visible' => 1,
						'created' => $today,
						'saved' => $today,
						'property_title' => $property['name'],
					);

					$rs = $builder->createInsertCommand('property_lang', $insert_property_lang)->execute();

					if (!$rs) {
						$delete_criteria = new CDbCriteria(
							array(
								"condition" => "property_id = :property_id" , 
								"params" => array(
									"property_id" => $property_id,
								)
							)
						);
						
						$builder->createDeleteCommand('property', $delete_criteria)->execute();

						return false;
					}
                }
                
                $property = Yii::app()->db
                    ->createCommand("SELECT p.property_id, p.property_code, pl.property_title 
                                     FROM property as p 
                                     JOIN property_lang as pl 
                                     ON p.property_id = pl.property_id AND pl.language_code = :code 
                                     WHERE p.property_id = :id
                                     LIMIT 1")
                    ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                    ->bindValue(':id', (int) $property_id, PDO::PARAM_INT)
					->queryRow();
					
				$property['values'] = [];

				return $property;
            }
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}
	
	public function saveCategory($property_id, $category_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		
		$category = Yii::app()->db
			->createCommand("SELECT category_id FROM property_category WHERE property_id = :property_id AND category_id = :category_id LIMIT 1")
			->bindValue(':property_id', (int) $property_id, PDO::PARAM_INT)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryRow();

		if (empty($category)) {
			$insert_category = array(
				'property_id' => (int) $property_id,
				'category_id' => (int) $category_id,
			);

			try {
				$builder->createInsertCommand('property_category', $insert_category)->execute();

				return true;
			} catch (CDbException $e) {
				return false;
			}
		}

		return false;
	}

	public function save($model, $model_lang, $categories, $filter_categories)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'property_id',
		);

		// integer attributes
		$int_attributes = array();

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array();

		// photos attributes
		$save_images = array();

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		if (empty($model->property_id)) {
			// insert property
			$insert_property = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_property[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_property[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_property[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_property[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('property', $insert_property)->execute();

				if ($rs) {
					$model->property_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array(
						'property_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_property_lang = array(
							'property_id' => $model->property_id,
							'language_code' => $language_code,
							'property_visible' => !empty($model_lang->property_title[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_property_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_property_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('property_lang', $insert_property_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "property_id = :property_id" , 
									"params" => array(
										"property_id" => $model->property_id,
									)
								)
							);
							
							$builder->createDeleteCommand('property', $delete_criteria)->execute();

							return false;
						}

						// save categories
						$this->saveCategories($model, $categories);
						
						// save filter categories
						$this->saveFilterCategories($model, $filter_categories);
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_property = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_property[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_property[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_property[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_property[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_property[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "property_id = :property_id" , 
					"params" => array(
						"property_id" => $model->property_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('property', $update_property, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'property' . DS . $model->property_id . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array(
						'property_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_property_lang = array(
							'property_visible' => !empty($model_lang->property_title[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'property_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_property_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_property_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "property_id = :property_id AND language_code = :lang" , 
								"params" => array(
									"property_id" => (int) $model->property_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('property_lang', $update_property_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}

						// save categories
						$this->saveCategories($model, $categories);

						// save filter categories
						$this->saveFilterCategories($model, $filter_categories);
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function saveCategories($model, $categories)
	{
		$rs_inserted = 0;
		$model->property_id = (int) $model->property_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete categories
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "property_id = :property_id" , 
				"params" => array(
					"property_id" => $model->property_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('property_category', $delete_criteria)->execute();
		} catch (CDbException $e) {
			// ...
		}

		if (!empty($categories)) {
			$insert_categories = array();

			foreach ($categories as $category) {
				$category = (int) $category;

				if (!empty($category)) {
					$insert_categories[] = array(
						'property_id' => $model->property_id,
						'category_id' => $category,
					);
				}
			}

			if (!empty($insert_categories)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('property_category', $insert_categories)->execute();
				} catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}
	
	private function saveFilterCategories($model, $categories)
	{
		$rs_inserted = 0;
		$model->property_id = (int) $model->property_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete categories
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "property_id = :property_id" , 
				"params" => array(
					"property_id" => $model->property_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('property_filter', $delete_criteria)->execute();
		} catch (CDbException $e) {
			// ...
		}

		if (!empty($categories)) {
			$insert_categories = array();

			foreach ($categories as $category) {
				$category = (int) $category;

				if (!empty($category)) {
					$insert_categories[] = array(
						'property_id' => $model->property_id,
						'category_id' => $category,
					);
				}
			}

			if (!empty($insert_categories)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('property_filter', $insert_categories)->execute();
				} catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	public function delete($property_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$property = $this->getPropertyByIdAdmin($property_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "property_id = :property_id" , 
				"params" => array(
					"property_id" => $property_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('property', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('property_lang', $delete_criteria)->execute();
				$builder->createDeleteCommand('property_category', $delete_criteria)->execute();
				$builder->createDeleteCommand('property_filter', $delete_criteria)->execute();
				$builder->createDeleteCommand('property_product', $delete_criteria)->execute();

				// get property values
				$property_value_model = PropertyValue::model();
				$values = $property_value_model->getValuesListAdmin($property_id);

				if (!empty($values)) {
					foreach ($values as $value) {
						$property_value_model->delete($value['value_id']);
					}
				}

				// remove property directory
				if (is_dir($assetPath . DS . 'property' . DS . $property_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'property' . DS . $property_id);
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}