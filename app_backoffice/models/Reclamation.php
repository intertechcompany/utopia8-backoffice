<?php
class Reclamation extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getReclamationsAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$reclamation_id = (int) $func_args[1];
			$reclamation_name = addcslashes($func_args[1], '%_');

			$total_reclamations = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM reclamation as r JOIN reclamation_lang as rl ON r.reclamation_id = rl.reclamation_id AND rl.language_code = :code WHERE r.reclamation_id = :id OR rl.reclamation_name LIKE :reclamation_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $reclamation_id, PDO::PARAM_INT)
				->bindValue(':reclamation_name', '%' . $reclamation_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_reclamations = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM reclamation as r JOIN reclamation_lang as rl ON r.reclamation_id = rl.reclamation_id AND rl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_reclamations,
			'pages' => ceil($total_reclamations / $per_page),
		);
	}

	public function getReclamationsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'reclamation_id':
				$order_by = ($direction == 'asc') ? 'r.reclamation_id' : 'r.reclamation_id DESC';
				break;
			case 'reclamation_position':
				$order_by = ($direction == 'asc') ? 'r.reclamation_position' : 'r.reclamation_position DESC';
				break;
			case 'reclamation_name':
				$order_by = ($direction == 'asc') ? 'rl.reclamation_name' : 'rl.reclamation_name DESC';
				break;
			default:
				$order_by = 'r.reclamation_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$reclamation_id = (int) $func_args[4];
			$reclamation_name = addcslashes($func_args[4], '%_');

			$reclamations = Yii::app()->db
				->createCommand("SELECT r.reclamation_id, r.reclamation_position, rl.reclamation_name FROM reclamation as r JOIN reclamation_lang as rl ON r.reclamation_id = rl.reclamation_id AND rl.language_code = :code WHERE r.reclamation_id = :id OR rl.reclamation_name LIKE :reclamation_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $reclamation_id, PDO::PARAM_INT)
				->bindValue(':reclamation_name', '%' . $reclamation_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$reclamations = Yii::app()->db
				->createCommand("SELECT r.reclamation_id, r.reclamation_position, rl.reclamation_name FROM reclamation as r JOIN reclamation_lang as rl ON r.reclamation_id = rl.reclamation_id AND rl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $reclamations;
	}

	public function getReclamationsListAdmin()
	{
		$reclamations = Yii::app()->db
			->createCommand("SELECT r.reclamation_id, rl.reclamation_name FROM reclamation as r JOIN reclamation_lang as rl ON r.reclamation_id = rl.reclamation_id AND rl.language_code = :code ORDER BY rl.reclamation_name")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();

		return $reclamations;
	}

	public function getReclamationByIdAdmin($id)
	{
		$reclamation = Yii::app()->db
			->createCommand("SELECT * FROM reclamation WHERE reclamation_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($reclamation)) {
			// reclamation langs
			$reclamation_langs = Yii::app()->db
				->createCommand("SELECT * FROM reclamation_lang WHERE reclamation_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($reclamation_langs)) {
				foreach ($reclamation_langs as $reclamation_lang) {
					$code = $reclamation_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$reclamation[$code] = $reclamation_lang;
					}
				}
			}
		}
			
		return $reclamation;
	}

	public function save($model, $model_lang)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'reclamation_id',
		);

		// integer attributes
		$int_attributes = array(
			'reclamation_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array();

		// photos attributes
		$save_images = array();

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get max page position
		if (empty($model->reclamation_position)) {
			$max_position = Yii::app()->db
				->createCommand("SELECT MAX(reclamation_position) FROM reclamation")
				->queryScalar();

			$model->reclamation_position = $max_position + 1;
		}

		if (empty($model->reclamation_id)) {
			// insert reclamation
			$insert_reclamation = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_reclamation[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_reclamation[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_reclamation[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_reclamation[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('reclamation', $insert_reclamation)->execute();

				if ($rs) {
					$model->reclamation_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_reclamation_lang = array(
							'reclamation_id' => $model->reclamation_id,
							'language_code' => $language_code,
							'reclamation_visible' => !empty($model_lang->reclamation_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_reclamation_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_reclamation_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('reclamation_lang', $insert_reclamation_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "reclamation_id = :reclamation_id" , 
									"params" => array(
										"reclamation_id" => $model->reclamation_id,
									)
								)
							);
							
							$builder->createDeleteCommand('reclamation', $delete_criteria)->execute();

							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_reclamation = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_reclamation[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_reclamation[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_reclamation[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_reclamation[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_reclamation[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "reclamation_id = :reclamation_id" , 
					"params" => array(
						"reclamation_id" => $model->reclamation_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('reclamation', $update_reclamation, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'reclamation' . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array();

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_reclamation_lang = array(
							'reclamation_visible' => !empty($model_lang->reclamation_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_reclamation_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_reclamation_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "reclamation_id = :reclamation_id AND language_code = :lang" , 
								"params" => array(
									"reclamation_id" => (int) $model->reclamation_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('reclamation_lang', $update_reclamation_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function setPosition($reclamation_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_reclamation = array(
			'saved' => $today,
			'reclamation_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "reclamation_id = :reclamation_id" , 
				"params" => array(
					"reclamation_id" => $reclamation_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('reclamation', $update_reclamation, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($reclamation_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$reclamation = $this->getReclamationByIdAdmin($reclamation_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "reclamation_id = :reclamation_id" , 
				"params" => array(
					"reclamation_id" => $reclamation_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('reclamation', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('reclamation_lang', $delete_criteria)->execute();

				// remove reclamation logo
				if (!empty($reclamation['reclamation_logo'])) {
					$photo = json_decode($reclamation['reclamation_logo'], true);
				
					if (is_file($assetPath . DS . 'reclamation' . DS . $photo['file'])) {
						CFileHelper::removeDirectory(dirname($assetPath . DS . 'reclamation' . DS . $photo['file']));
					}
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}