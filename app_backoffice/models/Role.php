<?php
class Role extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getRolesAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$role_id = (int) $func_args[1];
			$role_name = addcslashes($func_args[1], '%_');

			$total_roles = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM role WHERE role_id = :id OR role_name LIKE :role_name")
				->bindValue(':id', $role_id, PDO::PARAM_INT)
				->bindValue(':role_name', '%' . $role_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_roles = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM role")
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_roles,
			'pages' => ceil($total_roles / $per_page),
		);
	}

	public function getRolesAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'role_id':
				$order_by = ($direction == 'asc') ? 'role_id' : 'role_id DESC';
				break;
			case 'role_name':
				$order_by = ($direction == 'asc') ? 'role_name' : 'role_name DESC';
				break;
			default:
				$order_by = 'role_id';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$role_id = (int) $func_args[4];
			$role_name = addcslashes($func_args[4], '%_');

			$roles = Yii::app()->db
				->createCommand("SELECT * FROM role WHERE role_id = :id OR role_name LIKE :role_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':id', $role_id, PDO::PARAM_INT)
				->bindValue(':role_name', '%' . $role_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
            $roles = Yii::app()->db
                ->createCommand("SELECT * FROM role ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
                ->queryAll();
		}
			
		return $roles;
	}

	public function getRoleByIdAdmin($id)
	{
		$role = Yii::app()->db
			->createCommand("SELECT * FROM role WHERE role_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();
			
		return $role;
	}

	public function getRolesListAdmin()
	{
		$roles = Yii::app()->db
			->createCommand("SELECT role_id, role_name FROM role ORDER BY role_name")
			->queryAll();
			
		return $roles;
	}

	public function save($model)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'role_id',
		);

		// integer attributes
		$int_attributes = array();

		// date attributes
		$date_attributes = array();

		if (empty($model->role_id)) {
			// insert role
			$insert_role = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_role[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$insert_role[$field] = $date->format('Y-m-d');
				}
				else {
					$insert_role[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('role', $insert_role)->execute();

				if ($rs) {
					$model->role_id = (int) Yii::app()->db->getLastInsertID();
					
					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_role = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_role[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$update_role[$field] = $date->format('Y-m-d');
				}
				else {
					$update_role[$field] = $value;
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "role_id = :role_id" , 
					"params" => array(
						"role_id" => $model->role_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('role', $update_role, $update_criteria)->execute();

				if ($rs) {
					// update all managers
					Yii::app()->db
						->createCommand("UPDATE manager SET saved = :saved")
						->bindValue(':saved', $today, PDO::PARAM_STR)
						->execute();
					
					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function delete($role_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetRole->basePath;
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "role_id = :role_id" , 
				"params" => array(
					"role_id" => $role_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('role', $delete_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}