<?php
class Page extends CModel
{
	private $imagine;
	private $mode_inset;
	private $mode_outbound;
	
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getPagesAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$page_id = (int) $func_args[1];
			$page_title = addcslashes($func_args[1], '%_');

			$total_pages = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM page as p JOIN page_lang as pl ON p.page_id = pl.page_id AND pl.language_code = :code WHERE p.page_id = :id OR pl.page_title LIKE :page_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $page_id, PDO::PARAM_INT)
				->bindValue(':page_title', '%' . $page_title . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_pages = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM page as p JOIN page_lang as pl ON p.page_id = pl.page_id AND pl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_pages,
			'pages' => ceil($total_pages / $per_page),
		);
	}

	public function getPagesAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'page_id':
				$order_by = ($direction == 'asc') ? 'p.page_id' : 'p.page_id DESC';
				break;
			case 'page_position':
				$order_by = ($direction == 'asc') ? 'p.page_position, FIELD(page_menu, \'top\',\'bottom1\',\'bottom2\',\'bottom3\')' : 'p.page_position DESC, FIELD(page_menu, \'top\',\'bottom1\',\'bottom2\',\'bottom3\')';
				break;
			case 'page_title':
				$order_by = ($direction == 'asc') ? 'pl.page_title' : 'pl.page_title DESC';
				break;
			default:
				$order_by = 'FIELD(page_menu, \'top\',\'bottom1\',\'bottom2\',\'bottom3\'), p.page_position';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$page_id = (int) $func_args[4];
			$page_title = addcslashes($func_args[4], '%_');

			$pages = Yii::app()->db
				->createCommand("SELECT p.page_id, p.active, p.page_menu, p.page_type, p.page_position, pl.page_title FROM page as p JOIN page_lang as pl ON p.page_id = pl.page_id AND pl.language_code = :code WHERE p.page_id = :id OR pl.page_title LIKE :page_title ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $page_id, PDO::PARAM_INT)
				->bindValue(':page_title', '%' . $page_title . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$pages = Yii::app()->db
				->createCommand("SELECT p.page_id, p.active, p.page_menu, p.page_type, p.page_position, pl.page_title FROM page as p JOIN page_lang as pl ON p.page_id = pl.page_id AND pl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}

		if (!empty($pages)) {
			foreach ($pages as $index => $page) {
				$pages[$index]['menu'] = $this->getPageMenu($page['page_id']);
			}
		}
			
		return $pages;
	}

	public function getPageByIdAdmin($id)
	{
		$page = Yii::app()->db
			->createCommand("SELECT * FROM page WHERE page_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($page)) {
			// page langs
			$page_langs = Yii::app()->db
				->createCommand("SELECT * FROM page_lang WHERE page_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($page_langs)) {
				foreach ($page_langs as $page_lang) {
					$code = $page_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$page[$code] = $page_lang;
					}
				}
			}

			$page['menu'] = $this->getPageMenu($id);
		}

		return $page;
	}

	public function getPageMenu($page_id)
	{
		return Yii::app()->db
			->createCommand("SELECT pm.menu 
							FROM page_menu as pm 
							WHERE pm.page_id = :id
							ORDER BY pm.menu")
			->bindValue(':id', (int) $page_id, PDO::PARAM_INT)
			->queryColumn();
	}

	public function issetPageByAlias($page_id, $page_alias)
	{
		if (!empty($page_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM page WHERE page_id != :page_id AND page_alias LIKE :alias")
				->bindValue(':page_id', (int) $page_id, PDO::PARAM_INT)
				->bindValue(':alias', $page_alias, PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM page WHERE page_alias LIKE :alias")
				->bindValue(':alias', $page_alias, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function save($model, $model_lang)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'page_id',
			'page_photo',
			'page_content',
			'page_content_faq',
			'page_content_faq_main',
			'menu',
		);

		// integer attributes
		$int_attributes = array(
			'page_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_page_photo',
		);

		// photos attributes
		$save_images = array(
			'page_photo',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get not empty title
		foreach (Yii::app()->params->langs as $language_code => $language_name) {
			if (!empty($model_lang->page_title[$language_code])) {
				$page_title = $model_lang->page_title[$language_code];
				break;
			}
		}

		// get alias
		$model->page_alias = empty($model->page_alias) ? URLify::filter($page_title, 200) : URLify::filter($model->page_alias, 200);

		while ($this->issetPageByAlias($model->page_id, $model->page_alias)) {
			$model->page_alias = $model->page_alias . '-' . uniqid();
		}

		// get max page position
		if (empty($model->page_position)) {
			$max_position = Yii::app()->db
				// ->createCommand("SELECT MAX(page_position) FROM page WHERE page_menu = :page_menu")
				->createCommand("SELECT MAX(page_position) FROM page")
				->bindValue(':page_menu', $model->page_menu, PDO::PARAM_STR)
				->queryScalar();

			$model->page_position = $max_position + 1;
		}

		if (empty($model->page_id)) {
			// insert page
			$insert_page = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_page[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_page[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_page[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_page[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('page', $insert_page)->execute();

				if ($rs) {
					$model->page_id = (int) Yii::app()->db->getLastInsertID();

					$skip_attributes = array(
						'page_content',
                        'page_content_faq',
                        'page_content_faq_main',
					);
					
					$int_attributes = array(
						'page_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_page_lang = array(
							'page_id' => $model->page_id,
							'language_code' => $language_code,
							'page_visible' => !empty($model_lang->page_title[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (in_array($field, $skip_attributes) || !is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_page_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_page_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('page_lang', $insert_page_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "page_id = :page_id" , 
									"params" => array(
										"page_id" => $model->page_id,
									)
								)
							);
							
							$builder->createDeleteCommand('page', $delete_criteria)->execute();

							return false;
						}
					}

					$this->saveMenu($model);
					$this->saveInfoblocks($model, $model_lang);
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_page = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_page[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_page[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_page[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_page[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_page[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "page_id = :page_id" , 
					"params" => array(
						"page_id" => $model->page_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('page', $update_page, $update_criteria)->execute();

				if ($rs) {
					// delete files
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$file_path = Yii::app()->assetManager->basePath . DS . 'page' . DS . $model->page_id . DS . $model->$del_attribute;

							if (is_file($file_path)) {
								CFileHelper::removeDirectory(dirname($file_path));
							}
						}
					}

					$skip_attributes = array(
						'page_content',
						'page_content_faq',
                        'page_content_faq_main'
					);

					$int_attributes = array(
						'page_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_page_lang = array(
							'page_visible' => !empty($model_lang->page_title[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'page_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (in_array($field, $skip_attributes) ||  !is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_page_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_page_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "page_id = :page_id AND language_code = :lang" , 
								"params" => array(
									"page_id" => (int) $model->page_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('page_lang', $update_page_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					$this->saveMenu($model);
					$this->saveInfoblocks($model, $model_lang);
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function saveMenu($model)
	{
		$rs_inserted = 0;
		$model->page_id = (int) $model->page_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete categories
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "page_id = :page_id" , 
				"params" => array(
					"page_id" => $model->page_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('page_menu', $delete_criteria)->execute();
		} catch (CDbException $e) {
			// ...
		}

		if (!empty($model->menu)) {
			$insert_menu = array();

			foreach ($model->menu as $menu) {
				$insert_menu[] = array(
					'page_id' => $model->page_id,
					'menu' => $menu,
				);
			}

			if (!empty($insert_menu)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('page_menu', $insert_menu)->execute();
				} catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	private function saveInfoblocks($model, $model_lang)
	{
		$page_content = array();
		$page_content_faq = array();
		$page_content_faq_main = array();
		$page_content_lang = array();
		$page_content_faq_lang = array();
		$page_content_faq_main_lang = array();

		// define fields set
		$fields_list = array(
			'page_content' => array(
				'file_multiple' => array(
					// 'photo',
				),
				'file' => array(
					'photo',
				),
				'text' => array(
					// 'type',
				),
				'text_lang' => array(
					'title',
					'text',
				),
			),
            'page_content_faq' => array(
                'file_multiple' => array(
                    // 'photo',
                ),
                'file' => array(
                    'photo',
                ),
                'text' => array(
                    // 'type',
                ),
                'text_lang' => array(
                    'title',
                    'text',
                ),
            ),
            'page_content_faq_main' => array(
                'file_multiple' => array(
                    // 'photo',
                ),
                'file' => array(
                    'photo',
                ),
                'text' => array(
                    // 'type',
                ),
                'text_lang' => array(
                    'title',
                    'text',
                ),
            )
		);

		// base lang
		$langs = Yii::app()->params->langs;
		$lang_code = Yii::app()->params->lang;

		foreach ($fields_list as $field => $fields) {
			if (!empty($model->$field)) {
				$position = 0;

				foreach ($model->$field as $index => $infoblock) {
					$field_data = array();	
					$field_lang_data = array();	

					foreach ($fields['file_multiple'] as $file_field) {
						$info_files = array();

						if (!empty($infoblock['del_' . $file_field])) {
							foreach ($infoblock['del_' . $file_field] as $i => $del_file) {
								$photo_path = Yii::app()->assetManager->basePath . DS . 'page' . DS . $model->page_id . DS . $del_file;

								if (is_file($photo_path) && is_dir(dirname($photo_path))) {
									CFileHelper::removeDirectory(dirname($photo_path));
								}

								if (!empty($infoblock['current_' . $file_field][$i])) {
									unset($infoblock['current_' . $file_field][$i]);
								}	
							}
						}

						if (!empty($infoblock['current_' . $file_field])) {
							$info_files = $infoblock['current_' . $file_field];
						}

						$file_type = ($file_field == 'video') ? 'video_mp4' : 'photo';
						
						$photos = CUploadedFile::getInstancesByName('page[' . $field . '][' . $index . '][' . $file_field . ']');

						if (!empty($photos)) {
							foreach ($photos as $photo) {
								$file_model = new AddFileForm($file_type);
								$file_model->$file_type = $photo;

								if (!empty($file_model->$file_type)) {
									if ($file_model->validate()) {
										$info_files[] = $this->saveFile($model, $file_model->$file_type, $file_type);
									}
								}
							}
						}

						$field_data[$file_field] = empty($info_files) ? '' : $info_files;
					}

					foreach ($fields['file'] as $file_field) {
						$info_file = '';

						if (!empty($infoblock['del_' . $file_field])) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'page' . DS . $model->page_id . DS . $infoblock['del_' . $file_field];

							if (is_file($photo_path) && is_dir(dirname($photo_path))) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}

							if (!empty($infoblock['current_' . $file_field])) {
								unset($infoblock['current_' . $file_field]);
							}
						}

						if (!empty($infoblock['current_' . $file_field])) {
							$info_file = $infoblock['current_' . $file_field];
						}

						$file_type = ($file_field == 'video') ? 'video_mp4' : 'photo';

						$file_model = new AddFileForm($file_type);
						$file_model->$file_type = CUploadedFile::getInstanceByName('page[' . $field . '][' . $index . '][' . $file_field . ']');

						if (!empty($file_model->$file_type)) {
							if ($file_model->validate()) {
								$info_file = $this->saveFile($model, $file_model->$file_type, $field, $file_type);
							} else {
								$info_file = '';
							}
						}

						$field_data[$file_field] = $info_file;
					}

					// special field position
					$infoblock['position'] = $position;
					$field_data['position'] = $infoblock['position'];

					foreach ($fields['text'] as $text_field) {
						$infoblock[$text_field] = trim($infoblock[$text_field]);
						$field_data[$text_field] = $infoblock[$text_field];
					}

                    foreach (Yii::app()->params->langs as $code => $lang) {
                        foreach ($model_lang->$field[$code] as $lang_index => $lang_infoblock) {
                            foreach ($fields['text_lang'] as $text_lang_field) {
								$lang_infoblock[$code][$text_lang_field] = trim($lang_infoblock[$text_lang_field]);
                            }
						}
                    }
					
					foreach ($fields['text_lang'] as $text_lang_field) {
						foreach (Yii::app()->params->langs as $code => $lang) {
							$field_lang_data[$code][$text_lang_field] = trim($model_lang->$field[$code][$index][$text_lang_field]);
							
							if ($code != $lang_code && empty($field_lang_data[$code][$text_lang_field])) {
								// set base lang field
								$field_lang_data[$code][$text_lang_field] = $field_lang_data[$lang_code][$text_lang_field];
							}
						}
					}

					// if (!empty($infoblock['title' . '_' . $lang_code]) || !empty($infoblock['text' . '_' . $lang_code]) || !empty($field_data['bg_photo']) || !empty($field_data['bg_video']) || !empty($field_data['vimeo'])) {
					if (!empty($field_lang_data[$lang_code]['title']) || !empty($field_lang_data[$lang_code]['text']) || !empty($field_data['photo'])) {
						${$field}[$position] = $field_data;

                        foreach (Yii::app()->params->langs as $code => $lang) {
                            ${$field . '_lang'}[$code][$position] = $field_lang_data[$code];
                        }

						$position++;
					}
				}
			}
		}

		$saved = false;
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_page = array(
			'saved' => $today,
			'page_content' => !empty($page_content) ? json_encode($page_content) : '',
			'page_content_faq' => !empty($page_content_faq) ? json_encode($page_content_faq) : '',
			'page_content_faq_main' => !empty($page_content_faq_main) ? json_encode($page_content_faq_main) : '',
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "page_id = :page_id", 
				"params" => array(
					"page_id" => $model->page_id,
				)
			)
		);

		try {
			$builder->createUpdateCommand('page', $update_page, $update_criteria)->execute();

			foreach (Yii::app()->params->langs as $language_code => $lang) {				
				$update_page_lang = array(
					'saved' => $today,
					'page_content' => !empty($page_content_lang[$language_code]) ? json_encode($page_content_lang[$language_code]) : '',
					'page_content_faq' => !empty($page_content_faq_lang[$language_code]) ? json_encode($page_content_faq_lang[$language_code]) : '',
					'page_content_faq_main' => !empty($page_content_faq_main_lang[$language_code]) ? json_encode($page_content_faq_main_lang[$language_code]) : '',
				);
				
				$update_lang_criteria = new CDbCriteria(
					array(
						"condition" => "page_id = :page_id AND language_code = :lang" , 
						"params" => array(
							"page_id" => (int) $model->page_id,
							"lang" => $language_code,
						)
					)
				);

				$builder->createUpdateCommand('page_lang', $update_page_lang, $update_lang_criteria)->execute();
			}

			$saved = true;
		} catch (CDbException $e) {
			// ...
		}

		return $saved;
	}

	private function saveFile($model, $file, $field, $file_type = 'photo')
	{
		if (empty($file)) {
			return false;
		}

		if ($this->imagine === null) {
			$this->initImagine();
		}

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_file = '';

		$file_id = uniqid();
		$file_name = strtolower(str_replace('-', '_', URLify::filter($file->getName(), 60, 'ru', true)));
		
		$file_dir = Yii::app()->assetManager->basePath . DS . 'page' . DS . $model->page_id . DS . $file_id . DS;
		$file_path = $file_dir . 'tmp_' . $file_name;

		$dir_rs = true;

		if (!is_dir($file_dir)) {
			$dir_rs = CFileHelper::createDirectory($file_dir, 0777, true);
		}

		if ($dir_rs) {
			$save_rs = $file->saveAs($file_path);

			if ($save_rs) {
				$file_name_parts = explode('.', $file_name);
				$file_ext = array_pop($file_name_parts);
				$file_base_name = implode('.', $file_name_parts);

				$file = $file_dir . $file_name;
				$file_save_name = $file_id . '/' . $file_name;

				copy($file_path, $file);

				if ($file_type == 'photo') {
					$image_obj = $this->imagine->open($file_path);
					$original_image_size = $image_obj->getSize();

					if ($original_image_size->getWidth() < 555) {
						return '';
					}

					$image_file = $file_dir . $file_base_name . '.' . $file_ext; // 1x
					$image_save_name = $file_id . '/' . $file_base_name . '.' . $file_ext;
					$image_file_2x = $file_dir . $file_base_name . '@2x.' . $file_ext; // 2x
					$image_save_name_2x = $file_id . '/' . $file_base_name . '@2x.' . $file_ext;

					$resized_image_2x = null;

					if ($original_image_size->getWidth() >= 1110) {
						$resized_image_2x = $image_obj->resize($original_image_size->widen(1110))
							->save($image_file_2x, array('quality' => 80));
					}

					$resized_image = $image_obj->resize($original_image_size->widen(555))
						->save($image_file, array('quality' => 80));

					$save_file = [
						'1x' => array(
							'file' => $image_save_name,
							'w' => $resized_image->getSize()->getWidth(),
							'h' => $resized_image->getSize()->getHeight(),
						),
						'2x' => empty($resized_image_2x) ? '' : array(
							'file' => $image_save_name_2x,
							'w' => $resized_image_2x->getSize()->getWidth(),
							'h' => $resized_image_2x->getSize()->getHeight(),
						),
					];
				} else {
					$save_file = array(
						'file' => $file_save_name,
					);
				}
				
				if (!is_file($file)) {
					$save_file = '';
				}

				// remove original file
				unlink($file_path);
			}
		}

		return $save_file;
	}

	private function initImagine()
	{
		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_file_rs = '';

		if (extension_loaded('imagick')) {
			$this->imagine = new Imagine\Imagick\Imagine();
		} elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$this->imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$this->mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$this->mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_parts = explode('.', strtolower($model->$attribute->getName()));
				$image_extension = array_pop($image_parts);
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'page' . DS . $model->page_id . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;
						
						if ($attribute == 'page_photo') {
							$image_files = array();

							$image_file = $image_dir . $image_full_name;
							$image_save_name = $image_id . '/' . $image_full_name;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							if ($original_image_size->getWidth() < 600) {
								continue;
							}

							if ($original_image_size->getWidth() > 1200) {
								$resized_image = $image_obj->resize($original_image_size->widen(1200))
									->save($image_file, array('quality' => 80));	
							} else {
								$resized_image = $image_obj->resize($original_image_size->widen(600))
									->save($image_file, array('quality' => 80));
							}

							$image_files = array(
								'path' => $image_save_name,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						}
						else {
							$image_file = $image_dir . $image_name;
							$image_save_name = $image_id . '/' . $image_name;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							switch ($attribute) {
								case 'page_photo':
									if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
										$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_outbound)
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));	
									}
									break;
							}

							$image_size = array(
								'w' => $resized_image->getSize()->getWidth(),
								'h' => $resized_image->getSize()->getHeight(),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode(array_merge(
									array(
										'file' => $image_save_name,
									),
									$image_size
								));
							}
							else {
								// remove resized files
								if (is_file($image_file)) {
									unlink($image_file);
								}
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_page = array(
				'saved' => $today,
			);

			$update_page = array_merge($update_page, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "page_id = :page_id" , 
					"params" => array(
						"page_id" => $model->page_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('page', $update_page, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	public function toggle($page_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_page = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "page_id = :page_id" , 
				"params" => array(
					"page_id" => $page_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('page', $update_page, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function setPosition($page_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_page = array(
			'saved' => $today,
			'page_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "page_id = :page_id" , 
				"params" => array(
					"page_id" => $page_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('page', $update_page, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($page_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$page = $this->getPageByIdAdmin($page_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "page_id = :page_id" , 
				"params" => array(
					"page_id" => $page_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('page', $delete_criteria)->execute();

			if ($rs) {
				// remove page directory
				if (is_dir($assetPath . DS . 'page' . DS . $page_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'page' . DS . $page_id);
				}

				// delete related tables
				$builder->createDeleteCommand('page_lang', $delete_criteria)->execute();

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}