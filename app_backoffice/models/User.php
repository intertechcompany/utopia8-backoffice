<?php
class User extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getUser()
	{
		$user = Yii::app()->db
			->createCommand("SELECT * FROM user WHERE user_id = :id AND active = 1")
			->bindValue(':id', (int) Yii::app()->user->id, PDO::PARAM_INT)
			->queryRow();
		
		return $user;
	}

	public function saveToken($token, $id)
	{
		$rs = Yii::app()->db
				->createCommand("UPDATE user SET user_token = :token WHERE user_id = :id")
				->bindValue(':token', $token, PDO::PARAM_STR)
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->execute();
				
		return (bool) $rs;
	}
	
	public function isValidToken($id, $token)
	{
		$user = Yii::app()->db
			->createCommand("SELECT saved FROM user WHERE user_id = :id AND active = 1 AND user_token = :token")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->bindValue(':token', $token, PDO::PARAM_STR)
			->queryRow();

		if (!empty($user)) {
			$is_valid = true;
			Yii::app()->user->setState('saved', $user['saved']);
		}
		else {
			$is_valid = false;
			Yii::app()->user->setState('saved', null);
		}
			
		return $is_valid;
	}
	
	public function getUsersAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$user_id = (int) $func_args[1];
			$user_name = addcslashes($func_args[1], '%_');

			$total_users = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM user WHERE user_id = :id OR user_first_name LIKE :user_name")
				->bindValue(':id', $user_id, PDO::PARAM_INT)
				->bindValue(':user_name', '%' . $user_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_users = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM user")
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_users,
			'pages' => ceil($total_users / $per_page),
		);
	}

	public function getUsersAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'user_id':
				$order_by = ($direction == 'asc') ? 'user_id' : 'user_id DESC';
				break;
			case 'user_name':
				$order_by = ($direction == 'asc') ? 'user_first_name' : 'user_first_name DESC';
				break;
			default:
				$order_by = 'user_id';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$user_id = (int) $func_args[4];
			$user_name = addcslashes($func_args[4], '%_');

			$users = Yii::app()->db
				->createCommand("SELECT * FROM user WHERE user_id = :id OR user_first_name LIKE :user_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':id', $user_id, PDO::PARAM_INT)
				->bindValue(':user_name', '%' . $user_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$users = Yii::app()->db
				->createCommand("SELECT * FROM user ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->queryAll();
		}
			
		return $users;
	}

	public function getUserByIdAdmin($id)
	{
		$user = Yii::app()->db
			->createCommand("SELECT * FROM user WHERE user_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();
			
		return $user;
	}

	public function getUsersListAdmin()
	{
		$users = Yii::app()->db
			->createCommand("SELECT user_id, user_first_name FROM user ORDER BY user_name")
			->queryAll();
			
		return $users;
	}

	public function issetUserByLogin($user_login, $user_id)
	{
		if (!empty($user_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM user WHERE user_login = :user_login AND user_id != :id")
				->bindValue(':user_login', $user_login, PDO::PARAM_STR)
				->bindValue(':id', $user_id, PDO::PARAM_INT)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM user WHERE user_login = :user_login")
				->bindValue(':user_login', $user_login, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function issetUserByEmail($user_email, $user_id)
	{
		if (!empty($user_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM user WHERE user_email = :user_email AND user_id != :id")
				->bindValue(':user_email', $user_email, PDO::PARAM_STR)
				->bindValue(':id', $user_id, PDO::PARAM_INT)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM user WHERE user_email = :user_email")
				->bindValue(':user_email', $user_email, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function save($model)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'user_id',
		);

		// integer attributes
		$int_attributes = array(
			'active',
			'town_id',
			'user_rights',
		);

		// date attributes
		$date_attributes = array();

		if (empty($model->user_id)) {
			// insert user
			$insert_user = array(
				'created' => $today,
				'saved' => $today,
			);

			$model->user_password = CPasswordHelper::hashPassword($model->user_password, 10);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_user[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$insert_user[$field] = $date->format('Y-m-d');
				}
				else {
					$insert_user[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('user', $insert_user)->execute();

				if ($rs) {
					$model->user_id = (int) Yii::app()->db->getLastInsertID();
					
					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_user = array(
				'saved' => $today,
			);
			
			if (!empty($model->user_password)) {
				$model->user_password = CPasswordHelper::hashPassword($model->user_password, 10);
				$model->user_token = '';
			}
			else {
				$skip_attributes[] = 'user_password';
				$skip_attributes[] = 'user_token';
			}

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_user[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$update_user[$field] = $date->format('Y-m-d');
				}
				else {
					$update_user[$field] = $value;
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "user_id = :user_id" , 
					"params" => array(
						"user_id" => $model->user_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('user', $update_user, $update_criteria)->execute();

				if ($rs) {
					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function toggle($user_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_user = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "user_id = :user_id" , 
				"params" => array(
					"user_id" => $user_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('user', $update_user, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($user_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "user_id = :user_id" , 
				"params" => array(
					"user_id" => $user_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('user', $delete_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}