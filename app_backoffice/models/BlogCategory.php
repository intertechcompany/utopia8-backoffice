<?php
class BlogCategory extends CModel
{
    private $id_key = 'category_id';
    private $code_key = 'category_alias';
    private $categories;
    private $paths;
    private $tree;

    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function getCategoriesAdminTotal($per_page = 10)
    {
        $func_args = func_get_args();

        if (!empty($func_args[1])) {
            $category_id = (int) $func_args[1];
            $category_name = addcslashes($func_args[1], '%_');

            $total_categories = Yii::app()->db
                ->createCommand("SELECT COUNT(*) FROM blog_category as c JOIN blog_category_lang as cl ON c.category_id = cl.category_id AND cl.language_code = :code WHERE c.category_id = :id OR cl.category_name LIKE :category_name")
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->bindValue(':id', $category_id, PDO::PARAM_INT)
                ->bindValue(':category_name', '%' . $category_name . '%', PDO::PARAM_STR)
                ->queryScalar();
        }
        else {
            $total_categories = Yii::app()->db
                ->createCommand("SELECT COUNT(*) FROM blog_category as c JOIN blog_category_lang as cl ON c.category_id = cl.category_id AND cl.language_code = :code")
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->queryScalar();
        }

        return array(
            'total' => (int) $total_categories,
            'pages' => ceil($total_categories / $per_page),
        );
    }

    public function getCategoriesAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
    {
        switch ($sort) {
            case 'category_id':
                $order_by = ($direction == 'asc') ? 'c.category_id' : 'c.category_id DESC';
                break;
            case 'category_position':
                $order_by = ($direction == 'asc') ? 'c.category_position' : 'c.category_position DESC';
                break;
            case 'category_name':
                $order_by = ($direction == 'asc') ? 'cl.category_name' : 'cl.category_name DESC';
                break;
            default:
                $order_by = 'c.category_position';
        }

        $func_args = func_get_args();

        if (!empty($func_args[4])) {
            $category_id = (int) $func_args[4];
            $category_name = addcslashes($func_args[4], '%_');

            $categories = Yii::app()->db
                ->createCommand("SELECT c.category_id, c.active, c.category_photo, c.category_position, cl.category_name FROM blog_category as c JOIN blog_category_lang as cl ON c.category_id = cl.category_id AND cl.language_code = :code WHERE c.category_id = :id OR cl.category_name LIKE :category_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->bindValue(':id', $category_id, PDO::PARAM_INT)
                ->bindValue(':category_name', '%' . $category_name . '%', PDO::PARAM_STR)
                ->queryAll();
        }
        else {
            $categories = Yii::app()->db
                ->createCommand("SELECT c.category_id, c.active, c.category_photo, c.category_position, cl.category_name FROM blog_category as c JOIN blog_category_lang as cl ON c.category_id = cl.category_id AND cl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->queryAll();
        }

        return $categories;
    }

    public function getCategoryByIdAdmin($id)
    {
        $category = Yii::app()->db
            ->createCommand("SELECT * FROM blog_category WHERE category_id = :id LIMIT 1")
            ->bindValue(':id', (int) $id, PDO::PARAM_INT)
            ->queryRow();

        if (!empty($category)) {
            // category langs
            $category_langs = Yii::app()->db
                ->createCommand("SELECT * FROM blog_category_lang WHERE category_id = :id")
                ->bindValue(':id', (int) $id, PDO::PARAM_INT)
                ->queryAll();

            if (!empty($category_langs)) {
                foreach ($category_langs as $category_lang) {
                    $code = $category_lang['language_code'];

                    if (isset(Yii::app()->params->langs[$code])) {
                        $category[$code] = $category_lang;
                    }
                }
            }
        }

        return $category;
    }

    public function getCategoriesListAdmin()
    {
        $categories = Yii::app()->db
            ->createCommand("SELECT c.category_id, cl.category_name FROM blog_category as c JOIN blog_category_lang as cl ON c.category_id = cl.category_id AND cl.language_code = :code ORDER BY c.category_position")
            ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
            ->queryAll();

        return $categories;
    }

    public function issetCategoryByAlias($category_id, $category_alias)
    {
        if (!empty($category_id)) {
            $isset = (bool) Yii::app()->db
                ->createCommand("SELECT COUNT(*) FROM blog_category WHERE category_id != :category_id AND category_alias LIKE :alias")
                ->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
                ->bindValue(':alias', $category_alias, PDO::PARAM_STR)
                ->queryScalar();
        }
        else {
            $isset = (bool) Yii::app()->db
                ->createCommand("SELECT COUNT(*) FROM blog_category WHERE category_alias LIKE :alias")
                ->bindValue(':alias', $category_alias, PDO::PARAM_STR)
                ->queryScalar();
        }

        return $isset;
    }

    public function save($model, $model_lang)
    {
        // import URLify library
        Yii::import('application.vendor.URLify.URLify');

        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        // skip unnecessary attributes
        $skip_attributes = array(
            'category_id',
            'category_photo',
        );

        // integer attributes
        $int_attributes = array(
            'category_position',
        );

        // date attributes
        $date_attributes = array();

        // delete attributes
        $del_attributes = array(
            'del_category_photo',
        );

        // photos attributes
        $save_images = array(
            'category_photo',
        );

        $skip_attributes = array_merge($skip_attributes, $del_attributes);

        // get not empty title
        foreach (Yii::app()->params->langs as $language_code => $language_name) {
            if (!empty($model_lang->category_name[$language_code])) {
                $category_name = $model_lang->category_name[$language_code];
                break;
            }
        }

        // get alias
        $model->category_alias = empty($model->category_alias) ? URLify::filter($category_name, 200) : URLify::filter($model->category_alias, 200);

        while ($this->issetCategoryByAlias($model->category_id, $model->category_alias)) {
            $model->category_alias = $model->category_alias . '-' . uniqid();
        }

        // get max cetgory position
        if (empty($model->category_position)) {
            $max_position = Yii::app()->db
                ->createCommand("SELECT MAX(category_position) FROM blog_category WHERE parent_id = :parent_id")
                ->bindValue(':parent_id', $model->parent_id, PDO::PARAM_INT)
                ->queryScalar();

            $model->category_position = $max_position + 1;
        }

        if (empty($model->category_id)) {
            // insert category
            $insert_category = array(
                'created' => $today,
                'saved' => $today,
            );

            foreach ($model as $field => $value) {
                if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
                    continue;
                }
                elseif (in_array($field, $int_attributes)) {
                    $insert_category[$field] = (int) $value;
                }
                elseif (in_array($field, $date_attributes)) {
                    if (empty($value)) {
                        $insert_category[$field] = '0000-00-00';
                    }
                    else {
                        $date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
                        $insert_category[$field] = $date->format('Y-m-d');
                    }
                }
                else {
                    $insert_category[$field] = $value;
                }
            }

            try {
                $rs = $builder->createInsertCommand('blog_category', $insert_category)->execute();

                if ($rs) {
                    $model->category_id = (int) Yii::app()->db->getLastInsertID();

                    $int_attributes = array(
                        'category_no_index',
                    );

                    foreach (Yii::app()->params->langs as $language_code => $language_name) {
                        // save details
                        $insert_category_lang = array(
                            'category_id' => $model->category_id,
                            'language_code' => $language_code,
                            'category_visible' => !empty($model_lang->category_name[$language_code]) ? 1 : 0,
                            'created' => $today,
                            'saved' => $today,
                        );

                        foreach ($model_lang->attributes as $field => $value) {
                            if (!is_array($value) || !isset($value[$language_code])) {
                                // skip non multilang fields
                                continue;
                            }
                            elseif (in_array($field, $int_attributes)) {
                                $insert_category_lang[$field] = (int) $value[$language_code];
                            }
                            else {
                                $insert_category_lang[$field] = trim($value[$language_code]);
                            }
                        }

                        $rs = $builder->createInsertCommand('blog_category_lang', $insert_category_lang)->execute();

                        if (!$rs) {
                            $delete_criteria = new CDbCriteria(
                                array(
                                    "condition" => "category_id = :category_id" ,
                                    "params" => array(
                                        "category_id" => $model->category_id,
                                    )
                                )
                            );

                            $builder->createDeleteCommand('blog_category', $delete_criteria)->execute();

                            return false;
                        }
                    }

                    // save photos
                    $this->savePhotos($model, $save_images);

                    return true;
                }
            }
            catch (CDbException $e) {
                // ...
            }
        }
        else {
            $update_category = array(
                'saved' => $today,
            );

            foreach ($model as $field => $value) {
                if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
                    continue;
                }
                elseif (in_array($field, $int_attributes)) {
                    $update_category[$field] = (int) $value;
                }
                elseif (in_array($field, $date_attributes)) {
                    if (empty($value)) {
                        $update_category[$field] = '0000-00-00';
                    }
                    else {
                        $date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
                        $update_category[$field] = $date->format('Y-m-d');
                    }
                }
                else {
                    $update_category[$field] = $value;
                }
            }

            foreach ($del_attributes as $del_attribute) {
                if (!empty($model->$del_attribute)) {
                    $del_attribute = str_replace('del_', '', $del_attribute);
                    $update_category[$del_attribute] = '';
                }
            }

            $update_criteria = new CDbCriteria(
                array(
                    "condition" => "category_id = :category_id" ,
                    "params" => array(
                        "category_id" => $model->category_id,
                    )
                )
            );

            try {
                $rs = $builder->createUpdateCommand('blog_category', $update_category, $update_criteria)->execute();

                if ($rs) {
                    // delete photos
                    foreach ($del_attributes as $del_attribute) {
                        if (!empty($model->$del_attribute)) {
                            $photo_path = Yii::app()->assetManager->basePath . DS . 'blog_category' . DS . $model->category_id . DS . $model->$del_attribute;

                            if (is_file($photo_path)) {
                                CFileHelper::removeDirectory(dirname($photo_path));
                            }
                        }
                    }

                    $int_attributes = array(
                        'category_no_index',
                    );

                    foreach (Yii::app()->params->langs as $language_code => $language_name) {
                        // save details
                        $update_category_lang = array(
                            'category_visible' => !empty($model_lang->category_name[$language_code]) ? 1 : 0,
                            'saved' => $today,
                        );

                        foreach ($model_lang->attributes as $field => $value) {
                            // checkboxes
                            if ($field == 'category_no_index' && !isset($value[$language_code])) {
                                $value[$language_code] = 0;
                            }

                            if (!is_array($value) || !isset($value[$language_code])) {
                                // skip non multilang fields
                                continue;
                            }
                            elseif (in_array($field, $int_attributes)) {
                                $update_category_lang[$field] = (int) $value[$language_code];
                            }
                            else {
                                $update_category_lang[$field] = trim($value[$language_code]);
                            }
                        }

                        $update_lang_criteria = new CDbCriteria(
                            array(
                                "condition" => "category_id = :category_id AND language_code = :lang" ,
                                "params" => array(
                                    "category_id" => (int) $model->category_id,
                                    "lang" => $language_code,
                                )
                            )
                        );

                        $rs = $builder->createUpdateCommand('blog_category_lang', $update_category_lang, $update_lang_criteria)->execute();

                        if (!$rs) {
                            return false;
                        }
                    }

                    // save photos
                    $this->savePhotos($model, $save_images);

                    return true;
                }
            }
            catch (CDbException $e) {
                // ...
            }
        }

        return false;
    }

    private function savePhotos($model, $attributes)
    {
        if (empty($attributes)) {
            return false;
        }

        // register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

        // import URLify library
        Yii::import('application.vendor.URLify.URLify');

        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        $save_images_rs = array();

        if (extension_loaded('imagick')) {
            $imagine = new Imagine\Imagick\Imagine();
        }
        elseif (extension_loaded('gd') && function_exists('gd_info')) {
            $imagine = new Imagine\Gd\Imagine();
        }

        // уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
        $mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
        // изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
        $mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

        foreach ($attributes as $attribute) {
            if (!empty($model->$attribute)) {
                $image_id = uniqid();
                $image_parts = explode('.', strtolower($model->$attribute->getName()));
                $image_extension = array_pop($image_parts);
                $image_name = implode('.', $image_parts);
                $image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
                $image_full_name = $image_name . '.' . $image_extension;

                $image_dir = Yii::app()->assetManager->basePath . DS . 'blog_category' . DS . $model->category_id . DS . $image_id . DS;
                $image_path = $image_dir . 'tmp_' . $image_full_name;

                $dir_rs = true;

                if (!is_dir($image_dir)) {
                    $dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
                }

                if ($dir_rs) {
                    $save_image_rs = $model->$attribute->saveAs($image_path);
                    if ($save_image_rs) {
                        $image_size = false;
                        if ($attribute == 'category_photo') {

                            $image_files = array();

                            $image_file = $image_dir . $image_full_name;
                            $image_save_name = $image_id . '/' . $image_full_name;

                            // resize file
                            $image_obj = $imagine->open($image_path);
                            $original_image_size = $image_obj->getSize();

//							if ($original_image_size->getWidth() < 600 || $original_image_size->getHeight() < 380) {
//								continue;
//							}

                            $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(600, 380), $mode_outbound)
                                ->save($image_file, array('quality' => 80));

                            $image_files = array(
                                'path' => $image_save_name,
                                'size' => array(
                                    'w' => $resized_image->getSize()->getWidth(),
                                    'h' => $resized_image->getSize()->getHeight(),
                                ),
                            );

                            if (is_file($image_file)) {
                                $save_images_rs[$attribute] = json_encode($image_files);
                            }
                        }
                        else {
                            $image_file = $image_dir . $image_name;
                            $image_save_name = $image_id . '/' . $image_name;

                            // resize file
                            $image_obj = $imagine->open($image_path);
                            $original_image_size = $image_obj->getSize();

                            switch ($attribute) {
                                case 'category_photo':
                                    if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
                                        $resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_outbound)
                                            ->save($image_file, array('quality' => 80));
                                    }
                                    else {
                                        $resized_image = $image_obj->save($image_file, array('quality' => 80));
                                    }
                                    break;
                            }

                            $image_size = array(
                                'w' => $resized_image->getSize()->getWidth(),
                                'h' => $resized_image->getSize()->getHeight(),
                            );

                            if (is_file($image_file)) {
                                $save_images_rs[$attribute] = json_encode(array_merge(
                                    array(
                                        'file' => $image_save_name,
                                    ),
                                    $image_size
                                ));
                            }
                            else {
                                // remove resized files
                                if (is_file($image_file)) {
                                    unlink($image_file);
                                }
                            }
                        }

                        // remove original image
                        unlink($image_path);
                    }
                }
            }
        }

        if (!empty($save_images_rs)) {
            $update_category = array(
                'saved' => $today,
            );

            $update_category = array_merge($update_category, $save_images_rs);

            $update_criteria = new CDbCriteria(
                array(
                    "condition" => "category_id = :category_id" ,
                    "params" => array(
                        "category_id" => $model->category_id,
                    )
                )
            );

            try {
                $save_photo_rs = (bool) $builder->createUpdateCommand('blog_category', $update_category, $update_criteria)->execute();
            }
            catch (CDbException $e) {
                // ...
            }
        }
    }

    public function setPosition($category_id, $position)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        $update_category = array(
            'saved' => $today,
            'category_position' => (int) $position,
        );

        $update_criteria = new CDbCriteria(
            array(
                "condition" => "category_id = :category_id" ,
                "params" => array(
                    "category_id" => $category_id,
                )
            )
        );

        try {
            $rs = $builder->createUpdateCommand('blog_category', $update_category, $update_criteria)->execute();

            if ($rs) {
                return true;
            }
        }
        catch (CDbException $e) {
            // ...
        }

        return false;
    }

    public function toggle($category_id, $active)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        $update_category = array(
            'saved' => $today,
            'active' => (int) $active,
        );

        $update_criteria = new CDbCriteria(
            array(
                "condition" => "category_id = :category_id" ,
                "params" => array(
                    "category_id" => $category_id,
                )
            )
        );

        try {
            $rs = $builder->createUpdateCommand('blog_category', $update_category, $update_criteria)->execute();

            if ($rs) {
                return true;
            }
        }
        catch (CDbException $e) {
            // ...
        }

        return false;
    }

    public function delete($category_id, $del_children = true)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $assetPath = Yii::app()->assetManager->basePath;

        $category = $this->getCategoryByIdAdmin($category_id);

        $delete_criteria = new CDbCriteria(
            array(
                "condition" => "category_id = :category_id" ,
                "params" => array(
                    "category_id" => $category_id,
                )
            )
        );

        try {
            $rs = $builder->createDeleteCommand('blog_category', $delete_criteria)->execute();

            if ($rs) {
                // delete related tables
                $builder->createDeleteCommand('blog_category_lang', $delete_criteria)->execute();

                // remove category directory
                if (is_dir($assetPath . DS . 'blog_category' . DS . $category_id)) {
                    CFileHelper::removeDirectory($assetPath . DS . 'blog_category' . DS . $category_id);
                }

                return true;
            }
        }
        catch (CDbException $e) {
            // ...
        }

        return false;
    }
}
