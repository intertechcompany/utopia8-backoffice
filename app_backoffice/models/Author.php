<?php
class Author extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getAuthorsAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$author_id = (int) $func_args[1];
			$author_name = addcslashes($func_args[1], '%_');

			$total_authors = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM author as a JOIN author_lang as al ON a.author_id = al.author_id AND al.language_code = :code WHERE a.author_id = :id OR al.author_name LIKE :author_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $author_id, PDO::PARAM_INT)
				->bindValue(':author_name', '%' . $author_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_authors = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM author as a JOIN author_lang as al ON a.author_id = al.author_id AND al.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_authors,
			'pages' => ceil($total_authors / $per_page),
		);
	}

	public function getAuthorsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'author_id':
				$order_by = ($direction == 'asc') ? 'a.author_id' : 'a.author_id DESC';
				break;
			case 'author_name':
				$order_by = ($direction == 'asc') ? 'al.author_name' : 'al.author_name DESC';
				break;
			case 'author_position':
				$order_by = ($direction == 'asc') ? 'a.author_position' : 'a.author_position DESC';
				break;
			default:
				$order_by = 'a.author_position, a.author_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$author_id = (int) $func_args[4];
			$author_name = addcslashes($func_args[4], '%_');

			$authors = Yii::app()->db
				->createCommand("SELECT a.*, al.author_name FROM author as a JOIN author_lang as al ON a.author_id = al.author_id AND al.language_code = :code WHERE a.author_id = :id OR al.author_name LIKE :author_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $author_id, PDO::PARAM_INT)
				->bindValue(':author_name', '%' . $author_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$authors = Yii::app()->db
				->createCommand("SELECT a.*, al.author_name FROM author as a JOIN author_lang as al ON a.author_id = al.author_id AND al.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $authors;
	}

	public function getAuthorByIdAdmin($id)
	{
		$author = Yii::app()->db
			->createCommand("SELECT * FROM author WHERE author_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

        if (!empty($author)) {
            // author langs
            $author_langs = Yii::app()->db
                ->createCommand("SELECT * FROM author_lang WHERE author_id = :id")
                ->bindValue(':id', (int) $id, PDO::PARAM_INT)
                ->queryAll();

            if (!empty($author_langs)) {
                foreach ($author_langs as $author_lang) {
                    $code = $author_lang['language_code'];

                    if (isset(Yii::app()->params->langs[$code])) {
                        $author[$code] = $author_lang;
                    }
                }
            }
        }
			
		return $author;
	}

	public function getAuthorsListAdmin()
	{
		$authors = Yii::app()->db
			->createCommand("SELECT a.author_id, al.author_name FROM author as a JOIN author_lang as al ON a.author_id = al.author_id AND al.language_code = :code ORDER BY a.author_position, a.author_id DESC")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();
			
		return $authors;
	}

	public function save($model, $model_lang)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'author_id',
			'author_image',
		);

		// integer attributes
		$int_attributes = array(
			'author_id',
			'author_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_author_image',
		);

		// photos attributes
		$save_images = array(
			'author_image',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get max author position
		if (empty($model->author_position)) {
			$max_position = Yii::app()->db
				->createCommand("SELECT MAX(author_position) FROM author")
				->queryScalar();

			$model->author_position = $max_position + 1;
		}

		if (empty($model->author_id)) {
			// insert author
			$insert_author = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_author[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_author[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_author[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_author[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('author', $insert_author)->execute();

				if ($rs) {
					$model->author_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array(
						'author_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_author_lang = array(
							'author_id' => $model->author_id,
							'language_code' => $language_code,
							'author_visible' => !empty($model_lang->author_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_author_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_author_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('author_lang', $insert_author_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "author_id = :author_id" , 
									"params" => array(
										"author_id" => $model->author_id,
									)
								)
							);
							
							$builder->createDeleteCommand('author', $delete_criteria)->execute();

							return false;
						}
					}

					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_author = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_author[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_author[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_author[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_author[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_author[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "author_id = :author_id" , 
					"params" => array(
						"author_id" => $model->author_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('author', $update_author, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'author' . DS . $model->author_id . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array(
						'author_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_author_lang = array(
							'author_visible' => !empty($model_lang->author_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'author_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_author_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_author_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "author_id = :author_id AND language_code = :lang" , 
								"params" => array(
									"author_id" => (int) $model->author_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('author_lang', $update_author_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_parts = explode('.', strtolower($model->$attribute->getName()));
				$image_extension = array_pop($image_parts);
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				$image_full_name_2x = $image_name . '@2x.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'author' . DS . $model->author_id . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;

						if ($attribute == 'author_image') {
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();
							
							$image_file = $image_dir . $image_name . '.' . $image_extension;
							$image_save_name = $image_id . '/' . $image_name . '.' . $image_extension;
							$image_file_2x = $image_dir . $image_full_name_2x;
							$image_save_name_2x = $image_id . '/' . $image_full_name_2x;

							if ($original_image_size->getWidth() < 272 || $original_image_size->getHeight() < 272) {
								continue;
							}

							$resized_image_2x = null;

                            if ($original_image_size->getWidth() >= 544 && $original_image_size->getHeight() >= 544) {
                                $resized_image_2x = $image_obj->thumbnail(new Imagine\Image\Box(544, 544), $mode_outbound)
                                    ->save($image_file_2x, array('quality' => 80));
                            }

							$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(272, 272), $mode_outbound)
								->save($image_file, array('quality' => 80));

							$image_files = array(
								'1x' => array(
									'path' => $image_save_name,
									'size' => array(
										'w' => $resized_image->getSize()->getWidth(),
										'h' => $resized_image->getSize()->getHeight(),
									),
								),
								'2x' => empty($resized_image_2x) ? null : array(
									'path' => $image_save_name_2x,
									'size' => array(
										'w' => $resized_image_2x->getSize()->getWidth(),
										'h' => $resized_image_2x->getSize()->getHeight(),
									),
								),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						} elseif ($attribute == 'author_photo') {
							$image_files = array();

							$image_file_original = $image_dir . $image_name . '_o.' . $image_extension; // original
							$image_save_name_original = $image_id . '/' . $image_name . '_o.' . $image_extension;

							$image_file_details = $image_dir . $image_name . '_d.' . $image_extension; // details
							$image_save_name_details = $image_id . '/' . $image_name . '_d.' . $image_extension;

							$image_file_list = $image_dir . $image_name . '_a.' . $image_extension; // list
							$image_save_name_list = $image_id . '/' . $image_name . '_a.' . $image_extension;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							if ($original_image_size->getWidth() < 270 || $original_image_size->getHeight() < 326) {
								continue;
							}

							$resized_image = $image_obj->save($image_file_original, array('quality' => 80));

							$image_files['original'] = array(
								'path' => $image_save_name_original,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// details image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 740) {
								$resized_image = $image_obj->resize($original_image_size->widen(740))
									->save($image_file_details, array('quality' => 80));	
							} else {
								$resized_image = $image_obj->resize($original_image_size->widen(370))
									->save($image_file_details, array('quality' => 80));
							}

							$image_files['details'] = array(
								'path' => $image_save_name_details,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// list image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 540 && $original_image_size->getHeight() > 652) {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(540, 652), $mode_outbound)
									->save($image_file_list, array('quality' => 80));
							} else {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(270, 326), $mode_outbound)
									->save($image_file_list, array('quality' => 80));
							}

							$image_files['list'] = array(
								'path' => $image_save_name_list,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							if (is_file($image_file_original) && is_file($image_file_details) && is_file($image_file_list)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						} else {
							$image_file = $image_dir . $image_name;
							$image_save_name = $image_id . '/' . $image_name;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							switch ($attribute) {
								case 'author_photo':
									if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
										$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_outbound)
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));	
									}
									break;
							}

							$image_size = array(
								'w' => $resized_image->getSize()->getWidth(),
								'h' => $resized_image->getSize()->getHeight(),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode(array_merge(
									array(
										'file' => $image_save_name,
									),
									$image_size
								));
							}
							else {
								// remove resized files
								if (is_file($image_file)) {
									unlink($image_file);
								}
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_author = array(
				'saved' => $today,
			);

			$update_author = array_merge($update_author, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "author_id = :author_id" , 
					"params" => array(
						"author_id" => $model->author_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('author', $update_author, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	public function setPosition($author_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_author = array(
			'saved' => $today,
			'author_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "author_id = :author_id" , 
				"params" => array(
					"author_id" => $author_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('author', $update_author, $update_criteria)->execute();
			
			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function toggle($author_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_author = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "author_id = :author_id" , 
				"params" => array(
					"author_id" => $author_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('author', $update_author, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($author_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$author = $this->getAuthorByIdAdmin($author_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "author_id = :author_id" , 
				"params" => array(
					"author_id" => $author_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('author', $delete_criteria)->execute();

			if ($rs) {
				// remove author directory
				if (is_dir($assetPath . DS . 'author' . DS . $author_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'author' . DS . $author_id);
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}