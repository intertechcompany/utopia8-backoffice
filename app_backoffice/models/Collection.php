<?php
class Collection extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getCollectionsAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$collection_id = (int) $func_args[1];
			$collection_title = addcslashes($func_args[1], '%_');

			$total_collections = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM collection as c JOIN collection_lang as cl ON c.collection_id = cl.collection_id AND cl.language_code = :code WHERE c.collection_id = :id OR cl.collection_title LIKE :collection_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $collection_id, PDO::PARAM_INT)
				->bindValue(':collection_title', '%' . $collection_title . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_collections = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM collection as c JOIN collection_lang as cl ON c.collection_id = cl.collection_id AND cl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_collections,
			'pages' => ceil($total_collections / $per_page),
		);
	}

	public function getCollectionsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'collection_id':
				$order_by = ($direction == 'asc') ? 'c.collection_id' : 'c.collection_id DESC';
				break;
			case 'collection_title':
				$order_by = ($direction == 'asc') ? 'cl.collection_title' : 'cl.collection_title DESC';
				break;
			case 'collection_rating':
				$order_by = ($direction == 'asc') ? 'c.collection_rating' : 'c.collection_rating DESC';
				break;
			default:
				$order_by = 'c.collection_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$collection_id = (int) $func_args[4];
			$collection_title = addcslashes($func_args[4], '%_');

			$collections = Yii::app()->db
				->createCommand("SELECT c.collection_id, c.active, c.collection_rating, c.collection_photo, cl.collection_title, cl.collection_country FROM collection as c JOIN collection_lang as cl ON c.collection_id = cl.collection_id AND cl.language_code = :code WHERE c.collection_id = :id OR cl.collection_title LIKE :collection_title ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $collection_id, PDO::PARAM_INT)
				->bindValue(':collection_title', '%' . $collection_title . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$collections = Yii::app()->db
				->createCommand("SELECT c.collection_id, c.active, c.collection_rating, c.collection_photo, cl.collection_title, cl.collection_country FROM collection as c JOIN collection_lang as cl ON c.collection_id = cl.collection_id AND cl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $collections;
	}

	public function getCollectionByIdAdmin($id)
	{
		$collection = Yii::app()->db
			->createCommand("SELECT * FROM collection WHERE collection_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($collection)) {
			// collection langs
			$collection_langs = Yii::app()->db
				->createCommand("SELECT * FROM collection_lang WHERE collection_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($collection_langs)) {
				foreach ($collection_langs as $collection_lang) {
					$code = $collection_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$collection[$code] = $collection_lang;
					}
				}
			}
		}
			
		return $collection;
	}

	public function getCollectionsListAdmin()
	{
		$collections = Yii::app()->db
			->createCommand("SELECT c.collection_id, cl.collection_title FROM collection as c JOIN collection_lang as cl ON c.collection_id = cl.collection_id AND cl.language_code = :code ORDER BY cl.collection_title")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();
			
		return $collections;
	}

	public function getGalleryPhotos($id)
	{
		$gallery_photos = Yii::app()->db
			->createCommand("SELECT * FROM collection_photo WHERE collection_id = :id ORDER BY position")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryAll();
			
		return $gallery_photos;
	}

    public function issetCollectionByAlias($collection_id, $collection_alias)
	{
		if (!empty($collection_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM collection WHERE collection_id != :collection_id AND collection_alias LIKE :alias")
				->bindValue(':collection_id', (int) $collection_id, PDO::PARAM_INT)
				->bindValue(':alias', $collection_alias, PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM collection WHERE collection_alias LIKE :alias")
				->bindValue(':alias', $collection_alias, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function save($model, $model_lang, $del_gallery_photos, $gallery_photos_position, $gallery_photos)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'collection_id',
		);

		// integer attributes
		$int_attributes = array();

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array();

		// photos attributes
		$save_images = array();

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get not empty title
		foreach (Yii::app()->params->langs as $language_code => $language_name) {
			if (!empty($model_lang->collection_title[$language_code])) {
				$collection_title = $model_lang->collection_title[$language_code];
				break;
			}
		}

		// get alias
		$model->collection_alias = empty($model->collection_alias) ? URLify::filter($collection_title, 200) : URLify::filter($model->collection_alias, 200);

		while ($this->issetCollectionByAlias($model->collection_id, $model->collection_alias)) {
			$model->collection_alias = $model->collection_alias . '-' . uniqid();
		}

		if (empty($model->collection_id)) {
			// insert collection
			$insert_collection = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_collection[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_collection[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_collection[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_collection[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('collection', $insert_collection)->execute();

				if ($rs) {
					$model->collection_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array(
						'collection_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_collection_lang = array(
							'collection_id' => $model->collection_id,
							'language_code' => $language_code,
							'collection_visible' => !empty($model_lang->collection_title[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_collection_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_collection_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('collection_lang', $insert_collection_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "collection_id = :collection_id" , 
									"params" => array(
										"collection_id" => $model->collection_id,
									)
								)
							);
							
							$builder->createDeleteCommand('collection', $delete_criteria)->execute();

							return false;
						}
					}

					// save gallery photos
					$this->saveGalleryPhotos($model->collection_id, $gallery_photos);

					// update thumbnail
					$this->updateThumbnail($model->collection_id);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_collection = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_collection[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_collection[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_collection[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_collection[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_collection[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "collection_id = :collection_id" , 
					"params" => array(
						"collection_id" => $model->collection_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('collection', $update_collection, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'collection' . DS . $model->collection_id . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$int_attributes = array(
						'collection_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_collection_lang = array(
							'collection_visible' => !empty($model_lang->collection_title[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'collection_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_collection_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_collection_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "collection_id = :collection_id AND language_code = :lang" , 
								"params" => array(
									"collection_id" => (int) $model->collection_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('collection_lang', $update_collection_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					// delete gallery photos
					$this->deleteGalleryPhotos($del_gallery_photos);

					// update gallery photos positions
					$this->saveGalleryPhotosPosition($gallery_photos_position);

					// save gallery photos
					$this->saveGalleryPhotos($model->collection_id, $gallery_photos);

					// update thumbnail
					$this->updateThumbnail($model->collection_id);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_parts = explode('.', strtolower($model->$attribute->getName()));
				$image_extension = array_pop($image_parts);
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'collection' . DS . $model->collection_id . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;
						
						if ($attribute == 'collection_photo') {
							$image_files = array();

							$image_file_original = $image_dir . $image_name . '_o.' . $image_extension; // original
							$image_save_name_original = $image_id . '/' . $image_name . '_o.' . $image_extension;

							$image_file_details = $image_dir . $image_name . '_d.' . $image_extension; // details
							$image_save_name_details = $image_id . '/' . $image_name . '_d.' . $image_extension;

							$image_file_list = $image_dir . $image_name . '_l.' . $image_extension; // list
							$image_save_name_list = $image_id . '/' . $image_name . '_l.' . $image_extension;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							if ($original_image_size->getWidth() < 270 || $original_image_size->getHeight() < 326) {
								continue;
							}

							$resized_image = $image_obj->save($image_file_original, array('quality' => 80));

							$image_files['original'] = array(
								'path' => $image_save_name_original,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// details image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 740) {
								$resized_image = $image_obj->resize($original_image_size->widen(740))
									->save($image_file_details, array('quality' => 80));	
							} else {
								$resized_image = $image_obj->resize($original_image_size->widen(370))
									->save($image_file_details, array('quality' => 80));
							}

							$image_files['details'] = array(
								'path' => $image_save_name_details,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// list image
							$image_obj = $imagine->open($image_path);

							if ($original_image_size->getWidth() > 540 && $original_image_size->getHeight() > 652) {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(540, 652), $mode_outbound)
									->save($image_file_list, array('quality' => 80));
							} else {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(270, 326), $mode_outbound)
									->save($image_file_list, array('quality' => 80));
							}

							$image_files['list'] = array(
								'path' => $image_save_name_list,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							if (is_file($image_file_original) && is_file($image_file_details) && is_file($image_file_list)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						}
						else {
							$image_file = $image_dir . $image_name;
							$image_save_name = $image_id . '/' . $image_name;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							switch ($attribute) {
								case 'collection_photo':
									if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
										$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_outbound)
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));	
									}
									break;
							}

							$image_size = array(
								'w' => $resized_image->getSize()->getWidth(),
								'h' => $resized_image->getSize()->getHeight(),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode(array_merge(
									array(
										'file' => $image_save_name,
									),
									$image_size
								));
							}
							else {
								// remove resized files
								if (is_file($image_file)) {
									unlink($image_file);
								}
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_collection = array(
				'saved' => $today,
			);

			$update_collection = array_merge($update_collection, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "collection_id = :collection_id" , 
					"params" => array(
						"collection_id" => $model->collection_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('collection', $update_collection, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	private function deleteGalleryPhotos($del_photos)
	{
		if (empty($del_photos)) {
			return false;
		}
		
		$delete_counter = 0;
		
		// delete photos
		$builder = Yii::app()->db->schema->commandBuilder;

		foreach ($del_photos as $photo_id) {
			// get delete data
			$delete_data = Yii::app()->db
				->createCommand("SELECT * FROM collection_photo WHERE photo_id = :pid")
				->bindValue(':pid', (int) $photo_id, PDO::PARAM_INT)
				->queryRow();

			if (!empty($delete_data)) {
				$delete_criteria = new CDbCriteria(
					array(
						"condition" => "photo_id = :photo_id" , 
						"params" => array(
							"photo_id" => (int) $photo_id,
						)
					)
				);

				try {
					$rs = $builder->createDeleteCommand('collection_photo', $delete_criteria)->execute();

					if ($rs) {
						$delete_counter++;

						// delete file
						$file_dir = Yii::app()->assetManager->basePath . DS . 'collection' . DS . $delete_data['collection_id'] . DS;
						
						$photo_path = json_decode($delete_data['photo_path'], true);

						$file_path = $file_dir . $photo_path['thumb'];

						if (is_file($file_path) && is_dir(dirname($file_path))) {
							CFileHelper::removeDirectory(dirname($file_path));
						}
					}
				}
				catch (CDbException $e) {
					// ...
				}
			}
		}

		if ($delete_counter) {
			return $delete_counter;
		}
		else {
			return false;	
		}
	}

	private function saveGalleryPhotosPosition($photos_position)
	{
		if (empty($photos_position)) {
			return false;
		}

		$update_counter = 0;
		
		// update photos positions
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		foreach ($photos_position as $photo_id => $position) {
			$update = array(
				'saved' => $today,
				'position' => $position + 1,
			);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "photo_id = :photo_id" , 
					"params" => array(
						"photo_id" => $photo_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('collection_photo', $update, $update_criteria)->execute();

				if ($rs) {
					$update_counter++;	
				}
			}
			catch (CDbException $e) {
				return false;
			}
		}

		if (count($photos_position) == $update_counter) {
			return true;
		}
		else {
			return false;	
		}
	}

	private function saveGalleryPhotos($collection_id, $photos)
	{
		if (empty($collection_id) || empty($photos)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$photos_counter = 0;

		foreach ($photos as $photo) {
			$model = new AddFileForm('photo');
			$model->photo = $photo;

			if ($model->validate()) {
				$save_image_rs = false;
				
				$image_id = uniqid();
				$image_parts = explode('.', $model->photo->getName());
				$image_extension = strtolower(array_pop($image_parts));
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'collection' . DS . $collection_id . DS . $image_id . DS;
				$image_path = $image_dir . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}				

				if ($dir_rs) {
					$save_image_rs = $model->photo->saveAs($image_path);

					if ($save_image_rs) {
						$saved = false;

						$image_file_large = $image_dir . $image_name . '_l.' . $image_extension;
						$image_file_catalog = $image_dir . $image_name . '_c.' . $image_extension;
						$image_file_thumb = $image_dir . $image_name . '_t.' . $image_extension;

						// 960*400 (1180*800 retina) - large
						// 600x484 (300*242 retina) - catalog
						// 140x140 retina - thumb
						
						// resize file
						if (extension_loaded('imagick')) {
							$imagine = new Imagine\Imagick\Imagine();
						}
						elseif (extension_loaded('gd') && function_exists('gd_info')) {
							$imagine = new Imagine\Gd\Imagine();
						}

						// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
						$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
						// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
						$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

						$image_obj = $imagine->open($image_path);
						$original_image_size = $image_obj->getSize();

						if ($original_image_size->getWidth() >= 1180 && $original_image_size->getHeight() >= 800) {
							$resized_image_large = $image_obj->thumbnail(new Imagine\Image\Box(1180, 800), $mode_outbound)
								->save($image_file_large, array('quality' => 80))
								->getSize();
						} elseif ($original_image_size->getWidth() >= 960 && $original_image_size->getHeight() >= 400) {
							$resized_image_large = $image_obj->thumbnail(new Imagine\Image\Box(960, 400), $mode_outbound)
								->save($image_file_large, array('quality' => 80))
								->getSize();
						} else {
							$resized_image_large = $image_obj->save($image_file_large, array('quality' => 80))
								->getSize();
						}

						if ($original_image_size->getWidth() >= 600 && $original_image_size->getHeight() >= 484) {
							$resized_image_catalog = $image_obj->thumbnail(new Imagine\Image\Box(600, 484), $mode_outbound)
								->save($image_file_catalog, array('quality' => 80))
								->getSize();
						} else {
							$resized_image_catalog = $image_obj->thumbnail(new Imagine\Image\Box(300, 242), $mode_outbound)
								->save($image_file_catalog, array('quality' => 80))
								->getSize();
						}

						$resized_image_thumb = $image_obj->thumbnail(new Imagine\Image\Box(140, 140), $mode_outbound)
							->save($image_file_thumb, array('quality' => 80))
							->getSize();

						$save_path = json_encode(array(
							'large'   => $image_id . '/' . $image_name . '_l.' . $image_extension,
							'catalog' => $image_id . '/' . $image_name . '_c.' . $image_extension,
							'thumb'   => $image_id . '/' . $image_name . '_t.' . $image_extension,
						));

						$save_size = json_encode(array(
							'large' => array(
								'w' => $resized_image_large->getWidth(),
								'h' => $resized_image_large->getHeight(),
							),
							'catalog' => array(
								'w' => $resized_image_catalog->getWidth(),
								'h' => $resized_image_catalog->getHeight(),
							),
							'thumb' => array(
								'w' => $resized_image_thumb->getWidth(),
								'h' => $resized_image_thumb->getHeight(),
							),
						));

						if (is_file($image_file_large)) {
							// get max position
							$max_position = Yii::app()->db
								->createCommand("SELECT MAX(position) FROM collection_photo")
								->queryScalar();

							$max_position += 1;

							// insert photo
							$insert_photo = array(
								'photo_id' => null,
								'created' => $today,
								'saved' => $today,
								'collection_id' => $collection_id,
								'photo_path' => $save_path,
								'photo_size' => $save_size,
								'position' => $max_position,
							);

							try {
								$save_image_rs = (bool) $builder->createInsertCommand('collection_photo', $insert_photo)->execute();
								$saved = true;
							} catch (CDbException $e) {
								
							}
						}
						
						if (!$saved) {
							// remove resized files
							if (is_file($image_file_large)) {
								unlink($image_file_large);
							}

							if (is_file($image_file_catalog)) {
								unlink($image_file_catalog);
							}

							if (is_file($image_file_thumb)) {
								unlink($image_file_thumb);
							}
						}

						// remove original image
						unlink($image_path);
					} else {
						continue;
					}
				}

				if ($save_image_rs) {
					$photos_counter++;
				}
			}
		}

		if ($photos_counter) {
			return $photos_counter;
		} else {
			return false;
		}
	}

	private function updateThumbnail($collection_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;

		// get first image of collection
		$first_image = Yii::app()->db
			->createCommand("SELECT photo_path, photo_size FROM collection_photo WHERE collection_id = :id ORDER BY position LIMIT 1")
			->bindValue(':id', (int) $collection_id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($first_image)) {
			$collection_photo = json_encode(array(
				'path' => json_decode($first_image['photo_path'], true),
				'size' => json_decode($first_image['photo_size'], true),
			));
		} else {
			$collection_photo = '';
		}

		// update collection thumbnail
		$update = array(
			'saved' => date('Y-m-d H:i:s'),
			'collection_photo' => $collection_photo,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "collection_id = :collection_id" , 
				"params" => array(
					"collection_id" => $collection_id,
				)
			)
		);

		try {
			$rs = (bool) $builder->createUpdateCommand('collection', $update, $update_criteria)->execute();
		}
		catch (CDbException $e) {
			return false;
		}

		return $rs;
	}

	public function setRating($collection_id, $rating)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_collection = array(
			'saved' => $today,
			'collection_rating' => (int) $rating,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "collection_id = :collection_id" , 
				"params" => array(
					"collection_id" => $collection_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('collection', $update_collection, $update_criteria)->execute();
			
			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function toggle($collection_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_collection = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "collection_id = :collection_id" , 
				"params" => array(
					"collection_id" => $collection_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('collection', $update_collection, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($collection_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$collection = $this->getCollectionByIdAdmin($collection_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "collection_id = :collection_id" , 
				"params" => array(
					"collection_id" => $collection_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('collection', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('collection_lang', $delete_criteria)->execute();
				$builder->createDeleteCommand('collection_product', $delete_criteria)->execute();
				$builder->createDeleteCommand('collection_photo', $delete_criteria)->execute();

				// remove collection directory
				if (is_dir($assetPath . DS . 'collection' . DS . $collection_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'collection' . DS . $collection_id);
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}