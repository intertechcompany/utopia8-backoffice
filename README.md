https://imperavi.com/redactor/plugins/limiter/
https://imperavi.com/redactor/plugins/counter/
https://slashdotdash.retailcrm.ru/orders/

https://github.com/somewebmedia/hc-sticky
https://abouolia.github.io/sticky-sidebar/


1. Цену берем розничную.
2. Дропшип и только онлайн берем из наличия по этим виртуальным складам.
3. Приоритетность статусов:
    1. Если товара нет ни на одном складе подразделения UTOPIA 8 вообще, у товара статус "нет в наличии"

    2. Если товар есть на любом складе (Одесса или Харьков), идем по сценарию обычного наличия

    3. Если товара нет на физических складах, но есть на складе "Предзаказ Н дней" мы ставим соответствующий статус и берем из карточки товара на сайте кол-во дней (давай дефолтное значение установим 14 дней)

    4. Если товара нет на физических складах, но есть на складе "Дропшиппинг" мы ставим соответствующий статус и берем из карточки товара на сайте кол-во дней (давай дефолтное значение установим 5 рабочих (!) дней) - оно не отображается в самом статусе, он выглядит просто как "только онлайн", но тебе нужно будет это кол-во дней для расчета даты отправки.

    5. Если наличие есть и на складе дропшиппинг, и на складе Предзаказ Н дней, действуем по сценарию дропшиппинга.

---

ALTER TABLE `banner_lang` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;

---

Stores: 
https://slashdotdash.retailcrm.ru/api/v5/reference/stores?apiKey=Bj2DWQ4YB4tEVy112kxUdNREXTyjROds&fbclid=IwAR32-_BoPAsPsUZG-nOtPOCR0_U4Az1qwC6WlcxVTpvfJhupt5ZIlF4vn3s

---

http://backoffice.utopia8.loc/api/update

*/15 * * * *	/usr/bin/wget -O /dev/null https://backoffice.utopia8.ua/api/update >/dev/null 2>&1

---

посмотри плз, покрывает нужные нам случаи?

Воскресенье считается выходным днем, остальные - рабочие.

1) Если товар есть в наличии на одном (или двух) из физических складов: 

Доставка со склада: 

если время сейчас >= 13.30, то ставится следующий за текущим рабочий день.

если время сейчас <13.30 (и сегодня не воскресенье), то ставится текущая дата.

если время сейчас <13.30 (и сегодня воскресенье), то ставится следующий за текущим рабочий день.

Получить в магазине в Харькове (если товар есть в наличии на этом складе) - текущая дата, не важно рабочая или нет

Аналогично по Одессе

Если товар есть на одном складе из двух, то ставится текущая дата для этого склада, а для склада другого (где товара нет) - текущая дата + 4 рабочих дня.
Например, сегодня сб 11 марта, должны получить дату 16 марта

2. Если товара нет на физических складах, но есть дропшиппинг, то датой отправки является текущая дата + кол-во РАБОЧИХ дней указанное в товаре - забрать из магазина просто нет как вариантов

Выходные:
1, 2, 7 января, 1 мая

---

1) В пользовательское поле (создам сейчас) записываем тот вариант получения, который пришел из корзины (буквально).
То есть там в пользовательском поле я создам справочник на все случаи жизни:
 - самовывоз в Харькове
 - самовывоз в Одессе
 - Отправка Новой Почтой на склад
 - Отправка Новой Почтой на адрес
 - международная доставка

2) Сделаем еще одно пользовательское принимающее данные для доставки (можно просто строковое, чтоб писать туда номер склада или адрес или что угодно).

Вся эта информация выше нужна будет операторам для общения с покупателем. Теперь по складам отгрузки.

1. Весь заказ есть на 1 складе: указываем этот склад отгрузки.
2. Куски заказа есть на разных складах: указываем склад Харькова.
Тут не важно где эти куски (часть на презказе или дропшиппе или в одессе), все равно - Харьков.

Дальше сами операторы будут оформлять соответствующие движения товаров.

https://beta.utopia8.ua/product/133224?vid=2310 — нет фоток под ретину
https://beta.utopia8.ua/product/125003 — неверный аспект ратио
https://beta.utopia8.ua/product/133770 — сброс характеристик
https://beta.utopia8.ua/product/133042 — нет фото
https://beta.utopia8.ua/product/133106 — нет фото
https://beta.utopia8.ua/product/134443 — нет фото


Вот список задач с первой итерации, который мы собирали ранее:
ИТЕРАЦИЯ 1: 
+ фильтра
+ thank you pages
+ текстовые страницы
+ поиск
+ блок Instagram в футере
+ анимация лого
+ размерная сетка
+ бейджи
+ анимация слайдера на главной станице

+ такс, а категории ухода и возврата - нет? точно?

https://docs.google.com/document/d/1INb1nAm4q3I4o89txJ-EkMKJFSFdyoZ1YsBXNmzIK_U/edit

https://dbushell.com/demos/tables/rt_05-01-12.html — rotate table
https://codepen.io/smashing-magazine/pen/PVGZEQ — !
https://www.smashingmagazine.com/2019/01/table-design-patterns-web/
https://elvery.net/demo/responsive-tables/
https://www.sitepoint.com/responsive-data-tables-comprehensive-list-solutions/

ffmpeg:
https://gist.github.com/mikoim/27e4e0dc64e384adbcb91ff10a2d3678
https://scribbleghost.net/2018/10/26/youtube-recommended-encoding-settings-for-ffmpeg/
https://habr.com/ru/post/333664/
https://gist.github.com/Vestride/278e13915894821e1d6f

https://stackoverflow.com/questions/9053819/ffmpeg-sensible-defaults/9057521#9057521
https://superuser.com/questions/268985/remove-audio-from-video-file-with-ffmpeg
https://video.stackexchange.com/questions/15174/how-to-get-ffmpeg-output-the-same-quality-as-vimeo

https://trac.ffmpeg.org/wiki/Encode/H.264
https://trac.ffmpeg.org/wiki/Scaling
https://support.google.com/youtube/answer/1722171?hl=en

ffmpeg -i IMG_8044.MOV -c:v libx264 img_8044.mp4

ffmpeg -i IMG_8044.MOV -c:v libx264 -preset slow -crf 18 -profile:v high -level 4.0 -bf 2 -coder 1 -pix_fmt yuv420p -b:v 3M -maxrate 5M -bufsize 2M -r 30 -an -movflags +faststart img_8044_4.mp4
ffmpeg -i IMG_8044.MOV -c:v libx264 -preset slow -crf 18 -profile:v high -level 4.0 -bf 2 -coder 1 -pix_fmt yuv420p -b:v 3M -maxrate 5M -bufsize 5M -r 30 -an -movflags +faststart -vf scale='min(960,iw)':-2 img_8044_47.mp4

https://stackoverflow.com/questions/8218363/maintaining-aspect-ratio-with-ffmpeg/14283648#14283648
https://superuser.com/questions/547296/resizing-videos-with-ffmpeg-avconv-to-fit-into-static-sized-player/1136305#1136305

ffprobe -v error -show_entries stream=r_frame_rate,width,height -of default=noprint_wrappers=1 IMG_8044.MOV
ffprobe -v error -select_streams v:0 -show_entries stream_tags=rotate -of default=noprint_wrappers=1:nokey=1 IMG_8044.MOV
ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 IMG_8044.MOV
ffprobe -v error -show_entries stream_tags=rotate -show_entries format=duration -show_entries stream=r_frame_rate,width,height -of default=noprint_wrappers=1 IMG_8044.MOV

https://www.yiiframework.com/wiki/148/understanding-assets
https://yiiframework.ru/doc/cookbook/ru/core.assets

https://www.yiiframework.com/doc/api/1.1/CClientScript
https://www.yiiframework.com/wiki/800/how-to-register-scripts-and-css
https://www.yiiframework.ru/doc/cookbook/ru/js.package
https://yiiframework.ru/doc/cookbook/ru/core.assets
http://php-zametki.ru/yii2-cheatsheets/146-yii2-asset-bundle.html
https://qna.habr.com/q/535632
https://stackoverflow.com/questions/43060568/yii2-assets-how-to-use-image-asset-inside-css-file
https://stackoverrun.com/ru/q/3482399
https://yiiframework.ru/forum/viewtopic.php?f=19&t=20187#p118499
https://books.google.com.ua/books?id=CJrcDgAAQBAJ&pg=PA419&lpg=PA419&dq=console+script+for+publish+assets+yii&source=bl&ots=lRMAiNeQ_M&sig=ACfU3U2Tm86lm4ANs15CWMCm3yBr6le5Nw&hl=ru&sa=X&ved=2ahUKEwjvsbS0trnqAhUM7KYKHbb6CEsQ6AEwBXoECAoQAQ#v=onepage&q=console%20script%20for%20publish%20assets%20yii&f=false

https://developers.google.com/speed/pagespeed/insights/?hl=RU&url=https%3A%2F%2Futopia8.ua%2Fcatalog%2Fwomen&tab=desktop
https://developers.google.com/speed/pagespeed/insights/?hl=RU&url=https%3A%2F%2Futopia8.ua%2Fproduct%2F135622&tab=desktop


---

<head>
    <meta charset="UTF-8">
    <meta name="skype_toolbar" content="skype_toolbar_parser_compatible" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui, user-scalable=no" />
    <meta name="google" content="notranslate" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="address=no" />
    <meta name="format-detection" content="date=no" />
    <meta name="copyright" content="&copy; 2020 SOKOLOV" />
    <meta name="robots" content="noyaca"/>
    <title>Серьги ᐉ купить в каталоге SOKOLOV • Выгодные цены в интернет-магазине • Доставка по Москве и РФ</title>

    <!--meta name="robots" content="index, follow" /-->

    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="sokolov.ru" />

    <meta name="twitter:card" content="summary_large_image"/>

    <meta name="apple-mobile-web-app-title" content="SOKOLOV">
    <meta name="application-name" content="SOKOLOV">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="/meta/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <!-- smart banner -->
    <meta name="apple-itunes-app" content="app-id=1501705341">
    
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="2mLBaCg-pY6DImKqijAXfQGRG8Dp8LOyk0Rl9_DJWJjjVKYMenbK-OZPBf7BR08IZedWjoyTxdf7Gz2FmJgAtQ==">

    <link rel="apple-touch-icon" sizes="180x180" href="/meta/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/meta/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/meta/favicon-16x16.png">
    <link rel="manifest" href="/meta/manifest.json">
    <link rel="mask-icon" href="/meta/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/meta/favicon.ico">

        
    <meta name="description" content="Серьги в каталоге интернет-магазина SOKOLOV: ⭐ Выгодные цены — Оплата бонусами ✔️ Реальные отзывы и фото — Доставка по Москве и России">
    <meta itemprop="url" content="https://sokolov.ru/jewelry-catalog/earrings/">
    <meta property="og:url" content="https://sokolov.ru/jewelry-catalog/earrings/">
    <meta itemprop="description" content="Серьги в каталоге интернет-магазина SOKOLOV: ⭐ Выгодные цены — Оплата бонусами ✔️ Реальные отзывы и фото — Доставка по Москве и России">
    <meta property="og:description" content="Серьги в каталоге интернет-магазина SOKOLOV: ⭐ Выгодные цены — Оплата бонусами ✔️ Реальные отзывы и фото — Доставка по Москве и России">
    <meta name="twitter:description" content="Серьги в каталоге интернет-магазина SOKOLOV: ⭐ Выгодные цены — Оплата бонусами ✔️ Реальные отзывы и фото — Доставка по Москве и России">
    <meta itemprop="image" content="https://sokolov.ru/upload/micromarking/1000x750/6e1b915d96e8473f27d63d0f953927c6.jpg">
    <meta property="og:image" content="https://sokolov.ru/upload/micromarking/1000x750/6e1b915d96e8473f27d63d0f953927c6.jpg">
    <meta property="og:image:secure_url" content="https://sokolov.ru/upload/micromarking/1000x750/6e1b915d96e8473f27d63d0f953927c6.jpg">
    <meta property="og:image:type" content="image/jpeg/jpg">
    <meta property="og:image:width" content="1000">
    <meta property="og:image:height" content="750">
    <meta name="twitter:image" content="https://sokolov.ru/upload/micromarking/1000x750/6e1b915d96e8473f27d63d0f953927c6.jpg">
    <meta itemprop="image" content="https://sokolov.ru/upload/micromarking/300x300/f8c03ab0870659dbc4cc91053609f4a6.jpg">
    <meta property="og:image" content="https://sokolov.ru/upload/micromarking/300x300/f8c03ab0870659dbc4cc91053609f4a6.jpg">
    <meta property="og:image:secure_url" content="https://sokolov.ru/upload/micromarking/300x300/f8c03ab0870659dbc4cc91053609f4a6.jpg">
    <meta property="og:image:type" content="image/jpeg/jpg">
    <meta property="og:image:width" content="300">
    <meta property="og:image:height" content="300">
    <meta property="fb:api_id" content="">
    <meta property="og:locale" content="ru">
    <meta name="robots" content="">
    <link href="https://sokolov.ru/jewelry-catalog/earrings/" rel="canonical">
    <link href="/redesign/pages/catalog/page.css?v=1589200056" rel="stylesheet">
    <link href="/redesign/template.css?v=1589358831" rel="stylesheet">
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Organization",
            "url": "https://sokolov.ru",
            "logo": "https://sokolov.ru/interface/logo/sokolov-logo_google.png"
        }
    </script>
    <meta name="google-site-verification" content="J_9aUGOeSh1mzA4QwQWMPRfGLrXycNr9TgqjoPsEJBw" />
    <meta name='wmail-verification' content='4d1992d746da4c87' />
</head>