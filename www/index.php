<?php
define('DS', DIRECTORY_SEPARATOR);
@set_time_limit(120);

if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'backoffice.utopia8.loc') {
	error_reporting(E_ALL);
	
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

	$yii = __DIR__ . DS . '..' . DS . '..' . DS . 'utopia_front' . DS . 'framework' . DS . 'yiilite.php';
	require_once $yii;

	Yii::setPathOfAlias('root', realpath(__DIR__ . DS . '..' . DS));

	$config = require_once __DIR__ . DS . '..' . DS . 'app_backoffice' . DS . 'config' . DS . 'backoffice.php';
}
else {
	$yii = __DIR__ . DS . '..' . DS . '..' . DS . 'utopia8.ua' . DS . 'framework' . DS . 'yiilite.php';
	require_once $yii;

	Yii::setPathOfAlias('root', realpath(__DIR__ . DS . '..' . DS));
	
	$config = require_once __DIR__ . DS . '..' . DS . 'app_backoffice' . DS . 'config' . DS . 'backoffice.php';
}

Yii::$enableIncludePath = false; // ��������� PHP include path
Yii::createWebApplication($config)->run();